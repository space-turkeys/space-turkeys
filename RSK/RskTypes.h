
#ifndef RSK_TYPES
#define RSK_TYPES 1

class intd;
class fd;
class doubled;


class intd{
 public: 
  int x,y;
  
  intd();
  intd(int a,int b);
  friend intd operator*(int d, intd a);
  friend intd operator*(intd a, int d);
  friend intd operator*(float d, intd a);
  friend intd operator*(intd a, float d);
  friend int operator*(intd a, intd d);
  friend intd operator+(intd a, intd d);
  friend intd operator-(intd a, intd d);
  friend char operator<(intd a, intd b);
  friend char operator<=(intd a, intd b);
  void operator+=(intd d);
  void operator%=(int d);
  float length();
  float getAngle();
  intd rotate(float cosinus, float sinus);
  intd rotate(float a);
  intd trim(intd x1, intd x2);
};

class fd{
 public: 
  float x,y;
  
  fd();
  fd(int a, int b);
  fd(float a, float b);
  fd(double a, double b);
  fd(intd a);
  friend fd operator*(float d, fd a);
  friend fd operator*(fd a, float d);
  friend float operator*(fd a, fd d);
  friend fd operator+(fd a, fd d);
  friend fd operator-(fd a, fd d);
  friend char operator<(fd a, fd b);
  friend char operator==(fd a, fd b);
  void operator+=(fd d);
  void operator%=(float d);
  float length();
  float length2();
  int MHLength();
  float getAngle();
  fd rotate(float cosinus, float sinus);
  fd rotate(float a);
  fd trim(float r);
  fd trim(fd r);
  fd trim(fd r, fd q);
  fd cirkTrim(float r);
  intd toIntd();
  fd normalize();
  static fd normv(float a);
  static fd normv(float cosine, float sine);
};



class doubled{
 public: 
  double x,y;
 
  doubled(fd a);
  doubled(intd a);
  doubled(float a, float b);
  doubled(double a, double b);
  doubled();
  fd toFd();
  static doubled mult(double d, intd a);
  friend doubled operator*(double d, doubled a);
  friend doubled operator*(doubled a, double d);
  friend double operator*(doubled a, doubled d);
  friend doubled operator+(doubled a, doubled d);
  friend doubled operator-(doubled a, doubled d);
  friend doubled operator+(fd a, doubled d);
  friend doubled operator+(doubled a, fd d);
  friend doubled operator-(fd a, doubled d);
  friend doubled operator-(doubled a, fd d);
  friend char operator<(doubled a, doubled b);
  void operator+=(doubled d);
  void operator%=(double d);    
  double length();
  double getAngle();
  doubled rotate(double cosinus, double sinus);
  doubled rotate(double a);
  static doubled normv(double a);
};

#endif
