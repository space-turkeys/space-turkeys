#include "RskGraphics.h"

namespace RskGraphics{
  atan_table global_atan_table;
  sin_table global_sin_table;
};
  
using namespace RskGraphics;

float sin_table::PI2 = 2 * PI;
float sin_table::PIO2 = PI / 2;

sin_table::sin_table(int n){
  init(n);
}

sin_table::sin_table(){
  init(0x10000);
}

sin_table::~sin_table(){
  delete [] pts;
}

void sin_table::init(int n){
  num = n;
  pts = new float[n];

  for (int i = 0; i < n; i++){
    pts[i] = sin((i * PI2)/n);
  }
}

float sin_table::get_sin(float x){
  return pts[(int)((normAngle(x) * num) / PI2)];
}

float sin_table::get_cos(float x){
  return get_sin(x + PIO2);
}

atan_table::atan_table(int n, float xmin, float xmax){
  init(n, xmin, xmax);
}

atan_table::atan_table(){
  init(0x10000, -100, 100);
}

void atan_table::init(int n, float xmin, float xmax){
  num = n;
  pts = new float[n];
  x1 = xmin;
  x2 = xmax;
  interval = x2 - x1;

  for (int i = 0; i < n; i++){
    pts[i] = atan(x1 + (i * interval)/n);
  }
}

atan_table::~atan_table(){
  delete [] pts;
}

float atan_table::get_atan(float x){
  if (x < x1){
    return - PI / 2;
  }else if(x >= x2){
    return PI / 2;
  }

  int i = ((x - x1) * num) / interval;
  return pts[i];
}


float angle(fd a){
  if (a.x > 0){
    return atan(a.y/a.x);
  }else if (a.x<0){
    return PI+atan(a.y/a.x);
  }else if (a.y>0){
    return PI/2;
  }else {
    return -PI/2;
  }
}
fd normv(float a){
  fd p;
  p.x=cos(a);
  p.y=sin(a);
  return p;
}
doubled dnormv(float a){
  doubled p;
  p.x=cos(a);
  p.y=sin(a);
  return p;
}

float sign(float x){
  if (x<0){
    return -1;
  }else if(x>0){
    return 1;
  }else{
    return 0;
  }
}
float sproject(fd a, fd r){
  return a*r/(r*r);
}
float shortestDistance(fd a, fd b, fd p){
  float s,t,v;
  s=(p-a).length();
  t=(p-b).length();
  v=(p-sproject(p-a,b-a)*(b-a)-a).length();
  if (angleDistance((p-a).getAngle(),(b-a).getAngle())>PI/2){
    return s;
  }else if (angleDistance((p-b).getAngle(),(a-b).getAngle())>PI/2){
    return t;
  }else{
    return v;
  }
}

char intervalCheck(fd a, fd i0, fd i1){
  if (i0<a && a<i1){
    return 1;
  }else{
    return 0;
  }
}

char intervalCheck(intd a, intd i0, intd i1){
  if (i0<a && a<i1){
    return 1;
  }else{
    return 0;
  }
}

fd intersect(fd* a, fd* b){
  //two fd in each pointer please! containing two points on the line...
  
  fd v=a[1]-a[0], w=b[1]-b[0];

  fd p=a[0], q=b[0];

  fd beta=q-p;

  //p+s*v=q+t*w;

  float t;
  
  if (v.x==0){
    if (w.x==0){
      t=0;
    }else{
      t=beta.x/w.x;
    }
  }else{
    if (w.y==v.y*w.x/v.x){
      t=0;
    }else{
      t=(beta.y-v.y*beta.x/v.x)/(v.y*w.x/v.x-w.y);
    }
  }

  return q+t*w;

}

float normAngle(float a){
  if (a < 0){
    return a - ((int)(a / (2 * PI)) - 1) * 2 * PI;
  }else{
    return a - ((int)(a / (2 * PI))) * 2 * PI;
  }
}

float angleDistance(float a, float b){

  float r;

  a=normAngle(a);
  b=normAngle(b);

  r=fabs(b-a);

  if (r>PI){
    r=2*PI-r;
  }

  return r;

}

char rect_contains(SDL_Rect *r, intd p){
  return p.x>=r->x && p.y>=r->y && p.x<r->x+r->w && p.y<r->y+r->h;
}
