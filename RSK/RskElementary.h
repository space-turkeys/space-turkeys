#ifndef RSK_ELEMENTARY
#define RSK_ELEMENTARY

#include <string>

#ifdef debug
void debug(const char* v);
void debug(const char* file, const char* v);
#endif

#ifdef CLOCKS_PER_SEC
double cpu_seconds();
#endif

double system_seconds();

bool str2int (int *i, std::string s);
std::string int2str (int i);
int readKeySym();
int parseKeyToInt(int k);
int getKeyFromEvent(SDL_Event e);
char** getdir (char* dir);
#endif
