#include <string>
#include <cmath>
#include <fstream>
#include <sys/types.h>
#ifndef __WIN32__
  #include <sys/time.h>
#else
  #include <time.h>
#endif
#include <errno.h>
#include <dirent.h>
#include <iostream>
#include <sstream>
#include <ctime>

#include "RskGraphics.h"

using namespace RskGraphics;
using namespace std;

#ifdef debug
void debug(const char* v){
  std::ofstream f;
  f.open("deb.txt");
  f<<v<<"\n";
  f.close();
}
void debug(const char* file, const char* v){
  std::ofstream f;
  f.open(file);
  f<<v<<"\n";
  f.close();
}
#endif

#ifdef CLOCKS_PER_SEC
double cpu_seconds(){
  clock_t t=clock();
  return (double)t/CLOCKS_PER_SEC;
}
#endif

double system_seconds(){
#ifdef __WIN32__
	return cpu_seconds();
#else
  struct timeval t;
  gettimeofday(&t, 0);
  return t.tv_sec + t.tv_usec / (double)1000000;
#endif
}

bool str2int (int *i, string s)
{
  stringstream ss(s);
  ss >> *i;
  if (ss.fail()) {
    // not an integer
    return false;
  }
  return true;
}

string int2str (int i){
  string v;
  stringstream ss(v);
  ss<<i;
  ss>>v;
  return v;
}


int readKeySym(){
  SDL_Event e;
  int r;
  while(SDL_PollEvent(&e)){}
  e.type=0;
  while(e.type!=SDL_KEYDOWN){
    while(SDL_PollEvent(&e)){}
    if (e.type==SDL_KEYDOWN){
      r=(int)e.key.keysym.sym;
    }
  }
  return r;
}

int parseKeyToInt(int k){
  if (k==SDLK_0){return 0;}
  if (k==SDLK_1){return 1;}
  if (k==SDLK_2){return 2;}
  if (k==SDLK_3){return 3;}
  if (k==SDLK_4){return 4;}
  if (k==SDLK_5){return 5;}
  if (k==SDLK_6){return 6;}
  if (k==SDLK_7){return 7;}
  if (k==SDLK_8){return 8;}
  if (k==SDLK_9){return 9;}
  return -1;
}

int getKeyFromEvent(SDL_Event e){
  return e.key.keysym.sym;
}

char** getdir (char* dir){
  DIR *dp;
  struct dirent *dirp;
  char** ret;
  int c=0;
  int max=0;


  if((dp  = opendir(dir)) == NULL) {
    fprintf(stdout,"getdir: error, exiting...\n");
    exit(1);
  }
  
  while ((dirp = readdir(dp)) != NULL) {
    max++;
  }

  closedir(dp);

  ret=new char*[max+1];


  if((dp  = opendir(dir)) == NULL) {
    fprintf(stdout,"getdir: error, exiting...\n");
    exit(1);
  }

  while ((dirp = readdir(dp)) != NULL && c<max) {
    ret[c]=new char[strlen(dirp->d_name)+1];
    strcpy(ret[c],dirp->d_name);
    c++;
  }

  closedir(dp);

  ret[max]=0;

  return ret;
}
