#include <cmath>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "RskGraphics.h"

namespace RskGraphics{
  TTF_Font* Rsk_global_font;
};

using namespace RskGraphics;


SDL_Rect create_sdl_rect(int x, int y, int w, int h){
	SDL_Rect r;
	r.x = x;
	r.y = y;
	r.w = w;
	r.h = h;

	return r;
}

SDL_Color create_sdl_color(unsigned char r, unsigned char g, unsigned char b){
	SDL_Color c;
	c.r = r;
	c.g = g;
	c.b = b;

	return c;
}

float colorLength(Uint32 c){
  char r=(c<<8)>>24;
  char g=(c<<16)>>24;
  char b=(c<<24)>>24;
  return sqrt((float)r*r+g*g+b*b);
}
SDL_Surface* initSDL(int width, int height){
  return initSDL(width, height, SDL_SWSURFACE);
}
SDL_Surface* initSDL(int width, int height, int flag){
  SDL_Surface* screen;
    
  if ( SDL_Init(SDL_INIT_VIDEO) < 0 ){
    fprintf(stderr, "sdl: Unable to init SDL: %s\n", SDL_GetError());
    exit(1);
  }

  SDL_EnableUNICODE(1);

  if (TTF_Init()<0){
    fprintf(stderr, "sdl: Unable to init TTF: %s\n", SDL_GetError());
    exit(1);
  }

  if (!(Rsk_global_font=TTF_OpenFont("fonts/arial.ttf",12))){
    fprintf(stderr,"RSK: drawText: can not open font\n");
    exit(1);
  }

  atexit(SDL_Quit);
  atexit(TTF_Quit);

  screen = SDL_SetVideoMode(width, height, 32, flag);

  if ( screen == NULL ){
    fprintf(stderr, "sdl: Unable to set %dx%d video: %s\n",width,height, SDL_GetError());
    exit(1);
  } 
  return screen;
}

void drawArrowHead(SDL_Surface* s, fd x, float a, int l, Uint32 c){
  fd n = fd::normv(a + PI/2);
  fd p = fd::normv(a);
  fd b;

  for (float i = -l + 1; i <= 0; i += .5){
    b = x + i * p;
    drawLineSafe(s, b + (i/2) * n, b - (i/2) * n, 1, c);
  }
}

void drawGlowingCircle(SDL_Surface* s, fd x, float r, int thick, Uint32 col, Uint32 gcol){
  int t;
  char f;
  int prec = 3;

  for (t=1; t<prec*thick; t++){
    f = 0xff * ((prec * thick - t) / (float) (prec * thick));
    drawCircle(s,x,r+(float)t/prec,gcol|(f<<24));
  }
  drawCircle(s,x,r,col);
}

void drawCircle(SDL_Surface* s, fd x, float r, int thick, Uint32 col){
  int prec = 3;
  int t;

  for (t=1; t<prec*thick; t++){
    drawCircle(s,x,r-(float)t/prec,col);
  }
}
void drawCircle(SDL_Surface* s, fd x, float r, Uint32 col){
  fd p;
  fd zero=fd(0,0);
  fd dims=fd(s->w,s->h);
  for (int y=0; y<2*PI*r; y++){
    p=x+r*fd::normv(y/r);
    if (zero<p && p<dims){
      psetAlpha(s,p.x,p.y,col);
    }
  }
}

void drawFilledCircle(SDL_Surface* s, fd x, float r, Uint32 col){
  int a,b,c;
  intd p1;
  SDL_Rect bounds = create_sdl_rect(0, 0, s -> w, s -> h);

  //why trim only visible upwise?

  if ((x-x.trim(fd(s->w-1,s->h-1))).length()<r){
    for (a=-r; a<r; a++){
      c=sqrt(r*r-a*a);
      for (b=-c; b<c; b++){
	p1=intd(x.x+a,x.y+b);
	if (rect_contains(&bounds, p1)){
	  psetAlpha(s,p1.x,p1.y,col);
	}
      }
    }
  }
}

void drawFilledCircleShaded(SDL_Surface* s, fd x, float r, Uint32 col){
  int a,b,c;
  intd p1;
  SDL_Rect bounds = create_sdl_rect(0, 0, s -> w, s -> h);

  //why trim only visible upwise?

  if ((x-x.trim(fd(s->w-1,s->h-1))).length()<r){
    for (a=-r; a<r; a++){
      c=sqrt(r*r-a*a);
      for (b=-c; b<c; b++){
	p1=intd(x.x+a,x.y+b);
	if (rect_contains(&bounds, p1)){
	  psetAlpha(s,p1.x,p1.y,fade(col, (r - a) / (float) (2 * r)));
	}
      }
    }
  }
}

void drawCircleSegment(SDL_Surface* s, fd x, float r, int thick, fd interval, Uint32 col){
  for (int c=r-thick+1; c<=r; c++){
    drawCircleSegment(s,x,c,interval,col);
  }
}

void drawCircleSegment(SDL_Surface* s, fd x, float r, fd interval, Uint32 col){
  fd p;
  fd zero=fd(0,0);
  fd dims=fd(s->w,s->h);
  /* interval in radians */
  for (float y=interval.x*r; y<interval.y*r; y++){
    p=x+r*fd::normv(y/r);
    if (zero<p && p<dims){
      pset(s,p,col);
    }
  }
}

void drawGlowingLineSafe(SDL_Surface* s, fd x, fd y, int thickness, Uint32 col, Uint32 gcol){

  fd dim0(thickness,thickness);
  fd dim1(s->w-thickness-1,s->h-thickness-1);

  fd p1,p2;

  int l=(y-x).length();

  if (!l){return;}

  fd norm=(float)1/l*(y-x);

  int c;

  char found1=0, found2=0;



  for (c=0; c<l && !found1; c+=1){
    p1=x+c*norm;

    if (intervalCheck(p1,dim0,dim1)){

      found1=1;
    }
  }


  for (c=0; c<l && !found2; c+=1){
    p2=y-c*norm;

    if (intervalCheck(p2,dim0,dim1)){

      found2=1;
    }
  }

  if (!(found1 && found2)){return;}



  drawGlowingLine(s,p1,p2,thickness,col,gcol);
  

}

void drawGlowingLine(SDL_Surface* s, fd x, fd y, int thickness, Uint32 col, Uint32 gcol){

  float a=(y-x).getAngle();
  fd n=fd::normv(a+PI/2);
  fd n2=fd::normv(a);
  int t;
  char f;

  for (t=1; t<5+1; t++){
    f=0xff/(float)t;
    drawLine(s,x+(thickness/2+t-1)*n,y+(thickness/2+t-1)*n,gcol|(f<<24) );
    drawLine(s,x-(thickness/2+t)*n,y-(thickness/2+t)*n,gcol|(f<<24) );
    //drawLine(s,x-(thickness*2)*n-t*n2,x+(thickness*2)*n-t*n2,gcol|(f<<24));
    //drawLine(s,y-(thickness*2)*n+t*n2,y+(thickness*2)*n+t*n2,gcol|(f<<24));
  }

  drawLine(s,x,y,thickness,col);
  
}
void drawLineSafe2(SDL_Surface* s, fd x, fd y, int thickness, Uint32 col){

  fd *ab=new fd[2], *alpha=new fd[2], *beta=new fd[2], *gamma=new fd[2], *delta=new fd[2];

  fd crossAlpha,crossBeta,crossGamma,crossDelta;

  int lengthAlpha,lengthBeta,lengthGamma,lengthDelta;

  float a;

  fd from, too;

  char active=0;

  if (x.x==y.x){

    if (x.y<0){
      x.y=0;
    }
    if (x.y>s->h-1){
      x.y=s->h-1;
    }

    if (y.y<0){
      y.y=0;
    }
    if (y.y>s->h-1){
      y.y=s->h-1;
    }

    from=x;
    too=y;

    active=1;

  }else if (x.y==y.y){

    if (x.x<0){
      x.x=0;
    }
    if (x.x>s->w-1){
      x.x=s->w-1;
    }

    if (y.x<0){
      y.x=0;
    }
    if (y.x>s->w-1){
      y.x=s->w-1;
    }

    from=x;
    too=y;

    active=1;

  }else{

    ab[0]=x;
    ab[1]=y;

    alpha[0]=fd(0,0);
    alpha[1]=fd(s->w-1,0);

    beta[0]=fd(0,0);
    beta[1]=fd(0,s->h-1);

    gamma[0]=fd(0,s->h-1);
    gamma[1]=fd(s->w-1,s->h-1);

    delta[0]=fd(s->w-1,0);
    delta[1]=fd(s->w-1,s->h-1);

    crossAlpha=intersect(ab,alpha);
    crossBeta=intersect(ab,beta);
    crossGamma=intersect(ab,gamma);
    crossDelta=intersect(ab,delta);

    lengthAlpha=(crossAlpha-x).length();
    lengthBeta=(crossBeta-x).length();
    lengthGamma=(crossGamma-x).length();
    lengthDelta=(crossDelta-x).length();

    a=(y-x).getAngle();
    if (0<a && a<PI/2){
      //sector I
      if (fmax(lengthAlpha,lengthBeta)<fmin(lengthGamma,lengthDelta)){
	active=1;

	if (lengthAlpha>lengthBeta){
	  from=crossAlpha;
	}else{
	  from=crossBeta;
	}

	if (lengthGamma<lengthDelta){
	  too=crossGamma;
	}else{
	  too=crossDelta;
	}
      }
    }else if (PI/2<a && a<PI){
      //sector II
      if (fmax(lengthAlpha,lengthDelta)<fmin(lengthGamma,lengthBeta)){
	active=1;

	if (lengthAlpha>lengthDelta){
	  from=crossAlpha;
	}else{
	  from=crossDelta;
	}

	if (lengthGamma<lengthBeta){
	  too=crossGamma;
	}else{
	  too=crossBeta;
	}
      }
    

    }else if (PI<a && a<3*PI/2){
      //sector III
      if (fmax(lengthDelta,lengthGamma)<fmin(lengthAlpha,lengthBeta)){
	active=1;

	if (lengthDelta>lengthGamma){
	  from=crossDelta;
	}else{
	  from=crossGamma;
	}

	if (lengthAlpha<lengthBeta){
	  too=crossAlpha;
	}else{
	  too=crossBeta;
	}

      }
    }else if (a>3*PI/2){
      //sector IV
      if (fmax(lengthBeta,lengthGamma)<fmin(lengthAlpha,lengthDelta)){
	active=1;

	if (lengthBeta>lengthGamma){
	  from=crossBeta;
	}else{
	  from=crossGamma;
	}

	if (lengthAlpha<lengthDelta){
	  too=crossAlpha;
	}else{
	  too=crossDelta;
	}
      }
    
    }
  }

  if (active){
    drawLine(s,from,too,thickness,col);
  }

  delete [] ab;
  delete [] alpha;
  delete [] beta;
  delete [] gamma;
  delete [] delta;
}

void drawLineSafe(SDL_Surface* s, fd x, fd y, int thickness, Uint32 col){

  fd dim0(thickness,thickness);
  fd dim1(s->w-thickness-1,s->h-thickness-1);

  fd p1,p2;

  int l=(y-x).length();

  if(!l){return;}

  fd norm=(float)1/l*(y-x);

  int c;

  char found1=0, found2=0;

  for (c=0; c<l && !found1; c+=1){
    if (intervalCheck(x+c*norm,dim0,dim1)){
      p1=x+c*norm;
      found1=1;
    }
  }

  for (c=0; c<l && !found2; c+=1){
    if (intervalCheck(y-c*norm,dim0,dim1)){
      p2=y-c*norm;
      found2=1;
    }
  }

  if (!(found1 && found2)){return;}

  drawLine(s,p1,p2,thickness,col);
  

}

void drawLine(SDL_Surface* s, fd x, fd y, int thickness, Uint32 col){
  float a,cosinus,sinus;

  int x1=x.x;
  int x2=y.x;
  int y1=x.y;
  int y2=y.y;

  int dx,exc,c,r;

  if (thickness==1){
    drawLine(s,x,y,col);
    return;
  }

  if (x1==x2){
    if (y2<y1){
      exc=y1;
      y1=y2;
      y2=exc;
    }
    for (c=y1; c<=y2; c++){
      for (dx=-thickness/2; dx<thickness/2; dx++){
	pset(s,x1+dx,c,col);
      }
    }
  }else if (y1==y2){
    if (x2<x1){
      exc=x1;
      x1=x2;
      x2=exc;
    }
    for (c=x1; c<=x2; c++){
      for (dx=-thickness/2; dx<thickness/2; dx++){
	pset(s,c,y1+dx,col);
      }
    }
  }else{
    a=(y-x).getAngle();
    r=(y-x).length();
    cosinus=global_sin_table.get_cos(a);
    sinus=global_sin_table.get_sin(a);
    for (c=0; c<=3*r/2; c++){
      for (dx=-thickness/2; dx<thickness/2; dx++){
	pset(s,x1+2*c*cosinus/3-dx*sinus, y1+2*c*sinus/3+dx*cosinus, col);
      }
    }
  }

}
void drawLine(SDL_Surface* s, fd x1, fd x2, Uint32 col){
  int a1=(int)x1.x;
  int b1=(int)x1.y;
  int a2=(int)x2.x;
  int b2=(int)x2.y;
  drawLine(s,a1,b1,a2,b2,col);
}

void drawLine(SDL_Surface* s, int x1, int y1, int x2, int y2, Uint32 col){
  int w=s->w;
  int h=s->h;
  int exc;
  int c;

  if (!(x1>=0 && x1<w && y1>=0 && y1<h && x2>=0 && x2<w && y2>=0 && y2<h)){
    return;
  }

  if (x1==x2){
    if (y2<y1){
      exc=y1;
      y1=y2;
      y2=exc;
    }
    for (c=y1; c<=y2; c++){
      pset(s,x1,c,col);
    }
  }else if (y1==y2){
    if (x2<x1){
      exc=x1;
      x1=x2;
      x2=exc;
    }
    for (c=x1; c<=x2; c++){
      pset(s,c,y1,col);
    }
  }else{
    fd x;
    x.x=x2-x1;
    x.y=y2-y1;
    int r=x.length();
    float a=angle(x);
    float cosinus=global_sin_table.get_cos(a);
    float sinus=global_sin_table.get_sin(a);
    for (c=0; c<=r; c++){
      pset(s,x1+c*cosinus, y1+c*sinus, col);
    }
  }
}

void drawLineAlpha(SDL_Surface* s, int x1, int y1, int x2, int y2, Uint32 col){
  int w=s->w;
  int h=s->h;
  int exc;
  int c;

  if (!(x1>=0 && x1<w && y1>=0 && y1<h && x2>=0 && x2<w && y2>=0 && y2<h)){
    return;
  }

  if (x1==x2){
    if (y2<y1){
      exc=y1;
      y1=y2;
      y2=exc;
    }
    for (c=y1; c<=y2; c++){
      psetAlpha(s,x1,c,col);
    }
  }else if (y1==y2){
    if (x2<x1){
      exc=x1;
      x1=x2;
      x2=exc;
    }
    for (c=x1; c<=x2; c++){
      psetAlpha(s,c,y1,col);
    }
  }else{
    fd x;
    x.x=x2-x1;
    x.y=y2-y1;
    int r=x.length();
    float a=angle(x);
    float cosinus=global_sin_table.get_cos(a);
    float sinus=global_sin_table.get_sin(a);
    for (c=0; c<=r; c++){
      psetAlpha(s,x1+c*cosinus, y1+c*sinus, col);
    }
  }
}

void psetAlpha(SDL_Surface* s, int x, int y, Uint32 c){
  ((Uint32*)s->pixels)[x+y*s->w]=c;
}
void pset(SDL_Surface* s, int x, int y, Uint32 c){
  int alpha=c>>24;
  Uint32 d;
  d=((Uint32*)s->pixels)[x+y*s->w];

  if (alpha){
    if (alpha>0xff || alpha<0){
      fprintf(stderr,"pset: alpha bad value");
      return;
    }
    c=(c<<8)>>8;
    c=(int)( alpha*((c<<8)>>24)/0xff + (0xff-alpha)*((d<<8)>>24)/0xff )<<16 | (int)(alpha*((c<<16)>>24)/0xff+(0xff-alpha)*((d<<16)>>24)/0xff)<<8 | (int)(alpha*((c<<24)>>24)/0xff+(0xff-alpha)*((d<<24)>>24)/0xff);
  }
  ((Uint32*)s->pixels)[x+y*s->w]=(int)(d>>24)<<24 | c;
}
void pset(SDL_Surface* s, fd x, Uint32 c){
  pset(s,(int)x.x,(int)x.y,c);
}
void dot(SDL_Surface* s, fd x, Uint32 c){
  for (int a=-1; a<2; a++){
    for (int b=-1; b<2; b++){
      pset(s,(int)x.x+a,(int)x.y+b,c);
    }
  }
}
Uint32 pget(SDL_Surface* s, fd p){
  return pget(s,(int)p.x,(int)p.y);
}
Uint32 pget(SDL_Surface* s, int x, int y){
  return ((Uint32*)s->pixels)[x+y*s->w];
}

void drawFrame(SDL_Surface* s, SDL_Rect r, Uint32 c){
  //strange rounding factors here .. 
  if (r.h<1 || r.w<1){
    return;
  }
  intd ul(r.x,r.y),ur(r.x+r.w-1,r.y),bl(r.x,r.y+r.h-1),br(r.x+r.w-1,r.y+r.h-1);
  intd d0(0,0),d1(s->w,s->h);

  char checked=0;
  
  checked|=(d0<=ul && ul<d1);//intervalCheck(ul,d0,d1);
  checked|=2*(d0<=ur && ur<d1);//intervalCheck(ur,d0,d1);
  checked|=4*(d0<=bl && bl<d1);//intervalCheck(bl,d0,d1);
  checked|=8*(d0<=br && br<d1);//intervalCheck(br,d0,d1);

  if (checked&(1|2)==1|2){
    drawLine(s,ul.x,ul.y,ur.x,ur.y,c);
  }
  if (checked&(2|8)==2|8){
    drawLine(s,ur.x,ur.y,br.x,br.y,c);
  }
  if (checked&(4|8)==4|8){
    drawLine(s,br.x,br.y,bl.x,bl.y,c);
  }
  if (checked&(1|4)==1|4){
    drawLine(s,bl.x,bl.y,ul.x,ul.y,c);
  }
}

void drawFrameSafe(SDL_Surface* s, SDL_Rect r, Uint32 c){
  //strange rounding factors here .. 
  if (r.h<1 || r.w<1){
    return;
  }
  intd ul(r.x,r.y),ur(r.x+r.w-1,r.y),bl(r.x,r.y+r.h-1),br(r.x+r.w-1,r.y+r.h-1);
  intd d0(0,0),d1(s->w,s->h);

  char checked=0;
  
  checked|=(d0<=ul && ul<d1);//intervalCheck(ul,d0,d1);
  checked|=2*(d0<=ur && ur<d1);//intervalCheck(ur,d0,d1);
  checked|=4*(d0<=bl && bl<d1);//intervalCheck(bl,d0,d1);
  checked|=8*(d0<=br && br<d1);//intervalCheck(br,d0,d1);

  if (checked&(1|2)==1|2){
    drawLineSafe(s,fd(ul.x,ul.y), fd(ur.x,ur.y),1,c);
  }
  if (checked&(2|8)==2|8){
    drawLineSafe(s,fd(ul.x,ul.y), fd(ur.x,ur.y),1,c);
  }
  if (checked&(4|8)==4|8){
    drawLineSafe(s,fd(ul.x,ul.y), fd(ur.x,ur.y),1,c);
  }
  if (checked&(1|4)==1|4){
    drawLineSafe(s,fd(ul.x,ul.y), fd(ur.x,ur.y),1,c);
  }
}

void drawFrame(SDL_Surface* s, SDL_Rect r, int t, Uint32 c){
  //strange rounding factors here .. 
  //t thickness
  SDL_Rect rect=r;
  
  for (int x=0; x<t; x++){
    rect.x=r.x+x;
    rect.y=r.y+x;
    rect.w=r.w-2*x;
    rect.h=r.h-2*x;
    drawFrame(s,rect,c);
  }
}

void drawRoundedFrame(SDL_Surface* s, SDL_Rect r, int a, int t, Uint32 c){
  int i;
  SDL_Rect rect;
  for (i=0; i<t && i<a && i<r.w/2 && i<r.h/2; i++){
    rect=create_sdl_rect(r.x+i,r.y+i,r.w-2*i,r.h-2*i);
    drawRoundedFrame(s,rect,a-i,c);
  }
}

void drawRoundedFrame(SDL_Surface* s, SDL_Rect r, int a, Uint32 c){
  fd p[4];
  intd d[8];
  int i;
  
  if ((r.x<0 && r.x+r.w>=s->w && r.y<0 && r.y+r.h>=s->h) || a>r.w/2 || a>r.h/2){
    return;
  }
  
  p[0]=fd(r.x+a,r.y+a);
  p[1]=fd(r.x+r.w-a,r.y+a);
  p[2]=fd(r.x+r.w-a,r.y+r.h-a);
  p[3]=fd(r.x+a,r.y+r.h-a);
  d[0]=intd(r.x+a,r.y);
  d[1]=intd(r.x+r.w-a,r.y);
  d[2]=intd(r.x+r.w,r.y+a);
  d[3]=intd(r.x+r.w,r.y+r.h-a);
  d[4]=intd(r.x+r.w-a,r.y+r.h);
  d[5]=intd(r.x+a,r.y+r.h);
  d[6]=intd(r.x,r.y+r.h-a);
  d[7]=intd(r.x,r.y+a);

  for (i=0; i<4; i++){
    drawLineSafe(s,d[2*i], d[2*i+1], 1, c);
    drawCircleSegment(s,p[i],a,fd(-PI+(i*PI)/(float)2,-PI/(float)2+(i*PI)/(float)2),c);
  }

}

void transparentRect(SDL_Surface* s, SDL_Rect r, int alpha, Uint32 c){
  SDL_Surface* i=SDL_CreateRGBSurface(SDL_HWSURFACE,r.w,r.h,32,rmask,gmask,bmask,amask);
  SDL_FillRect(i,NULL,(c&0xffffff)|(alpha<<24));
  SDL_SetAlpha(i,SDL_SRCALPHA,0);
  SDL_BlitSurface(i,NULL,s,&r);
  SDL_FreeSurface(i);
}

void gradientRect(SDL_Surface* s, SDL_Rect *rect, Uint32 c[4]){
  SDL_Rect r;
  if (rect){
    r=*rect;
    if (r.x<0){
      r.x=0;
    }
    if (r.x+r.w>s->w){
      r.w=fmax(s->w-r.x,0);
    }
    if (r.y<0){
      r.y=0;
    }
    if (r.y+r.h>s->h){
      r.h=fmax(s->h-r.y,0);
    }
  }else{
    r=create_sdl_rect(0,0,s->w,s->h);
  }
  int x,y;
  int sred, sgreen, sblue, strans;
  float coef[4];
  float max_dist = sqrt((float)2);
  int j;
  float scalex = 1 / (float)pow((float)s -> w, 2);
  float scaley = 1 / (float)pow((float)s -> h, 2);

  for (x=0; x<r.w; x++){
    for (y=0; y<r.h; y++){
      // coef[0]=x/(float)(r.w);
      // coef[1]=(r.w-x)/(float)(r.w);
      // coef[2]=y/(float)(r.h);
      // coef[3]=(r.h-y)/(float)(r.h);
      // sred=sgreen=sblue=strans=0;
      // for (j=0; j<4; j++){
      // 	strans+=coef[j]*((c[j]&amask)>>24);
      // 	sred+=coef[j]*((c[j]&rmask)>>16);
      // 	sgreen+=coef[j]*((c[j]&gmask)>>8);
      // 	sblue+=coef[j]*((c[j]&bmask));
      // }
      // sred/=2;
      // sgreen/=2;
      // sblue/=2;
      // strans/=2;
      
      sred=sgreen=sblue=strans=0;

      coef[0] = 1 - sqrt(scalex * x*x + scaley * y*y) / max_dist;
      coef[1] = 1 - sqrt(scalex * pow((float)s -> w - x, 2) + scaley * y * y) / max_dist;
      coef[2] = 1 - sqrt(scalex * pow((float)s -> w - x, 2) + scaley * pow((float)s -> h - y, 2)) / max_dist;
      coef[3] = 1 - sqrt(scalex * pow((float)x, 2) + scaley * pow((float)s -> h - y, 2)) / max_dist;

      for (j=0; j<4; j++){
      	strans += coef[j]*((c[j]&amask)>>24);
      	sred += coef[j]*((c[j]&rmask)>>16);
      	sgreen += coef[j]*((c[j]&gmask)>>8);
      	sblue += coef[j]*((c[j]&bmask));
      }

      strans = fmin(strans >> 1, 0xff);
      sred = fmin(sred >> 1, 0xff);
      sgreen = fmin(sgreen >> 1, 0xff);
      sblue = fmin(sblue >> 1, 0xff);

      // fprintf(stdout, "gradient_rect: %dx%d: %d, %d, %d, %d - %x, %x, %x, %x\n", x, y, strans, sred, sgreen, sblue, c[0], c[1], c[2], c[3]);

      // fprintf(stdout, "alpha = ");
      // for (j=0; j<4; j++){
      // 	fprintf(stdout, "%.2f * %.2f + ", coef[j], (float)((c[j]&amask)>>24));
      // }

      // fprintf(stdout, "\n");

      psetAlpha(s,x+r.x,y+r.y,(sred<<16)|(sgreen<<8)|sblue|(strans<<24));
    }
  }
}

SDL_Rect testTextSize(const char *v, TTF_Font *f){
  SDL_Surface* i;
  if (!(i=TTF_RenderText_Solid(f, v, create_sdl_color(0xff,0xff,0xff)))){
    fprintf(stderr,"RSK: testTextSize: can not render font\n");
    exit(1);
  }
  SDL_Rect ret={0,0,i->w,i->h};
  SDL_FreeSurface(i);
  return ret;
}

SDL_Rect drawText(SDL_Surface* s, SDL_Rect* r, const char* v, Uint32 c, Uint32 size){
  TTF_Font* font;
  SDL_Rect rect;
  if (size!=12 && !(font=TTF_OpenFont("fonts/arial.ttf",size))){
    fprintf(stderr,"RSK: drawText: can not open font\n");
    exit(1);
  }
  if (size==12){
    font=Rsk_global_font;
  }
  rect=drawText(s,r,v,c,font);
  if (size!=12){
    TTF_CloseFont(font);
  }
  return rect;
}

SDL_Rect drawText(SDL_Surface* s, SDL_Rect* r, const char* v, Uint32 c, TTF_Font* font){
  SDL_Surface* textImage;
  SDL_Rect textRect=create_sdl_rect(0,0,0,0);
  SDL_Color textColor;
  char alpha;
  textColor.r=(c<<8)>>24;
  textColor.g=(c<<16)>>24;
  textColor.b=(c<<24)>>24;
  alpha=c>>24;
  if (!alpha){ alpha=0xff;}

  if (strlen(v)==0){
    return textRect;
  }

  if (!(textImage = TTF_RenderText_Solid( font , v , textColor ))){
    fprintf(stderr,"RSK: drawText: can not render text\n");
    return textRect;
  }
  textRect = create_sdl_rect(r->x,r->y,textImage->w,textImage->h);

  if (s != NULL){
    SDL_SetAlpha(textImage,SDL_SRCALPHA,alpha);
    SDL_BlitSurface(textImage,NULL,s,&textRect);
  }
  
  SDL_FreeSurface(textImage);
  return textRect;
}

SDL_Color make_sdl_color(int c){
  SDL_Color a;
  a.r=(c<<8)>>24;
  a.g=(c<<16)>>24;
  a.b=(c<<24)>>24;
  
  return a;
}

Uint32 fade(Uint32 c, float a){
  a = fmax(a,0);
  int red = a*((c<<8)>>24);
  int green = a*((c<<16)>>24);
  int blue = a*((c<<24)>>24);
  int alpha = c & 0xff000000;
  red = red > 0xff ? 0xff : red;
  green = green > 0xff ? 0xff : green;
  blue = blue > 0xff ? 0xff : blue;
  return red<<16|green<<8|blue|alpha;
}

SDL_Surface* rotateImage(SDL_Surface* image, float a){
  int w;
  int h;
  int x,y;
  int r;
  fd p;
  float sinus=global_sin_table.get_sin(a);
  float cosinus=global_sin_table.get_cos(a);
  SDL_Surface* ret;
  Uint32* s;
  Uint32* d;

  //image depth check
  if (image->pitch/image->w!=4){
    fprintf(stderr,"rotate: bad image pitch");
    return NULL;
  }

  //find smallest dimension
  if (image->w<image->h){
    w=image->w;
  }else{
    w=image->h;
  }
  h=w;
  r=w/2-1;

  //allocate buffer
  if (!(ret=SDL_CreateRGBSurface(SDL_SWSURFACE,w,h,32,rmask,gmask,bmask,amask))){
    fprintf(stderr,"rotateImage: failed to allocate sdl surface");
    exit(1);
  }
  s=(Uint32*)(image->pixels);
  d=(Uint32*)(ret->pixels);

  for (x=-r; x<r; x++){
    for (y=-r; y<r; y++){
      p=fd(x,y);
      if (p.length()<r){
	p=p.rotate(cosinus,sinus);
	p+=fd(r,r);
	d[x+r+(y+r)*w]=s[(int)(p.x)+(image->w*(int)p.y)];
      }
    }
  }
  return ret;
}
SDL_Surface* rescaleImage(SDL_Surface* s, fd a){
  int w=(int)(fabs(a.x)*s->w);
  int h=(int)(fabs(a.y)*s->h);
  SDL_Surface* d=SDL_CreateRGBSurface(SDL_SWSURFACE,w,h,32,rmask,gmask,bmask,amask);
  
  rescaleImage(s,d);
  return d;
}
void rescaleImage(SDL_Surface* s, SDL_Surface* d){
  int x,y;
  int w,h;
  int sw, sh;
  int sd, dd;
  int c;
  w=d->w;
  h=d->h;
  sw=s->w;
  sh=s->h;

  if (w==0 || h==0 || sw==0 || sh==0){
    return;
  }

  sd=(int)(s->pitch/sw);
  dd=(int)(d->pitch/w);
  if (!(sd == 4 && dd == 4)){
    fprintf(stderr,"rescale: depth (%d -> %d)\n", sd, dd);
  }
  if (!((sd>0 && sd<=4) && (dd>0 && dd<=4))){
    fprintf(stderr,"rescale: bad depth (%d -> %d)\n", sd, dd);
    exit(1);
  }
  for (x=0; x<w; x++){
    for (y=0; y<h; y++){
      for (c=0; c<sd && c<dd; c++){
	((char*)d->pixels)[dd*(x+y*w)+c]=((char*)s->pixels)[sd*((x*sw)/w+((y*sh)/h)*sw)+c];
      }
      if (dd==4 && sd!=4){
	((Uint32*)d->pixels)[x+y*w]|=0xff<<24;
      }
    }
  }
}

char* inputTextFromScreen(SDL_Surface* screen, int max, intd pos, Uint32 color, int size){
  SDL_Event e;
  int done=0;
  int c=0;
  char* text=new char[max+1];
  SDL_Surface* buf;
  int sym;
  int shiftPressed;
  SDL_Rect r;

  if (!(buf=SDL_CreateRGBSurface(SDL_SWSURFACE,screen->w,screen->h,32,rmask,gmask,bmask,0))){
    fprintf(stdout,"RSK:inputText: failed to allocate buf, exiting...\n");
    exit(1);
  }

  SDL_BlitSurface(screen,NULL,buf,NULL);

  text[0]='\0';
  
  while(!done){
    while(SDL_PollEvent(&e)){
      if (e.type==SDL_KEYDOWN){
	sym=e.key.keysym.sym;
	if (sym==SDLK_BACKSPACE){
	  if(c>=0){
	    if (c>0){c--;}
	    text[c]='\0';
	  }
	}else if (sym==SDLK_RETURN){
	  if(c>=0){done=1;}
	}else if (sym==SDLK_ESCAPE){
	  done=1;
	}else if (sym==SDLK_SPACE && c<max){
	  text[c++]=' ';
	  text[c]='\0';
	}else if (sym==SDLK_LSHIFT || sym==SDLK_RSHIFT){
	  shiftPressed=1;
	}else if (c<max && (sym!=SDLK_LSHIFT || SDL_GetModState()==KMOD_NONE)){
	  //add conversion to upper case
	  text[c++]=*SDL_GetKeyName((SDLKey)sym);
	  text[c]='\0';
	}
      }else if (e.type==SDL_KEYUP){
	if (sym==SDLK_LSHIFT || sym==SDLK_RSHIFT){
	  shiftPressed=0;
	}
      }
    }
    SDL_BlitSurface(buf,NULL,screen,NULL);
    if (strlen(text)){
      r = create_sdl_rect(pos.x,pos.y,0,0);
      drawText(screen,&r,text,color,size);
    }
    SDL_UpdateRect(screen,0,0,0,0);
    SDL_Delay(40);

  }

  SDL_FreeSurface(buf);

  return text;
  

} 

int loadKey(SDL_Surface* screen, int x, int y, const char* v){
  
  int r;
  SDL_Rect rect={x,y,0,0};
  char output[1024];

  drawText(screen, &rect, v, 0xffaa20,12);    
  SDL_UpdateRect(screen,0,0,0,0);

  r=readKeySym();

  return r;

}

void code_to_transparent(int c, SDL_Surface* s){
  int i;
  Uint32* p = (Uint32*)s -> pixels;

  if (s -> pitch / s -> w != 4){
    return ;
  }
  
  for (i = 0; i < s -> w * s -> h; i++){
    *(p + i) = *(p + i) == c ? 0 : *(p + i);
  }
}
