#include "RSK_GUI.h"

RSG_Slidebar::RSG_Slidebar(){
}
RSG_Slidebar::RSG_Slidebar(int p, color_scheme cs, SDL_Rect b) : RSG_Component(cs,b){
  init(p);
}
void RSG_Slidebar::init(int p){
  visible = 1;
  was_pressed = 0;
  percent = p;
}

void RSG_Slidebar::cleanup(){

}

void RSG_Slidebar::handle_event(SDL_Event e){
  if (!visible){
    return;
  }

  if (e.type == SDL_MOUSEMOTION && (e.motion.state & SDL_BUTTON(1)) && RSK_GUI::fits(e.motion.x,e.motion.y,root_bounds)){
    percent = (100 * (e.motion.x - root_bounds.x)) / bounds.w;
    was_pressed = 1;
  }
  RSG_Component::handle_event(e);
}

void RSG_Slidebar::paint(SDL_Surface* s){
  SDL_Rect r;
  r = create_sdl_rect(bounds.x,bounds.y+bounds.h/2-1,bounds.w,3);
  SDL_FillRect(s,&r,scheme.fg);
  drawFrame(s,bounds,scheme.frame);
    
  r = create_sdl_rect(bounds.x+(percent*bounds.w)/100-3,bounds.y,6,bounds.h);
  SDL_FillRect(s,&r,scheme.fg_highlight);

}

void RSG_Slidebar::set_percent(int p){
  if (p >= 0 && p <= 100){
    percent = p;
  }
}

int RSG_Slidebar::get_percent(){
  return percent;
}

int RSG_Slidebar::get_state(){
  return was_pressed;
}

void RSG_Slidebar::clear_state(){
  was_pressed=0;
}
