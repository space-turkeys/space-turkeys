
class RSG_Button : public RSG_Component{
private:
  string name;
  int was_pressed;
  SDL_Surface *image, *image_highlight;
  int initialized;
public:
  RSG_Button();
  RSG_Button(string n, SDL_Rect b);
  RSG_Button(string n, color_scheme cs, TTF_Font* f, SDL_Rect b);
  RSG_Button(event_listener *l, string n, color_scheme cs, TTF_Font* f, SDL_Rect b);
  ~RSG_Button();
  void init(string n, TTF_Font* f);
  void cleanup();
  void paint(SDL_Surface* s);
  void handle_event(SDL_Event e);
  void set_state(int i);
  int get_state();
  string get_text();
  void reset();
};
