#include "RSK_GUI.h"

RSG_Panel::RSG_Panel(){

}

RSG_Panel::RSG_Panel(SDL_Rect b) : RSG_Component(b){

  if (!(b.w && b.h)){
    return;
  }
  if (!(image = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, b.w, b.h, 32, rmask, gmask, bmask, amask))){
    fprintf(stderr,"RSG_Panel(): failed to allocate image.\n");
    exit(1);
  }

  gradientRect(image,NULL,scheme.bg);
  drawFrame(image,create_sdl_rect(0,0,b.w,b.h),scheme.frame);
}

RSG_Panel::RSG_Panel(list<RSG_Component*> c, SDL_Rect b) : RSG_Component(b){
  list<RSG_Component*>::iterator i;

  if (!(b.w && b.h)){
    return;
  }
  if (!(image = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, b.w, b.h, 32, rmask, gmask, bmask, amask))){
    fprintf(stderr,"RSG_Panel(): failed to allocate image.\n");
    exit(1);
  }
  
  for (i = c.begin(); i != c.end(); i++){
    add(*i);
  }

  gradientRect(image,NULL,scheme.bg);
  drawFrame(image,create_sdl_rect(0,0,b.w,b.h),scheme.frame);
}

RSG_Panel::~RSG_Panel(){
  cleanup();
}

void RSG_Panel::cleanup(){
  children.clear();
  if (image){
    SDL_FreeSurface(image);
  }
}

void RSG_Panel::paint(SDL_Surface *s){
  list<RSG_Component*>::iterator i;
  SDL_Surface* buf;

  // if (!(buf = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, bounds.w, bounds.h, 32, rmask, gmask, bmask, amask))){
  //   fprintf(stdout,"RSG_Panel::paint(): failed to allocate buf.\n");
  //   return;
  // }
  
  // SDL_FillRect(buf,NULL,0xff000000);
  // SDL_BlitSurface(image,NULL,buf,NULL);

  if (!(buf = SDL_ConvertSurface(image, image -> format, image -> flags))){
    fprintf(stdout,"RSG_Panel::paint(): failed to allocate buf.\n");
    return;
  }

  for (i = children.begin(); i != children.end(); i++){
    (*i) -> paint(buf);
  }

  SDL_BlitSurface(buf,NULL,s,&bounds);
  SDL_FreeSurface(buf);
}

void RSG_Panel::handle_event(SDL_Event e){
  if (!visible){
    return;
  }

  list<RSG_Component*>::iterator i;
  for (i = children.begin(); i != children.end(); i++){
    (*i) -> handle_event(e);
  }
  RSG_Component::handle_event(e);
}

void RSG_Panel::delete_all(){
  list<RSG_Component*>::iterator i;
  for (i = children.begin(); i != children.end(); i++){
    (*i) -> delete_all();
  }
  cleanup();
}

void RSG_Panel::add(RSG_Component* c){
  c -> update_rootb(root_bounds);
  children.push_back(c);
}

list<RSG_Component*> *RSG_Panel::get_children(){
  return &children;
}
