class RSG_Slidebar : public RSG_Component{
private:
  int percent;
  int was_pressed;
public:
  RSG_Slidebar();
  RSG_Slidebar(int p, color_scheme cs, SDL_Rect b);
  ~RSG_Slidebar();
  void init(int p);
  void cleanup();
  void handle_event(SDL_Event e);
  void paint(SDL_Surface* s);
  void set_percent(int p);
  int get_percent();
  int get_state();
  void clear_state();
};
