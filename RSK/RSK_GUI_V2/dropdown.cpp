#include "RSK_GUI.h"


RSG_Dropdown::RSG_Dropdown(){
  initialized = 0;
}
RSG_Dropdown::RSG_Dropdown(vector<string> elements, int ns, color_scheme cs,SDL_Rect b) : RSG_Component(cs,b){
  init(elements,ns,&b);
}

void RSG_Dropdown::init(vector<string> e, int ns, SDL_Rect *se){
  int i;
  int guess;
  int max  =  0;
  int k  =  0;

  if (e.size() <= 0){
    initialized = 0;
    return;
  }

  /* configure */
  visible = 1;
  dropped = 0;
  initialized = 1;
  was_clicked = 0;
  selected = 0;
  num_show = fmin(ns,e.size());
  offset = 0;
  elements  =  e;

  for (i = 0; i<e.size(); i++){
    if (e[i].length()>max){
      max = e[i].length();
      k = i;
    }
  }

  guess = fmin((1.5 * bounds.w) / max, 0.8 * bounds.h);
  if (font){
    TTF_CloseFont(font);
  }
  if (!(font = TTF_OpenFont("fonts/arial.ttf", guess))){
    fprintf(stdout, "RSG_Dropdown::init() failed to opent font.\n");
  }
  /* determine element size */
  // if (se  ==  NULL || se -> w == 0 || se -> h == 0){
  size_element = testTextSize(e[k].c_str(),font);
  size_element.w += 4;
  size_element.h += 4;
  // }else{
  //   size_element = *se;
  // }
}

void RSG_Dropdown::handle_event(SDL_Event e){
  if (!visible){
    return;
  }

  if (e.type == SDL_MOUSEBUTTONDOWN && RSK_GUI::fits(e.button.x, e.button.y, root_bounds)){
    if (e.button.button == SDL_BUTTON_LEFT){
      if (dropped){
	selected = (e.button.y-root_bounds.y)/size_element.h+offset;
	was_clicked = 1;
      }
      dropped = !dropped;
    }else if(e.button.button == SDL_BUTTON_WHEELUP && dropped){
      if (offset>0){
	offset--;
      }
    }else if(e.button.button == SDL_BUTTON_WHEELDOWN && dropped){
      if (offset+num_show<elements.size()){
	offset++;
      }
    }
  }
  RSG_Component::handle_event(e);
  if (dropped){
    root_bounds.w = bounds.w = size_element.w+4;
    root_bounds.h = bounds.h = size_element.h*num_show+4;
  }else{
    root_bounds.w = bounds.w = size_element.w+4;
    root_bounds.h = bounds.h = size_element.h+4;
  }
}

void RSG_Dropdown::loose_focus(){
  bounds.w = size_element.w;
  bounds.h = size_element.h;
  dropped = 0;
}

void RSG_Dropdown::paint(SDL_Surface* s){
  SDL_Rect r;
  int x;
  bounds.w = size_element.w;
  if (dropped){
    bounds.h = num_show*size_element.h+4;
  }else{
    bounds.h = size_element.h+4;
  }
  SDL_FillRect(s,&bounds,scheme.bg[0]);
  drawFrame(s,bounds,scheme.frame);
  if (dropped){
    for (x = 0; x<num_show; x++){
      r = create_sdl_rect(bounds.x+2,bounds.y+x*size_element.h+2,size_element.w,size_element.h);
      if (x+offset == selected){
	SDL_FillRect(s,&r,scheme.bg_highlight[0]);
      }
      drawFrame(s,r,scheme.frame);
      r.x++;
      r.y++;
      drawText(s,&r,elements[x+offset].c_str(),scheme.fg,font);
    }
  }else{
    r = create_sdl_rect(bounds.x+2,bounds.y+2,size_element.w,size_element.h);
    drawText(s,&r,elements[selected].c_str(),scheme.fg,font);
  }
}

void RSG_Dropdown::set_selected_index(int i){
  if (i >= 0 && i < elements.size()){
    selected = i;
  }
}

void RSG_Dropdown::set_selected_index(string v){
  int x;
  for (x = 0; x<elements.size(); x++){
    if (!(elements[x].compare(v))){
      selected = x;
    }
  }
}
int RSG_Dropdown::get_state(){
  return was_clicked;
}

int RSG_Dropdown::get_selected_index(){
  return selected;
}

string RSG_Dropdown::get_selected_name(){
  return elements[selected];
}

void RSG_Dropdown::clear_state(){
  was_clicked = 0;
}

void RSG_Dropdown::cleanup(){
  if (initialized){
    initialized = 0;
    elements.clear();
  }
  if (font){
    TTF_CloseFont(font);
  }
  font = NULL;
}
