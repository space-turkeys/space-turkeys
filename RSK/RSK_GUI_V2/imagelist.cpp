#include "RSK_GUI.h"

RSG_Imagelist::RSG_Imagelist(){
  image_sl = 0;
  image_sr = 0;
  image_dark = 0;
  initialized = 0;
  was_clicked = 0;
}

RSG_Imagelist::RSG_Imagelist(int nshow, int flag, vector<SDL_Surface*> s, SDL_Rect b) : RSG_Component(b){
  init(nshow,flag,s);
}

void RSG_Imagelist::init(int nshow, int flag, vector<SDL_Surface*> s){
  int i;
  SDL_Rect r = create_sdl_rect(0,0,bounds.w,bounds.h);

  if (!(nshow && s.size() && bounds.w && bounds.h)){
    return;
  }

  /* configure */
  cleanup();
  visible = 1;
  num_show = fmin(nshow,s.size());
  offset = 0;
  active = - 1;
  mark_selected = flag & 1;
  orientation = flag & 2;
  padding = 2;
  if (orientation){
    //vertical
    w = bounds.w - 2 * padding;
    h = bounds.h / (num_show + 2) - 2 * padding;
  }else{
    //horizontal
    w = bounds.w / (num_show + 2) - 2 * padding;
    h = bounds.h - 2 * padding;
  }

  /* generate images */
  images.resize(s.size());

  if (!(image_sl = SDL_CreateRGBSurface(SDL_HWSURFACE,w,h,32,rmask,gmask,bmask,0))){
    fprintf(stderr,"RskImageList: failed to allocate elements.\n");
    exit(1);
  }
  if (!(image_sr = SDL_CreateRGBSurface(SDL_HWSURFACE,w,h,32,rmask,gmask,bmask,0))){
    fprintf(stderr,"RskImageList: failed to allocate elements.\n");
    exit(1);
  }
  if (!(image_dark = SDL_CreateRGBSurface(SDL_HWSURFACE,w,h,32,rmask,gmask,bmask,amask))){
    fprintf(stderr,"RskImageList: failed to allocate elements.\n");
    exit(1);
  }
  SDL_SetAlpha(image_dark,SDL_SRCALPHA,0);
  SDL_FillRect(image_dark,NULL,0x88000000);

  drawText(image_sl,&r,"<<",0xffffff,fmin(w,h) - 4);
  drawText(image_sr,&r,">>",0xffffff,fmin(w,h) - 4);

  for (i = 0; i<s.size(); i ++ ){
    if (!(images[i].image = SDL_CreateRGBSurface(SDL_HWSURFACE,w,h,32,rmask,gmask,bmask,0))){
      fprintf(stderr,"RskImageList: failed to allocate image.\n");
      exit(1);
    }
    rescaleImage(s[i],images[i].image);
    SDL_FreeSurface(s[i]);
  }
  initialized = 1;
  was_clicked = 0;
}

void RSG_Imagelist::cleanup(){
  int i;
  if (initialized){
    for (i = 0; i<images.size(); i ++ ){
      SDL_FreeSurface(images[i].image);
    }
    images.clear();
    SDL_FreeSurface(image_sl);
    SDL_FreeSurface(image_sr);
    SDL_FreeSurface(image_dark);
    image_sl = 0;
    image_sr = 0;
    image_dark = 0;
    initialized = 0;
  }
}

void RSG_Imagelist::handle_event(SDL_Event e){
  if (!visible){
    return;
  }

  int i,x;
  if (e.type == SDL_MOUSEBUTTONDOWN && RSK_GUI::fits(e.button.x, e.button.y, root_bounds)){
    was_clicked = 1;
    switch (orientation){
    case 0:
      x = e.button.x - root_bounds.x;
      if (x<getw()){
	//shift left was clicked
	offset --;
	if (offset<0){
	  offset = images.size() - 1;
	}
      }else if (x>(num_show + 1)*getw()){
	//shift right was clicked
	offset = (offset + 1)%images.size();
      }else{
	//an element was clicked
	i = (offset + (x - getw())/getw())%images.size();
	if (!images[i].inactivated){
	  images[i].selected = !images[i].selected;
	  active = i;
	}
      }
      break;
    case 1:
      x = e.button.y - root_bounds.y;
      if (x<geth()){
	//shift left was clicked
	offset --;
	if (offset<0){
	  offset = images.size() - 1;
	}
      }else if (x>(num_show + 1)*geth()){
	//shift right was clicked
	offset = (offset + 1)%images.size();
      }else{
	//an element was clicked
	i = (offset + (x - geth())/geth())%images.size();
	if (!images[i].inactivated){
	  images[i].selected = !images[i].selected;
	  active = i;
	}
      }
      break;
    }
  }
  RSG_Component::handle_event(e);
}

void RSG_Imagelist::paint(SDL_Surface* screen){
  int i;
  SDL_Rect r;

  if (!initialized){
    return;
  }

  switch (orientation){
  case 0:
    //horizontal orientation
    r = create_sdl_rect(bounds.x + padding,bounds.y + padding,w,h);
    SDL_BlitSurface(image_sl,NULL,screen,&r);
    r = create_sdl_rect(bounds.x + bounds.w - getw() + padding,bounds.y + padding,w,h);
    SDL_BlitSurface(image_sr,NULL,screen,&r);

    for (i = 0; i<num_show; i ++ ){
      r.x = bounds.x + (i + 1)*getw() + padding;
      SDL_BlitSurface(images[(i + offset)%images.size()].image,NULL,screen,&r);
      if (images[(i + offset)%images.size()].inactivated){
	SDL_BlitSurface(image_dark,NULL,screen,&r);
      }
      if (mark_selected && images[(i + offset)%images.size()].selected){
	drawFrame(screen,r,0xffffff);
      }else{
	drawFrame(screen,r,0x888888);
      }
    }
    if (active > - 1 && get_active_frame() < num_show){
      r.x = bounds.x + (get_active_frame() + 1)*getw() + padding;
      drawFrame(screen,r,2,0xffffffff);
    }

    break;
  case 1:
    //vertical
    r = create_sdl_rect(bounds.x + padding, bounds.y + padding, w, h);
    SDL_BlitSurface(image_sl,NULL,screen,&r);
    r = create_sdl_rect(bounds.x + padding, bounds.y + bounds.h - geth() + padding, w, h);
    SDL_BlitSurface(image_sr,NULL,screen,&r);

    for (i = 0; i<num_show; i ++ ){
      r.y = bounds.y + (i + 1)*geth() + padding;
      SDL_BlitSurface(images[(i + offset)%images.size()].image,NULL,screen,&r);
      if (images[(i + offset)%images.size()].inactivated){
	SDL_BlitSurface(image_dark,NULL,screen,&r);
      }
      if (mark_selected && images[(i + offset)%images.size()].selected){
	drawFrame(screen,r,0xffffff);
      }else{
	drawFrame(screen,r,0x888888);
      }
    }
    if (active > - 1 && get_active_frame() < num_show){
      r.y = bounds.y + (get_active_frame() + 1)*geth() + padding;
      drawFrame(screen,r,2,0xffffffff);
    }
    break;
  }
  r = bounds;
  drawFrame(screen,r,2,0xaaaaaa);
}

void RSG_Imagelist::reset(){
  was_clicked = 0;
}

int RSG_Imagelist::get_state(){
  return was_clicked;
}

int RSG_Imagelist::get_active(){
  return active;
}

void RSG_Imagelist::set_active(int i){
  active = i;
}

int RSG_Imagelist::get_active_frame(){
  int i;
  i = active - offset;
  while (i<0){
    i += images.size();
  }
  i %= images.size();
  return i;
}

int RSG_Imagelist::get_active_index(){
  return active;
}

SDL_Surface* RSG_Imagelist::get_image(int idx){
  if (idx >=0 && idx < images.size()){
    return images[idx].image;
  }
  return NULL;
}
 
char RSG_Imagelist::get_selected(int i){
  if (i >= 0 && i<images.size() && initialized){
    return images[i].selected;
  }else{
    return - 1;
  }
}

char RSG_Imagelist::get_inactivated(int i){
  if (i >= 0 && i<images.size() && initialized){
    return images[i].inactivated;
  }else{
    return - 1;
  }
}

int RSG_Imagelist::getw(){
  return w + 2 * padding;
}

int RSG_Imagelist::geth(){
  return h + 2 * padding;
}
