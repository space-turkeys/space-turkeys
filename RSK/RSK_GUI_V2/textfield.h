class RSG_Textfield : public RSG_Component{
private:
  bool g;
  string text;
  int was_pressed;
  TTF_Font *font;
  double last_click;
  bool text_selected;

public:
  RSG_Textfield(SDL_Rect b);
  RSG_Textfield(int size, string t, color_scheme cs, TTF_Font *f, SDL_Rect b);
  ~RSG_Textfield();
  void init(int size, string t, TTF_Font *f);
  void cleanup();
  void paint(SDL_Surface* s);
  void handle_event(SDL_Event e);
  void loose_focus();
  void set_state(int i);
  int get_state();
  void set_text(string n);
  string get_text();
  void clear_text();
  void reset();
};
