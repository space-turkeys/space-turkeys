
class RSG_Label : public RSG_Component{
private:
  string text;
  int width,height;
  SDL_Surface* image;
  int initialized;
  bool g;
public:
  RSG_Label();
  RSG_Label(string n, SDL_Rect b);
  RSG_Label(string n, color_scheme cs, TTF_Font* f, SDL_Rect b);
  ~RSG_Label();
  void init(string n, TTF_Font* f);
  void cleanup();
  void paint(SDL_Surface* s);
  void handle_event(SDL_Event e);
  string get_text();
  void set_text(string t);
};
