class RSG_Percent : public RSG_Component{
 private: 
  int percent;

 public: 
  RSG_Percent(SDL_Rect b);
  void cleanup();
  void paint(SDL_Surface* s);
  void set_percent(int p);
  int get_percent();
};
