#include "RSK_GUI.h"


RSG_Label::RSG_Label(){
  initialized=0;
  image=0;
  font = NULL;
}
RSG_Label::RSG_Label(string n, SDL_Rect b) : RSG_Component(b){
  initialized=0;
  image=0;
  init(n,NULL);
}
RSG_Label::RSG_Label(string n, color_scheme cs, TTF_Font* f, SDL_Rect b) : RSG_Component(cs,b){
  initialized=0;
  image=0;
  init(n,f);
}

RSG_Label::~RSG_Label(){
  cleanup();
}

void RSG_Label::init(string n, TTF_Font* f){
  SDL_Rect r,ts;
  SDL_Surface* buf, *hbuf;
  bool adapt;
  g = false;

  /* clear previous data */
  cleanup();

  /* configure */
  font = f;

  if (font == NULL){
    int guess = fmin((2 * bounds.w) / n.length(), bounds.h);
    font = TTF_OpenFont("fonts/arial.ttf", guess);
    g = true;
  }

  visible = 1;
  if (!n.length()){
    text = " ";
  }else{
    text.assign(n);
  }
  adapt = !(bounds.w && bounds.h);
  ts = testTextSize((const char*)text.c_str(),font);
  if (adapt){
    r = ts;
  }else{
    r = bounds;
    r.x = r.y = 0;
  }
  bounds.w = r.w;
  bounds.h = r.h;

  /* create buffers */
  image = SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA,r.w,r.h,32,rmask,gmask,bmask,amask);
  buf = SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA,ts.w,ts.h,32,rmask,gmask,bmask,amask);
  if (!(buf && image)){
    fprintf(stderr,"failed to create rsk_label image buffers! exiting...\n");
    exit(1);
  }

  SDL_FillRect(buf,NULL,0xff000000);
  drawText(buf,&ts,text.c_str(),scheme.fg,font);
  rescaleImage(buf,image);
  code_to_transparent(0xff000000, image);
  SDL_FreeSurface(buf);

  initialized=1;
}

void RSG_Label::cleanup(){
  if (!initialized){
    return;
  }

  if (image){
    SDL_FreeSurface(image);
  }
  
  if (font != NULL && g){
    TTF_CloseFont(font);
    font = NULL;
  }

  initialized = false;
  image = NULL;
  g = false;
}

void RSG_Label::handle_event(SDL_Event e){
  RSG_Component::handle_event(e);
}

void RSG_Label::paint(SDL_Surface* s){
  if (image && initialized){
    SDL_BlitSurface(image,NULL,s,&bounds);
  }
}

string RSG_Label::get_text(){
  return text;
}
  
void RSG_Label::set_text(string t){
  cleanup();
  init(t,font);
}

  
