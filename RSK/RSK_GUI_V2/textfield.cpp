
#include "RSK_GUI.h"

RSG_Textfield::RSG_Textfield(SDL_Rect b) : RSG_Component(b){
  init(10, "", RSG_Component::default_font);
}
RSG_Textfield::RSG_Textfield(int size, string t, color_scheme cs, TTF_Font *f, SDL_Rect b) : RSG_Component(cs, b){
  init(size, t, f);
}

void RSG_Textfield::init(int size, string t, TTF_Font *f){
  visible = 1;
  text = t;
  was_pressed = 0;
  font = f;
  g = false;
  last_click = 0;
  text_selected = 0;
  if (font == NULL){
    int tsw = (bounds.w - 4) / t.length();
    int tsh = bounds.h - 4;
    
    font = TTF_OpenFont("fonts/arial.ttf", tsw < tsh ? tsw : tsh);
    g = true;
  }
}

RSG_Textfield::~RSG_Textfield(){
  cleanup();
}

void RSG_Textfield::cleanup(){
  if (font && g){
    TTF_CloseFont(font);
    font = NULL;
  }
  g = false;
}
  
void RSG_Textfield::paint(SDL_Surface* s){
  SDL_Rect dum;
  SDL_Rect r = bounds;
  SDL_Surface *ibuf = SDL_CreateRGBSurface(SDL_SWSURFACE|SDL_SRCALPHA, r.w, r.h, 32, rmask, gmask, bmask, amask);

  r.x+=2;
  r.y+=2;

  if (ibuf){
    if (has_mouse_over || active_component == this){
      //SDL_FillRect(s,&bounds,scheme.bg_highlight[0]);
      gradientRect(ibuf, 0, scheme.bg_highlight);
    }else{
      //SDL_FillRect(s,&bounds,scheme.bg[0]);
      gradientRect(ibuf, 0, scheme.bg);
    }
    SDL_BlitSurface(ibuf, 0, s, &bounds);
    SDL_FreeSurface(ibuf);
  }

  if (text.length()>0){
    dum = testTextSize((const char*)text.c_str(), font);
    r.y = bounds.y + (bounds.h - dum.h) / 2;
    if (has_mouse_over){
      drawText(s,&r,(const char*)text.c_str(),scheme.fg_highlight,font);
    }else{
      drawText(s,&r,(const char*)text.c_str(),scheme.fg,font);
    }
  }

  if (has_mouse_over){
    drawFrame(s,bounds,scheme.frame_highlight);
  }else{
    drawFrame(s,bounds,scheme.frame);
  }

  if (text_selected){
    dum.x = r.x;
    dum.y = r.y;
    transparentRect(s, dum, 100, 0xffffff);
  }
}

void RSG_Textfield::handle_event(SDL_Event e){
  if (!visible){
    return;
  }

  if (e.type==SDL_KEYDOWN && RSG_Component::active_component == this){
    if (e.key.keysym.sym==SDLK_RETURN){
      was_pressed=1;
    }else if (e.key.keysym.sym==SDLK_BACKSPACE){
      if (text_selected){
	clear_text();
	text_selected = false;
      }else{
	if (text.length()>0){
	  text.resize(text.length()-1);
	}
      }
    }else if (e.key.keysym.unicode>0){
      if (text_selected){
	clear_text();
	text_selected = false;
      }
      text+=(char)e.key.keysym.unicode;
    }
  }

  RSG_Component::handle_event(e);

  if (e.type == SDL_MOUSEBUTTONDOWN && RSG_Component::active_component == this){
    double curtime = system_seconds();

    if (text_selected){
      last_click = curtime;
      text_selected = false;
    }else{
      if (curtime - last_click < .3){
	text_selected = true;
	last_click = 0;
      }else{
	last_click = curtime;
      }
    }
  }
}

void RSG_Textfield::loose_focus(){
  text_selected = false;
}

int RSG_Textfield::get_state(){
  return was_pressed;
}

void RSG_Textfield::set_state(int s){
  was_pressed = s;
}

void RSG_Textfield::set_text(string n){
  text=n;
}

string RSG_Textfield::get_text(){
  return text;
}

void RSG_Textfield::clear_text(){
  text="";
}


void RSG_Textfield::reset(){
  was_pressed=0;
}
