class RSG_Table:public RSG_Component{
 private:
  int rows, cols;
  int selected_row;
  int selected_col;
  SDL_Rect element_size;
  vector<vector<RSG_Button*> > elements;
  RSG_Label *title;

 public:
  RSG_Table(vector<vector<string> > content, SDL_Rect b);
  ~RSG_Table();
  void cleanup();
  void handle_event(SDL_Event e);
  void loose_focus();
  void init(vector<vector<string> > content);
  void update_content(vector<vector<string> > content);
  void paint(SDL_Surface* s);
  int get_selected_row();
  int get_selected_col();
};
