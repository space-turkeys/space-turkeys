#include "RSK_GUI.h"

RSK_GUI::RSK_GUI(){
  init();
}

void RSK_GUI::init(){
  paint_background = 0;
  bg_color = 0;
  active_component = 0;
  mouse_on = 0;
}

void RSK_GUI::cleanup(){
  for (i = components.begin(); i != components.end(); i++){
    (*i) -> cleanup();
  }
}

void RSK_GUI::delete_components(){
  for (i = components.begin(); i != components.end(); i++){
    (*i) -> delete_all();
    delete *i;
  }
}

void RSK_GUI::merge(list<RSG_Component*> x){
  components.merge(x);
}

void RSK_GUI::add(RSG_Component *x){
  add(x,0);
}

void RSK_GUI::add(RSG_Component *x, int l){
  if (l > 8){l = 8;}
  if (l < -7){l = -7;}
  x->layer = l;
  components.push_back(x);
}

void RSK_GUI::remove(RSG_Component *x){
  components.remove(x);
}

void RSK_GUI::paint(SDL_Surface* screen){
  int l;
  if (paint_background){
    SDL_FillRect(screen,NULL,bg_color);
  }
  for (l = -7; l < 9; l++){
    for (i = components.begin(); i != components.end(); i++){
      if ((*i) -> visible && (*i) -> layer == l && (*i) != active_component){
	(*i) -> paint(screen);
      }
    }
  }
  if (active_component){
    active_component -> paint(screen);
  }
}

int RSK_GUI::fits(int x, int y, SDL_Rect b){
  return x>=b.x && x<b.x+b.w && y>=b.y && y<b.y+b.h;
}

// RSG_Component* RSK_GUI::get_component(int x, int y){
//   int i;
//   int id=-1;
//   int done=0;
//   int l;

//   if (active_component && fits(x, y, active_component -> root_bounds)){
//     return active_component;
//   }
//   for (l = 8; l > -6; l--){
//     for (i = components.begin(); i != components.end(); i++){
//       if (fits(x, y, (*i) -> root_bounds) && (*i) -> layer == l){
// 	return *i;
//       }
//     }
//   }
//   return 0;
// }

void RSK_GUI::handle_event(SDL_Event e){
  for (i = components.begin(); i != components.end(); i++){
    (*i) -> handle_event(e);
  }
}
