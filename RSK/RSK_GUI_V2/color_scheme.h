struct color_scheme{
  int fg;
  int fg_highlight;
  unsigned int bg[4];
  unsigned int bg_highlight[4];
  int frame;
  int frame_highlight;

  color_scheme();
};
