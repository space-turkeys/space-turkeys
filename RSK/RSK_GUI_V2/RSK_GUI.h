#include <list>
#include <vector>
#include <string>

#ifndef RSK_GUI_DEF
#define RSK_GUI_DEF 1
using namespace std;

#include <RSK/RskGraphics.h>

#include <RSK/RSK_GUI_V2/color_scheme.h>
#include <RSK/RSK_GUI_V2/component.h>
#include <RSK/RSK_GUI_V2/RSK_GUI_MAIN.h>

#include <RSK/RSK_GUI_V2/button.h>
#include <RSK/RSK_GUI_V2/label.h>
#include <RSK/RSK_GUI_V2/textfield.h>
#include <RSK/RSK_GUI_V2/dropdown.h>
#include <RSK/RSK_GUI_V2/imagelist.h>
#include <RSK/RSK_GUI_V2/slidebar.h>
#include <RSK/RSK_GUI_V2/panel.h>
#include <RSK/RSK_GUI_V2/percent.h>
#include <RSK/RSK_GUI_V2/table2.h>
#include <RSK/RSK_GUI_V2/dialogue.h>

#endif
