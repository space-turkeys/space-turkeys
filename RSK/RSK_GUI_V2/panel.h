class RSG_Panel : public RSG_Component{
 private:
  list<RSG_Component*> children;
  SDL_Surface *image;
 public: 
  RSG_Panel();
  RSG_Panel(SDL_Rect b);
  RSG_Panel(list<RSG_Component*> c, SDL_Rect b);
  ~RSG_Panel();
  void cleanup();
  void paint(SDL_Surface *s);
  void handle_event(SDL_Event e);
  void delete_all();

  void add(RSG_Component* c);
  list<RSG_Component*> *get_children();
};
