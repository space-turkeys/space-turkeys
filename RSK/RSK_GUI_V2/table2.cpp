#include "RSK_GUI.h"

RSG_Table::RSG_Table(vector<vector<string> > content, SDL_Rect b):RSG_Component(b){
  rows = 0;
  cols = 0;
  selected_col = -1;
  selected_row = -1;
  title = NULL;
  init(content);
}

void RSG_Table::init(vector<vector<string> > content){
  /*
    expects first element to be a vector of one string; the title
    the second element's size sets the num. cols.
   */
  SDL_Rect b;

  cleanup();

  /* setup dimensions */
  rows = content.size() - 1;
  if (rows < 0){
    return;
  }

  cols = rows > 0 ? content[1].size() : 0;
  if (cols < 0){
    return;
  }

  b = bounds;
  b.h = bounds.h / 8;
  title = new RSG_Label(content[0][0], b);

  if (!title){
    fprintf(stdout, "table::init: failed to make title!\n");
    exit(1);
  }

  if (rows > 0 && cols > 0){
    int e_base = bounds.y + b.h;

    element_size = create_sdl_rect(0,0,bounds.w/cols,(bounds.h - title -> bounds.h) / rows);

    elements.resize(rows);
  
    /* draw content */
    for (int i = 0; i < rows; i++){
      elements[i].resize(cols);
      for (int j = 0; j < cols; j++){
	elements[i][j] = NULL;

	if (j >= content[i + 1].size()){
	  break;
	}

	b = element_size;
	b.x = bounds.x + j * b.w;
	b.y = e_base + i * b.h;
	elements[i][j] = new RSG_Button(content[i+1][j], b);

	if (!elements[i][j]){
	  fprintf(stdout, "table::init: failed to create button!\n");
	  exit(1);
	}
      }
    }
  }
}

void RSG_Table::update_content(vector<vector<string> > content){
  /*
    expects first element to be a vector of one string; the title
    the second element's size sets the num. cols.
   */
  SDL_Rect b;

  /* setup dimensions */
  if (rows != content.size() - 1 || cols != (rows > 0 ? content[1].size() : 0) || rows == -1){
    fprintf(stdout, "table::update_content: dimension mismatch! Got %dx%d, expected %dx%d. Calling init instead...\n", content.size() - 1, (rows > 0 && content.size() > 1 ? content[1].size() : 0));
    init(content);
    return;
  }
    
  if (content[0][0].compare(title -> get_text())){
    title -> set_text(content[0][0]);
  }

  for (int i = 0; i < rows; i++){
    for (int j = 0; j < cols && j < content[i].size(); j++){
      if (elements[i][j] && content[i+1][j].compare(elements[i][j] -> get_text())){
	elements[i][j] -> init(content[i+1][j], NULL);
      }
    }
  }
}


RSG_Table::~RSG_Table(){
  cleanup();
}

void RSG_Table::cleanup(){
  int i,j;

  if (title){
    delete title;
    title = NULL;
  }

  for (i = 0; i < rows; i++){
    for (j = 0; j < cols; j++){
      if (elements[i][j]){
	delete elements[i][j];
      }
    }
    elements[i].clear();
  }

  elements.clear();
  rows = -1;
  cols = 0;
  selected_col = -1;
  selected_row = -1;
}

void RSG_Table::paint(SDL_Surface* s){
  int i,j;

  if (!visible){
    return;
  }

  if (!title){
    fprintf(stdout, "table::paint: title is missing!\n");
    return;
  }

  title -> paint(s);
  for (i = 0; i < rows; i++){
    for (j = 0; j < cols; j++){
      if (elements[i][j]){
	elements[i][j] -> paint(s);
      }
    }
  }

  if (selected_row > -1){
    SDL_Rect b = bounds;
    b.y = bounds.y + title -> bounds.h + selected_row * element_size.h;
    b.h = element_size.h;
    transparentRect(s, b, 80, 0xffffff);
  }
}

void RSG_Table::handle_event(SDL_Event e){
  int i,j;
  if (!visible){
    return;
  }

  if (!title){
    fprintf(stdout, "table::handle_event: title is missing!\n");
    return;
  }

  title -> handle_event(e);
  for (i = 0; i < rows; i++){
    for (j = 0; j < cols; j++){
      if (elements[i][j]){
	elements[i][j] -> handle_event(e);

	if (elements[i][j] -> get_state()){
	  fprintf(stdout, "table::handle_event: STATE SET AT %dx%d\n", i, j);
	  elements[i][j] -> reset();
	  selected_row = i;
	  selected_col = j;
	}
      }
    }
  }

  RSG_Component::handle_event(e);
}

void RSG_Table::loose_focus(){
  // selected_row = -1;
  // selected_col = -1;
}

int RSG_Table::get_selected_row(){
  return selected_row;
}

int RSG_Table::get_selected_col(){
  return selected_col;
}
