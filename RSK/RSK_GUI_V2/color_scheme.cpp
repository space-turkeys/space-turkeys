#include "RSK_GUI.h"

color_scheme::color_scheme(){
  fg=0xffddaacc;
  fg_highlight=0xffffccff;
  bg[0]=0xff0044aa;
  bg[1]=0xffcc2266;
  bg[2]=0xaa777777;
  bg[3]=0xaa000000;
  bg_highlight[0]=0xff0044aa;
  bg_highlight[1]=0xffcc2266;
  bg_highlight[2]=0xffaaaaaa;
  bg_highlight[3]=0xff222222;
  frame=0xffaa4422;
  frame_highlight=0xffff4422;
}
