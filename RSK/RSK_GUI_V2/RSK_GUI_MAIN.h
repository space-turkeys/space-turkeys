
class RSK_GUI{
 private:
  std::list<RSG_Component*> components;
  std::list<RSG_Component*>::iterator i;
  RSG_Component* active_component;
  RSG_Component* mouse_on;

 public:
  int bg_color;
  char paint_background;

  RSK_GUI();
  void init();
  void cleanup();
  void delete_components();
  void init_font();
  void close_font();
  void add(RSG_Component *x);
  void merge(std::list<RSG_Component*> x);
  void add(RSG_Component *x, int l);
  void remove(RSG_Component *x);
  void deletec(RSG_Component *x);
  virtual void paint(SDL_Surface* screen);
  static int fits(int x, int y, SDL_Rect b);
  RSG_Component* get_component(int x, int y);
  bool inputting();
  virtual void handle_event(SDL_Event e);
};
