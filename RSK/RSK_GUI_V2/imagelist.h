
struct RSG_Imagelist_element{
  SDL_Surface* image;
  char selected;
  char inactivated;
  RSG_Imagelist_element(){
    image=0;
    selected=0;
    inactivated=0;
  }
};

class RSG_Imagelist: public RSG_Component{
public:
  vector<RSG_Imagelist_element> images;
  SDL_Surface *image_sl, *image_sr, *image_dark;
  int num;
  int num_show;
  int w,h; //element dims
  int padding;
  int offset;
  int active;
  int initialized;
  int orientation; //0 for horizontal, 1 for vertical
  int mark_selected;
  int was_clicked;

  RSG_Imagelist();
  RSG_Imagelist(int nshow, int flag, vector<SDL_Surface*> s, SDL_Rect b);
  //flags: 1 => mark_selected, 2 => vertical orientation
  ~RSG_Imagelist();
  void init(int nshow, int flag, vector<SDL_Surface*> s);
  void cleanup();
  void handle_event(SDL_Event e);
  void paint(SDL_Surface* screen);
  void reset();
  int get_state();
  int get_active();
  void set_active(int i);
  int get_active_frame();
  int get_active_index();
  SDL_Surface* get_image(int idx);
  char get_selected(int i);
  char get_inactivated(int i);
  int getw();
  int geth();
};
