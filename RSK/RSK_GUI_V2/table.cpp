#include "RSK_GUI.h"

RSG_Table::RSG_Table(vector<vector<string> > content, SDL_Rect b):RSG_Component(b){
  image = s_image = NULL;
  selected_row = -1;
  init(content);
}

void RSG_Table::init(vector<vector<string> > content){
  SDL_Rect r_buf;
  SDL_Rect sr;

  cleanup();

  /* setup dimensions */
  rows = content.size();
  if (rows < 1){
    return;
  }

  cols = content[0].size();
  if (cols < 1){
    return;
  }

  element_size = create_sdl_rect(0,0,bounds.w/cols,bounds.h/rows);
  
  /* setup image bufs */
  if (!(image = SDL_CreateRGBSurface(SDL_HWSURFACE,bounds.w, bounds.h, 32, rmask, gmask, bmask, 0))){
    return;
  }

  if (!(s_image = SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA,bounds.w,element_size.h,32,rmask,gmask,bmask,amask))){
    SDL_FreeSurface(image);
    return;
  }
  
  /* draw content */
  for (int i = 0; i < rows; i++){
    for (int j = 0; j < fmin(cols,content[i].size()); j++){
      
      //find text dims
      sr = drawText(NULL, &r_buf, content[i][j].c_str(),scheme.fg,fmin(1.5 * element_size.w / strlen(content[i][j].c_str()) - 2, .8 * element_size.h - 2));

      //calc dims
      r_buf = create_sdl_rect(j * element_size.w, i * element_size.h, element_size.w, element_size.h);
      r_buf.x += r_buf.w/2 - sr.w/2;
      r_buf.y += r_buf.h/2 - sr.h/2;

      //render text
      drawText(image, &r_buf, content[i][j].c_str(),scheme.fg,fmin(1.5 * element_size.w / strlen(content[i][j].c_str()) - 2, .8 * element_size.h - 2));

      //fprintf(stdout,"table: drew element [%d][%d] with value %s at position (%d, %d) with size %d (text dims were %dx%d)\n", i, j, content[i][j].c_str(), r_buf.x, r_buf.y, (int)fmin(1.5 * element_size.w / strlen(content[i][j].c_str()) - 2, .8 * element_size.h - 2), sr.w, sr.h);

      //calc dims
      r_buf = create_sdl_rect(j * element_size.w, i * element_size.h, element_size.w, element_size.h);

      //frame
      drawFrame(image,r_buf,scheme.frame);

      if (i == 0){
	drawFrame(image,r_buf, 5, scheme.frame);
      }
    }
  }

  /* create selection image */
  SDL_FillRect(s_image,NULL,0x60ffffff);
}

void RSG_Table::cleanup(){
  if (image){
    SDL_FreeSurface(image);
    image = 0;
  }
  if (s_image){
    SDL_FreeSurface(s_image);
    s_image = 0;
  }
  rows = cols = 0;
}

void RSG_Table::paint(SDL_Surface* s){
  SDL_BlitSurface(image,NULL,s,&root_bounds);
  if (selected_row > -1){
    SDL_Rect b = create_sdl_rect(root_bounds.x, root_bounds.y + selected_row * element_size.h, element_size.w, element_size.h);
    SDL_BlitSurface(s_image,NULL,s,&b);
  }
}

void RSG_Table::handle_event(SDL_Event e){
  if (!visible){
    return;
  }

  if (e.type == SDL_MOUSEBUTTONDOWN && RSK_GUI::fits(e.button.x, e.button.y, root_bounds)){
    int y = e.button.y - root_bounds.y;
    selected_row = y / element_size.h;
  }
  RSG_Component::handle_event(e);
}

void RSG_Table::loose_focus(){
  selected_row = -1;
}

int RSG_Table::get_selected_row(){
  return selected_row;
}
