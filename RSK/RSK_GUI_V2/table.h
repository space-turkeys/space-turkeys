class RSG_Table:public RSG_Component{
  SDL_Surface* image;
  SDL_Surface* s_image;
  int rows, cols;
  int selected_row;
  SDL_Rect element_size;
 public:
  RSG_Table(vector<vector<string> > content, SDL_Rect b);
  void cleanup();
  void handle_event(SDL_Event e);
  void loose_focus();
  void init(vector<vector<string> > content);
  void paint(SDL_Surface* s);
  int get_selected_row();
};
