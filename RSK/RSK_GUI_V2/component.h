class RSG_Component;

class event_listener{
 public:
  virtual void listen(SDL_Event e, RSG_Component *o) = 0;
};

class RSG_Component{
public:
  color_scheme scheme;
  bool has_mouse_over;
  int layer;
  bool visible;
  bool initialized;
  SDL_Rect bounds;
  SDL_Rect root_bounds;
  event_listener *listener;
  TTF_Font* font;

  static TTF_Font *default_font;
  static RSG_Component *active_component;

  RSG_Component();
  RSG_Component(SDL_Rect b);
  RSG_Component(color_scheme cs, SDL_Rect b);
  RSG_Component(event_listener *l, color_scheme sc, SDL_Rect b);
  virtual void cleanup() = 0;
  virtual void delete_all();
  virtual void paint(SDL_Surface *s) = 0;
  virtual void handle_event(SDL_Event e);
  virtual int get_state();
  virtual void set_state(int s);
  virtual void gain_focus();
  virtual void loose_focus();
  virtual list<RSG_Component*> *get_children();
  virtual void update_rootb(SDL_Rect r);

  static void init_font();
  static void close_font();
};
