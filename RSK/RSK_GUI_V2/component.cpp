#include "RSK_GUI.h"


TTF_Font *RSG_Component::default_font = NULL;
RSG_Component *RSG_Component::active_component = NULL;

RSG_Component::RSG_Component(){}

RSG_Component::RSG_Component(event_listener* l, color_scheme sc, SDL_Rect b){
  listener = l;
  bounds = b;
  root_bounds = b;
  scheme = sc;
  has_mouse_over = 0;
  layer = 0;
  visible = true;
  font = NULL;
}

RSG_Component::RSG_Component(color_scheme sc, SDL_Rect b){
  listener = NULL;
  bounds = b;
  root_bounds = b;
  scheme = sc;
  has_mouse_over = 0;
  layer = 0;
  visible = true;
  font = NULL;
}

RSG_Component::RSG_Component(SDL_Rect b){
  listener = NULL;
  bounds = b;
  root_bounds = b;
  has_mouse_over = 0;
  layer = 0;
  visible = true;
  font = NULL;
}

void RSG_Component::handle_event(SDL_Event e){

  if (!visible){
    return;
  }

  if (e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP){
    if (RSK_GUI::fits(e.button.x,e.button.y,root_bounds)){
      //this component was clicked
      has_mouse_over = true;
      active_component = this;
      if (listener){
	listener -> listen(e,this);
      }
    }else if (active_component == this){
      active_component = NULL;
      has_mouse_over = false;
      loose_focus();
    }
  }else if (e.type == SDL_MOUSEMOTION){
    //mouse moved
    if (RSK_GUI::fits(e.motion.x,e.motion.y,root_bounds)){
      has_mouse_over = 1;
      if (listener){
	listener -> listen(e,this);
      }
    }else{
      has_mouse_over = 0;
    }
  }else{
    if (listener){
      listener -> listen(e,this);
    }
  }
}
int RSG_Component::get_state(){
  return -1;
}
void RSG_Component::delete_all(){
  cleanup();
}
void RSG_Component::set_state(int s){}
void RSG_Component::gain_focus(){}
void RSG_Component::loose_focus(){}
list<RSG_Component*> *RSG_Component::get_children(){
  return NULL;
}

void RSG_Component::init_font(){
  close_font();
  default_font = TTF_OpenFont("fonts/arial.ttf",12);
}

void RSG_Component::close_font(){
  if (default_font){
    TTF_CloseFont(default_font);
    default_font = 0;
  }
}

void RSG_Component::update_rootb(SDL_Rect r){
  list<RSG_Component*>::iterator i;

  root_bounds.x += r.x;
  root_bounds.y += r.y;
  
  if (get_children()){
    for (i = get_children() -> begin(); i != get_children() -> end(); i++){
      (*i) -> update_rootb(r);
    }
  }
}
