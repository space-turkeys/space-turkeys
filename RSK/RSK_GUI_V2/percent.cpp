#include "RSK_GUI.h"


RSG_Percent::RSG_Percent(SDL_Rect b) : RSG_Component(b){
  percent = 0;
}
void RSG_Percent::cleanup(){}
void RSG_Percent::paint(SDL_Surface* s){
  SDL_Rect r = create_sdl_rect(bounds.x + 2, bounds.y + 2, (percent * (bounds.w - 4)) / 100, bounds.h - 4);
  SDL_FillRect(s, &r, scheme.fg);
  drawFrame(s, bounds, scheme.frame);
}
void RSG_Percent::set_percent(int p){
  percent = fmax(0,fmin(100,p));
}
int RSG_Percent::get_percent(){
  return percent;
}
