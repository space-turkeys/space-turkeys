
class RSG_Dropdown : public RSG_Component{
private:
  vector<string> elements;
  int selected;
  int was_clicked;
  int initialized;
  SDL_Rect size_element;
  int num_show;
  int offset;
  int dropped;
public:
  RSG_Dropdown();
  RSG_Dropdown(vector<string> elements, int ns, color_scheme cs,SDL_Rect b);
  ~RSG_Dropdown();
  void init(vector<string> elements, int ns, SDL_Rect *se);
  void cleanup();
  void handle_event(SDL_Event e);
  void paint(SDL_Surface* s);
  void loose_focus();
  void set_selected_index(int i);
  void set_selected_index(string v);
  int get_state();
  int get_selected_index();
  string get_selected_name();
  void clear_state();
};
