#include "RSK_GUI.h"

int dialogue(SDL_Surface *screen, const SDL_Rect area, const char *question, std::vector<const char*> responses){
  /* presents the question, and buttons for the responsens, on the screen within the area. 
     returns the index of the respons corresponding to the pressed button, or -1 if escape is pressed.
   */

  RSK_GUI gui;
  SDL_Rect b = area;
  SDL_Event e;

  b.h = area.h / 2;  
  RSG_Label *l_question = new RSG_Label(question, b);
  gui.add(l_question);

  b.y += b.h + 1;
  int wb = area.w / responses.size();
  b.w = wb - 2;
  vector<RSG_Button *> b_resp;
  b_resp.resize(responses.size());
  int i;
  
  for (i = 0; i < b_resp.size(); i++){
    b.x = i * wb + 1;
    b_resp[i] = new RSG_Button(responses[i], b);
    gui.add(b_resp[i]);
  }

  bool done = false;
  int ret_idx = -1;

  while(!done){
    while(SDL_PollEvent(&e)){
      if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
	done = true;
      }
      gui.handle_event(e);
    }
    
    for (i = 0; i < b_resp.size(); i++){
      if (b_resp[i] -> get_state()){
	ret_idx = i;
	done = true;
      }
    }

    gui.paint(screen);
    SDL_UpdateRect(screen, 0,0,0,0);
    SDL_Delay(50);
  }

  for (i = 0; i < b_resp.size(); i++){
    delete b_resp[i];
  }

  delete l_question;

  return ret_idx;
}
