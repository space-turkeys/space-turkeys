#include "RSK_GUI.h"


RSG_Button::RSG_Button(){
  initialized=0;
}
RSG_Button::RSG_Button(string n, SDL_Rect b) : RSG_Component(b){
  initialized=0;
  init(n,NULL);
}
RSG_Button::RSG_Button(string n, color_scheme cs, TTF_Font* f, SDL_Rect b) : RSG_Component(cs,b){
  initialized=0;
  init(n,f);
}
RSG_Button::RSG_Button(event_listener *l, string n, color_scheme cs, TTF_Font* f, SDL_Rect b) : RSG_Component(l,cs,b){
  initialized=0;
  init(n,f);
}
void RSG_Button::init(string n, TTF_Font* f){
  SDL_Surface *buf, *hbuf;
  SDL_Rect r;
  Uint32 c[4];
  bool adapt;
  SDL_Rect tsize;
  bool g = false;

  /* test dims */
  adapt = (bounds.w==0 || bounds.h==0);

  /* cleanup and set attributes */
  cleanup();
  visible=1;
  name=n;
  was_pressed=0;
  if (name.length()<1){
    name=" ";
  }

  if (f == NULL){
    int guess = fmin((1.5 * bounds.w) / n.length(), 0.8 * bounds.h);
    f = TTF_OpenFont("fonts/arial.ttf", guess);
    g = true;
  }

  /* define size */
  tsize = testTextSize((const char*)name.c_str(),f);
  if (adapt){
    r = tsize;
    r.w += 6;
    r.h += 6;
  }else{
    r = bounds;
    r.x = r.y = 0;
  }
  bounds.w = r.w;
  bounds.h = r.h;

  /* create image buffer */
  if (!(image=SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA,r.w,r.h,32,rmask,gmask,bmask,amask)) || !(image_highlight=SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA,r.w,r.h,32,rmask,gmask,bmask,amask)) || !(buf=SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA,tsize.w,tsize.h,32,rmask,gmask,bmask,amask)) || !(hbuf=SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA,r.w,r.h,32,rmask,gmask,bmask,amask))){
    fprintf(stderr,"RSG_Button::init(): failed to create image buffers! Exiting...\n");
    exit(1);
  }

  /* draw button bulk */
  gradientRect(image,NULL,scheme.bg);
  gradientRect(image_highlight,NULL,scheme.bg_highlight);
  drawRoundedFrame(image,r,4,2,scheme.frame);
  drawRoundedFrame(image_highlight,r,4,2,scheme.frame_highlight);

  /* draw text */
  r.x = r.y = 0;
  drawText(buf,&r,(const char*)name.c_str(),scheme.fg,f);
  rescaleImage(buf,hbuf);
  SDL_BlitSurface(hbuf,NULL,image,NULL);
  drawText(buf,&r,(const char*)name.c_str(),scheme.fg_highlight,f);
  rescaleImage(buf,hbuf);
  SDL_BlitSurface(hbuf,NULL,image_highlight,NULL);

  /* free temp bufs */
  SDL_FreeSurface(buf);
  SDL_FreeSurface(hbuf);

  if (g){
    TTF_CloseFont(f);
  }

  initialized=1;
}

RSG_Button::~RSG_Button(){
  cleanup();
}

void RSG_Button::cleanup(){
  if (initialized){
    SDL_FreeSurface(image);
    SDL_FreeSurface(image_highlight);
    initialized=0;
  }
}
  
void RSG_Button::paint(SDL_Surface* s){
  if (initialized && visible){
    if (has_mouse_over){
      SDL_BlitSurface(image_highlight,NULL,s,&bounds);
    }else{
      SDL_BlitSurface(image,NULL,s,&bounds);
    }
  }
};

void RSG_Button::handle_event(SDL_Event e){
  if (!visible){
    return;
  }

  bool clicked = e.type == SDL_MOUSEBUTTONDOWN && RSK_GUI::fits(e.button.x, e.button.y, root_bounds);
  bool return_pressed = e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN && active_component == this;
  was_pressed |= clicked | return_pressed;
  RSG_Component::handle_event(e);
}

void RSG_Button::set_state(int i){
  was_pressed=i;
}

int RSG_Button::get_state(){
  return was_pressed;
}

string RSG_Button::get_text(){
  return name;
}

void RSG_Button::reset(){
  was_pressed=0;
}
