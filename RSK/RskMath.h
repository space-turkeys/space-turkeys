#include <cmath>
#include "RskTypes.h"

#ifndef RSK_MATH
#define RSK_MATH 1
#define PI 3.14159265358979

#ifndef INFINITY
#include <limits>
#define INFINITY std::numeric_limits<float>::infinity()
#endif

#ifndef fmax
#define fmax(a,b) ((a) > (b) ? (a) : (b))
#endif

#ifndef fmin
#define fmin(a,b) ((a) < (b) ? (a) : (b))
#endif

float angle(fd a);
fd normv(float a);
doubled dnormv(float a);
float sign(float x);
float sproject(fd a, fd r);
float shortestDistance(fd a, fd b, fd p);
char intervalCheck(fd a, fd i0, fd i1);
char intervalCheck(intd a, intd i0, intd i1);
fd intersect(fd* a, fd* b);
float normAngle(float a);
float angleDistance(float a, float b);
char rect_contains(SDL_Rect *r, intd p);

struct sin_table{
  int num;
  float *pts;
  static float PI2;
  static float PIO2;

  sin_table(int n);
  sin_table();
  ~sin_table();
  void init(int n);
  float get_sin(float x);
  float get_cos(float x);
};

struct atan_table{
  int num;
  float x1, x2;
  float *pts;
  float interval;

  atan_table(int n, float xmin, float xmax);
  atan_table();
  void init(int n, float xmin, float xmax);
  ~atan_table();
  float get_atan(float x);
};

namespace RskGraphics{
  extern atan_table global_atan_table;
  extern sin_table global_sin_table;
};

#endif
