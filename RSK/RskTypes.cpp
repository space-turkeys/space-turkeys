#include "RskGraphics.h"

using namespace RskGraphics;

intd::intd(){
  x=0;
  y=0;
}

intd::intd(int a,int b){
  x=a;
  y=b;
}

intd operator*(int d, intd a){
  a.x*=d;
  a.y*=d;
  return a;
}

intd operator*(intd a, int d){
  a.x*=d;
  a.y*=d;
  return a;
}

intd operator*(float d, intd a){
  a.x*=d;
  a.y*=d;
  return a;
}

intd operator*(intd a, float d){
  a.x*=d;
  a.y*=d;
  return a;
}

int operator*(intd a, intd d){
  return a.x*d.x+a.y*d.y;
}

intd operator+(intd a, intd d){
  intd p;
  p.x=a.x+d.x;
  p.y=a.y+d.y;
  return p;
}
  
intd operator-(intd a, intd d){
  intd p;
  p.x=a.x-d.x;
  p.y=a.y-d.y;
  return p;
}

char operator<(intd a, intd b){
  return (a.x<b.x && a.y<b.y);
}

char operator<=(intd a, intd b){
  return (a.x<=b.x && a.y<=b.y);
}

void intd::operator+=(intd d){
  x+=d.x;
  y+=d.y;
}

void intd::operator%=(int d){
  while(x>=d){x-=d;}
  while(x<0){x+=d;}
  while(y>=d){y-=d;}
  while(y<0){y+=d;}
}
    
float intd::length(){
  return sqrt((float)x*x+y*y);
}

intd intd::rotate(float cosinus, float sinus){
  return intd(cosinus*x+sinus*y,-sinus*x+cosinus*y);
}

intd intd::trim(intd x1, intd x2){
  intd r(x,y);
  if (x<x1.x){r.x=x1.x;}
  if (x>x2.x){r.x=x2.x;}
  if (y<x1.y){r.y=x1.y;}
  if (y>x2.y){r.y=x2.y;}
  return r;
}

float intd::getAngle(){
  if (x > 0){
    return global_atan_table.get_atan((float)y/x);
  }else if (x<0){
    return PI+global_atan_table.get_atan((float)y/x);
  }else if (y>0){
    return PI/2;
  }else {
    return -PI/2;
  }
}

intd intd::rotate(float a){
  return intd(global_sin_table.get_cos(a)*x+global_sin_table.get_sin(a)*y,-global_sin_table.get_sin(a)*x+global_sin_table.get_cos(a)*y);
}

fd::fd(){
  x=0;
  y=0;
}

fd::fd(int a, int b){
  x=a;
  y=b;
}

fd::fd(float a, float b){
  x=a;
  y=b;
}

fd::fd(double a, double b){
  x=a;
  y=b;
}

fd::fd(intd a){
  x=a.x;
  y=a.y;
}

fd operator*(float d, fd a){
  a.x*=d;
  a.y*=d;
  return a;
}
  
fd operator*(fd a, float d){
  a.x*=d;
  a.y*=d;
  return a;
}
  
float operator*(fd a, fd d){
  return a.x*d.x+a.y*d.y;
}

fd operator+(fd a, fd d){
  return fd(a.x+d.x,a.y+d.y);
}
  
fd operator-(fd a, fd d){
  return fd(a.x-d.x,a.y-d.y);
}
  
char operator<(fd a, fd b){
  return ((int)a.x<(int)b.x && (int)a.y<(int)b.y);
}

char operator==(fd a, fd b){
  return (a.x==b.x && a.y==b.y);
}

void fd::operator+=(fd d){
  x+=d.x;
  y+=d.y;
}

void fd::operator%=(float d){
  while(x>=d){x-=d;}
  while(x<0){x+=d;}
  while(y>=d){y-=d;}
  while(y<0){y+=d;}
}
    
float fd::length(){
  return sqrt(x*x+y*y);
}
    
float fd::length2(){
  return x*x+y*y;
}

int fd::MHLength(){
  return (int)fabs(x)+(int)fabs(y);
}

fd fd::rotate(float cosinus, float sinus){
  return fd(cosinus*x+sinus*y,-sinus*x+cosinus*y);
}

fd fd::trim(float r){
  float a=x,b=y;
  if (a<-r){a=-r;}
  if (a>=r){a=r;}
  if (b<-r){b=-r;}
  if (b>=r){b=r;}
  return fd(a,b);
}

fd fd::trim(fd r){
  return trim(fd(0,0),r);
}

fd fd::trim(fd r, fd q){
  float a=x,b=y;
  if (a<r.x){a=r.x;}
  if (a>=q.x){a=q.x;}
  if (b<r.y){b=r.y;}
  if (b>=q.y){b=q.y;}
  return fd(a,b);
}

fd fd::cirkTrim(float r){
  if (length()>r){
    return r*fd::normv(getAngle());
  }else{
    return fd(x,y);
  }
}

intd fd::toIntd(){
  return intd((int)x,(int)y);
}

fd fd::normalize(){
  return normv(getAngle());
}

fd fd::normv(float a){
  return normv((float)global_sin_table.get_cos(a), (float)global_sin_table.get_sin(a));
}

fd fd::normv(float cosine, float sine){
  return fd(cosine,sine);
}


float fd::getAngle(){
  if (x > 0){
    return global_atan_table.get_atan(y/x);
  }else if (x<0){
    return PI+global_atan_table.get_atan(y/x);
  }else if (y>0){
    return PI/2;
  }else {
    return -PI/2;
  }
}

fd fd::rotate(float a){
  return fd((float)(global_sin_table.get_cos(a)*x+global_sin_table.get_sin(a)*y),(float)(-global_sin_table.get_sin(a)*x+global_sin_table.get_cos(a)*y));
}

doubled::doubled(fd a){
  x=a.x;
  y=a.y;
}

doubled::doubled(intd a){
  x=(double)a.x;
  y=(double)a.y;
}

doubled::doubled(float a, float b){
  x=a;
  y=b;
}

doubled::doubled(double a, double b){
  x=a;
  y=b;
}

doubled::doubled(){
}

fd doubled::toFd(){
  return fd((float)x,(float)y);
}

doubled doubled::mult(double d, intd a){
  doubled r;
  r.x=a.x*d;
  r.y=a.y*d;
  return r;
}

doubled operator*(double d, doubled a){
  a.x*=d;
  a.y*=d;
  return a;
}

doubled operator*(doubled a, double d){
  a.x*=d;
  a.y*=d;
  return a;
}

double operator*(doubled a, doubled d){
  return a.x*d.x+a.y*d.y;
}

doubled operator+(doubled a, doubled d){
  doubled p;
  p.x=a.x+d.x;
  p.y=a.y+d.y;
  return p;
}

doubled operator-(doubled a, doubled d){
  doubled p;
  p.x=a.x-d.x;
  p.y=a.y-d.y;
  return p;
}

doubled operator+(fd a, doubled d){
  doubled p;
  p.x=a.x+d.x;
  p.y=a.y+d.y;
  return p;
}

doubled operator+(doubled a, fd d){
  doubled p;
  p.x=a.x+d.x;
  p.y=a.y+d.y;
  return p;
}

doubled operator-(fd a, doubled d){
  doubled p;
  p.x=a.x-d.x;
  p.y=a.y-d.y;
  return p;
}

doubled operator-(doubled a, fd d){
  doubled p;
  p.x=a.x-d.x;
  p.y=a.y-d.y;
  return p;
}

char operator<(doubled a, doubled b){
  return ((int)a.x<(int)b.x && (int)a.y<(int)b.y);
}

void doubled::operator+=(doubled d){
  char o[1024];
  x+=d.x;
  y+=d.y;
}

void doubled::operator%=(double d){
  while(x>=d){x-=d;}
  while(x<0){x+=d;}
  while(y>=d){y-=d;}
  while(y<0){y+=d;}
}
    
double doubled::length(){
  return sqrt(x*x+y*y);
}

doubled doubled::rotate(double cosinus, double sinus){
  doubled r(cosinus*x+sinus*y,-sinus*x+cosinus*y);
  return r;
}

double doubled::getAngle(){
  if (x > 0){
    return global_atan_table.get_atan(y/x);
  }else if (x<0){
    return PI+global_atan_table.get_atan(y/x);
  }else if (y>0){
    return PI/2;
  }else {
    return -PI/2;
  }
}
doubled doubled::rotate(double a){
  return doubled(global_sin_table.get_cos(a)*x+global_sin_table.get_sin(a)*y,-global_sin_table.get_sin(a)*x+global_sin_table.get_cos(a)*y);
}
doubled doubled::normv(double a){
  return doubled(global_sin_table.get_cos(a),global_sin_table.get_sin(a));
}

