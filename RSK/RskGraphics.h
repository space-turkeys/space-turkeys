#ifndef RSK_GRAPHICS_DEF
#define RSK_GRAPHICS_DEF 

#include <cmath>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include <RSK/RskTypes.h>
#include <RSK/RskElementary.h>
#include <RSK/RskMath.h>

#define rmask 0x00ff0000
#define gmask 0x0000ff00
#define bmask 0x000000ff
#define amask 0xff000000

namespace RskGraphics{
  extern TTF_Font* Rsk_global_font;
};

float colorLength(Uint32 c);
SDL_Rect create_sdl_rect(int x, int y, int w, int h);
SDL_Color create_sdl_color(unsigned char r, unsigned char g, unsigned char b);
SDL_Surface* initSDL(int width, int height);
SDL_Surface* initSDL(int width, int height, int flag);
void drawArrowHead(SDL_Surface* s, fd x, float a, int l, Uint32 c);
void drawGlowingCircle(SDL_Surface* s, fd x, float r, int thick, Uint32 col, Uint32 gcol);
void drawCircle(SDL_Surface* s, fd x, float r, Uint32 col);
void drawCircle(SDL_Surface* s, fd x, float r, int thick, Uint32 col);
void drawFilledCircle(SDL_Surface* s, fd x, float r, Uint32 col);
void drawFilledCircleShaded(SDL_Surface* s, fd x, float r, Uint32 col);
void drawCircleSegment(SDL_Surface* s, fd x, float r, int thick, fd interval, Uint32 col);
void drawCircleSegment(SDL_Surface* s, fd x, float r, fd interval, Uint32 col);
void drawGlowingLineSafe(SDL_Surface* s, fd x, fd y, int thickness, Uint32 col, Uint32 gcol);
void drawGlowingLine(SDL_Surface* s, fd x, fd y, int thickness, Uint32 col, Uint32 gcol);
void drawLine(SDL_Surface* s, fd x1, fd x2, Uint32 col);
void drawLineSafe(SDL_Surface* s, fd x1, fd x2, int thickness, Uint32 col);
void drawLine(SDL_Surface* s, fd x1, fd x2, int thickness, Uint32 col);
void drawLine(SDL_Surface* s, int x1, int y1, int x2, int y2, Uint32 col);
void drawLineAlpha(SDL_Surface* s, int x1, int y1, int x2, int y2, Uint32 col);
void pset(SDL_Surface* s, int x, int y, Uint32 c);
void pset(SDL_Surface* s, fd x, Uint32 c);
void psetAlpha(SDL_Surface* s, int x, int y, Uint32 c);
Uint32 pget(SDL_Surface* s, int x, int y);
void drawFrameSafe(SDL_Surface* s, SDL_Rect r, Uint32 c);
void drawFrame(SDL_Surface* s, SDL_Rect r, Uint32 c);
void drawFrame(SDL_Surface* s, SDL_Rect r, int t, Uint32 c);
void drawRoundedFrame(SDL_Surface* s, SDL_Rect r, int a, Uint32 c);
void drawRoundedFrame(SDL_Surface* s, SDL_Rect r, int a, int t, Uint32 c);
void transparentRect(SDL_Surface* s, SDL_Rect r, int alpha, Uint32 c);
void gradientRect(SDL_Surface* s, SDL_Rect *r, Uint32 c[4]);
Uint32 fade(Uint32 c, float a);
SDL_Surface* rotateImage(SDL_Surface* image, float a);
void rescaleImage(SDL_Surface* s, SDL_Surface* d);
SDL_Rect drawText(SDL_Surface* s, SDL_Rect* r, const char* v, Uint32 c, Uint32 size);
SDL_Rect drawText(SDL_Surface* s, SDL_Rect* r, const char* v, Uint32 c, TTF_Font* font);
SDL_Color make_sdl_color(int c);
void code_to_transparent(int c, SDL_Surface* s);
SDL_Rect testTextSize(const char *v, TTF_Font *f);

#endif
