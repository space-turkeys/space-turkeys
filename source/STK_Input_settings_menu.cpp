#include <cstdio>

#include "STK_Input_settings_menu.h"
#include "STK_Client.h"

STK_Input_settings_menu::STK_Input_settings_menu(STK_Menu_references *r):STK_Menu(r){
  SDL_Rect b;
  color_scheme cs;
  int h = m_ref -> h / 11;
  int w = m_ref -> w / 2;
  int e_w = w - 20;
  int e_h = h - 20;

  RSG_Label *label_name;
  RSG_Label *label_num_planets;
  RSG_Label *label_rel_emptiness;
  RSG_Label *label_prod;
  RSG_Label *label_fleet_bonus;
  RSG_Label *label_planet_defense;
  RSG_Label *label_ship_speed;
  RSG_Label *label_signal_speed;
  RSG_Label *label_round_length;

  if (!(font = TTF_OpenFont("fonts/arial.ttf", (int)(.6 * h)))){
    fprintf(stdout, "input_settings(): no font!\n");
    exit(1);
  }

  b = create_sdl_rect(10,10,e_w,e_h);
  label_name = new RSG_Label("Name of Universe:",b);

  b.x += w;
  input_name = new RSG_Textfield(32,"U Alpha",cs,font,b);

  b = create_sdl_rect(10,h + 10,e_w,e_h);
  label_prod = new RSG_Label("Productivity:",b);
  
  b = create_sdl_rect(w + 10,h + 10, e_w, e_h);
  slider_prod = new RSG_Slidebar(50, cs, b);

  b = create_sdl_rect(10,2*h+10,e_w,e_h);
  label_num_planets = new RSG_Label("nr of planets:",b);

  b.x += w;
  input_num_planets = new RSG_Textfield(32,"20",cs,font,b);

  b = create_sdl_rect(10,3*h+10,e_w,e_h);
  label_rel_emptiness = new RSG_Label("percent emptiness:",b);

  b.x += w;
  input_rel_emptiness = new RSG_Textfield(32,"80",cs,font,b);

  b = create_sdl_rect(10,4*h+10,e_w,e_h);
  label_fleet_bonus = new RSG_Label("initial fleet bonus:",b);
  
  b.x += w;
  slider_fleet_bonus = new RSG_Slidebar(50, cs, b);

  b = create_sdl_rect(10,5*h+10,e_w,e_h);
  label_planet_defense = new RSG_Label("planet defense bonus:",b);
  
  b.x += w;
  slider_planet_defense = new RSG_Slidebar(50, cs, b);

  b = create_sdl_rect(10,6*h+10,e_w,e_h);
  label_ship_speed = new RSG_Label("ship speed:",b);
  
  b.x += w;
  slider_ship_speed = new RSG_Slidebar(50, cs, b);

  b = create_sdl_rect(10,7*h+10,2*w/3 - 20,e_h);
  label_signal_speed = new RSG_Label("signal speed:",b);
  
  b.x += 2*w/3;
  slider_signal_speed = new RSG_Slidebar(50, cs, b);

  b.x += 2*w/3;
  button_signal_speed = new RSG_Button(this, "disable", cs, NULL, b);

  b = create_sdl_rect(10,8*h+10,e_w,e_h);
  label_round_length = new RSG_Label("round length:",b);
  
  b.x += w;
  slider_round_length = new RSG_Slidebar(50, cs, b);

  b = create_sdl_rect(10,9*h+10,e_w,2*e_h);
  button_back = new RSG_Button(this, " << Back ", cs, NULL, b);

  b.x += b.w;
  button_done = new RSG_Button(this, " CREATE >> ", cs, NULL, b);

  RSG_Component::active_component = input_name;

  add(label_prod);
  add(label_name);
  add(input_name);
  add(label_num_planets);
  add(input_num_planets);
  add(label_rel_emptiness);
  add(input_rel_emptiness);
  add(label_fleet_bonus);
  add(slider_fleet_bonus);
  add(label_planet_defense);
  add(slider_planet_defense);
  add(button_back);
  add(button_done);
  add(slider_prod);
  add(label_ship_speed);
  add(slider_ship_speed);
  add(label_signal_speed);
  add(slider_signal_speed);
  add(button_signal_speed);
  add(label_round_length);
  add(slider_round_length);

};

STK_Input_settings_menu::~STK_Input_settings_menu(){
  delete_components();
  TTF_CloseFont(font);
}

void STK_Input_settings_menu::evoke(){
  int com, s;
  sbmode.client = m_ref -> client;
  sbmode.command = &com;
  sbmode.state = &s;
  sbmode.start();

  fprintf(stdout, "ISM::evoke: started sbmode with %x and %x.\n", (long int)sbmode.command, (long int)sbmode.state);
  STK_Menu::evoke();
}

void STK_Input_settings_menu::listen(SDL_Event e, RSG_Component *c){
  if (done){
    fprintf(stdout, "input_settings::listen: called while done.\n");
    return;
  }

  if (e.type == SDL_MOUSEBUTTONUP){
    if (c == button_done){
      run();
      button_done -> reset();
      done = true;
    }else if (c == button_back){
      button_back -> reset();
      done = true;
    }else if (c == button_signal_speed){
      slider_signal_speed -> visible = !slider_signal_speed -> visible;
      button_signal_speed -> init(slider_signal_speed -> visible ? "disable" : "enable", NULL);
    }
  }
}

void STK_Input_settings_menu::handle_event(SDL_Event e){
  if (e.type == SDL_KEYDOWN){
    if (e.key.keysym.sym == SDLK_RETURN){
      button_done -> reset();
      run();
      done = true;
    }
  }
  RSK_GUI::handle_event(e);
}

void STK_Input_settings_menu::run(){
  STK_Universe_settings s;
  int num_planets, p_empt;
  SDL_Event e;

  fprintf(stdout, "ISM::run: sbmode now has: %x, %x\n", (long int)sbmode.command, (long int)sbmode.state);

  sbmode.terminate();

  //setup some good defaults for universe settings.
  if (!str2int(&num_planets, input_num_planets -> get_text())){
    fprintf(stdout,"Input_settings_menu::evoke(): parse str2int failed!\n");
    num_planets = 20;
  }

  if (!str2int(&p_empt, input_rel_emptiness -> get_text())){
    fprintf(stdout,"Input_settings_menu::evoke(): parse str2int empt failed!\n");
    p_empt = 80;
  }

  if (p_empt > 95){
    p_empt = 95;
  }
  
  if (p_empt < 50){
    p_empt = 50;
  }

  s.planet_productivity = 0.001 + 0.0004 * slider_prod -> get_percent();

  s.planet_base_fleet = 200 * slider_fleet_bonus -> get_percent();

  s.planet_defense = (slider_planet_defense -> get_percent() * DIRICHLET_BASE) / (float)100;

  s.ship_settings.ship_speed = 0.2 + 0.01 * slider_ship_speed -> get_percent();

  s.speed_of_light = slider_signal_speed -> visible * (1 + .1 * slider_signal_speed -> get_percent());
  s.round_length = 50 + 4 * slider_round_length -> get_percent();

  // area = pi * 40 * 40 * np
  s.num_planets = fmin(num_planets, 200);
  s.width = sqrt((100 / (float)(100 - p_empt)) * PI * 40 * 40 * s.num_planets);
  s.height = s.width;
  if (s.width > 2000){
    s.ratio = 500 / (float)s.width;
  }
  strcpy(s.name, input_name -> get_text().c_str());

  fprintf(stdout,"Input_settings_menu::evoke(): user selected %d planets for universe %s, chose dimensions: %d x %d.\n", s.num_planets, s.name, s.width, s.height);

  //create game
  m_ref -> game_id = m_ref -> client -> create_game(s);
  if (m_ref -> game_id == -1){
    fprintf(stdout,"input_s_m::run(): create game failed.\n");
    return;
  }
  m_ref -> player_is_root = true;
  m_ref -> wait_for_game_menu -> evoke();
  while(SDL_PollEvent(&e)){}
}
