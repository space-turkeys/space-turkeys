#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/mersenne_twister.hpp>

#include "STK_Server_threads.h"
#include "STK_Server_structs.h"
#include "STK_Game_loader.h"
#include "STK_Namespace.h"

STK_Game_thread::STK_Game_thread(STK_Game_thread_data *g){
  gtd = g;

  serv = 0;
  gl = 0;
  mxp = 0;
  running = 0;
  rand_seed = 0;
}

STK_Game_thread::~STK_Game_thread(){
  if (running){
    cleanup();
  }
}

int STK_Game_thread::count_running(){
  int n = 0;
  int i;

  for (i = 0; i < gtd -> ctd.size(); i++){
    n += !(gtd -> ctd[i] -> tc_out == STKS_GAME_COMPLETE || gtd -> ctd[i] -> tc_out == STKS_DEAD || gtd -> ctd[i] -> tc_out == STKS_LEAVE_GAME);

    if (!(gtd -> ctd[i] -> tc_out == STKS_GAME_COMPLETE || gtd -> ctd[i] -> tc_out == STKS_TERMINATE || gtd -> ctd[i] -> tc_out == STKS_LEAVE_GAME)){
      fprintf(stdout, "count_running: client %d still running, has tc_out = %d.\n", gtd -> ctd[i] -> id, gtd -> ctd[i] -> tc_out);
    }
  }

  return n;
}

void STK_Game_thread::cleanup(){
  /* marks client data and game data for removal. The server will close the client connections, and delete the client and game data objects from the database. */
  int i;
  STK_Inet_protocol p;
  boost::mutex::scoped_lock l(STK::client_data_lock);
  boost::mutex::scoped_lock l2(STK::game_data_lock);

  if (gtd -> ctd.size() == 1){
    gtd -> ctd[0] -> inform_condition = STKIP_VICTORY;
  }

  for (i = 0; i < gtd -> ctd.size(); i++){
    gtd -> ctd[i] -> tc_in = STKS_TERMINATE;
  }

  while(i = count_running()){
    fprintf(stdout, "game_handle::cleanup: waiting for %d clients.\n", i);
    SDL_Delay(250);
  }

  for (i = 0; i < gtd -> ctd.size(); i++){
    gtd -> ctd[i] -> game_id = -1;
    gtd -> ctd[i] -> state = STKS_LFG;
    gtd -> ctd[i] -> tc_out = STKS_RETURN_TO_MAINFRAME;
  }

  if (gl){
    delete [] gl;
  }
  if (mxp){
    delete [] mxp;
  }
  if (gtd){
    gtd -> tc_out = STKS_TERMINATE;
  }

  specbuf_thread.terminate();
  delete specbuf_mutex;

  //return clients from ctd_spectators_buf to mainframe?
  //(there shouldn't be any)

}


void STK_Game_thread::operator()(){
  /* 
     This routine runs in a seperate thread, and handles interactions with the clients assigned to a specific game. First, it is validated that all clients are in the game. Then the steps of the game round are repeated until only one client is alive/remaining. If a client wants to leave the game, this is reported to the main server thread, which then takes over handling that client.

     Destructor handles deallocation and tells server thread of shut down.
  */

  int i,j;
  int loaded;
  int n;
  bool k, first_round = true;
  STK_Inet_protocol p;
  int counter;
  int countout = 10;
  STK_IGC_thread gt;
  vector<STK_Choice> c_list;
  bool game_over = false;
  
  running = true;

  if (!(gtd && gtd -> tc_in == STKS_RUN)){
    fprintf(stdout,"game handle thread: can not run w/o game thread data.\n");
    return;
  }
  gtd->tc_out = STKS_INITIALIZING;

  /* start client game threads*/
  gl = new STK_Game_loader[gtd -> ctd.size()];
  mxp = new boost::mutex[gtd -> ctd.size()];
  for (i = 0; i < gtd -> ctd.size(); i++){
    gtd -> ctd[i] -> tc_out = STKS_INITIALIZING;
    gtd -> ctd[i] -> tc_in = STKS_RUN;
    gtd -> ctd[i] -> gl = gl + i;
    gtd -> ctd[i] -> gl -> rand_seed = &rand_seed;
    gt.td = gtd -> ctd[i];
    gt.gl = gl + i;
    gt.gl -> mutex = mxp + i;
    boost::thread t(gt);
  }

  gtd->tc_out = STKS_RUN;

  // start spectator buf handler
  specbuf_mutex = new boost::mutex();
  specbuf_thread.setup(&gtd -> ctd_spectators_buf, specbuf_mutex);
  boost::thread specbuft(specbuf_thread);

  k = true;
  n = 0;
  while(k && n++ < 100){
    k = false;
    for (i = 0; i < gtd -> ctd.size() && !k; i++){
      k = gtd -> ctd[i] -> tc_out == STKS_INITIALIZING;
    }
    SDL_Delay(50);
  }

  if (k){
    fprintf(stdout, "game thread: client threads failed to initialise!\n");
    return;
  }

  /*::::::::::::::::::::::::::::::::::::::::*/
  /*:::::::::::::::MAIN LOOP::::::::::::::::*/
  /*::::::::::::::::::::::::::::::::::::::::*/

  while (gtd->tc_in == STKS_RUN){
    fprintf(stdout,"gh: starting round.\n");

    if (first_round){
      first_round = false;

      fprintf(stdout, "game_thread: pre gamestart inf conds: \n");
      for (i = 0; i < gtd -> ctd.size(); i++){
	gtd -> ctd[i] -> inform_condition = STKIP_NONE;
	fprintf(stdout, "%d: %d\n", gtd -> ctd[i] -> id, gtd -> ctd[i] -> inform_condition);
      }
    }else{
      /* Dismiss clients who do not have planets */
      n = 0;
      for (i = 0; i < gtd -> ctd.size(); i++){
	gtd -> ctd[i] -> inform_condition = STKIP_NONE;
	if (!gtd -> ctd[0] -> gl -> universe.has_planets(gtd -> ctd[i] -> id)){
	  gtd -> ctd[i] -> inform_condition = STKIP_LOSS;
	  fprintf(stdout,"game_thread::()(): concluded loss condition for client %d.\n", gtd -> ctd[i] -> id);
	  n++;
	}
	fprintf(stdout, "game_thread: set inform cond for %d to %d\n", gtd -> ctd[i] -> id, gtd -> ctd[i] -> inform_condition);
      }

      if (n && n == gtd -> ctd.size() - 1){
	// only one player left;
	// inform other players of loss
	// inform spectators of game over
	// terminate game
	game_over = true;

	for (i = 0; i < gtd -> ctd.size(); i++){
	  if (gtd -> ctd[i] -> inform_condition == STKIP_NONE){
	    gtd -> ctd[i] -> inform_condition = STKIP_VICTORY;
	  }
	  fprintf(stdout,"game_thread::()(): recount: concluded %s condition for client %d.\n", gtd -> ctd[i] -> inform_condition == STKIP_VICTORY ? "victory" : "loss", gtd -> ctd[i] -> id);
	}

	for (i = 0; i < gtd -> ctd_spectators.size(); i++){
	  gtd -> ctd_spectators[i] -> inform_condition = STKIP_LEAVE_GAME;
	}
      }

      /* -------------- add new spectators -------------- */
      for (i = 0; i < gtd -> ctd_spectators_buf.size(); i++){
	boost::mutex::scoped_lock l(*specbuf_mutex);
	if (gtd -> ctd_spectators_buf[i] -> state == STKS_SPECTATING){
	  fprintf(stdout, "game_thread: attempting to add new spectator...\n");
	  // send game data to client
	  STK_Inet_protocol p = gtd -> ctd_spectators_buf[i] -> get();
	  
	  if (p == STKIP_REQUIRE_GAME_DATA){
	    fprintf(stdout, " -> spectator requires data ...\n");
	    gtd -> ctd_spectators_buf[i] -> send(STKIP_CONFIRM);
	  
	    gtd -> ctd[0] -> gl -> universe.send_data(gtd -> ctd_spectators_buf[i] -> socket, STK_SERVER_TIMEOUT);

	    gtd -> ctd_spectators.push_back(gtd -> ctd_spectators_buf[i]);
	    gtd -> ctd_spectators.back() -> tc_in = STKS_RUN;
	    gtd -> ctd_spectators.back() -> gl = new STK_Game_loader();
	    gtd -> ctd_spectators.back() -> gl -> rand_seed = &rand_seed;
	    if (game_over){
	      gtd -> ctd_spectators.back() -> inform_condition = STKIP_LEAVE_GAME;
	    }
	    gt.td = gtd -> ctd_spectators.back();
	    gt.gl = gtd -> ctd_spectators.back() -> gl;
	    gt.gl -> mutex = new boost::mutex();
	    boost::thread t(gt);
	    gtd -> ctd_spectators_buf.erase(gtd -> ctd_spectators_buf.begin() + i--);
	    fprintf(stdout, " -> new spectator thread started with gl %x\n", (unsigned long int) gt.gl);
	  }else{
	    gtd -> ctd_spectators_buf[i] -> send(STKIP_DENY);
	    fprintf(stdout, " -> spectator failed to require data!\n");
	  }
	}
      }

    }


    /* ---------- validate client is active ----------- */
    if (!(gamestart() && flush_clients() > 1)){
      fprintf(stdout,"game handle: clients failed to validate game started!\n");
      return;
    }
        
    /* ---------- collect all choices ----------------- */
    fprintf(stdout,"gh: collecting choices from %d clients.\n",gtd -> ctd.size());
    if (!(load_choices() && flush_clients() > 1)){
      fprintf(stdout,"game handle: client_id failed to validate send chioces!\n");
      return;
    }

    // set choice list pointer for both players and spectators!
    c_list.resize(gtd -> ctd.size());
    for (i = 0; i < gtd -> ctd.size(); i++){
      validate_choice(&(gtd -> ctd[i] -> gl -> choice), gtd -> ctd[i] -> id);
      c_list[i] = gtd -> ctd[i] -> gl -> choice;
      fprintf(stdout,"server_game_thread: collected choice with %d cc and %d pc.\n",c_list[i].fleet_commands.size(), c_list[i].planet_commands.size());
      gtd -> ctd[i] -> gl -> c_list = &c_list;
    }

    for (i = 0; i < gtd -> ctd_spectators.size(); i++){
      gtd -> ctd_spectators[i] -> gl -> c_list = &c_list;
    }

    /* set troop values for choices */
    list<STK_Upgrade_choice>::iterator ui;
    for (i = 0; i < c_list.size(); i++){
      for (ui = c_list[i].upgrades.begin(); ui != c_list[i].upgrades.end(); ui++){
	if (ui -> choice == PREF_TROOPS){
	  ui -> troops = rand()%REINFORCEMENT;
	}else{
	  ui -> troops = -1;
	}
      }
    }

    /* ---------- validate them against upgrade events in uv[0] */

    /* ---------- send them to clients ---------------- */
    fprintf(stdout,"gh: sending choices to %d clients.\n",gtd -> ctd.size());
    if (!(send_choices() && flush_clients() > 1)){
      fprintf(stdout,"game handle thread: %d clients, failed to require choices!\n",gtd -> ctd.size());
      return;
    }

    /* ---------- wait for client iterations ---------- */
    //do nothing...

    /* ---------- load all universes -------------- */
    fprintf(stdout,"gh: loading universes from %d clients.\n",gtd -> ctd.size());
    if (!(load_universes() && flush_clients() > 1)){
      fprintf(stdout,"game handle thread: clients failed to validate universe!\n");
      return;
    }

    /* ---------- validate all universes -------------- */
    /* Validator is player 0, who is the game creator. */
    
    fprintf(stdout,"gh: validating universes of %d clients.\n",gtd -> ctd.size());
    if (!(validate_universes() && flush_clients() > 1)){
      fprintf(stdout,"game handle thread: clients failed to validate universe!\n");
      return;
    }
  }

  fprintf(stdout,"gh: left main loop with %d clients remaining.\n",gtd -> ctd.size());
  return;
}

void STK_Game_thread::validate_choice(STK_Choice *c, int id){
  list<STK_Command>::iterator i;
  STK_Universe *uni = gtd -> ctd[0] -> gl -> valid_universe;
  int j;

  fprintf(stdout, "game_thread::validate_choice: choice for player %d.\n", id);

  if (c -> player != id){
    fprintf(stdout, "game_thread::validate_choice: invalid player! got %d, expected %d\n", c -> player, id);
    c -> player = id;
  }
  bool checked = false;

  for (i = c -> planet_commands.begin(); i != c -> planet_commands.end(); i++){
    int checkid = i -> source.associated_id;
    if (uni){
      checked = false;
      vector<STK_Planet> *pl = &(uni -> get_planet_master() -> planets);
      for (j = 0; j < pl -> size() && !checked; j++){
	if ((*pl)[j].id == checkid){
	  checked = true;
	  if ((*pl)[j].owner != id){
	    fprintf(stdout, "game_thread::validate_choice: WARNING: player %d attempting to control planet %d owned by %d! Removing...\n", id, checkid, (*pl)[j].owner);
	    c -> planet_commands.erase(i--);
	  }
	}
      }
      if (!checked){
	fprintf(stdout, "game_thread::validate_choice: WARNING: player %d attempting to control non-existent planet %d! Removing...\n", id, checkid);
	c -> planet_commands.erase(i--);
      }
    }else{
      checked = false;
      for (j = 0; j < gtd -> ctd.size(); j++){
	if (gtd -> ctd[j] -> id == id){
	  if (checkid != gtd -> home_planets[j]){
	    fprintf(stdout, "game_thread::validate_choice: WARNING: player %d attempting to control planet %d owned by other! Removing...\n", id, checkid);
	    c -> planet_commands.erase(i--);
	  }
	  checked = true;
	}
      }
      if (!checked){
	fprintf(stdout, "game_thread::validate_choice: WARNING: player %d attempting to control non-listed planet %d! Removing...\n", id, checkid);
	c -> planet_commands.erase(i--);
      }
    }
  }

  for (i = c -> fleet_commands.begin(); i != c -> fleet_commands.end(); i++){
    int checkid = i -> source.associated_id;
    if (uni){
      bool checked = false;
      list<STK_Fleet> *sl = uni -> get_ship_master() -> get_fleets();
      list<STK_Fleet>::iterator si;
      for (si = sl -> begin(); si != sl -> end() && !checked; si++){
	if (si -> id == checkid){
	  checked = true;
	  if (si -> owner != id){
	    fprintf(stdout, "game_thread::validate_choice: WARNING: player %d attempting to control fleet %d owned by %d! Removing...\n", id, si -> id, si -> owner);
	    c -> fleet_commands.erase(i--);
	  }
	}
      }
      if (!checked){
	fprintf(stdout, "game_thread::validate_choice: WARNING: player %d attempting to control non-existent fleet %d! Removing...\n", id, checkid);
	c -> planet_commands.erase(i--);
      }
    }
  }
}

bool STK_Game_thread::dismiss_client(int id){
  int i;
  for (i = 0; i < gtd -> ctd.size(); i++){
    if (gtd -> ctd[i] -> id == id){
      fprintf(stdout,"game_thread: returning client %d to main server.\n", gtd -> ctd[i] -> id);
      gtd -> ctd[i] -> game_id = -1;
      gtd -> ctd[i] -> state = STKS_LFG;
      gtd -> ctd[i] -> tc_out = STKS_RETURN_TO_MAINFRAME;
      gtd -> ctd.erase(gtd -> ctd.begin() + i);
      return true;
    }
  }

  for (i = 0; i < gtd -> ctd_spectators.size(); i++){
    if (gtd -> ctd_spectators[i] -> id == id){
      fprintf(stdout,"game_thread: returning client %d to main server.\n", gtd -> ctd_spectators[i] -> id);
      delete gtd -> ctd_spectators[i] -> gl;
      gtd -> ctd_spectators[i] -> game_id = -1;
      gtd -> ctd_spectators[i] -> state = STKS_LFG;
      gtd -> ctd_spectators[i] -> tc_out = STKS_RETURN_TO_MAINFRAME;
      gtd -> ctd_spectators.erase(gtd -> ctd_spectators.begin() + i);
      return true;
    }
  }
  return false;
}

int STK_Game_thread::count_loaded(vector<STK_Client_thread_data*> *ctd, int rsize){
  int c = 0, i;
  int ndead = 0;

  for (i = 0; i < ctd -> size() && gl; i++){
    if ((*ctd)[i]){
      if ((*ctd)[i] -> tc_out == STKS_LEAVE_GAME){
	if (dismiss_client((*ctd)[i] -> id)){
	  i--;
	}else{
	  fprintf(stdout, "game_thread::count_loaded: error: failed to dismiss client %d/%d!\n", i, ctd -> size());
	  exit(1);
	}
      }else {
	c += (*ctd)[i] -> gl -> loaded;
	ndead += (*ctd)[i] -> tc_out == STKS_DEAD;
      }
    }
  }

  fprintf(stdout,"game_thread::count_loaded(): %d loaded / %d total.\n", ctd -> size() - ndead >= rsize ? c : -1, ctd -> size());
  return ctd -> size() - ndead >= rsize ? c : -1;
}

void STK_Game_thread::setup_command(vector<STK_Client_thread_data*> ctd, STK_Inet_protocol p){
  int i;

  for (i = 0; i < ctd.size() && gl; i++){
    boost::mutex::scoped_lock l(*(ctd[i] -> gl -> mutex));
    fprintf(stdout, "game_thread::command(): setting client %d for command %d.\n", ctd[i] -> id, (int)p);
    ctd[i] -> gl -> loaded = false;
    ctd[i] -> gl -> command = p;
    ctd[i] -> gl -> give_up = false;
  }

}

int STK_Game_thread::load_command(vector<STK_Client_thread_data*> *ctd, STK_Inet_protocol p, int cmax, int rsize){
  int i, n = 0, c = 0;

  fprintf(stdout,"game_thread::command(): starting main loop.\n");
  while ((c = count_loaded(ctd, rsize)) < ctd -> size() && c > -1 && n++ < cmax){
    SDL_Delay(250);
  }
  
  fprintf(stdout,"game_thread::command(): ended loop with %d loaded / %d total.\n", c, ctd -> size());

  if (c < ctd -> size()){
    if (c > -1){
      fprintf(stdout,"game_handle: command timed out.\n");
    }else{
      fprintf(stdout,"game_handle: command: all clients gone.\n");
    }
    for (i = 0; i < ctd -> size() && gl; i++){
      (*ctd)[i] -> gl -> give_up = !(*ctd)[i] -> gl -> loaded;
      (*ctd)[i] -> gl -> command = STKIP_STANDBY;
    }
  }

  return c;
}

bool STK_Game_thread::gamestart(){
  int i;
  boost::random::mt19937 rng;
  boost::random::uniform_int_distribution<unsigned int> dist(1, RAND_MAX);

  rand_seed = dist(rng);

  setup_command(gtd -> ctd, STKIP_REQUIRE_GAMESTART);
  setup_command(gtd -> ctd_spectators, STKIP_REQUIRE_GAMESTART);

  int num = load_command(&gtd -> ctd, STKIP_REQUIRE_GAMESTART, 10000, 2);
  int num_spec = load_command(&gtd -> ctd_spectators, STKIP_REQUIRE_GAMESTART, 10000, 1);

  fprintf(stdout,"%d clients passed gamestart.\n", num);
  return num > 1;
}

bool STK_Game_thread::load_choices(){
  fprintf(stdout, "game_thread::load_choices(): start\n");
  setup_command(gtd -> ctd, STKIP_REQUIRE_CHOICES);
  int num = load_command(&gtd -> ctd, STKIP_REQUIRE_CHOICES, 10000, 2);
  fprintf(stdout,"%d clients passed load_choices.\n", num);
  return num > 1;
}

bool STK_Game_thread::send_choices(){
  fprintf(stdout, "game_thread::send_choices: there exist %d players and %d spectators.\n", gtd -> ctd.size(), gtd -> ctd_spectators.size());

  setup_command(gtd -> ctd, STKIP_SEND_CHOICES);
  setup_command(gtd -> ctd_spectators, STKIP_SEND_CHOICES);

  int num = load_command(&gtd -> ctd, STKIP_SEND_CHOICES, 10000, 2);
  int num_spec = load_command(&gtd -> ctd_spectators, STKIP_SEND_CHOICES, 10000, 1);
  fprintf(stdout,"%d clients passed send_choices.\n", num);
  return num > 1;
}

bool STK_Game_thread::load_universes(){
  setup_command(gtd -> ctd, STKIP_REQUIRE_UNIVERSE);
  setup_command(gtd -> ctd_spectators, STKIP_REQUIRE_UNIVERSE);

  int num = load_command(&gtd -> ctd, STKIP_REQUIRE_UNIVERSE, 10000, 2);
  int num_spec = load_command(&gtd -> ctd_spectators, STKIP_REQUIRE_UNIVERSE, 10000, 1);
  fprintf(stdout,"%d clients passed load_universe.\n", num);
  return num > 1;
}

bool STK_Game_thread::validate_universes(){
  int i, n;

  if (!gl){return false;}

  /* calcluate which universes are valid */
  gtd -> ctd[0] -> gl -> universe_valid = true;
  gtd -> ctd[0] -> gl -> valid_universe = &(gtd -> ctd[0] -> gl -> universe);
  
  fprintf(stdout,"game_thread::validate_universe():\n");
  for (i = 1; i < gtd -> ctd.size(); i++){
    boost::mutex::scoped_lock l(*(gtd -> ctd[i] -> gl -> mutex));
    gtd -> ctd[i] -> gl -> universe_valid = !gtd -> ctd[0] -> gl -> universe.differs(gtd -> ctd[i] -> gl -> universe);
    gtd -> ctd[i] -> gl -> valid_universe = &(gtd -> ctd[0] -> gl -> universe);
    fprintf(stdout,"client %d: %s.\n", gtd -> ctd[i] -> id, gtd -> ctd[i] -> gl -> universe_valid ? "valid" : "NOT valid");
  }

  for (i = 0; i < gtd -> ctd_spectators.size(); i++){
    boost::mutex::scoped_lock l(*(gtd -> ctd_spectators[i] -> gl -> mutex));
    gtd -> ctd_spectators[i] -> gl -> universe_valid = !gtd -> ctd[0] -> gl -> universe.differs(gtd -> ctd_spectators[i] -> gl -> universe);
    gtd -> ctd_spectators[i] -> gl -> valid_universe = &(gtd -> ctd[0] -> gl -> universe);
    fprintf(stdout,"client %d: %s.\n", gtd -> ctd_spectators[i] -> id, gtd -> ctd_spectators[i] -> gl -> universe_valid ? "valid" : "NOT valid");
  }

  setup_command(gtd -> ctd, STKIP_VALIDATE_UNIVERSE);
  setup_command(gtd -> ctd_spectators, STKIP_VALIDATE_UNIVERSE);

  int num = load_command(&gtd -> ctd, STKIP_VALIDATE_UNIVERSE, 10000, 2);
  int num_spec = load_command(&gtd -> ctd_spectators, STKIP_VALIDATE_UNIVERSE, 10000, 1);

  fprintf(stdout,"%d clients passed validate_universe.\n", num);
  return num > 1;
}


int STK_Game_thread::flush_clients(){
  int i;
  /* for each client with bad connection, tell server it terminates and remove it. */

  for (i = 0; i < gtd -> ctd.size(); i++){
    if (gtd -> ctd[i] -> connection_state != STKS_READY){
      fprintf(stdout,"game_thread:flush_clients: erazing client %d.\n", gtd -> ctd[i] -> id);
      CLOSE_SOCKET(gtd -> ctd[i] -> socket);
      gtd -> ctd[i] -> tc_in = STKS_TERMINATE;
      gtd -> ctd.erase(gtd -> ctd.begin() + i--);
    }
  }

  for (i = 0; i < gtd -> ctd_spectators.size(); i++){
    if (gtd -> ctd_spectators[i] -> connection_state != STKS_READY){
      fprintf(stdout,"game_thread:flush_clients: erazing spectator client %d.\n", gtd -> ctd_spectators[i] -> id);
      CLOSE_SOCKET(gtd -> ctd_spectators[i] -> socket);
      gtd -> ctd_spectators[i] -> tc_in = STKS_TERMINATE;
      gtd -> ctd_spectators.erase(gtd -> ctd_spectators.begin() + i--);
    }
  }

  return gtd -> ctd.size();
}
