#ifndef _STK_PLANET_MAP
#define _STK_PLANET_MAP

#include "STK_Class_headers.h"

class STK_Planet_map{
 public:
  STK_Universe_settings *uv_settings;
  STK_Planet** map;

  STK_Planet_map();
  bool initialize(STK_Universe_settings *uvs, STK_Planet_master *p);
  bool differs(STK_Planet_map m);
  void cleanup();
};
#endif
