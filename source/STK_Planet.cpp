#include <cstdio>
#include <vector>

#include "STK_Planet.h"
#include "STK_Ship.h"
#include "STK_Player.h"
#include "STK_Namespace.h"
#include "STK_Statistics.h"
#include "STK_Universe.h"
#include "STK_Basic_communication.h"

using namespace std;

float *STK_Planet::multiplier = 0;
int *STK_Planet::def_bonus = 0;
vector<int> *STK_Planet::upgrade_events = 0;
float STK_Planet::research_complete = 0;


bool STK_Planet::load_data(char *buf, int *c, int max){
  STK_Command com;
  int i, n;

  if (*c + f_size + 4 > max){
    fprintf(stdout,"planet::load_data: buffer overflow!\n");
    return false;
  }

  read_from_buf(buf + *c, (char*)&position, f_size);
  *c += f_size;

  read_from_buf(buf + *c, (char*)&n, 4);
  *c += 4;


  if (*c + n * STK_Command::fsize > max){
    fprintf(stdout,"planet::load_data: buffer overflow!\n");
    return false;
  }

  command_queue.clear();
  for (i = 0; i < n; i++){
    // read_from_buf(buf + *c, (char*)&com, sizeof(STK_Command));
    // *c += sizeof(STK_Command);
    com.load_data(buf, c, max);
    command_queue.push_back(com);
  }
  is_selected = false;
  shutdown = 0;
  inactive = 0;

  return true;
}

bool STK_Planet::send_data(char *buf, int *c, int max){
  list<STK_Command>::iterator i;
  int n;

  if (*c + calculate_size() > max){
    fprintf(stdout,"planet::send_data: buffer overflow!\n");
    return false;
  }

  write_to_buf((char*)&position, buf + *c, f_size);
  *c += f_size;

  n = command_queue.size();
  write_to_buf((char*)&n, buf + *c, 4);
  *c += 4;

  for (i = command_queue.begin(); i != command_queue.end(); i++){
    // write_to_buf((char*)&(*i), buf + *c, sizeof(STK_Command));
    // *c += sizeof(STK_Command);
    i -> send_data(buf, c, max);
  }
  return true;
}

int STK_Planet::calculate_size(){
  return f_size + command_queue.size() * STK_Command::fsize + 4;
}

void STK_Planet::give_command(STK_Command c){
  fprintf(stdout,"planet %d: command added to queue!\n", id);
  command_queue.push_back(c);
}

float STK_Planet::get_production(){
  float factor = p_owner == NULL ? 1 : (1 + p_owner -> upgrade.industry / (float)10);
  return factor * productivity * (owner > -1 ? *multiplier : 1);
}

void STK_Planet::iterate(STK_Universe *u){
  list<STK_Command>::iterator i;
  float factor = p_owner == NULL ? 1 : (1 + p_owner -> upgrade.industry / (float)10);

  if (inactive > 0){
    inactive--;
  }else if (owner > -1){
    research += num_turkeys;
  }

  if (research > research_complete){
    research = 0;
    u -> add_points(owner, 1);
    fprintf(stdout,"planet::iterate(): **UPGRADE EVENT**: %d.\n", owner);
  }

  if (shutdown > 0){
    shutdown -= factor * productivity * (owner > -1 ? *multiplier : 1);
  }else if (resource > 0){
    num_turkeys += factor * productivity * (owner > -1 ? *multiplier : 1);
    resource -= factor * productivity * (owner > -1 ? *multiplier : 1);
  }

  for (i = command_queue.begin(); i != command_queue.end(); i++){
    i -> signal_delay--;
  }

}

void STK_Planet::clear_retrieved_commands(){
  list<STK_Command>::iterator i;
  for (i = command_queue.begin(); i != command_queue.end(); i++){
    if (i -> signal_delay <= 0){
      command_queue.erase(i--);
    }
  } 
}

list<STK_Command> STK_Planet::retrieve_commands(){
  list<STK_Command>::iterator i;
  list<STK_Command> list;
  
  for (i = command_queue.begin(); i != command_queue.end(); i++){
    if (i -> signal_delay <= 0){
      list.push_back(*i);
    }
  }

  if (list.size()){
    fprintf(stdout, "STK_Planet::retrieve_commands(): %d commands retrieved, %d left.\n", list.size(), command_queue.size());
  }

  return list;
}
