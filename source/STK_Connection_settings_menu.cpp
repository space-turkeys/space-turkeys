#include <cstdio>
#include <cmath>

#include "STK_Connection_settings_menu.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Client.h"
#include "STK_Graphics.h"

STK_Connection_settings_menu::STK_Connection_settings_menu(STK_Menu_references *r):STK_Menu(r){
  SDL_Rect b;
  color_scheme cs;
  int w = STK::ns_var -> screen -> w;
  int h = STK::ns_var -> screen -> h;
  
  b = create_sdl_rect(10, 10, w/2 - 20, h/3 - 20);
  label_name = new RSG_Label("Your name:",b);

  b.x = w/2 + 10;
  input_name = new RSG_Textfield(32,"client",cs,NULL,b);

  b.x = 10;
  b.y = h/3 + 10;
  label_address = new RSG_Label("Server address:",b);

  b.x = w/2 + 10;
  input_address = new RSG_Textfield(32,"pqz.se",cs,NULL,b);

  b.x = 10;
  b.y = 2 * h/3 + 10;
  button_back = new RSG_Button(this, "Quit", cs, NULL, b);

  b.x = w/2 + 10;
  button_done = new RSG_Button(this, "Connect", cs, NULL, b);

  RSG_Component::active_component = input_name;

  add(label_name);
  add(input_name);
  add(label_address);
  add(input_address);
  add(button_back);
  add(button_done);
};

STK_Connection_settings_menu::~STK_Connection_settings_menu(){
  delete_components();
}

void STK_Connection_settings_menu::set_address(const char* v){
  input_address -> set_text(v);
}

void STK_Connection_settings_menu::connect(){
  fprintf(stdout, "consetmen::connect\n");
  m_ref -> client = new STK_Client();
  strcpy(m_ref -> client -> name, input_name -> get_text().c_str());
  if (m_ref -> client -> stkc_connect(input_address -> get_text()) < 0){
    input_address -> set_text("");
    if (m_ref -> client -> server_message == STKIP_INVALID_VERSION){
      STK_Graphics::draw_message(STK::ns_var -> screen, "Incompatible version! Please uptade to latest space turkeys.");
    }else{
      STK_Graphics::draw_message(STK::ns_var -> screen, "Failed to connect!");
    }
    SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);
    SDL_Delay(2000);
  }else{
    m_ref -> select_game_menu -> evoke();
  }

  if(m_ref -> client){
    m_ref -> client -> stkc_close();
    delete m_ref -> client;
    m_ref -> client = NULL;
  }

  fprintf(stdout, "consetmen::connect: end\n");
}

void STK_Connection_settings_menu::listen(SDL_Event e, RSG_Component *c){
  if (e.type == SDL_MOUSEBUTTONDOWN){
    if (c == button_back){
      input_address -> set_text("");
      done = true;
    }else if (c == button_done){
      connect();
      button_done -> reset();
    }
  }else if (e.type == SDL_KEYDOWN){
    if (e.key.keysym.sym == SDLK_RETURN && c == input_address){
      connect();
      button_done -> reset();
    }else if (e.key.keysym.sym == SDLK_ESCAPE && c == button_back){
      input_address -> set_text("");
      done = true;
    }
  }else if (input_name -> get_state()){
    input_name -> reset();
    RSG_Component::active_component = input_address;
  }else if (input_address -> get_state()){
    input_address -> reset();
    connect();
  }
}

void STK_Connection_settings_menu::paint(SDL_Surface *s){
  STK_Graphics::fancy_spirals(s);
  RSK_GUI::paint(s);
}
