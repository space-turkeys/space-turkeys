#ifndef _STK_COLLISION_MAP
#define _STK_COLLISION_MAP

#include "RSK/RskTypes.h"
#include "STK_Class_headers.h"
#include "STK_Ship_map.h"
#include "STK_Planet_map.h"
#include "STK_Planet_field.h"

class STK_Collision_map{
 public:
  STK_Universe_settings* uv_settings;
  bool initialized;
  int ref_width;
  int ref_height;
  STK_Ship_map ship_map;
  STK_Planet_map planet_map;
  STK_Planet_field planet_field;

  STK_Collision_map();
  bool initialize(STK_Universe_settings *uvs, STK_Planet_master *p);
  void rebuild_references(STK_Planet_master *p);
  void settings_ref(STK_Universe_settings *uvs);
  void draw_planets(STK_Planet_master *p);
  void add_ship(fd position, STK_Ship* ship);
  void remove_ship(fd position, STK_Ship* ship);
  e_pfield get_field(fd position);
  fd closest_free_square(fd position, int player);
  fd neighbour_directions(STK_Ship *s);
  bool is_occupied(fd position, STK_Ship* me, STK_Planet* target);
  STK_Planet* get_planet(fd position);
  STK_Ship* get_ship(fd position);
  STK_Ship* find_opponent_ship(STK_Ship *s);
  bool validate_position(intd p);
  intd col2map(intd p);
  float col2map(float r);
  intd map2col(fd p);
  float map2col(float r);
  bool differs(STK_Collision_map m);
  void cleanup();
};
#endif
