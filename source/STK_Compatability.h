#ifndef _STK_COMPATABILITY
#define _STK_COMPATABILITY

#define VERSION "2.0.7"
#define NVERSION 207
#define COMPAT_CLIENT_CLIENT "2.0.7"
#define COMPAT_CLIENT_SERVER "2.0.7"
#define COMPAT_SERVER_CLIENT "2.0.7"

bool compatible_client_client(int c1, int c2);
bool compatible_client_server(int c, int s);

#endif
