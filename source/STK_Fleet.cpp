#include "STK_Keys.h"
#include "STK_Fleet.h"
#include "STK_Ship_master.h"
#include "STK_Ship.h"
#include "STK_Planet_master.h"
#include "STK_Planet.h"
#include "STK_Server_structs.h"

using namespace std;

int STK_Fleet::id_counter = 0;

STK_Fleet::STK_Fleet(STK_Ship_master* sm){
  ship_master = sm;
  id = id_counter++;
  is_selected = false;
  converge = false;
  update_counter = 0;
  equipment = STKFE_Fight;
}


void STK_Fleet::cleanup(){
  fleet.clear();
  commands.clear();
}

void STK_Fleet::remove(STK_Ship* s){
  fleet.remove(s);
}

void STK_Fleet::add_ship(STK_Ship *s){
  fleet.push_back(s);
}

void STK_Fleet::calculate_position(){
  list<STK_Ship*>::iterator i;
  position=fd(0,0);
  for (i = fleet.begin(); i != fleet.end(); i++){
    position+=(*i) -> get_position();
  }
  position=(1/(float)fleet.size())*position;
}

void STK_Fleet::cleanup_ships(){
  list<STK_Ship*>::iterator i;
  for (i = fleet.begin(); i != fleet.end(); i++){
    if ((*i) -> remove_me){
      fleet.erase(i--);
    }
  }
}

bool STK_Fleet::approx_angle(float *a){
  list<STK_Ship*>::iterator i;
  int count[10];
  int ntest = 20;
  int c = 0;
  int best, best_idx, j;

  memset((char*)count, 0, 10 * sizeof(int));

  for (i = fleet.begin(); i != fleet.end() && c++ < ntest; i++){
    count[(int)(10 * normAngle((*i) -> angle) / (2 * PI))]++;
  }

  best = -1;
  best_idx = -1;
  for (j = 0; j < 10; j++){
    if (count[j] > best){
      best = count[j];
      best_idx = j;
    }
  }

  *a = 2 * PI / 20 + best_idx * 2 * PI / 10;
  return best > MIN(ntest, fleet.size())/3;
}

fd STK_Fleet::get_destination_position(){
  float x,y,xo,yo,vxo,vyo,v,c;
  fd p, p2, p3;
  switch (get_order().type){
  case TARGET_PLANET:
    return get_order().planet -> position;
  case TARGET_FLEET:
    return get_order().fleet -> get_position();
  case TARGET_POSITION:
    return get_order().position;
  default: 
    fprintf(stdout,"fleet::get_destination_position(): invalid target type!\n");
    exit(1);
  }
}

fd STK_Fleet::get_direction_position(){
  fd mid_point, normalv, displacement, intersect_point;
  fd line1[2], line2[2];
  float phi, theta;
  switch (get_order().type){
  case TARGET_PLANET:
    return get_order().planet -> position;
  case TARGET_FLEET:
    displacement = get_order().fleet -> get_position() - get_position();
    phi = displacement.getAngle();
    if (!(get_order().fleet -> approx_angle(&theta))){
      return get_order().fleet -> get_position();
    }

    if (angleDistance(phi, theta) < 5 * PI / 8){
      return get_order().fleet -> get_position();
    }
    mid_point = get_position() + (float).5 * displacement;
    normalv = fd::normv(phi + PI/2);
    line1[0] = mid_point;
    line1[1] = mid_point + normalv;
    line2[0] = get_order().fleet -> get_position();
    line2[1] = line2[0] + fd::normv(theta);
    intersect_point = intersect(&line1[0], &line2[0]);
    return intersect_point;
  case TARGET_POSITION:
    return get_order().position;
  default: 
    fprintf(stdout,"fleet::get_destination_position(): invalid target type!\n");
    exit(1);
  }
}

int STK_Fleet::summon_radius(){
  //pi r^2 = n / rho <=> r = sqrt(n / (rho pi))
  return sqrt(fleet.size() / (float)(PI * FLEET_DEFAULT_DENSITY));
}

void STK_Fleet::update(){
  list<STK_Command>::iterator i;
  list<STK_Ship*>::iterator k;
  list<STK_Ship*> v;
  int j;
  int n = 0;
  list<pair<STK_Command, int> > quant;
  list<pair<STK_Command, int> >::iterator qi;
  float factor;

  /* locate ships */
  calculate_position();

  //fleet instruction variables
  if ((get_destination_position() - position).length() < FLEET_CONVERGE_RADIUS){
    converge = true;

    if (order.type == TARGET_FLEET && order.fleet -> owner == owner && fleet.size()){
      order.fleet -> assimilate(this);
      return;
    }
  }

  update_counter = (update_counter + 1) % SHIP_UPDATE_PERIOD;

  /* evaluate commands */
  for (i = commands.begin(); i != commands.end(); i++){
    // perform separately so that qtty priority comparison is performed correctly
    -- i -> signal_delay;
  }
  for (i = commands.begin(); i != commands.end(); i++){
    if (i -> signal_delay <= 0){
      fprintf(stdout, "fleet::update: id %d: evaluating command with prio %d.\n", id, i -> priority);
      if (i -> priority > 0){
	pair<STK_Command, int> p;
	p.first = *i;
	p.second = i -> quantity();
	quant.push_back(p);
      }
    }
  }

  // special case: maintain id if command applies to whole fleet.
  if (quant.size() == 1 && quant.front().first.priority == 100){
    apply_command(quant.front().first);
    quant.clear();
  }

  for (qi = quant.begin(); qi != quant.end(); qi++){
    k = fleet.begin();
    for (j = 0; j < qi -> second; j++){
      if (k == fleet.end()){
	fprintf(stdout, "fleet::update: attempted to split into more ships than available!\n");
	break;
      }
      k++;
    }

    v.clear();
    v.splice(v.begin(), fleet, fleet.begin(), k);
    
    fprintf(stdout, "fleet %d: splitting to new fleet of %d ships.\n", id, qi -> second);
    ship_master -> add_fleet(&v,qi -> first);
  }

  for (i = commands.begin(); i != commands.end(); i++){
    if(i -> signal_delay <= 0){
      commands.erase(i--);
    }
  }

}

/* setters */

void STK_Fleet::assign(list<STK_Ship*> *a){
  fleet = *a;
  calculate_position();
}

void STK_Fleet::assimilate(STK_Fleet *a){
  list<STK_Ship*>::iterator i;

  fleet.splice(fleet.begin(), *(a -> get_fleet()));

  for (i = fleet.begin(); i != fleet.end(); i++){
    (*i) -> fleet = this;
  }
}

// void STK_Fleet::assign(STK_Fleet *a){
//   position = a -> position;
//   order = a -> get_order();
//   tactics = a -> tactics;
//   owner = a -> owner;
//   fleet = a -> fleet;
//   commands = a -> commands;
// }

int STK_Fleet::calculate_size(){
  return sizeof(fd) + STK_Ship_order::fsize + 7 * 4 + commands.size() * STK_Command::fsize + 4 * fleet.size();
}

bool STK_Fleet::send_data(char* buf, int* c, int max){
  int n,m;
  list<STK_Command>::iterator i;
  list<STK_Ship*>::iterator j;

  m = commands.size();
  n = fleet.size();
  if (*c + calculate_size() > max){
    fprintf(stdout,"fleet::send: ERROR: buffer overflow!\n");
    return false;
  }
  write_to_buf((char*)&n,buf+*c,4);
  *c+=4;
  write_to_buf((char*)&m,buf+*c,4);
  *c+=4;
  write_to_buf((char*)&position,buf+*c,sizeof(fd));
  *c+=sizeof(fd);
  // write_to_buf((char*)&order,buf+*c,sizeof(STK_Ship_order));
  // *c+=sizeof(STK_Ship_order);
  order.send_data(buf, c, max);
  write_to_buf((char*)&tactics,buf+*c,4);
  *c+=4;
  write_to_buf((char*)&equipment,buf+*c,4);
  *c+=4;
  write_to_buf((char*)&owner,buf+*c,4);
  *c+=4;
  write_to_buf((char*)&id,buf+*c,4);
  *c+=4;
  write_to_buf((char*)&update_counter,buf+*c,4);
  *c+=4;

  fprintf(stdout, "fleet::send_data: update_counter = %d\n", update_counter);

  for (i = commands.begin(); i != commands.end(); i++){
    // write_to_buf((char*)&(*i),buf+*c,sizeof(STK_Command));
    // *c+=sizeof(STK_Command);
    i -> send_data(buf, c, max);
  }

  
  for (j = fleet.begin(); j != fleet.end(); j++){
    write_to_buf((char*)(&((*j)->associated_id)),buf+*c,4);
    
    *c += 4;
  }
  
  return true;
}

void STK_Fleet::rebuild_references(STK_Ship_master *sm, STK_Planet_master *pm){
  list<STK_Ship*>::iterator i;
  list<STK_Command>::iterator j;

  /* Rewrite ship master reference (nessecary?), ship backreferences to this, order's planet/fleet reference, commands' source and order references. */

  ship_master = sm;
  
  

  for (i = fleet.begin(); i != fleet.end(); i++){
    (*i) -> fleet = this;
  }

  switch (order.type){
  case TARGET_PLANET:
    order.planet = pm -> get_planet(order.associated_id);
    break;
  case TARGET_FLEET:
    order.fleet = sm -> get_fleet(order.associated_id);
    break;
  }

  for (j = commands.begin(); j != commands.end(); j++){
    j -> source.fleet = this;
    
    switch (j -> order.type){
    case TARGET_PLANET:
      j -> order.planet = pm -> get_planet(j -> order.associated_id);
      break;
    case TARGET_FLEET:
      j -> order.fleet = sm -> get_fleet(j -> order.associated_id);
      break;
    }
  }
}

void STK_Fleet::print_references(){
  list<STK_Ship*>::iterator i;

  fprintf(stdout, "fleet::print_references: fleet %d (%x), has %d ships.\n", id, (unsigned long int)this, fleet.size());
  for (i = fleet.begin(); i != fleet.end(); i++){
    if ((*i) -> fleet != this){
      fprintf(stdout, " -> ship %d: has invalid fleet: %x\n", (unsigned long int) (*i) -> fleet);
      exit(1);
    }
    (*i) -> print_references();
  }
}

bool STK_Fleet::load_data(char* buf, int* c, int max){
  /* This routine is intended for server side use, and does not build references. If data has become invalid and needs to be rebuilt, use routine rebuild_data instead. */
  int n,m;
  int i;
  STK_Command com;
  int sid;
  STK_Ship *s;

  if (*c+8>max){
    fprintf(stdout,"fleet::load: ERROR: buffer overflow!\n");
    return false;
  }

  read_from_buf(buf+*c,(char*)&n,4);
  *c+=4;
  read_from_buf(buf+*c,(char*)&m,4);
  *c+=4;
  if (m<0 || n<0){
    fprintf(stdout,"fleet::load: ERROR: bad parameters!\n");
    return false;
  }

  commands.clear();
  if (*c + sizeof(fd) + 5 * 4 + m * STK_Command::fsize + n * 4>max){
    fprintf(stdout,"fleet::load: ERROR: buffer overflow!\n");
    return false;
  }

  read_from_buf(buf+*c,(char*)&position,sizeof(fd));
  *c+=sizeof(fd);

  // read_from_buf(buf+*c,(char*)&order,sizeof(STK_Ship_order));
  // *c+=sizeof(STK_Ship_order);
  order.load_data(buf, c, max);

  read_from_buf(buf+*c,(char*)&tactics,4);
  *c+=4;
  read_from_buf(buf+*c,(char*)&equipment,4);
  *c+=4;
  read_from_buf(buf+*c,(char*)&owner,4);
  *c+=4;
  read_from_buf(buf+*c,(char*)&id,4);
  *c+=4;
  read_from_buf(buf+*c,(char*)&update_counter,4);
  *c+=4;

  fprintf(stdout, "fleet::load_data: update_counter = %d\n", update_counter);

  for (i = 0; i < m; i++){
    // read_from_buf(buf+*c,(char*)&com,sizeof(STK_Command));
    // *c+=sizeof(STK_Command);
    com.load_data(buf, c, max);
    commands.push_back(com);
  }

  fleet.clear();

  
  for (i = 0; i < n; i++){
    read_from_buf(buf+*c, (char*)&sid, 4);
    *c += 4;
    
    if (!(s = ship_master -> get_ship(sid))){
      fprintf(stdout,"fleet::load_data(): rebuild ship nr %d of id %d: doesn't exist!\n", i, sid);
      return false;
    }
    fleet.push_back(s);
  }

  is_selected = false;
  return true;
}

void STK_Fleet::give_command(STK_Command c){
  
  commands.push_back(c);
}

/* getters */

list<STK_Command> STK_Fleet::get_commands(){
  return commands;
}

int STK_Fleet::get_owner(){
  return owner;
}

fd STK_Fleet::get_position(){
  return position;
}

int STK_Fleet::get_num(){
  return fleet.size();
}

void STK_Fleet::apply_command(STK_Command c){
  order = c.order;
}

list<STK_Ship*> *STK_Fleet::get_fleet(){
  return &fleet;
}

STK_Ship_order STK_Fleet::get_order(){
  return order;
}

STK_Ship_order *STK_Fleet::get_order_p(){
  return &order;
}

void STK_Fleet::clear_fleet_order(STK_Fleet *f){
  int i;
  list<STK_Command>::iterator j;

  

  for (j = commands.begin(); j != commands.end(); j++){
    if (j -> order.type == TARGET_FLEET && j -> order.fleet == f){
      
      commands.erase(j--);
    }
  }

  if (order.type == TARGET_FLEET && order.fleet == f){
    order.type = TARGET_POSITION;
    order.position = position;
    
  } 
}

int STK_Fleet::get_tactics(){
  return tactics;
}

// bool STK_Fleet::differs(STK_Fleet m){
//   list<STK_Command>::iterator i;
//   vector<int> v;
//   vector<STK_Command> c;
//   if (fleet.size()!=m.get_fleet().size() || commands.size()!=m.get_commands().size() || !(position==m.get_position()) || target!=m.get_target() || target_type!=m.get_target_type() || tactics!=m.get_tactics() || owner!=m.get_owner() || id!=m.id){
//     return true;
//   }
//   v=m.get_fleet();
//   c=m.get_commands();
//   for (i=0; i<fleet.size(); i++){
//     if (fleet[i]!=v[i]){
//       return true;
//     }
//   }
//   for (i=0; i<commands.size(); i++){
//     if (!((*i)==c[i])){
//       return true;
//     }
//   }
//   return false;
// }
