#include <cstdio>
#include "STK_Planet_field.h"
#include "STK_Planet.h"
#include "STK_Planet_master.h"
#include "STK_Universe_settings.h"
#include "STK_Collision_map.h"

using namespace std;

STK_Planet_field::STK_Planet_field(){
  map = 0;
}

STK_Planet* STK_Planet_field::closest_planet_from_list(fd p, list<STK_Planet*> planets){
  /* Returns the planet id of the planet located closest to the position 'p' on the map. */
  list<STK_Planet*>::iterator i;
  int d;
  STK_Planet* k = NULL;
  int c;

  if (!planets.size()){
    return NULL;
  }
  d = (p-planets.front() -> position).length();
  k = planets.front();
  for (i = planets.begin(); i != planets.end(); i++){
    if ((c=(p - (*i) -> position).length())<d){
      k=*i;
      d=c;
    }
  }
  return k;
}

bool STK_Planet_field::initialize(STK_Universe_settings *uvs, STK_Planet_master *p, STK_Collision_map *cm){
  /* Allocates planet field map, of dimensions equal to the collision map dimensions which should be passed in 'w' and 'h'. The map is initialized with vectors pointing to the closest planet, of size inversely proportional to the distance to the planet. The size should equal to 1 at the surface, and 0 inside the planet. These vectors can later be used for ships to determine how to avoid colliding with the planet. Returns false on failure. */
  list<STK_Planet*> p_list = p -> get_planets();
  STK_Planet *closest_planet, *test_planet;
  intd mpos;
  int i,j;
  fd r,v;
  int d;

  if (!uvs){
    fprintf(stdout, "planet_field::initialize(): can not init w/o uv_settings!\n");
    return false;
  }

  uv_settings = uvs;

  cleanup();
  if (!(map=new e_pfield[uv_settings -> cmap_width() * uv_settings -> cmap_height()])){
    fprintf(stderr,"STK_Planet_field::initialize(): out of memory!\n");
    return false;
  }

  /* construct a vector field pointing to the closest planet, of strength inversely proportional to the distance to the planet, such that the strength equals to one at the planet surface. */

  fprintf(stdout,"planet_field::inititalize():.\n");

  for (i = 0; i < uv_settings -> cmap_width()*uv_settings -> cmap_height(); i++){
    if (!(i%uv_settings -> cmap_width())){
      fprintf(stdout," :: %d %% done.\r", (100 * i) / (uv_settings -> cmap_width()*uv_settings -> cmap_height()));
    }
    mpos = intd((i%uv_settings -> cmap_width())/uv_settings -> ratio,(i/uv_settings -> cmap_width())/uv_settings -> ratio);

    map[i].length = 0;
    map[i].p = NULL;
    if (cm -> get_planet(mpos) == NULL){
      if (true){
	closest_planet = closest_planet_from_list(mpos,p_list);
      }else{
	// //approximate closest planet from map
	// d = PFIELD_DISTANCE;
	// closest_planet = NULL;

	// for (j = 0; j < PFIELD_SAMPLES; j++){
	//   r = (rand()/(float)RAND_MAX) * d * fd::normv((rand()/(float)RAND_MAX) * 2 * PI);
	//   test_planet = cm -> get_planet(mpos + r.toIntd());
	//   if (test_planet != NULL && (r - mpos).length() < d){
	//     d = (r - mpos).length();
	//     closest_planet = test_planet;
	//     fprintf(stdout,"pfield: planet found!\n");
	//   }
	// }
      }

      //calculate values
      if (closest_planet){
	r = (fd)closest_planet -> position - (fd)mpos;
	if (r.length() - closest_planet -> radius < PFIELD_BUFZONE){
	  map[i].length = 1;
	}else{
	  map[i].length = pow((float)(closest_planet -> radius + PFIELD_BUFZONE)/r.length(), (float).5);
	}
	map[i].angle = r.getAngle();
	map[i].p = closest_planet;
      }
    }
  }
  return true;
}

void STK_Planet_field::rebuild_references(STK_Planet_master *p){
  int i;
  intd mpos;
  STK_Planet *closest_planet;
  list<STK_Planet*> p_list = p -> get_planets();

  for (i = 0; i < uv_settings -> cmap_width()*uv_settings -> cmap_height(); i++){
    mpos = intd((i%uv_settings -> cmap_width())/uv_settings -> ratio,(i/uv_settings -> cmap_width())/uv_settings -> ratio);
    closest_planet = closest_planet_from_list(mpos,p_list);
    if (closest_planet){
      map[i].p = closest_planet;
    }else{
      map[i].p = NULL;
    }
  }
}

bool STK_Planet_field::differs(STK_Planet_field m){
  return false;
}

void STK_Planet_field::cleanup(){
  if (map){
    delete [] map;
  }
  map=0;
}
