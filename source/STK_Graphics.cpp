#include <iostream>
#include <fstream>
#include <cstdio>

#include "STK_Graphics.h"
#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Planet.h"
#include "STK_Player.h"
#include "STK_Fleet.h"
#include "STK_Collision_map.h"
#include "STK_Ship.h"

using namespace std;

namespace STK_Graphics{
  map<planet_graphics_info,SDL_Surface*> planet_data;
  SDL_Surface *ship_glow;
  STK_Universe_settings *uvs;
};

char STKFE_Symbols[3] = {'T', 'F', 'A'};
list<string> STKFE_Names;

const char* STK_Graphics::troop_approx_name(int n){
  if (n < 5){
    return "FEW";
  }else if (n < 10){
    return "SEVERAL";
  }else if (n < 20){
    return "PACK";
  }else if (n < 50){
    return "LOTS";
  }else if (n < 100){
    return "HORDE";
  }else if (n < 250){
    return "THRONG";
  }else if (n < 500){
    return "SWARM";
  }else if (n < 1000){
    return "ZOUNDS";
  }else{
    return "LEGION";
  }
}

STK_Graphics::planet_graphics_info::planet_graphics_info(int c, int r, bool s){
  color = c;
  radius = r;
  selected = s;
}

bool STK_Graphics::operator <(planet_graphics_info a, planet_graphics_info b){
  return a.radius < b.radius || (a.radius == b.radius && a.color < b.color) || (a.radius == b.radius && a.color == b.color && b.selected && !a.selected);
}

void STK_Graphics::set_uvs(STK_Universe_settings *uv){
  uvs = uv;
}

void STK_Graphics::init(){
  float r = SHIP_SIZE;

  if (!(ship_glow = SDL_CreateRGBSurface(SDL_SRCALPHA|SDL_HWSURFACE, 2 * r, 2 * r, 32, rmask, gmask, bmask, amask))){
    fprintf(stdout, "graphics::init(): allocation failed!\n");
    exit(1);
  }

  SDL_FillRect(ship_glow, NULL, 0);
  drawFilledCircle(ship_glow, fd(r, r), r, 0xaaffddcc); 

  STKFE_Names.clear();
  STKFE_Names.push_back("Travel");
  STKFE_Names.push_back("Fight");
  STKFE_Names.push_back("Artillery");

}

void STK_Graphics::quit(){
  if (ship_glow){
    SDL_FreeSurface(ship_glow);
    ship_glow = NULL;
  }

  clear_planet_data();
}

void STK_Graphics::draw_message(SDL_Surface* s, const char* text){
  SDL_Rect rect;
  float ratio = strlen(text) / 2;
  int f_w = s -> w - 8;
  int f_h = s -> h - 8;
  int size = fmin((1.5 * f_w) / strlen(text), 0.8 * f_h);
  int t_w = (2 * (size * strlen(text))) / 3;
  int t_h = size;
  int w = t_w + 12;
  int h = t_h + 12;
  int iw = t_w + 8;
  int ih = t_h + 8;

  rect.x = (s -> w - w) / 2;
  rect.y = (s -> h - h) / 2;
  rect.w = w;
  rect.h = h;

  SDL_FillRect(s, &rect, 0xffffff);

  rect.x = (s -> w - iw) / 2;
  rect.y = (s -> h - ih) / 2;
  rect.w = iw;
  rect.h = ih;
  SDL_FillRect(s, &rect, 0);

  rect.x = (s -> w - t_w) / 2;
  rect.y = (s -> h - t_h) / 2;
  rect.w = t_w;
  rect.h = t_h;
  drawText(s, &rect, text, 0xff9966, size);
}

void STK_Graphics::draw_ship(SDL_Surface* screen, intd pos, float angle, int color, bool is_selected){
  intd p1,p2,p3,p;
  SDL_Rect srect=create_sdl_rect(0,0,screen->w,screen->h), r;

  if (!rect_contains(&srect,pos)){return;}

  p1=pos;
  p2=pos+(2*SHIP_SIZE*fd::normv(angle+PI+.4)).toIntd();
  p3=pos+(2*SHIP_SIZE*fd::normv(angle+PI-.4)).toIntd();

  if (!rect_contains(&srect,p2)){return;}
  if (!rect_contains(&srect,p3)){return;}

  drawLine(screen,p1,p2,color);
  drawLine(screen,p2,p3,color);
  drawLine(screen,p3,p1,color);

  if (is_selected){
    p = 1/(float)3 * (p1 + p2 + p3);
    r = create_sdl_rect(p.x - ship_glow -> w / 2, p.y - ship_glow -> h / 2, ship_glow -> w, ship_glow -> h);
    SDL_BlitSurface(ship_glow, NULL, screen, &r);
  }
}

void STK_Graphics::draw_home_planet(SDL_Surface* screen, STK_Planet *p, int nleaving, int color){
  // float radius = STK::ns_var -> zoom * p -> radius;
  // intd pos = STK::ns_var -> map2screen(p -> position);
  // SDL_Rect check = create_sdl_rect(-radius, -radius, screen -> w + 2 * radius, screen -> h + 2 * radius);

  // if (!rect_contains(&check, pos)){
  //   return;
  // }
  
  // drawCircle(screen, pos, radius + 10, 2, color);
  draw_planet(screen, p, nleaving, color, true);
}

void STK_Graphics::clear_planet_data(){
  map<planet_graphics_info,SDL_Surface*>::iterator i;

  for (i = planet_data.begin(); i != planet_data.end(); i++){
    if (i -> second != NULL){
      SDL_FreeSurface(i -> second);
    }
  }

  planet_data.clear();
}

SDL_Surface *STK_Graphics::generate_planet_image(planet_graphics_info g){
  int w = 2 * (g.radius + 7);
  SDL_Surface *x = SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA, w, w, 32, rmask, gmask, bmask, amask);

  if (x == NULL){
    return NULL;
  }

  drawFilledCircleShaded(x,intd(w/2,w/2),g.radius,g.color|0xff000000);
  if (g.selected){
    drawGlowingCircle(x, intd(w/2,w/2), g.radius, 5, g.color|0xff000000, 0xffffff);
  }else{
    //drawCircle(x, intd(w/2,w/2), g.radius + 2, 3, g.color|0xff000000);
  }

  return x;
}

void STK_Graphics::draw_planet(SDL_Surface* screen, STK_Planet *p, int nleaving, int color, bool owned){
  intd pos = STK::ns_var -> map2screen(p -> position); 
  int num_turkeys = p -> num_turkeys;
  bool selected = p -> is_selected;
  float radius = STK::ns_var -> zoom * p -> radius;
  SDL_Rect check = create_sdl_rect(-radius, -radius, screen -> w + 2 * radius, screen -> h + 2 * radius);

  if (!rect_contains(&check, pos)){
    return;
  }

  planet_graphics_info g(color, radius, selected);
  map<planet_graphics_info, SDL_Surface*>::iterator i;
  SDL_Rect r;
  SDL_Surface *buf;

  i = planet_data.find(g);
  if (i == planet_data.end()){
    buf = generate_planet_image(g);
    if (buf == NULL){
      fprintf(stdout, "graphics::draw_planet: generate image failed.\n");
      return;
    }
    planet_data[g] = buf;
    i = planet_data.find(g);
    if (i == planet_data.end()){
      fprintf(stdout, "graphics::draw_planet: map image failed.\n");
      return;
    }
  }

  r = create_sdl_rect(pos.x - i -> second -> w / 2, pos.y - i -> second -> h / 2, i -> second -> w, i -> second -> h);
  SDL_BlitSurface(i -> second, NULL, screen, &r);

}

void STK_Graphics::draw_planet_data(SDL_Surface* screen, STK_Planet *p, int nleaving, int color, bool owned){
  intd pos = STK::ns_var -> map2screen(p -> position); 
  int num_turkeys = p -> num_turkeys;
  bool selected = p -> is_selected;
  float radius = STK::ns_var -> zoom * p -> radius;
  SDL_Rect check = create_sdl_rect(-radius, -radius, screen -> w + 2 * radius, screen -> h + 2 * radius);
  char s[128];

  if (!rect_contains(&check, pos)){
    return;
  }

  if (owned){
    sprintf(s,"%d",num_turkeys);
  }else{
    sprintf(s,"%s", troop_approx_name(num_turkeys));
  }

  switch (STK::ns_var -> quant_paint_mode % PAINT_MODE_NUM){
  case PAINT_MODE_ALL:
    troop_indicator(screen, intd(pos.x, pos.y - radius), s);
    break;
  case PAINT_MODE_SELECTED:
    if (selected){
      troop_indicator(screen, intd(pos.x, pos.y - radius), s);
    }
    break;
  }
  
  if (nleaving){
    drawCircleSegment(screen, pos, radius + 3, 3, fd((float)0, (float)(2 * PI * nleaving / num_turkeys)), 0xffccaa);
  }
  
  if (STK::ns_var -> stats_paint_mode % PAINT_MODE_NUM == PAINT_MODE_ALL || (STK::ns_var -> stats_paint_mode % PAINT_MODE_NUM == PAINT_MODE_SELECTED && selected)){
    int i;
    sprintf(s, "position: (%dx%d)", (int)p -> position.x, (int)p -> position.y);
    troop_indicator(screen, intd(pos.x + radius, pos.y + 20), s);
    int slen = strlen(s);
    if (owned){
      sprintf(s, "resource: %d", (int)p -> resource);
    }else{
      sprintf(s, "resource: %s", troop_approx_name((int)p -> resource));
    }
    int slen2 = strlen(s);
    for (i = 0; i < slen - slen2; i++){
      s[slen2 + i] = ' ';
    }
    s[slen2 + i] = '\0';
    troop_indicator(screen, intd(pos.x + radius, pos.y - 20), s);
    if (owned){
      sprintf(s, "research: %d%%", (int)((100 * p -> research) / STK_Planet::research_complete));
    }else{
      sprintf(s, "research: ??");
    }
    slen2 = strlen(s);
    for ( i = 0; i < slen - slen2; i++){
      s[slen2 + i] = ' ';
    }
    s[slen2 + i] = '\0';
    troop_indicator(screen, intd(pos.x + radius, pos.y), s);
    
    if (owned){
      
      float flight_radius = uvs -> round_length * (uvs -> ship_settings.ship_speed + (p -> p_owner -> temp_upgrade.navigation + p -> p_owner -> upgrade.navigation) / (float)2);

      drawCircle(screen, pos, flight_radius, 0xff);
    }
  }

}

void STK_Graphics::draw_fleet_data(SDL_Surface *s, list<STK_Fleet>::iterator j, bool owned){
  list<STK_Command>::iterator k;
  list<STK_Command> com_list;
  char v[128];

  if (owned){
    com_list = j -> get_commands();
    for (k = com_list.begin(); k != com_list.end(); k++){
      draw_command(s, *k);
    }

    switch (STK::ns_var -> command_paint_mode % PAINT_MODE_NUM){
    case PAINT_MODE_ALL:
      draw_arrow(s, j -> get_position(), j -> get_destination_position());
      break;
    case PAINT_MODE_SELECTED:
      if (j -> is_selected){
	draw_arrow(s, j -> get_position(), j -> get_destination_position());
      }
      break;
    }

    if (STK::ns_var -> stats_paint_mode % PAINT_MODE_NUM == PAINT_MODE_ALL || (STK::ns_var -> stats_paint_mode % PAINT_MODE_NUM == PAINT_MODE_SELECTED && j -> is_selected)){
      float flight_radius = uvs -> round_length * j -> get_fleet() -> front() -> attributes.speed;
      drawCircle(s, STK::ns_var -> map2screen(j -> get_position()), flight_radius, 0xff);
    }

    sprintf(v, "%c:%d", STKFE_Symbols[j -> equipment], j -> get_num());
  }else{
    sprintf(v, "%s", troop_approx_name(j -> get_num()));
  }

  switch (STK::ns_var -> quant_paint_mode % PAINT_MODE_NUM){
  case PAINT_MODE_ALL:
    troop_indicator(s, STK::ns_var -> map2screen(j -> get_position()), v);
    break;
  case PAINT_MODE_SELECTED:
    if (j -> is_selected){
      troop_indicator(s, STK::ns_var -> map2screen(j -> get_position()), v);
    }
    break;
  }
}

void STK_Graphics::troop_indicator(SDL_Surface *screen, const intd pos, const char* s){
  SDL_Rect r = create_sdl_rect(pos.x, pos.y, 10 * strlen(s) + 4, 14 + 4);

  transparentRect(screen, r, 100, 0);
  drawText(screen, &r, s, 0xffaa99, 14);
  r.x -= 2;
  r.y -= 2;
  drawRoundedFrame(screen, r, 4, 2, 0x998877);

}

void STK_Graphics::draw_arrow(SDL_Surface *screen, fd x, fd y){
  fd from = STK::ns_var -> map2screen(x);
  fd to = STK::ns_var -> map2screen(y);
  float a_size = 10;
  float angle = (to - from).getAngle();
  fd v = fd::normv(angle);
  drawLineSafe(screen, from, to - (a_size * v), 2, 0xff);
  drawArrowHead(screen, to, angle, a_size, 0x4455ff);
}

void STK_Graphics::draw_command(SDL_Surface* screen, STK_Command c){
  fd from;
  fd to; 
  fd mid;
  int color;

  c.get_positions(&from, &to);
  if (c.source.type == TARGET_PLANET){
    float r = c.source.planet -> radius;
    from = from + r * fd::normv((to - from).getAngle());
  }
  mid = (float).5 * (from + to);

  intd x = STK::ns_var -> map2screen(from);
  intd y = STK::ns_var -> map2screen(to);
  intd z = STK::ns_var -> map2screen(mid);
  float angle = (y - x).getAngle();
  fd v = fd::normv(angle);
  char s[128];
  char s2[128];

  int a_size = 20 + 10 * c.is_selected;
  int thick = 3 + c.priority / 10;

  color = c.source.type == TARGET_PLANET ? c.source.planet -> p_owner -> color : 0xaa8844;

  if (c.is_locked){
    sprintf(s, "<[%d]>", c.quantity());
  }else{
    sprintf(s, "%d", c.quantity());
  }

  if (c.source.type == TARGET_PLANET){
    sprintf(s2, "%c:%s", STKFE_Symbols[c.equipment], s);
  }else{
    sprintf(s2, "%s", s);
  }

  if (c.is_selected){
    drawGlowingLineSafe(screen, x, (fd)y - (fd)(a_size * v), thick, color, 0xffccaa);
  }else{
    drawLineSafe(screen, x, (fd)y - (fd)(a_size * v), thick, color);
  }
  drawArrowHead(screen, y, angle, a_size, 0xffccaa);

  switch (STK::ns_var -> quant_paint_mode % PAINT_MODE_NUM){
  case PAINT_MODE_ALL:
    troop_indicator(screen, z, s2);
    break;
  case PAINT_MODE_SELECTED:
    if (c.is_selected){
      troop_indicator(screen, z, s2);
    }
    break;
  case PAINT_MODE_NONE:
    //do nothing
    break;
  }

}

void STK_Graphics::draw_minimap(SDL_Surface* screen, STK_Collision_map *cm, player_info *pi){
  SDL_Rect b = STK::ns_var -> minimap_rect;
  SDL_Surface *buf = SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA, b.w, b.h, 32, rmask, gmask, bmask, amask);

  if (!buf){return;}

  int d = buf -> w * buf -> h;
  int x,y;
  STK_Planet *p;
  STK_Ship *s;
  intd q;
  int c;
  intd cul, cbr;
  float ax, ay;
  SDL_Rect srect;

  cul = STK::ns_var -> screen2map(intd(0,0));
  cbr = STK::ns_var -> screen2map(intd(screen -> w - 1, screen -> h - 1));
  ax = buf -> w / (float) cm -> uv_settings -> width;
  ay = buf -> h / (float) cm -> uv_settings -> width;
  cul.x *= ax;
  cul.y *= ay; 
  cbr.x *= ax;
  cbr.y *= ay;

  SDL_FillRect(buf, NULL, 0xcc000000);

  for (int i = 0; i < d; i++){
    x = i % buf -> w;
    y = i / buf -> w;
    q.x = (x * cm -> uv_settings -> width) / buf -> w;
    q.y = (y * cm -> uv_settings -> height) / buf -> h;

    p = cm -> get_planet(q);
    s = cm -> get_ship(q);

    c = (p ? pi -> get_color(p -> owner) : 0) | (s ? fade(pi -> get_color(s -> owner), 0.5) : 0);

    pset(buf, x, y, c);
  }

  srect = create_sdl_rect(cul.x, cul.y, cbr.x - cul.x, cbr.y - cul.y);
  srect.x = fmax(srect.x, 0);
  srect.y = fmax(srect.y, 0);
  srect.w = fmin(srect.w, buf -> w - srect.x);
  srect.h = fmin(srect.h, buf -> h - srect.y);
  drawFrame(buf, srect, 0xaaaaaa);
  SDL_BlitSurface(buf, NULL, screen, &b);
  SDL_FreeSurface(buf);

  drawFrame(screen, b, 0xaa8844);
}

void STK_Graphics::display_from_file(const char* v){
  ifstream fs;
  char com[128];
  char buf[128];
  int nlines;
  string dummy;

  fprintf(stdout, "starting display from file: %s\n", v);

#ifdef __WIN32__
  fs.open(v);
  nlines = 0;
  while(getline(fs, dummy) && ++nlines){}
  fs.close();

  fprintf(stdout, " -> counted %d lines.\n", nlines);
#else
  FILE *lc;

  sprintf(com, "wc -l %s", v);
  if(!(lc = popen(com, "r"))){
    fprintf(stdout, "graphics::dff(): failed to scan file: %s.\n", v);
    return;
  }

  fscanf(lc, "%d", &nlines);
  pclose(lc);
#endif
  
  if (nlines <= 0){
    fprintf(stdout, "graphics::dff(): file: %s: no lines.\n", v);
    return;
  }

  fs.open(v);

  if (!fs.is_open()){
    fprintf(stdout, "graphics::dff(): failed to open file: %s.\n", v);
    return;
  }

  string *data = new string[nlines];
  int c = 0;

  while(getline(fs, data[c]) && ++c < nlines){
	fprintf(stdout, "read line %d: %s\n", c, data[c-1].c_str());
  }

  fs.close();

  
  SDL_Rect r = create_sdl_rect(10, 10, 400, 30);
  bool done = false;
  int i;
  int increment = 0;
  int max_show = (STK::ns_var -> screen -> h - 20) / 20;
  int nshow = max_show < nlines ? max_show : nlines;
  SDL_Event e;
  bool kg;

  while (!done){
    while(SDL_PollEvent(&e)){
      kg = e.type == SDL_KEYDOWN;
      done |= kg && e.key.keysym.sym == SDLK_RETURN;
      done |= kg && e.key.keysym.sym == SDLK_ESCAPE;
      increment += kg && e.key.keysym.sym == SDLK_DOWN;
      increment -= kg && e.key.keysym.sym == SDLK_UP;
      increment += nshow * (kg && e.key.keysym.sym == SDLK_SPACE);
    }

    increment = MIN(increment, nlines - nshow);
    increment = MAX(increment, 0);

    SDL_FillRect(STK::ns_var -> screen, NULL, 0x102030);
  
    for (i = 0; i < nshow; i++){
      r.y = 10 + 20 * i;
      drawText(STK::ns_var -> screen, &r, data[i + increment].c_str(), 0xffccaa, 18);
    }

    SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);
    SDL_Delay(250);
  }

  delete [] data;
}

int STK_Graphics::make_selection(SDL_Surface* screen, list<string> selection, string title){
  list<string>::iterator i;
  RSK_GUI g;
  SDL_Rect b;
  SDL_Rect title_rect;
  SDL_Event e;
  bool done = false;
  int n = selection.size();
  int h;
  vector<RSG_Button*> buttons;
  int x = 0;
  int j;
  
  title_rect = create_sdl_rect(10, 10, screen -> w - 20, screen -> h / 8);
  h = (screen -> h - title_rect.h - 10) / n;
  b = create_sdl_rect(10, 10 + title_rect.h + 10, screen -> w - 20, h - 10);

  j = 0;
  buttons.resize(n);
  for(i = selection.begin(); i != selection.end(); i++){
    buttons[j] = new RSG_Button(*i, b);
    g.add(buttons[j]);
    b.y += h;
    j++;
  }

  g.add(new RSG_Label(title, title_rect));

  SDL_FillRect(screen, NULL, 0);

  while (!done){
    while(SDL_PollEvent(&e)){
      g.handle_event(e);
      if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
	done = true;
      }
    }

    for (j = 0; j < buttons.size(); j++){
      if (buttons[j] -> get_state()){
	x = j;
	done = true;
      }
    }

    g.paint(screen);
    SDL_UpdateRect(screen, 0,0,0,0);
    SDL_Delay(100);
  }

  g.delete_components();
  return x;
}

void STK_Graphics::fancy_spirals(SDL_Surface *s){
  static float a, r, p;
  static fd pos, pos_prev;
  static bool setup = 0;
  static unsigned int col;
  static SDL_Surface *buf = 0;
  

  int xmid = s -> w / 2;
  int ymid = s -> h / 2;
  fd smid(xmid, ymid);
  int nrun = 1000;
  int max_count = 100000;
  SDL_Surface *dark_buf;

  if (!setup){
    a = 2 * PI * rand() / (float)RAND_MAX;
    p = 10 + rand() % 10;
    r = 0;
    pos = fd(0,0);
    pos_prev = fd(0,0);
    col = (rand() & 0xffffff) | 0xff000000;
    if (!buf){
      buf = SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA, s -> w, s -> h, 32, rmask, gmask, bmask, amask);
    }

    dark_buf = SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA, s -> w, s -> h, 32, rmask, gmask, bmask, amask);
    SDL_FillRect(dark_buf, 0, 0x88000000);
    SDL_BlitSurface(dark_buf, 0, buf, 0);
    SDL_FreeSurface(dark_buf);

    setup = true;
  }

  for (int i = 0; i < nrun && setup; i++){
    r = r + 1;
    a = normAngle(a + pos.length() * r / pow(p,3));
    pos = pos + fd::normv(a);
    drawLineAlpha(buf, (pos_prev + smid).x, (pos_prev + smid).y, (pos + smid).x, (pos + smid).y, fade(col, (max_count - r)/(float)max_count + .1));

    pos_prev = pos;
    setup = pos.length() < MAX(xmid, ymid) && r < max_count;
  }

  SDL_BlitSurface(buf, 0, s, 0);
}
