#ifndef _STK_PLANET_MASTER
#define _STK_PLANET_MASTER

#include <vector>
#include <list>

#include <SDL/SDL.h>

#include "RSK/RskTypes.h"
#include "STK_Class_headers.h"
#include "STK_Universe_settings.h"

class STK_Planet_master{
 private:
  player_info *p_info;
  STK_Ship_master *ship_master;
 public:
  STK_Game *game;
  std::vector<STK_Planet> planets;
  bool regenerate_maps;

  STK_Planet_master();
  bool initialize(STK_Game *g, STK_Universe_settings s, player_info *p_info, STK_Ship_master *sm);
  void print_com_gen();
  void rebuild_references(STK_Game *g, player_info *p_info, STK_Ship_master *sm);
  void iterate();
  void clear_fleet(STK_Fleet *f);
  int get_num_planets();
  int closest_planet(fd p);
  STK_Planet* closest_owned_planet(fd p, int own);
  STK_Planet* get_planet(int id);
  std::list<STK_Planet*> get_planets();
  void paint(SDL_Surface *s, int pid);
  void paint_stats(SDL_Surface *s, int pid);
  bool send_data(char* buf, int* c, int max);
  bool load_data(char* buf, int* c, int max);
  int calculate_size();
  void assign(STK_Planet_master *m);
  bool differs(STK_Planet_master *m);
  void set_pinfo(player_info *p);
  void set_game_ref(STK_Game *g);
  void cleanup();
};
#endif
