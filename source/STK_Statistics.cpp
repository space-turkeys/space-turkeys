#include "STK_Statistics.h"
#include "STK_Fleet.h"
#include "STK_Namespace.h"
#include "list_operators.h"
#include "STK_Planet.h"
#include "STK_Player.h"

using namespace std;

int STK::statistics::leaving(STK_Planet *p){
  list<STK_Command*>::iterator i;
  int c = 0;
  
  for (i = p -> command_generation.begin(); i != p -> command_generation.end(); i++){
    c += (*i) -> priority;
  }

  return c > 100 ? p -> num_turkeys : (c * p -> num_turkeys) / 100;
}

int STK::statistics::production(STK_Planet *p, STK_Universe_settings *us){
  float factor = p -> p_owner == NULL ? 1 : (1 + (p -> p_owner -> upgrade.industry + p -> p_owner -> temp_upgrade.industry) / (float)10);
  float prod = factor * p -> productivity * (p -> owner > -1 ? *STK_Planet::multiplier : 1);
  return prod * us -> round_length;
}

int STK::statistics::production(list<STK_Planet*> p, STK_Universe_settings *us){
  list<STK_Planet*>::iterator i;
  int c = 0;
  
  for (i = p.begin(); i != p.end(); i++){
    c += production(*i, us);
  }

  return c;
}

int STK::statistics::nships(list<STK_Planet*> p){
  list<STK_Planet*>::iterator i;
  int c = 0;
  
  for (i = p.begin(); i != p.end(); i++){
    c += (*i) -> num_turkeys;
  }

  return c;
}

int STK::statistics::leaving(list<STK_Planet*> p){
  list<STK_Planet*>::iterator i;
  list<STK_Command>::iterator j;
  int c = 0;
  
  for (i = p.begin(); i != p.end(); i++){
    c += leaving(*i);
  }

  return c;
}

int STK::statistics::leaving(STK_Fleet *f){
  list<STK_Command> coms = f -> get_commands();
  list<STK_Command>::iterator i;
  int c = 0;
  
  for (i = coms.begin(); i != coms.end(); i++){
    c += i -> priority;
  }

  return c > 100 ? f -> get_num() : (c * f -> get_num()) / 100;
}

int STK::statistics::eta(STK_Fleet *f, float v){
  if (v<=0){
    return -1;
  }
  return (f -> get_destination_position() - f -> get_position()).length() / v;
}

int STK::statistics::dist(STK_Fleet *f, STK_Planet *hp, STK_Universe_settings *us){
  return us -> calculate_signal_delay((f -> get_position() - hp -> position).length());
}

int STK::statistics::nships(list<STK_Fleet*> f){
  list<STK_Fleet*>::iterator i;
  int c = 0;
  
  for (i = f.begin(); i != f.end(); i++){
    c += (*i) -> get_num();
  }

  return c;
}

bool STK::statistics::contains_fleet(list<STK_Fleet*> f, STK_Fleet *x){
  list<STK_Fleet*>::iterator i;
  for (i = f.begin(); i != f.end(); i++){
    if (*i == x){
      return true;
    }
  }

  return false;
}

int STK::statistics::nships(list<STK_Command*> f){
  list<STK_Command*>::iterator i;
  int c = 0;

  for (i = f.begin(); i != f.end(); i++){
    c += (*i) -> quantity();
  }

  // map<STK_Fleet*,int> fleet_map;
  // map<STK_Planet*,int> planet_map;
  // map<STK_Planet*,int>::iterator pi;
  // map<STK_Fleet*,int>::iterator fi;
  
  // for (i = f.begin(); i != f.end(); i++){
  //   switch((*i) -> source.type){
  //   case TARGET_PLANET:
  //     planet_map[(*i) -> source.planet] = 0;
  //     break;
  //   case TARGET_FLEET:
  //     fleet_map[(*i) -> source.fleet] = 0;
  //     break;
  //   default:
  //     fprintf(stdout, "statistics::nships: bad source type!\n");
  //     return -1;
  //   }
  // }
  
  // for (i = f.begin(); i != f.end(); i++){
  //   switch((*i) -> source.type){
  //   case TARGET_PLANET:
  //     planet_map[(*i) -> source.planet] += (*i) -> quantity;
  //     break;
  //   case TARGET_FLEET:
  //     fleet_map[(*i) -> source.fleet] += (*i) -> quantity;
  //     break;
  //   default:
  //     fprintf(stdout, "statistics::nships: bad source type!\n");
  //     return -1;
  //   }
  // }

  // for (pi = planet_map.begin(); pi != planet_map.end(); pi++){
  //   c += fmin(pi -> first -> num_turkeys, pi -> second);
  // }

  // for (fi = fleet_map.begin(); fi != fleet_map.end(); fi++){
  //   c += fmin(fi -> first -> get_num(), fi -> second);
  // }

  return c;
}

// int STK::statistics::nships(list<STK_Command> f){
//   list<STK_Command>::iterator i;
//   list<STK_Command*> l;
  
//   for (i = f.begin(); i != f.end(); i++){
//     l.push_back(&(*i));
//   }

//   return nships(l);
// }

bool STK::statistics::dirichlet(float a, float b){
  float p = a / (a + b);
  if (p < 0 || p > 1){
    return true;
  }

  return STK::rand_keeper.next_float() > p;
}

int STK::statistics::closest_planet(vector<STK_Planet> planets, fd p){
  float d, min = 0x10000;
  int i;
  int x = -1;
  
  for (i = 0; i < planets.size(); i++){
    d = (planets[i].position - p).length();
    if (d < min){
      min = d;
      x = i;
    }
  }

  return x;
}

vector<STK_Planet> STK::statistics::scramble_planets(STK_Universe_settings s){
  int i;
  int j;
  fd p;
  bool k;
  int c;
  int off;
  vector<STK_Planet> x;

  boost::random::mt19937 rng(time(NULL));
  boost::random::uniform_int_distribution<unsigned int> dist(1, RAND_MAX);

  x.resize(s.num_planets);
  for (i=0; i<x.size(); i++){
    x[i].position=fd(0,0);
    x[i].id = i;
    x[i].owner = -1;
    x[i].p_owner = NULL;
    x[i].is_selected = false;
  }

  for (i=0; i<x.size(); i++){
    x[i].radius=s.min_radius+(s.max_radius-s.min_radius)*((float)dist(rng)/(float)RAND_MAX);
    x[i].productivity = x[i].radius/s.max_radius*s.planet_productivity;
    x[i].num_turkeys = (2000 + (dist(rng)%1000)) * x[i].productivity;
    x[i].resource = 10 * (2000 + (dist(rng)%1000)) * x[i].productivity;

    k=false;
    c=0;
    off=s.planet_seperator+x[i].radius;
    while(!k && c++<STK_MAX_PLANET_SEARCH){
      p = fd(off + (int)(dist(rng)%(s.width-2*off)), off + (int)(dist(rng)%(s.height-2*off)));

	  if (p.x < 0 || p.y < 0){
		fprintf(stdout, "ERROR: statistics::scramble_planets(): negative position generated! Parameters were off=%d, s.w = %d, s.h = %d.\n", off, s.width, s.height);
		exit(1);
	  }

      j = closest_planet(x, p);
      if (j<0 || (p-x[j].position).length()>off+x[j].radius){
		x[i].position=p;
		k=true;
      }
    }

    if (c==STK_MAX_PLANET_SEARCH){
      x.clear();
      return x;
    }
  }

  return x;
}

float STK::statistics::planet_score(vector<STK_Planet> p, int i, STK_Universe_settings s){
  float weight_initial_fleet = .8;
  float weight_initial_production = 1;
  float weight_reachable_production = .1;
  float weight_reachable_ratio = .1;
  float weight_takable_production = .2;
  float weight_takable_ratio = .2;
  float weight_best_production = .1;
  float weight_best_ratio = .2;
  float weight_num_takable = .2 * s.planet_productivity * s.round_length;

  float reach = .8 * s.round_length * s.ship_settings.ship_speed;
  list<STK_Planet*> reachable_planets;
  list<STK_Planet*>::iterator k;
  int j;

  float sum_prod = 0;
  float sum_req = 0;
  float sum_tprod = 0;
  float sum_treq = 0;
  float q, q_best = 0, p_best;
  int num_takable = 0;
  float base_turkeys = s.planet_productivity * s.round_length;

  for (j = 0; j < p.size(); j++){
    if (i != j && (p[j].position - p[i].position).length() < reach){
      reachable_planets.push_back(&p[j]);
    }
  }

  for (k = reachable_planets.begin(); k != reachable_planets.end(); k++){
	  if (!(*k) -> num_turkeys){
		  fprintf(stdout, "statistics::planet_score: planet %d has no turkeys!\n", (*k) -> id);
		  q = 1;
	  }else{
	    q = (*k) -> productivity / (*k) -> num_turkeys;
	  }
    if ((*k) -> num_turkeys * (DIRICHLET_BASE + s.planet_defense)/(float)DIRICHLET_BASE < p[i].num_turkeys){
      sum_tprod += (*k) -> productivity;
      sum_treq += (*k) -> num_turkeys;
      num_takable ++;
    } 
    sum_prod += (*k) -> productivity;
    sum_req += (*k) -> num_turkeys;
    if (q > q_best){
      q_best = q;
      p_best = (*k) -> productivity;
    }
  }
  
  return 1/base_turkeys * (weight_initial_fleet * p[i].num_turkeys 
    + weight_initial_production * p[i].productivity * s.round_length
    + weight_reachable_production * sum_prod * s.round_length 
    + (sum_req ? (weight_reachable_ratio * (s.round_length * sum_prod / sum_req)) : 0)
    + weight_takable_production * sum_tprod * s.round_length 
    + (sum_treq ? (weight_takable_ratio * (s.round_length * sum_tprod / sum_treq)) : 0)
    + weight_best_production * p_best * s.round_length 
    + weight_best_ratio * q_best * s.round_length 
    + weight_num_takable * num_takable);
}

STK::statistics::best_homes_stats STK::statistics::best_homes(vector<STK_Planet> p, int n, STK_Universe_settings s){
  vector<int> test;
  vector<float> score;
  STK::statistics::best_homes_stats x;
  int i,j,k,c;
  bool done, reachable;
  float reach = s.round_length * s.ship_settings.ship_speed;
  float err, err_min = 0x10000;
  list<int> idx_list;
  list<int> idx_list_buf;
  float prod_sum;
  float shortest_dist;

  if (n > p.size() || n == 0){
    return x;
  }

  for (i = 0; i < p.size(); i++){
    idx_list.push_back(i);
  }

  test.resize(n);
  score.resize(n);
  for (i = 0; i < 100; i++){
    //random test vector
    idx_list_buf = idx_list;
    for (j = 0; j < n; j++){
      k = rand()%idx_list_buf.size();
      test[j] = *(idx_list_buf.begin() + k);
      idx_list_buf.erase(idx_list_buf.begin() + k);
    }

    //check if opponents are too close
    shortest_dist = INFINITY;
    reachable = false;
    for (j = 0; j < n && !reachable; j++){
      for (k = j + 1; k < n && !reachable; k++){
	shortest_dist = fmin(shortest_dist, (p[test[j]].position - p[test[k]].position).length());
	if ((p[test[j]].position - p[test[k]].position).length() < reach){
	  reachable = true;
	}
      }
    }


    //if best so far, set as selected
    if (!reachable){
      err = 0;
    }else{
      err = n;
    }


    err += n * pow(reach / shortest_dist, 2);


    for (j = 0; j < n; j++){
      score[j] = planet_score(p, test[j], s);
      for (k = 0; k < j; k++){
	err += pow(score[j] - score[k], 2);
      }
    }


    // prod_sum = 0;
    // for (j = 0; j < n; j++){
    //   prod_sum += p[test[j]].productivity;
    // }

    // // really include this term?
    // err += (n * s.planet_productivity) / prod_sum;

    if (err < err_min){
      // fprintf(stdout, "best_homes: new best: %s reachable.\n", reachable ? "" : "not");
      // fprintf(stdout, " -> reach %.2f, shortest_dist %.2f, err %.3f\n", reach, shortest_dist, err);
      // fprintf(stdout, "round length: %d, ship speed: %f\n", s.round_length, s.ship_settings.ship_speed);
      // fprintf(stdout, "scores: \n");
      // for (j = 0; j < n; j++){
      // 	fprintf(stdout, "%2.f, ", score[j]);
      // }
      // fprintf(stdout, "\n");
      err_min = err;
      x.homes = test;
      x.unfairness = err;
      x.scores = score;
    }
  }

  return x;
}

STK::statistics::battle_result STK::statistics::battle_predictor(int na, int wfa, int nb, int wfb, bool planet){
  int nsim = 1000;
  int i;
  int hpa = 2 + wfa / 10;
  int hpb = 2 + wfb / 10;
  // num of loose < .25, < .5, < .75, < 1, loss
  STK::statistics::battle_result res;
  memset(res.score, 0, 5 * sizeof(int));
  
  for (i = 0; i < nsim; i++){
    int fleeta = hpa * na;
    int fleetb = scramble_quantity(nb);
    fleetb *= hpb;

    while (fleeta > 0 && fleetb > 0){
      bool result = dirichlet(wfa + DIRICHLET_BASE, wfb + DIRICHLET_BASE + planet * (*STK_Planet::def_bonus));
      fleeta -= result;
      fleetb -= !result;
    }

    if (fleeta == 0){
      res.score[4]++;
    }else{
      res.score[(4 * (na * hpa - fleeta)) / (na * hpa)]++;
    }
  }

  for (i = 0; i < 5; i++){
    res.score[i] *= 100 / (float)nsim;
  }

  return res;
}

int STK::statistics::scramble_quantity(int n){
  if (n < 5){
    return rand()%5;
  }else if (n < 10){
    return 5 + rand()%5;
  }else if (n < 20){
    return 10 + rand()%10;
  }else if (n < 50){
    return 20 + rand()%30;
  }else if (n < 100){
    return 50 + rand()%50;
  }else if (n < 250){
    return 100 + rand()%150;
  }else if (n < 500){
    return 250 + rand() % 250;
  }else if (n < 1000){
    return 500 + rand() % 500;
  }else{
    return 1000 + rand() % 1000;
  }
}
