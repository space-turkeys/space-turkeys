#ifndef _STK_INPUT_SETTINGS_MENU
#define _STK_INPUT_SETTINGS_MENU

#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Menu.h"
#include "STK_Structures.h"

class STK_Input_settings_menu : public STK_Menu{
 private:
  TTF_Font *font;

  RSG_Textfield *input_name; 
  RSG_Textfield *input_num_planets;
  RSG_Textfield *input_rel_emptiness;
  RSG_Slidebar *slider_prod;
  RSG_Slidebar *slider_fleet_bonus;
  RSG_Slidebar *slider_planet_defense;
  RSG_Slidebar *slider_ship_speed;
  RSG_Slidebar *slider_signal_speed;
  RSG_Slidebar *slider_round_length;
  RSG_Button *button_signal_speed;

  RSG_Button *button_done, *button_back;

  STK_Standby_mode sbmode;

 public:
  STK_Input_settings_menu(STK_Menu_references *r);
  ~STK_Input_settings_menu();
  void evoke();
  void listen(SDL_Event e, RSG_Component *c);
  void handle_event(SDL_Event e);
  void run();
};
#endif
