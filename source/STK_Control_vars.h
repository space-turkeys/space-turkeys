#ifndef _STK_CONTROL_VARS
#define _STK_CONTROL_VARS

#include <SDL/SDL.h>

#include "RSK/RskTypes.h"

#include "STK_Class_headers.h"
#include "STK_Structures.h"

class STK_Control_vars{
 private:
  bool minimap_trigger;
  intd mmove_vel;
 public:
  SDL_Rect minimap_rect;
  STK_Pressed_keys pressed_keys;
  float zoom;
  intd map_position;
  STK_Collision_map *map;
  SDL_Surface* screen;
  int require_exit;
  bool minimap_active;

  // paint modes
  int command_paint_mode;
  int quant_paint_mode;
  int position_paint_mode;
  int stats_paint_mode;
  int grid_paint_mode;

  STK_Control_vars(SDL_Surface *s);
  void reset();
  void center_at(fd map_pos);
  void update(SDL_Event e);
  void minimap_goto(int x, int y);
  void iterate();
  intd screen2map(intd screen_pos);
  intd map2screen(fd map_pos);
  intd map2mini(fd map_pos);
  fd mini2map(intd mini_pos);
  bool get_exit();
  void zoom_by(float a);
  void clear();
};
#endif
