#ifndef _STK_CLIENT
#define _STK_CLIENT

#include <string>
#include <vector>

#include "STK_Base.h"
#include "STK_Networking_base.h"
#include "STK_Game.h"

class STK_Client{
 private:
  int id;
  int main_socket;
  sockaddr_in server_info;

 public:
  int con_fail;
  int tc_in;
  int tc_out;
  STK_SERVER_STATES state;
  STK_Inet_protocol server_message;
  char name[STK_MAX_NAME];

  STK_Client();
  bool init_tc();
  void finalise_tc(bool s);
  int stkc_connect(std::string adr);
  void stkc_close();
  bool send(void* data, int size, int qid);
  bool get(void* data, int size, int qid);
  int confirm_command(STK_Inet_protocol p);
  int confirm_command(int n, STK_Inet_protocol p);
  int create_game(STK_Universe_settings s);
  bool join_game(int id);
  bool require_spectate(int id);
  bool leave_game();
  bool get_games_list(std::vector<STK_Game_info> *r);
  STK_Game_info get_game_info(int game_id);
  bool r_gamestart();
  bool start_game();
  bool load_game(STK_Game *g, STK_Game_settings s);
  bool load_game(STK_Game *g, STK_Game_settings s, int ntest);
  bool send_choice(STK_Choice c);
  bool get_choices(std::vector<STK_Choice> *r, std::vector<STK_Planet> *p, STK_Ship_master *sm);
  bool validate_universe(STK_Universe *u);
  STK_Inet_protocol reload_universe(STK_Universe *u);
  int get_id();
  int gen_qid();
  STK_SERVER_STATES pop_leave_condition();
  STK_SERVER_STATES peek_leave_condition() const;
};
#endif
