#include <ctime>
#include <cstdlib>
#include <cstdio>

#include <boost/thread/thread.hpp>
#include <SDL/SDL.h>

#include "STK_Keys.h"
#include "STK_Server.h"
#include "STK_Compatability.h"

using namespace std;

int main(int argc, char **argv){

  /* creation and assignment of global variables */
  STK_SERVER_STATES server_in;
  STK_SERVER_STATES server_out;
  STK_Server server(&server_in, &server_out);
  int c=0;
  char in[128];

  if (argc == 2 && !strcmp(argv[1], "--version")){
    fprintf(stdout, "version: %s\n", VERSION);
    fprintf(stdout, "compat. with client version from: %s\n", COMPAT_SERVER_CLIENT);
    return 0;
  }
  
  srand(time(NULL));

  server_in=STKS_RUN;
  server_out=STKS_PENDING;
  boost::thread t(server);
  while (server_out!=STKS_RUN && c++<200){
    fprintf(stdout,"%d   \r",c);
    SDL_Delay(10);
  }
  fprintf(stdout,"\n");
  if (server_out!=STKS_RUN){
    fprintf(stdout,"NO\n");
    exit(1);
  }
  fprintf(stdout,"server ready\n");
  strcpy(in,"");
  while(strcmp(in,"quit")){
    cin>>in;
  }
  server_in=STKS_TERMINATE;
  c=0;
  fprintf(stdout,"waiting for server to terminate...\n");
  while (server_out!=STKS_TERMINATE && c++<40){
    fprintf(stdout,"%d   \r",c);
    SDL_Delay(100);
  }
  if (server_out!=STKS_TERMINATE){
    server_in=STKS_TERMINATE_NOW;
    c=0;
    fprintf(stdout,"Forcing server to terminate...\n");
    while (server_out!=STKS_TERMINATE && c++<40){
      fprintf(stdout,"%d   \r",c);
      SDL_Delay(100);
    }
    if (server_out!=STKS_TERMINATE){
      fprintf(stdout,"server failed to terminate!\n");
    }else{
      fprintf(stdout,"server was forced to terminate.\n");
    }
  }else{
    fprintf(stdout,"done\n");
  }
  return 0;
}
