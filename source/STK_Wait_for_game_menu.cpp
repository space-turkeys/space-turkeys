#include <cstdio>
#include <vector>
#include <string>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "STK_Structures.h"
#include "STK_Game.h"
#include "STK_Game_spectator.h"
#include "STK_Wait_for_game_menu.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Client.h"
#include "STK_Graphics.h"

using namespace std;

STK_Wait_for_game_menu::STK_Wait_for_game_menu(STK_Menu_references *r):STK_Menu(r){
  SDL_Rect b;
  vector<vector<string> > data;
  color_scheme cs;
  
  b = create_sdl_rect(10,10,m_ref -> w - 20, 40);
  label_game_state = new RSG_Label("Game state: ----",b);

  data.resize(1);
  data[0].resize(1);
  data[0][0] = "players:";

  b = create_sdl_rect(10,60,m_ref -> w - 20, m_ref -> h - 70 - 80);
  players_list = new RSG_Table(data,b);

  b = create_sdl_rect(10, b.h + 70, m_ref -> w / 2 - 20, 60);
  button_back = new RSG_Button(this, "Back", cs, NULL, b);

  b.x += b.w + 10;
  button_start = new RSG_Button(this, "Start", cs, NULL, b);

  add(label_game_state);
  add(players_list);
  add(button_back);
  add(button_start);
}

STK_Wait_for_game_menu::~STK_Wait_for_game_menu(){
  delete_components();
}

void STK_Wait_for_game_menu::evoke(){
  SDL_Event e;
  boost::mutex mutex;
  int tc_in = STKS_RUN;
  int tc_out;
  bool loading = false;
  bool end = false;

  done = (!m_ref -> client) || m_ref -> game_id < 0;
  if (done){
    fprintf(stdout,"wfg::evoke: failed to start! gid = %d, client %s.\n",m_ref -> game_id, m_ref -> client ? "allocated" : "NOT allocated");
    return;
  }

  updater.tc_in = &tc_in;
  updater.tc_out = &tc_out;
  updater.client = m_ref -> client;
  updater.table = players_list;
  updater.indicator = label_game_state;
  updater.mutex = &mutex;
  updater.game_id = m_ref -> game_id;
  updater.loading = &loading;
  updater.end = &end;
  updater.game_state = &game_state;
  boost::thread t(updater);
  boost::mutex::scoped_lock l_lock(mutex);
  lock = &l_lock;

  button_start -> visible = m_ref -> player_is_root;
  lock -> unlock();
  game_state = STKS_PENDING;

  fprintf(stdout, "WFGM: evoke: starting main loop: done=%d, end=%d, loading=%d.\n", (int)done, (int)*updater.end, (int)loading);

  while (!done && !*updater.end){
    while (SDL_PollEvent(&e)){
      handle_event(e);
    }

    if (loading){
      run();
    }

    button_back -> visible = game_state != STKS_BUILDING;
    button_start -> visible = m_ref -> player_is_root && (game_state != STKS_BUILDING);

    lock -> lock();
    SDL_FillRect(STK::ns_var -> screen, NULL, 0);
    paint(STK::ns_var -> screen);
    if (game_state == STKS_BUILDING){
      // paint building
      STK_Graphics::draw_message(STK::ns_var -> screen, "waiting for server to build universe...");
    }
    SDL_UpdateRect(STK::ns_var -> screen,0,0,0,0);
    lock -> unlock();
    SDL_Delay(100);
  }

  fprintf(stdout, "WFGM: exited main loop: done=%d, end=%d, loading=%d\n", (int)done, (int)*updater.end, (int)loading);

  updater.terminate();
  lock -> lock();
  while(SDL_PollEvent(&e)){}
}

void STK_Wait_for_game_menu::listen(SDL_Event e, RSG_Component *c){
  // this is a bit weird; presuming that listen is called under lock
  // from RSK_GUI::handle_event().

  lock -> unlock();
  if(done){
    lock -> lock();
    return; 
  }

  if (e.type == SDL_MOUSEBUTTONDOWN){
    if (c == button_back){
      updater.terminate();
      if (!m_ref -> client -> leave_game()){
      }
      done = true;
      m_ref -> game_id = -1;
      m_ref -> player_is_root = false;
      button_back -> reset();
    }else if (c == button_start){
      start();
    }
  }
  lock -> lock();
}

void STK_Wait_for_game_menu::start(){
  lock -> lock();

  button_start -> reset();
  
  if (game_state != STKS_BUILDING && !m_ref -> client -> start_game()){
    fprintf(stdout, "start game not confirmed!\n");
  }
  lock -> unlock();
}

void STK_Wait_for_game_menu::handle_event(SDL_Event e){
  if(e.type == SDL_KEYDOWN){
    if (e.key.keysym.sym == SDLK_RETURN && m_ref -> player_is_root){
      start();
    }else if (e.key.keysym.sym == SDLK_ESCAPE && game_state != STKS_BUILDING){
      updater.terminate();
      if (!m_ref -> client -> leave_game()){
	fprintf(stdout,"failed to leave game!\n");
      }
      done = true;
      m_ref -> game_id = -1;
      m_ref -> player_is_root = false;
    }
  }

  lock -> lock();
  RSK_GUI::handle_event(e);
  lock -> unlock();
}

void STK_Wait_for_game_menu::run(){
  STK_Game_settings s;
  STK_Game g;
  SDL_Event e;

  updater.terminate();
  
  if (m_ref -> client -> load_game(&g, s)){
    /* RUN THE GAME */
    g.evoke();

    if (g.require_spectate){
      g.require_spectate = false;
      if (m_ref -> client -> require_spectate(m_ref -> game_id)){
	STK_Game_spectator gspec;

	// wait for server game thread to reach beginning of next round
	STK_Graphics::draw_message(STK::ns_var -> screen, "waiting for game to start next round...");
	SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);
	if (m_ref -> client -> load_game(&gspec, s, 10000)){
	  gspec.evoke();
	}
      }
    }
  }else {
    fprintf(stdout,"wfg: failed to load game.\n");
  }

  while(SDL_PollEvent(&e)){}
  done = true;
  m_ref -> game_id = -1;
  m_ref -> player_is_root = false;
  if (m_ref -> client -> state == STKS_DEAD){
    fprintf(stdout, "wfg: connection dead.\n");
    m_ref -> client -> stkc_close();
    delete m_ref -> client;
    m_ref -> client = NULL;
  }
}
