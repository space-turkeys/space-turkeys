#ifndef _STK_GENERATE_CHOICE_UI
#define _STK_GENERATE_CHOICE_UI

#include <list>
#include <vector>
#include <string>

#include <SDL/SDL.h>

#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Class_headers.h"

class STK_Generate_choice_UI{
 private:
  RSK_GUI gui;
  player_info *p_info;

  //options
  RSG_Button *button_options;
  RSG_Button *button_options_upgrades;
  RSG_Button *button_options_players;
  RSG_Button *button_options_hs_minimap;
  RSG_Button *button_options_help;
  RSG_Button *button_options_controls;
  RSG_Button *button_options_leave;
  RSG_Panel *panel_options;

  //upgrades
  RSG_Table *table_upgrades;

  //done
  RSG_Button *button_done;
 public:
  
  //panels
  RSG_Panel *panel_sfleet, *panel_mfleet, *panel_splanet, *panel_mplanet, *panel_commands;
  
  //single planet panel
  RSG_Label *label_splanet_name;
  RSG_Label *label_splanet_nships;
  RSG_Label *label_splanet_production;
  RSG_Label *label_splanet_leaving;

  //multiple planets panel
  RSG_Label *label_mplanet_nplanets;
  RSG_Label *label_mplanet_nships;
  RSG_Label *label_mplanet_production;
  RSG_Label *label_mplanet_leaving;

  //single fleet panel
  RSG_Label *label_sfleet_name;
  RSG_Label *label_sfleet_nships;
  RSG_Label *label_sfleet_leaving;
  RSG_Label *label_sfleet_eta;
  RSG_Label *label_sfleet_dist;

  //multiple fleets panel
  RSG_Label *label_mfleet_nfleets;
  RSG_Label *label_mfleet_nships;

  //commands panel
  RSG_Label *label_commands_nships;
  std::list<std::pair<int,char*> > system_message;

  bool show_players;

  STK_Generate_choice_UI(std::vector<std::vector<std::string> > upgrades, player_info *pi);
  ~STK_Generate_choice_UI();
  void paint(SDL_Surface *s);
  void handle_event(SDL_Event e);
  bool overlaps_menu(int x, int y);
  bool get_done();
  bool get_leave();
  void set_visible(bool c);
  void set_menu_state(bool c);
  bool get_menu_state();
  void push_sysmes(char *v);
  void clear_sysmes();
};
#endif
