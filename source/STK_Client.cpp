#include "STK_Client.h"
#include "STK_Networking_base.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Graphics.h"
#include "STK_Server_structs.h"
#include "STK_Compatability.h"

using namespace std;

STK_Client::STK_Client(){
  server_message = STKIP_NONE;
  tc_in = STKS_RUN;
  tc_out = STKS_NONE;
}

int STK_Client::stkc_connect(string server){
  STK_Inet_protocol p;
  int port_number=STK_MAINPORT;
  hostent *record;
  in_addr *addressptr;

  if (!init_tc()){
    return -1;
  }

  fprintf(stdout,"client connecting...");
  
  // Step 0: init winsock if on windows
#ifdef __WIN32__
    WSADATA wsa_data;
    int iResult = WSAStartup(MAKEWORD(2,2), &wsa_data);
    if (iResult != NO_ERROR) {
      printf("WSAStartup failed: %d\n", iResult);
      finalise_tc(false);
      return -1;
    }
#endif
  
   // Step 1 Look up server to get numeric IP address
  record = gethostbyname(server.c_str());
  if (record==NULL) { 
    printf("gethostbyname failed"); 
    finalise_tc(false);
    return -1; 
  }
  
  addressptr = (in_addr *) record->h_addr;

  // Step 2 create a socket
  main_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (main_socket<0) { 
    perror("socket creation"); 
    finalise_tc(false);
    return -1;
  }

  // Step 3 create a sockaddr_in to describe the server
  server_info.sin_family = AF_INET;
  server_info.sin_addr = * addressptr;
  server_info.sin_port = htons(port_number);

  // Step 4 connect
  int r = connect(main_socket, (sockaddr *) &server_info, sizeof(server_info));
  if (r<0) { 
    perror("connect"); 
    finalise_tc(false);
    return r; 
  }

  if (!confirm_command(STKIP_REGISTER)){
    fprintf(stdout,"client connect: register failed.\n");
    stkc_close();
    finalise_tc(false);
    return -1;
  }

  int qid = gen_qid();
  int ver = NVERSION;

  if (!send((void*)&ver,4, qid)){
    fprintf(stdout,"client connect: send version failed, state: %d.\n",(int)state);
    stkc_close();
    finalise_tc(false);
    return -1;
  }

  if (!send((void*)name,STK_MAX_NAME, qid)){
    fprintf(stdout,"client connect: send name failed, state: %d.\n",(int)state);
    stkc_close();
    finalise_tc(false);
    return -1;
  }

  if (!(qid = confirm_command(STKIP_REGISTER))){
    fprintf(stdout,"client connect: post register failed.\n");
    stkc_close();
    finalise_tc(false);
    return -1;
  }
  
  // check reason for failure in client::confirm_buffer

  if (!get((void*)&id,4, qid)){
    fprintf(stdout,"client connect: get id failed.\n");
    stkc_close();
    finalise_tc(false);
    return -1;
  }

  con_fail = 0;
  printf("Connected to %s\n", inet_ntoa(* addressptr));

  finalise_tc(true);

  return 0;
}

void STK_Client::stkc_close(){
  if (main_socket < 0){
    return;
  }

  STK_Inet_protocol p = STKIP_QUIT;

  send((void*)&p, 4, gen_qid());
  CLOSE_SOCKET(main_socket);

#ifdef __WIN32__
  WSACleanup();
#endif

  main_socket=-1;
}

bool STK_Client::send(void* data, int size, int qid){
  bool result;

  if (!init_tc()){
    return false;
  }

  fprintf(stdout, "client: send(s=%d, id=%d, p=%d)\n", size, qid, *((int*)data));
  result =  (state = send_buffer_qid(main_socket,(char*)data,size,BUFKEY_STKIP, qid, STK_CLIENT_TIMEOUT)) == STKS_READY;

  con_fail = result ? 0 : con_fail + 1;

  return result;
}

bool STK_Client::get(void* data, int size, int qid){
  bool result;

  if (!init_tc()){
    return false;
  }

  fprintf(stdout, "client: get(s=%d, id=%d)\n", size, qid);
  result = (state = load_to_buffer(main_socket,(char*)data,size,BUFKEY_STKIP, qid, STK_CLIENT_TIMEOUT)) == STKS_READY;
  
  fprintf(stdout, "client: get: data: %d\n", *((int*)data));

  con_fail = result ? 0 : con_fail + 1;

  return result;
}


bool STK_Client::init_tc(){
  if (tc_in != STKS_RUN){
    server_message = STKIP_DENY;
    tc_out = STKS_FAILED;
    return false;
  }

  tc_out = STKS_RUN;
  return true;
}

void STK_Client::finalise_tc(bool success){
  if (success){
    server_message = STKIP_CONFIRM;
    tc_out = STKS_READY;
  }else{
    server_message = STKIP_DENY;
    tc_out = STKS_FAILED;
  }    
}

int STK_Client::confirm_command(STK_Inet_protocol p){
  return confirm_command(60, p);
}

int STK_Client::confirm_command(int n,STK_Inet_protocol p){
  STK_Inet_protocol rp;
  int qid = gen_qid();

  if (!init_tc()){
    return -1;
  }

  if (n < 0){fprintf(stdout,"confirm_command(%d): failed: timeout!\n", (int)p); return false;}
  fprintf(stdout,"confirm_command: n = %d\n", n);
  if (main_socket < 0){fprintf(stdout,"confirm_command(%d): failed: no socket!\n", (int)p); return false;}
  if (!send((void*)&p, 4, qid)){fprintf(stdout,"confirm_command(%d): failed: to send!\n", (int)p); return false;}
  if (!get((void*)&rp, 4, qid)){fprintf(stdout,"confirm_command(%d): failed: to load!\n", (int)p); return false;}

  if (rp == STKIP_STANDBY){
    SDL_Delay(250);
    return confirm_command(n - 1, p);
  }else{
    server_message = rp;

    if (rp != STKIP_CONFIRM){
      fprintf(stdout, "client::confirm_command(%d): server does not confirm!\n", (int)p);
    }
    
    return qid * (rp == STKIP_CONFIRM);
  }
}

bool STK_Client::r_gamestart(){
  int qid;
  if (!init_tc()){
    return false;
  }

  if (qid = confirm_command(STKIP_REQUIRE_GAMESTART)){
    if (!get((void*)&STK::rand_keeper.seed, sizeof(int), qid)){
      fprintf(stdout, "r_gamestart: failed to load random data.\n");
      finalise_tc(false);
      return false;
    }
    STK::rand_keeper.reset();
    finalise_tc(true);
    return true;
  }

  tc_out = STKS_FAILED;
  return false;
}

bool STK_Client::leave_game(){
  finalise_tc(confirm_command(STKIP_LEAVE_GAME));
  return tc_out == STKS_READY;
}

int STK_Client::create_game(STK_Universe_settings s){
  int id = -1;
  int qid = gen_qid();

  if (!init_tc()){
    return -1;
  }

  finalise_tc(confirm_command(STKIP_CREATE_GAME)
	      && send((void*)&s,sizeof(STK_Universe_settings), qid)
	      && get((void*)&id,4, qid)
	      );

  fprintf(stdout,"client::create_game(): uv.name = '%s', round length: %d, ship speed: %f\n", s.name, s.round_length, s.ship_settings.ship_speed);

  return id;
}

bool STK_Client::join_game(int gid){
  STK_Inet_protocol p = STKIP_NONE;
  int qid = gen_qid();

  if (!init_tc()){
    return false;
  }

  if (!(confirm_command(STKIP_JOIN_GAME)
	&& send((void*)&gid,4, qid) 
	&& get((void*)&p,4, qid))){
    server_message = p == STKIP_INVALID_VERSION ? p : STKIP_DENY;
    tc_out = STKS_FAILED;
    return false;
  }

  server_message = p;
  tc_out = p == STKIP_CONFIRM ? STKS_READY : STKS_FAILED;
  return p == STKIP_CONFIRM;
}

bool STK_Client::require_spectate(int gid){
  STK_Inet_protocol p = STKIP_NONE;
  int qid = gen_qid();

  if (!init_tc()){
    return false;
  }

  if (!(confirm_command(STKIP_SPECTATE_GAME)
	&& send((void*)&gid,4, qid) 
	&& get((void*)&p,4, qid))){
    server_message = p == STKIP_INVALID_VERSION ? p : STKIP_DENY;
    tc_out = STKS_FAILED;
    return false;
  }

  server_message = p;
  tc_out = p == STKIP_CONFIRM ? STKS_READY : STKS_FAILED;
  return p == STKIP_CONFIRM;
  
}

bool STK_Client::get_games_list(vector<STK_Game_info> *r){
  int n;
  int i;
  int qid;

  if (!init_tc()){
    return false;
  }

  if (!((qid = confirm_command(STKIP_REQUIRE_GAMES)) 
	&& get((void*)&n,4, qid))){
    finalise_tc(false);
    return false;
  }

  r -> resize(n);
  
  for (i = 0; i < n; i++){
    if (!get((void*)&((*r)[i]),sizeof(STK_Game_info), qid)){
      finalise_tc(false);
      return false;
    }
  }

  finalise_tc(true);
  return true;
}

STK_Game_info STK_Client::get_game_info(int game_id){
  STK_Game_info r;
  STK_Inet_protocol p;
  int qid = gen_qid();

  r.game_state=-1;

  if (!init_tc()){
    return r;
  }

  finalise_tc(confirm_command(STKIP_REQUIRE_GAME_INFO) 
	      && send((void*)&game_id,4, qid) 
	      && get((void*)&p,4, qid) 
	      && (p==STKIP_CONFIRM) 
	      && get((void*)&r,sizeof(STK_Game_info), qid)
	      && r.game_state > -1);

  return r;
}

bool STK_Client::start_game(){
  STK_Graphics::draw_message(STK::ns_var -> screen, "Requiring game init...");
  SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);

  finalise_tc(confirm_command(STKIP_START_GAME));
  return tc_out == STKS_READY;
}
bool STK_Client::load_game(STK_Game *g, STK_Game_settings set){
  return load_game(g, set, 60);
}

bool STK_Client::load_game(STK_Game *g, STK_Game_settings set, int ntest){
  int n, i;
  STK_Universe_settings uvs;

  if (!init_tc()){
    return false;
  }

  if (!confirm_command(ntest, STKIP_REQUIRE_GAME_DATA)){
    finalise_tc(false);
    return false;
  }

  STK_Graphics::draw_message(STK::ns_var -> screen, "Loading game data...");
  SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);

  g -> initialize(set, uvs, this);

  fprintf(stdout,"Client: loading universe...\n");
  g -> universe.set_game_ref(g);

  // load the data
  if (!g->universe.load_data(main_socket,STK_CLIENT_TIMEOUT)){
    finalise_tc(false);
    return false;
  }

  n = g -> universe.get_settings() -> p_info.num_players;
  if (n < 1 || n > STK_MAX_PPG){
    fprintf(stdout,"load game: bad nr players: %d.\n",n);
    finalise_tc(false);
    return false;
  }

  g -> player.resize(n);
  fprintf(stdout,"Client: universe loaded, loading %d players...\n", n);
  for (i = 0; i < n; i++){
    g -> player[i].id = g -> universe.get_settings() -> p_info.players[i];
    strcpy(g -> player[i].name, g -> universe.get_settings() -> p_info.player_names[i]);
    g -> player[i].color = g -> universe.get_settings() -> p_info.player_colors[i];
    g -> player[i].preference = rand() % 3;
    g -> player[i].dead = false;
  }

  fprintf(stdout,"client::load_game(): building references.\n");
  g -> universe.rebuild_references();

  // must be performed after build references!
  for (i = 0; i < n; i++){
    g -> player[i].home_planet = g -> universe.get_planet_master() -> get_planet(g -> universe.get_settings() -> p_info.initial_home_planets[i]);
    fprintf(stdout,"client: recieved player %s with id %d and home planet %d at %dx%d\n", g -> player[i].name, g -> player[i].id, g -> player[i].home_planet -> id, (int)g -> player[i].home_planet -> position.x, (int)g -> player[i].home_planet -> position.y);
  }

  fprintf(stdout, "client::load_game(): building stars...\n");
  intd dims(g -> universe.get_settings() -> width, g -> universe.get_settings() -> height);
  g -> universe.stars.initialize(dims);

  finalise_tc(true);
  
  return true;
}

bool STK_Client::send_choice(STK_Choice c){
  int n;
  bool success = false;
  success = confirm_command(1000, STKIP_SEND_CHOICES) && c.send_data(main_socket, STK_CLIENT_TIMEOUT);
  finalise_tc(success);
  return success;
}

bool STK_Client::get_choices(vector<STK_Choice> *r, vector<STK_Planet> *p, STK_Ship_master *sm){
  int i,m,n;  
  int qid;

  if (!init_tc()){
    return false;
  }

  if (!(qid = confirm_command(10000, STKIP_REQUIRE_CHOICES))){
    finalise_tc(false);
    fprintf(stdout,"client::get_choices: command not confirmed!\n"); 
    return false;
  }

  if (!(get((void*)&n,4, qid) && n > 0)){
    finalise_tc(false);
    fprintf(stdout,"client::get_choices: failed to get quantity!\n"); 
    return false;
  }
  
  r->resize(n);
  for (i=0; i<n; i++){
    if (!(*r)[i].load_data(main_socket, p, sm, STK_CLIENT_TIMEOUT)){
      finalise_tc(false);
      return false;
    }
  }

  finalise_tc(true);
  return true;
}

bool STK_Client::validate_universe(STK_Universe *u){
  bool success = false;
  if (!init_tc()){
    return false;
  }

  STK_Graphics::draw_message(STK::ns_var -> screen, "Sending universe for validation...");
  SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);

  success = confirm_command(1000,STKIP_VALIDATE_UNIVERSE) && u -> send_data(main_socket, STK_CLIENT_TIMEOUT);
  finalise_tc(success);
  return success;
}

STK_Inet_protocol STK_Client::reload_universe(STK_Universe *u){
  STK_Inet_protocol p;
  int qid;

  if (!init_tc()){
    return STKIP_DENY;
  }

  if (!((qid = confirm_command(1000,STKIP_REQUIRE_UNIVERSE)) 
	&& get((void*)&p,4, qid))){
    finalise_tc(false);
    return STKIP_DENY;
  }

  if (p==STKIP_UNIVERSE_VALID){
    finalise_tc(true);
    return STKIP_UNIVERSE_VALID;
  }

  STK_Graphics::draw_message(STK::ns_var -> screen, "Universe corrupted! Reloading...");
  SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);

  if (!u -> load_ref_data(main_socket, STK_CLIENT_TIMEOUT)){
    finalise_tc(false);
    return STKIP_DENY;
  }

  fprintf(stdout, " :: RELOADED UNIVERSE: REFERENCES: ::\n");
  u -> get_ship_master() -> print_references();

  finalise_tc(true);
  return STKIP_CONFIRM;
}

int STK_Client::get_id(){
  return id;
}

STK_SERVER_STATES STK_Client::pop_leave_condition(){
  STK_SERVER_STATES x = STKS_NONE;

  switch(server_message){
  case STKIP_VICTORY:
    x = STKS_VICTORY;
    break;
  case STKIP_LOSS:
    x = STKS_LOSS;
    break;
  case STKIP_LEAVE_GAME:
    x = STKS_TERMINATE;
    break;
  }

  server_message = STKIP_NONE;
  return x;
}

STK_SERVER_STATES STK_Client::peek_leave_condition() const{
  switch(server_message){
  case STKIP_VICTORY:
    return STKS_VICTORY;
  case STKIP_LOSS:
    return STKS_LOSS;
  case STKIP_LEAVE_GAME:
    return STKS_TERMINATE;
  default:
    return STKS_NONE;
  }
}

int STK_Client::gen_qid(){
  int q;
  while ((q = rand()) == STKIP_QID_NONE){}
  return q;
}
