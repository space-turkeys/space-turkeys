#include <cstdio>
#include "STK_Ship.h"
#include "STK_Collision_map.h"
#include "STK_Fleet.h"
#include "STK_Basic_communication.h"

int STK_Ship::id_counter = 0;

STK_Ship::STK_Ship(){
  remove_me=0;
}

void STK_Ship::initialize(STK_Ship_attributes attr, fd p, float a, int pl, STK_Fleet *cl, STK_Collision_map* cm){
  attributes = attr;
  cmap = cm;
  hp=attr.base_hp;
  position=p;
  angle=a;
  owner=pl;
  fleet=cl;
  associated_id = id_counter++;
  required_angle = angle;
}

void STK_Ship::deal_damage(int warfare){
  /* Deal damage to this ship by an enemy ship of strength equal to 'warfare'. If the ship is killed, set the 'remove me' variable. */
  
  hp -= warfare;
  if (hp < 1){
    remove_me = true;
    explode = true;
  }
}

fd STK_Ship::get_position(){
  return position;
}

void STK_Ship::alter_position(fd d){
  /* Alter this ships position to 'd', and update the collision map with this information. */
  
  cmap->remove_ship(position,this);
  position=d;
  cmap->add_ship(position,this);
}

bool STK_Ship::operator == (STK_Ship a){
  return position == a.get_position() && attributes == a.attributes && angle == a.angle && owner == a.owner && hp == a.hp && associated_id == a.associated_id;
}

void STK_Ship::print_references(){
  fprintf(stdout,"ship::print_references: id = %d, fleet = %d (%x).\n", associated_id, fleet ? fleet -> id : -1, (long unsigned int)fleet);
}

void STK_Ship::print_techdata(){
  fprintf(stdout, "id: %d, owner: %d, hp: %d, angle: %f, position: (%fx%f)\n", associated_id, owner, hp, angle, position.x, position.y);
}

bool STK_Ship::load_data(char *buf, int *c, int max){
  if (*c + fsize > max){
    return false;
  }
  read_from_buf(buf + *c, (char*)&position, fsize);
  *c += fsize;
  return true;
}

bool STK_Ship::send_data(char *buf, int *c, int max){
  if (*c + fsize > max){
    return false;
  }
  write_to_buf((char*)&position, buf + *c, fsize);
  *c += fsize;
  return true;
}
