#ifndef _STK_SELECT_GAME_MENU
#define _STK_SELECT_GAME_MENU

#include <vector>

#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Menu.h"
#include "STK_Structures.h"

class STK_Select_game_menu : public STK_Menu{
 private:
  std::vector<STK_Game_info> games;
  int selected_game;
  RSG_Table *games_table;
  RSG_Button *button_create;
  RSG_Button *button_back;
  select_game_updater updater;
  bool do_create, do_join;
 public:
  STK_Select_game_menu(STK_Menu_references *r);
  ~STK_Select_game_menu();
  void create_game();
  void join_game();
  void update_games_list();
  void evoke();
  void listen(SDL_Event e, RSG_Component *c);
  void handle_event(SDL_Event e);
};
#endif
