#ifndef _STK_SERVER_STRUCTS
#define _STK_SERVER_STRUCTS

#include <vector>

#include "STK_Keys.h"
#include "STK_Class_headers.h"
#include "STK_Networking_base.h"
#include "STK_Structures.h"

STK_SERVER_STATES ready_in(int s, int t);
STK_SERVER_STATES ready_out(int s, int t);
STK_SERVER_STATES send_buffer(int sock, char* buf, int max, STK_Buffer_key key, int timeout);
STK_SERVER_STATES send_buffer_qid(int sock, char* buf, int max, STK_Buffer_key key, int qid, int timeout);
char* load_buffer(int sock, int* max, STK_Buffer_key key, int timeout);
STK_SERVER_STATES load_to_buffer(int sock, char* buf, int max, STK_Buffer_key key, int qid, int timeout);
STK_SERVER_STATES server_load_to_buffer(int sock, char* buf, int max, STK_Buffer_key key, int *qid, int timeout);
STK_SERVER_STATES serve_load_to_buffer(int sock, char* buf, int max, STK_Buffer_key key, int *qid, int timeout);
STK_SERVER_STATES lsend(int socket, void* data, int size, int timeout);
STK_SERVER_STATES lget(int socket, void* data, int size, int timeout);

struct STK_Universe_builder{
  STK_S_Game_info *g;
  void operator () ();
};

struct STK_Client_thread_data{
  STK_SERVER_STATES tc_in;
  STK_SERVER_STATES tc_out;
  int socket;
  sockaddr_in sa;
  char name[STK_MAX_NAME];
  int version;
  int game_id;
  int qid;
  int id;
  int inform_condition;
  STK_SERVER_STATES state;
  STK_SERVER_STATES connection_state;
  STK_Game_loader* gl;

  bool send(STK_Inet_protocol p);
  bool send(void* data, int size);
  STK_Inet_protocol get();
  bool get(void* data, int size);
};

class STK_S_Game_info : public STK_Game_info{
 public:
  bool initialized;
  STK_Universe *universe;
  STK_Client_thread_data* players[STK_MAX_PPG];
  std::vector<int> home_planets;

  STK_S_Game_info();
  bool add_client(STK_Client_thread_data *c);
  void cleanup();
  void remove_client(int id);
  bool build_universe();
  int root_client();
};

struct STK_Game_thread_data{
  STK_SERVER_STATES tc_in;
  STK_SERVER_STATES tc_out;
  int id;
  std::vector<int> home_planets;
  std::vector<STK_Client_thread_data*> ctd;
  std::vector<STK_Client_thread_data*> ctd_spectators;
  std::vector<STK_Client_thread_data*> ctd_spectators_buf;
};
#endif
