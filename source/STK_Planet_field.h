#ifndef _STK_PLANET_FIELD
#define _STK_PLANET_FIELD

#include <list>

#include "STK_Class_headers.h"
#include "RSK/RskTypes.h"

class STK_Planet_field{
 public:
  STK_Universe_settings *uv_settings;
  e_pfield* map;

  STK_Planet_field();
  bool initialize(STK_Universe_settings *uvs, STK_Planet_master *p, STK_Collision_map *cm);
  void rebuild_references(STK_Planet_master *p);
  bool differs(STK_Planet_field m);
  void cleanup();

  static STK_Planet* closest_planet_from_list(fd p, std::list<STK_Planet*> planets);
};
#endif
