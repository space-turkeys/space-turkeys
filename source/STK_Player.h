#ifndef _STK_PLAYER
#define _STK_PLAYER

#include "STK_Definitions.h"
#include "STK_Class_headers.h"
#include "STK_Upgrade.h"

struct STK_Player{
  char name[STK_MAX_NAME];
  STK_Upgrade upgrade;
  int preference;
  int color;
  int id;
  bool dead;
  STK_Planet* home_planet;
  STK_Upgrade temp_upgrade;
};
#endif
