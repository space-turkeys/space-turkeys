class Strategist{
  /*****************************************
   * STRUCTURES
   *****************************************/
  struct sector{
    char opposition;
    char neutral;
    char alliance;
    char desire;
  };

  struct attack{
    char from;
    char to;
    char severe;
  };

  struct defend{
    char sector;
    char severe;
  };

  struct choice{
    list<attack> attacks;
    list<defend> defences;
    float score;
  };
  
  struct environment{
    vector<sector> sectors;
    
    int precise_hash();
    int equivalence_hash();
    float score();
  };

  struct database{
    vector<pair<environment, list<choice> > > data;

    list<choice> get_choice(environment);
    list<pair<environment, list<choice> > > get_equivalence_class(environment);
    void insert(pair<environment, list<choice> >);
  };

  /****************************************
   * VARIABLES
   ****************************************/
  choice previous_choice;
  environment previous_environment;
  bool has_evaluated;

  static database sdb;

  /****************************************
   * ROUTINES
   ****************************************/
  

 public:
  /* This routine is responsible for both scoring the previous choice and selecting a new choice. */
  choice select_choice(environment);
};
