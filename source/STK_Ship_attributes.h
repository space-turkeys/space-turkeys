#ifndef _STK_SHIP_ATTRIBUTES
#define _STK_SHIP_ATTRIBUTES

#include "STK_Class_headers.h"
#include "STK_Ship_settings.h"

struct STK_Ship_attributes{
  float speed;
  int base_hp;
  int color;
  int warfare;

  STK_Ship_attributes(STK_Player *p, STK_Ship_settings s);
  STK_Ship_attributes(){}
  bool operator == (STK_Ship_attributes a);
};
#endif
