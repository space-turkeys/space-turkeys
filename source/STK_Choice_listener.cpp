#include "STK_Choice_listener.h"
#include "STK_Client.h"

void STK_Choice_listener::operator ()(){
  if (choice){
    *success = client -> send_choice(*choice) && client -> get_choices(choice_list, planet_list, s_master);
  }else{
    *success = client -> get_choices(choice_list, planet_list, s_master);
  }
  *done = true;
  client -> tc_out = STKS_READY;
}

void STK_Choice_listener::terminate(){
  if (*done) {
    return;
  }

  client -> tc_in = STKS_TERMINATE;
  while (client -> tc_out == STKS_RUN){
    SDL_Delay(50);
  }

  client -> tc_in = STKS_RUN;

  *done = true;
}
