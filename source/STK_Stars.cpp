#include "STK_Stars.h"
#include "STK_Definitions.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"

#include "RSK/RskGraphics.h"

STK_Stars::STK_Stars(){
  stars = NULL;
  num = 0;
}

void STK_Stars::initialize(intd size){
  cleanup();

  fprintf(stdout, "initializing stars with dims: %d x %d.\n", size.x, size.y);
  num = (size.x * size.y) * STARS_DENSITY;
  
  if (num > 0){
    stars = new intd[num];
  }

  for (int i = 0; i < num; i++){
    stars[i].x = rand()%size.x;
    stars[i].y = rand()%size.y;
  }
}

void STK_Stars::paint(SDL_Surface *s){
  int i;
  intd p;
  SDL_Rect check = create_sdl_rect(0,0,s -> w, s -> h);

  for (i = 0; i < num; i++){
    p = STK::ns_var -> map2screen(stars[i]);
    if (rect_contains(&check, p)){
      pset(s, p.x, p.y, 0xffffff);
    }
  }
}

void STK_Stars::cleanup(){
  if (stars){
    delete [] stars;
  }
  stars = 0; 
  num = 0;
}
