#ifndef _STK_GAME_SETTINGS
#define _STK_GAME_SETTINGS

#include <RSK/RSK_GUI_V2/RSK_GUI.h>

struct STK_Game_settings{

  //graphics settings
  int screen_resx;
  int screen_resy;
  bool fullscreen;
  bool use_smudge;
  color_scheme scheme;
  int animate_explosions;

  //game settings
  int frame_rate;

  STK_Game_settings();
};
#endif
