#ifndef _STK_LOCAL_GAME_MENU
#define _STK_LOCAL_GAME_MENU
#include <RSK/RSK_GUI_V2/RSK_GUI.h>
#include "STK_Menu.h"

class STK_Local_game_menu : public STK_Menu{
 public:
  STK_Local_game_menu(STK_Menu_references *r);
  ~STK_Local_game_menu();
  void listen(SDL_Event e, RSG_Component *c);
};
#endif
