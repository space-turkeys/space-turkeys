#include <boost/thread/thread.hpp>

#include "STK_Game_spectator.h"
#include "STK_Graphics.h"
#include "STK_Client.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Choice_listener.h"
#include "STK_Generate_choice_UI.h"
#include "STK_Statistics.h"

using namespace std;

void STK_Game_spectator::exit_interface(){

  list<string> data;
  int sb_com = STKS_RUN, sb_state = STKS_INITIALIZING;
  STK_Standby_mode m;

  m.command = &sb_com;
  m.state = &sb_state;
  m.client = client;
  boost::thread t(m);

  data.push_back("OK");

  switch (client -> server_message){
  case STKIP_LEAVE_GAME:
    STK_Graphics::make_selection(STK::ns_var -> screen, data, "THE GAME HAS ENDED!");
    break;
  default:
    STK_Graphics::make_selection(STK::ns_var -> screen, data, "GAME TERMINATED BY SERVER.");
    break;	
  }

  client -> server_message = STKIP_NONE;
  m.terminate();
}

void STK_Game_spectator::evoke(){
  bool done = false;
  SDL_Event e;
  vector<STK_Choice> choice_list;
  int c = 0;
  int i;
  list<STK_Planet*> planets;
  list<STK_Planet*>::iterator j;
  list<string> selection;
  STK_Upgrade no_upgrade;

  bool first_round = true;

  STK::ns_var -> get_exit();
  STK::ns_var -> reset();
  STK::ns_var -> map = universe.get_collision_map();
  STK_Graphics::set_uvs(universe.get_settings());
  client -> server_message = STKIP_NONE;
  STK_Planet::research_complete = 8 * universe.get_settings() -> planet_productivity * pow((float)universe.get_settings() -> round_length, 2);

  while (!done){
    fprintf(stdout,"game::evoke(): debug step 0.\n");
    universe.get_ship_master() -> print_references();

    fprintf(stdout,"client spectator round: start.\n");
    while(SDL_PollEvent(&e)){
      if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
	client -> confirm_command(STKIP_LEAVE_GAME);
	cleanup();
	fprintf(stdout,"game_spectator::evoke(): ESCAPE pressed.\n");
	return;
      }
    }
    fprintf(stdout,"client: events handled.\n");
    
    if (!client -> r_gamestart()){
      fprintf(stdout, "client: gamestart not confirmed!\n");
      break;
    }
    fprintf(stdout,"client: gamestart confirmed.\n");
    
    fprintf(stdout, "list of players:\n");
    for (i = 0; i < player.size(); i++){
      fprintf(stdout, " -> %s %d: %s\n", player[i].id == client -> get_id() ? "player" : "opponent", player[i].id, player[i].dead ? "dead" : "living");
    }
    fprintf(stdout, "list of p_info:\n");
    player_info *pinf = &universe.get_settings() -> p_info;
    for (i = 0; i < pinf -> num_players; i++){
      fprintf(stdout, " -> %s %d\n", pinf -> players[i] == client -> get_id() ? "player" : "opponent", pinf -> players[i]);
    }
    fprintf(stdout, "list of upgrade points:\n");
    for (i = 0; i < pinf -> num_players; i++){
      fprintf(stdout, " -> player %d: %d\n", pinf -> players[i], universe.get_points(pinf -> players[i]));
    }
    
    if (!wait_for_choices(&choice_list)){
      fprintf(stdout,"client: load/send choices NOT confirmed.\n");
      break;
    }
    fprintf(stdout,"client: send/load choice confirmed.\n");
    
    //game start confirmed and choices successfully exchanged
    universe.apply_choices(choice_list);
    apply_upgrades(choice_list);
    
    universe.clear_choice();
    STK::rand_keeper.reset();
    evaluate_round();

    if (client -> validate_universe(&universe)){
      if (client -> reload_universe(&universe) == STKIP_DENY){
	fprintf(stdout,"game_spectator::evoke(): reload universe denied.\n");
	break;
      }else{
	fprintf(stdout,"game_spectator::evoke: round completed successfully!\n");
      }
    }else{
      fprintf(stdout,"game_spectator::evoke(): validate universe failed.\n");
      break;
    }
  }

  exit_interface();
  cleanup();
  fprintf(stdout,"game_spectator::evoke: ended!\n");
}

bool STK_Game_spectator::wait_for_choices(vector<STK_Choice> *choice_list){
  STK_Choice_listener listener;
  bool done = false;
  bool success = false;
  SDL_Event event;
  vector<vector<string> > upgrades;
  player_info *p_info = &(universe.get_settings() -> p_info);

  listener.client = client;
  listener.choice_list = choice_list;
  listener.choice = 0;
  listener.planet_list = &(universe.get_planet_master() -> planets);
  listener.s_master = universe.get_ship_master();
  listener.done = &done;
  listener.success = &success;
  
  boost::thread t(listener);

  universe.clear_selection();
  STK::ns_var -> clear();
  universe.get_ship_master() -> cleanup_explosions();
  drag.x = -1;
  drag.y = -1;
  drag.w = 0;
  drag.h = 0;

  gcui = new STK_Generate_choice_UI(upgrades, p_info);

  while (!STK::ns_var -> require_exit && !done){
    while(!STK::ns_var -> require_exit && SDL_PollEvent(&event)){
      STK::ns_var -> update(event);
      evaluate_event(event);
      gcui -> handle_event(event);
      if (STK::ns_var -> require_exit && gcui -> get_menu_state()){
	STK::ns_var -> require_exit = false;
	gcui -> set_menu_state(false);
      }
      STK::ns_var -> require_exit |= gcui -> get_leave();
    }

    SDL_FillRect(STK::ns_var -> screen,NULL,0);
    STK::ns_var -> iterate();
    universe.paint(STK::ns_var -> screen, client -> get_id());
    gcui -> paint(STK::ns_var -> screen);
    if (STK::ns_var -> minimap_active){
      STK_Graphics::draw_minimap(STK::ns_var -> screen, universe.get_collision_map(), &(universe.get_settings() -> p_info));
    }
    drawFrame(STK::ns_var -> screen, create_sdl_rect(0,0,STK::ns_var -> screen -> w, STK::ns_var -> screen -> h), 5, universe.get_settings() -> p_info.get_color(client -> get_id()));

    SDL_UpdateRect(STK::ns_var -> screen,0,0,0,0);
    SDL_Delay(50);
  }

  if (gcui){
    delete gcui;
  }
  gcui = 0;

  listener.terminate();

  return success;
}

void STK_Game_spectator::evaluate_event(SDL_Event e){
  //manipulates c according to e, adding commands
  fd pos;

  list<STK_Planet*>::iterator pi;
  list<STK_Fleet*>::iterator ci;
  list<STK_Planet*> selected_planets;
  list<STK_Planet*> pl;
  list<STK_Fleet*> selected_fleets;
  list<STK_Fleet*> fleet_list;
  intd ulc, brc;

  selected_planets = universe.get_selected_planets();
  selected_fleets = universe.get_selected_fleets();

  switch(e.type){
  case SDL_MOUSEBUTTONUP:

    if (e.button.button > 3){
      break;
    }

    /* test for area selection */
    if (e.button.button == SDL_BUTTON_LEFT && drag.w && drag.h){
      //the mouse was moved since left click
      area_select();
      drag.x = -1;
      drag.y = -1;
      drag.w = 0;
      drag.h = 0;
      break;
    }

    if (e.button.button == SDL_BUTTON_RIGHT && drag.w && drag.h){
      //the mouse was moved since right click
      area_select_fleets();
      drag.x = -1;
      drag.y = -1;
      drag.w = 0;
      drag.h = 0;
      break;
    }

    drag.x = -1;
    drag.y = -1;
    drag.w = 0;
    drag.h = 0;

    // break if the click was in the mini map
    if (STK::ns_var -> minimap_active && rect_contains(&STK::ns_var -> minimap_rect, intd(e.button.x, e.button.y))){
      break;
    }

    /* something was clicked, better find out what! */
    STK_Planet *c_planet;
    STK_Fleet *c_fleet;

    pos = STK::ns_var -> screen2map(intd(e.button.x,e.button.y));
    
    if (c_planet = universe.get_collision_map() -> get_planet(pos)){
      fprintf(stdout,"client: planet %d with owner %d detected! (active player is %d).\n",c_planet -> id, c_planet -> owner, client -> get_id());
      if (c_planet -> owner > -1){
	//clicked friendly planet, add to selection
	if (!(STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT) )){
	  //clear old selections
	  universe.clear_selection();
	}
	c_planet -> is_selected = true;
      }
    }else if ((STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT)) && (fleet_list = find_all_fleets(pos)).size()){
      //clicked fleet, add to selection
      for (ci = fleet_list.begin(); ci != fleet_list.end(); ci++){
	(*ci) -> is_selected = true;
      }

    }else if (c_fleet = find_fleet(pos)){
      if (!(STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT) )){
	//clear old selections
	universe.clear_selection();
      }
      //clicked friendly fleet, add to selection
      c_fleet -> is_selected = true;
    }else {

      //clear old selections
      universe.clear_selection();
    }

    break;
  case SDL_MOUSEBUTTONDOWN:
    if (e.button.button == SDL_BUTTON_LEFT || e.button.button == SDL_BUTTON_RIGHT){
      if (STK::ns_var -> minimap_active && rect_contains(&STK::ns_var -> minimap_rect, intd(e.button.x, e.button.y))){
	break;
      }
      if (gcui -> overlaps_menu(e.button.x, e.button.y)){
	break;
      }
      drag.x = e.button.x;
      drag.y = e.button.y;
      drag.w = 0;
      drag.h = 0;
      cpos.x = e.button.x;
      cpos.y = e.button.y;
    }
    break;

  case SDL_MOUSEMOTION:
    if (drag.x > -1 && drag.y > -1){
      ulc.x = fmin(e.motion.x, cpos.x);
      ulc.y = fmin(e.motion.y, cpos.y);
      brc.x = fmax(e.motion.x, cpos.x);
      brc.y = fmax(e.motion.y, cpos.y);
      drag = create_sdl_rect(ulc.x, ulc.y, brc.x - ulc.x, brc.y - ulc.y);
    }
    break;
  case SDL_KEYDOWN:
    switch (e.key.keysym.sym){
    case SDLK_a:
      // Select all planets

      pl = universe.get_planet_master() -> get_planets();
      for (pi = pl.begin(); pi != pl.end(); pi++){
	if ((*pi) -> owner == client -> get_id()){
	  (*pi) -> is_selected = true;
	}
      }
      break;
    case SDLK_u:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	STK::ns_var -> command_paint_mode++;
	
	switch(STK::ns_var -> command_paint_mode % PAINT_MODE_NUM){
	case 0:
	  gcui -> push_sysmes("command display mode: ALL");
	  break;
	case 1:
	  gcui -> push_sysmes("command display mode: SELECTED");
	  break;
	case 2:
	  gcui -> push_sysmes("command display mode: NONE");
	  break;
	}
      }
      break;
    case SDLK_i:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	STK::ns_var -> quant_paint_mode++;
	
	switch(STK::ns_var -> quant_paint_mode % PAINT_MODE_NUM){
	case 0:
	  gcui -> push_sysmes("troop display mode: ALL");
	  break;
	case 1:
	  gcui -> push_sysmes("troop display mode: SELECTED");
	  break;
	case 2:
	  gcui -> push_sysmes("troop display mode: NONE");
	  break;
	}
      }
      break;
    case SDLK_o:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	STK::ns_var -> stats_paint_mode++;
	
	switch(STK::ns_var -> stats_paint_mode % PAINT_MODE_NUM){
	case 0:
	  gcui -> push_sysmes("stats display mode: ALL");
	  break;
	case 1:
	  gcui -> push_sysmes("stats display mode: SELECTED");
	  break;
	case 2:
	  gcui -> push_sysmes("stats display mode: NONE");
	  break;
	}
      }
      break;
    }
  }

  //alterations to interface
  if (e.type == SDL_MOUSEBUTTONUP || e.type == SDL_KEYDOWN){
    selected_planets = universe.get_selected_planets();
    selected_fleets = universe.get_selected_fleets();

    //planets
    if (selected_planets.size() == 1){
      
      gcui -> panel_splanet -> visible = true;
      gcui -> panel_mplanet -> visible = false;

      gcui -> label_splanet_name -> set_text(((string)"planet id: ").append(int2str(selected_planets.back() -> id)));
      gcui -> label_splanet_nships -> set_text(((string)"fleet: ").append(int2str((int)selected_planets.back() -> num_turkeys)));
      gcui -> label_splanet_production -> set_text(((string)"prod.: ").append(int2str(STK::statistics::production(selected_planets.back(), universe.get_settings()))));
      gcui -> label_splanet_leaving -> set_text(((string)"leaving: ").append(int2str(STK::statistics::leaving(selected_planets.back()))));

    }else if (selected_planets.size() > 1){
      
      gcui -> panel_mplanet -> visible = true;
      gcui -> panel_splanet -> visible = false;

      gcui -> label_mplanet_nplanets -> set_text(((string)"#planets: ").append(int2str(selected_planets.size())));
      gcui -> label_mplanet_nships -> set_text(((string)"#ships: ").append(int2str(STK::statistics::nships(selected_planets))));
      gcui -> label_mplanet_production -> set_text(((string)"prod.: ").append(int2str(STK::statistics::production(selected_planets, universe.get_settings()))));
      gcui -> label_mplanet_leaving -> set_text(((string)"leaving: ").append(int2str(STK::statistics::leaving(selected_planets))));

    }else{
      
      gcui -> panel_splanet -> visible = false;
      gcui -> panel_mplanet -> visible = false;
    }

    //fleets
    if (selected_fleets.size() == 1){
      
      gcui -> panel_sfleet -> visible = true;
      gcui -> panel_mfleet -> visible = false;

      gcui -> label_sfleet_nships -> set_text(((string)"fleet id: ").append(int2str((int)selected_fleets.back() -> id)));
      gcui -> label_sfleet_nships -> set_text(((string)"fleet: ").append(int2str((int)selected_fleets.back() -> get_num())));
      gcui -> label_sfleet_leaving -> set_text(((string)"leaving: ").append(int2str(STK::statistics::leaving(selected_fleets.back()))));
      gcui -> label_sfleet_eta -> set_text(((string)"eta: ").append(int2str(STK::statistics::eta(selected_fleets.back(), universe.get_settings() -> ship_settings.ship_speed))));
      gcui -> label_sfleet_dist -> set_text(((string)"dist: ").append(int2str(STK::statistics::dist(selected_fleets.back(), get_player(client -> get_id()) -> home_planet, universe.get_settings()))));

    }else if (selected_fleets.size() > 1){
      gcui -> panel_mfleet -> visible = true;
      gcui -> panel_sfleet -> visible = false;

      gcui -> label_mfleet_nfleets -> set_text(((string)"#fleets: ").append(int2str(selected_fleets.size())));
      gcui -> label_mfleet_nships -> set_text(((string)"#ships: ").append(int2str(STK::statistics::nships(selected_fleets))));
    }else{
      gcui -> panel_sfleet -> visible = false;
      gcui -> panel_mfleet -> visible = false;
    }
  }
}
