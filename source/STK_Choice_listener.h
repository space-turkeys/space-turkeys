#ifndef _STK_CHIOCE_LISTENER
#define _STK_CHIOCE_LISTENER

#include <vector>
#include "STK_Class_headers.h"
#include "STK_Planet.h"
#include "STK_Choice.h"

struct STK_Choice_listener{
  STK_Client *client;
  STK_Choice *choice;
  std::vector<STK_Choice> *choice_list;
  std::vector<STK_Planet> *planet_list;
  STK_Ship_master *s_master;
  bool *done;
  bool *success;

  void operator()();
  void terminate();
};
#endif
