#include "STK_Root_menu.h"
#include "STK_Client.h"

STK_Root_menu::STK_Root_menu(STK_Menu_references *r):STK_Menu(r){
  SDL_Rect b;
  color_scheme cs;
  int w = m_ref -> w - 20;
  int h = (m_ref -> h - 20) / 2;
  TTF_Font* f = TTF_OpenFont("fonts/evanescent.ttf", h);

  b = create_sdl_rect(10, 10, w, h - 10);
  b_online = new RSG_Button(this,"Play Online",cs,f,b);

  b.y += h;
  b_quit = new RSG_Button(this,"Terminate",cs,f,b);
  
  add(b_online);
  add(b_quit);

  TTF_CloseFont(f);
}

STK_Root_menu::~STK_Root_menu(){
  delete_components();
}

void STK_Root_menu::listen(SDL_Event e, RSG_Component *c){
  if (e.type == SDL_MOUSEBUTTONDOWN){
    if (c == b_online){
      m_ref -> select_game_menu -> evoke();
      if (m_ref -> client){
	m_ref -> client -> stkc_close();
	delete m_ref -> client;
	m_ref -> client = NULL;
      }
    }else if (c == b_quit){
      done = true;
    }
  }
}

void STK_Root_menu::handle_event(SDL_Event e){
  RSK_GUI::handle_event(e);
  
  if (e.type == SDL_KEYDOWN){
    switch (e.key.keysym.sym){
    case SDLK_ESCAPE:
      done = true;
      break;
    case SDLK_o:
      m_ref -> select_game_menu -> evoke();
      if (m_ref -> client){
	m_ref -> client -> stkc_close();
	delete m_ref -> client;
	m_ref -> client = NULL;
      }
    }
  }
}
