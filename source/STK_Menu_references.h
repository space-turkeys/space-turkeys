#ifndef _STK_MENU_REFERENCES
#define _STK_MENU_REFERENCES

#include "STK_Class_headers.h"
#include "STK_Keys.h"

struct STK_Menu_references{
  STK_Menu *root_menu;
  STK_Menu *input_settings_menu;
  STK_Menu *select_game_menu;
  STK_Menu *local_game_menu;
  STK_Menu *wait_for_game_menu;
  STK_Menu *connection_settings_menu;
  STK_Client *client;  
  STK_SERVER_STATES* tc_in;
  STK_SERVER_STATES* tc_out;
  
  int game_id;
  bool player_is_root;
  int w,h;

  STK_Menu_references();
};
#endif
