#include <stddef.h>
#include "STK_Menu_references.h"
STK_Menu_references::STK_Menu_references(){
  w = h = -1;
  game_id = -1;
  player_is_root = false;
  root_menu = NULL;
  input_settings_menu = NULL;
  select_game_menu = NULL;
  local_game_menu = NULL;
  connection_settings_menu = NULL;
  wait_for_game_menu = NULL;
  client = NULL;
  tc_in = NULL;
  tc_out = NULL;
}
