#ifndef _STK_KEYS
#define _STK_KEYS

#include <list>
#include <string>

enum STK_SERVER_STATES{
  STKS_RUN,                    // 0
  STKS_PENDING,                // 1
  STKS_LOADING,                // 2
  STKS_BUILDING,               // 3
  STKS_BUILD_FAILED,           // 4
  STKS_PLAYING,                // 5
  STKS_LFG,                    // 6
  STKS_INITIALIZING,           // 7
  STKS_PORT,                   // 8
  STKS_FAILED,                 // 9
  STKS_GAME_COMPLETE,          // 10
  STKS_TERMINATE,              // 11
  STKS_TERMINATE_NOW,          // 12
  STKS_READY,                  // 13
  STKS_NOTREADY,               // 14
  STKS_TIMEOUT,                // 15
  STKS_DEAD,                   // 16
  STKS_GOTO_GAME,              // 17
  STKS_LEAVE_GAME,             // 18
  STKS_RETURN_TO_MAINFRAME,    // 19
  STKS_VICTORY,                // 20
  STKS_LOSS,                   // 21
  STKS_NONE,                   // 22
  STKS_SPECTATING,             // 23
  STKS_NUM_STATES              // 24
};

enum STK_Inet_protocol{
  STKIP_REGISTER,			// 0
  STKIP_QUIT,				// 1
  STKIP_CONFIRM,			// 2
  STKIP_DENY,				// 3
  STKIP_JOIN_GAME,			// 4
  STKIP_LEAVE_GAME,			// 5
  STKIP_REQUIRE_GAMES,		        // 6
  STKIP_CREATE_GAME,		        // 7
  STKIP_REQUIRE_GAME_INFO,	        // 8
  STKIP_START_GAME,			// 9
  STKIP_REQUIRE_GAME_DATA,		// 10
  STKIP_REQUIRE_GAMESTART,		// 11
  STKIP_SEND_CHOICES,			// 12
  STKIP_REQUIRE_CHOICES,		// 13
  STKIP_VALIDATE_UNIVERSE,		// 14
  STKIP_REQUIRE_UNIVERSE,		// 15
  STKIP_UNIVERSE_VALID,			// 16
  STKIP_UNIVERSE_INVALID,		// 17
  STKIP_DO_NOTHING,			// 18
  STKIP_STANDBY,			// 19
  STKIP_INVALID_COMMAND,		// 20
  STKIP_VICTORY,			// 21
  STKIP_LOSS,				// 22
  STKIP_NONE,                    	// 23
  STKIP_INVALID_VERSION,         	// 24
  STKIP_SPECTATE_GAME,           	// 25
  STKIP_NUM_PROTOCOLS			// 27
};

enum STK_Buffer_key{
  BUFKEY_UNIVERSE = STKIP_NUM_PROTOCOLS,
  BUFKEY_CHOICE,
  BUFKEY_STKIP,
  BUFKEY_NONE
};

enum STK_Ship_action{STKSA_Rotate_right, STKSA_Rotate_left, STKSA_None};

enum STK_Fleet_equipment{STKFE_Travel, STKFE_Fight, STKFE_Artillery};
extern std::list<std::string> STKFE_Names;
#endif
