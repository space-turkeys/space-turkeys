#include <vector>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "STK_Server_threads.h"
#include "STK_Networking_base.h"
#include "STK_Server.h"
#include "STK_Game_loader.h"
#include "STK_Compatability.h"

int STK_Port_listener::cid_counter = 0;

void STK_Client_thread::operator()(){
  STK_Inet_protocol p;
  int count = 0, countout = 10;
  bool done = false;
  int tid = td -> id;
  
  while(!done && td && td->tc_in == STKS_RUN){
    p = td->get();
    if (td->connection_state == STKS_READY){
      count = 0;
      if (p != STKIP_DO_NOTHING && p >= 0 && p<STKIP_NUM_PROTOCOLS){
	serv->read_request(p, td);
      }
    }else{
      if (td == 0 || td->connection_state == STKS_DEAD || count++>countout){
	done = true;
      }
      //fprintf(stdout, "connection not in ready state!\n");
    }
    SDL_Delay(50);
  }
  
  if (!td){
    fprintf(stdout,"client thread ended: client not found.\n");
  }else if (td->tc_in != STKS_RUN){
    fprintf(stdout,"client thread %d ended: by server.\n",td -> id);
  }else if (done){
    fprintf(stdout,"client thread %d ended: dead/timed out.\n",td -> id);
  }

  if (td->tc_in == STKS_GOTO_GAME && td->connection_state != STKS_DEAD){
    fprintf(stdout,"client (%d) thread set to playing state.\n",td -> id);
    td->tc_out = STKS_PLAYING;
    td->state = STKS_PLAYING;
  }else if (td->tc_in == STKS_SPECTATING && td->connection_state != STKS_DEAD){
    fprintf(stdout,"client (%d) thread set to spectating state.\n",td -> id);
    td->tc_out = STKS_SPECTATING;
    td->state = STKS_SPECTATING;
  }else{
    fprintf(stdout,"client (%d) thread set to terminated state.\n",td -> id);
    td->tc_out = STKS_TERMINATE;
  }
  
}

STK_Port_listener::STK_Port_listener(){
  cid_counter = 0;
  serv = 0;
  tc_in = 0;
  tc_out = 0;
}

void STK_Port_listener::operator()(){
  if (*tc_in != STKS_RUN){return;}
  *tc_out = STKS_RUN;
  while(*tc_in == STKS_RUN){
    accept_client();
  }
  *tc_out = STKS_TERMINATE;
}

bool STK_Port_listener::accept_client(){
  /* Opens a connection to the next waiting client. Adds the client to the list of clients. Returns true on success or false on failure. */
  int client_info_size = sizeof(sockaddr_in);
  sockaddr_in sa;

  STK_Inet_protocol p;
  STK_Client_thread_data *c;
  int qtest;

  int test;
  int iResult;
  struct timeval tv;
  fd_set rfds;

  FD_ZERO(&rfds);
  FD_SET(serv -> main_socket, &rfds);

  tv.tv_sec = (long)2;
  tv.tv_usec = 0;

  iResult = select(serv -> main_socket + 1, &rfds, (fd_set *) 0, (fd_set *) 0, &tv);
  if(iResult > 0){
    test = accept(serv -> main_socket, (sockaddr *) &sa,(SOCKLEN_TYPE*)&client_info_size);
  }else{
    //fprintf(stdout, "port_listener::accept_client: iresult is %d\n", iResult);
    return -1;
  }

  if (test >= 0){
    fprintf(stdout, "port_listene::accept_client: testing client ...\n");
    if (serv -> clients_full()){
      CLOSE_SOCKET(test);
      return false;
    }
    
    if (!server_load_to_buffer(test,(char*)&p,4,BUFKEY_STKIP, &qtest,STK_SERVER_TIMEOUT)){
      return false;
    }

    if (p == STKIP_REGISTER){
      c = new STK_Client_thread_data();
      c -> socket = test;
      c -> sa = sa;
      c -> state = STKS_LFG;
      c -> connection_state = STKS_READY;
      c -> game_id = -1;
      c -> id = cid_counter++;
      c -> gl = NULL;
      c -> qid = qtest;

      c -> send(STKIP_CONFIRM);
      if (!server_load_to_buffer(c -> socket,(char*)&c -> version,4, BUFKEY_STKIP, &qtest, STK_SERVER_TIMEOUT)){
	fprintf(stdout,"accept: client failed to send version.\n");
	CLOSE_SOCKET(c -> socket);
	return false;
      }

      fprintf(stdout, "accept: loaded version number %d\n", c -> version);
      
      if (!server_load_to_buffer(c -> socket,(char*)c -> name,STK_MAX_NAME, BUFKEY_STKIP, &qtest, STK_SERVER_TIMEOUT)){
	fprintf(stdout,"accept: client failed to send name.\n");
	CLOSE_SOCKET(c -> socket);
	return false;
      }

      if (!(c -> get() == STKIP_REGISTER)){
	CLOSE_SOCKET(c -> socket);
	return false;
      }

      if (compatible_client_server(c -> version, NVERSION)){
	c -> send(STKIP_CONFIRM);
      }else{
	c -> send(STKIP_INVALID_VERSION);
	c -> get();
	CLOSE_SOCKET(c -> socket);
	fprintf(stdout, "accept: client has incompatible version!\n");
	return false;
      }
      
      if (!send_buffer_qid(c -> socket, (char*) &(c -> id), 4, BUFKEY_STKIP, c -> qid, STK_SERVER_TIMEOUT)){
	fprintf(stdout,"accept: client failed to read id.\n");
	CLOSE_SOCKET(c -> socket);
	return false;
      }

      serv -> add_client_td(c);
      fprintf(stdout,"server::listener: a client %s (id %d) has connected on socket %d.\n",c -> name, c -> id, c -> socket);
      return true;
    }
  } 
  return false;
}



void STK_IGC_thread::operator()(){
  STK_Inet_protocol p;

  if (!(td && gl)){
    fprintf(stdout, "IGC_thread: started w/o data ref!\n");
    return;
  }

  td -> tc_out = STKS_RUN;

  while (td -> tc_in == STKS_RUN){
    boost::mutex::scoped_lock l(*(gl -> mutex));
    switch (gl -> command){
    case STKIP_REQUIRE_GAMESTART:
      fprintf(stdout,"igc(%d)::gamestart.\n", td -> id);
      gamestart();
      break;
    case STKIP_REQUIRE_CHOICES:
      fprintf(stdout,"igc(%d)::load_c.\n", td -> id);
      load_choices();
      break;
    case STKIP_SEND_CHOICES:
      fprintf(stdout,"igc(%d)::send_c.\n", td -> id);
      send_choices();
      break;
    case STKIP_REQUIRE_UNIVERSE:
      fprintf(stdout,"igc(%d)::load_uv.\n", td -> id);
      load_universe();
      break;
    case STKIP_VALIDATE_UNIVERSE:
      fprintf(stdout,"igc(%d)::validate.\n", td -> id);
      validate_universe();
      break;
    case STKIP_STANDBY:
      fprintf(stdout,"igc(%d)::standby.\n", td -> id);
      if (td -> get() == STKIP_LEAVE_GAME){
	fprintf(stdout,"IGC thread(%d): left game!\n", td -> id);
	td -> tc_out = STKS_LEAVE_GAME;
	td -> send(STKIP_CONFIRM);
      }else{
	td -> send(STKIP_STANDBY);
      }
      break;
    }

    if (td -> connection_state == STKS_DEAD){
      td -> tc_out = STKS_DEAD;
      td -> tc_in = STKS_TERMINATE;
    }

    if (td -> tc_out != STKS_RUN){
      if (td -> tc_in == STKS_TERMINATE){
	//need to do end setup here
	td -> game_id = -1;
	td -> state = STKS_LFG;
	td -> tc_out = td -> connection_state == STKS_READY ? STKS_GAME_COMPLETE : STKS_DEAD;
      }
      
      return;
    }else{
      gl -> command = STKIP_STANDBY;
      l.unlock();
      fprintf(stdout, "igc(%d): unlocked.\n", td -> id);
      SDL_Delay(50);
      l.lock();
      fprintf(stdout, "igc(%d): locked.\n", td -> id);
    }
  }

  if (td -> tc_in == STKS_TERMINATE){
    if (td -> get() == STKIP_LEAVE_GAME){
      td -> send(STKIP_CONFIRM);
    }else{
      if (td -> inform_condition == STKIP_VICTORY){
	td -> send(STKIP_VICTORY);
      }else{
	td -> send(STKIP_LEAVE_GAME);
      }
    }
    td -> game_id = -1;
    td -> state = STKS_LFG;
  }

  td -> tc_out = td -> connection_state == STKS_READY ? STKS_GAME_COMPLETE : STKS_DEAD;

  fprintf(stdout, "igc_thread::(%d): exiting with tc_out = %d\n", td -> id, (int)(td -> tc_out));
}

bool STK_IGC_thread::require_protocol(STK_Inet_protocol p){

  STK_Inet_protocol q;

  while (td -> tc_in == STKS_RUN && !gl -> give_up){
    q = td -> get();
    if (q == p && !gl -> give_up){
      return true;
    }else if (q == STKIP_LEAVE_GAME){
      fprintf(stdout,"IGC thread(%d): left game!\n", td -> id);
      td -> tc_out = STKS_LEAVE_GAME;
      td -> send(STKIP_CONFIRM);
      return false;
    }else if (q == STKIP_STANDBY){
      td -> send(STKIP_CONFIRM);
    }else if (td -> connection_state == STKS_READY){
      fprintf(stdout, "igc_thread::require_protocol (client %d): got bad command: %d (expected %d), sending standby...\n", td -> id, (int)q, (int)p);
      td -> send(STKIP_STANDBY);
    }else if (td -> connection_state == STKS_DEAD){
      fprintf(stdout,"IGC thread(%d): dead!\n", td -> id);
      return false;
    }
    SDL_Delay(100);
  }

  if (gl -> give_up){
    fprintf(stdout,"IGC thread(%d): gave up.\n", td -> id);
  }else{
    fprintf(stdout,"IGC thread(%d): killed by server.\n", td -> id);
  }

  td -> connection_state = STKS_NOTREADY;
  gl -> give_up = false;
  return false;
}

void STK_IGC_thread::gamestart(){
  if (require_protocol(STKIP_REQUIRE_GAMESTART)){
    switch(td -> inform_condition){
    case STKIP_LOSS:
      td -> send(STKIP_LOSS);
      fprintf(stdout, "igc_thread::gamestart(): sent loss to client %d.\n", td -> id);
      td -> tc_out = STKS_LEAVE_GAME;
      break;
    case STKIP_VICTORY:
      td -> send(STKIP_VICTORY);
      fprintf(stdout, "igc_thread::gamestart(): sent victory to client %d.\n", td -> id);
      td -> tc_out = STKS_LEAVE_GAME;
      break;
    case STKIP_LEAVE_GAME:
      td -> send(STKIP_LEAVE_GAME);
      fprintf(stdout, "igc_thread::gamestart(): sent leave game to client %d.\n", td -> id);
      td -> tc_out = STKS_LEAVE_GAME;
      break;
    case STKIP_NONE:
      td -> send(STKIP_CONFIRM);
      td -> send((void*)gl -> rand_seed, sizeof(int));
      break;
    default:
      fprintf(stdout, "igc_thread::gamestart(): bad inform_condition for client %d: %d\n", td -> id, td -> inform_condition);
      td -> send(STKIP_LEAVE_GAME);
      td -> tc_out = STKS_LEAVE_GAME;
    }
    td -> inform_condition = STKIP_NONE;
    gl -> loaded = true;
  }
}

void STK_IGC_thread::load_choices(){
  if (require_protocol(STKIP_SEND_CHOICES)){
    if (!td -> send(STKIP_CONFIRM)){
      fprintf(stdout, "igc_thread::load_choices(): client denied confirmation.\n");
      return;
    }
    if (gl -> choice.load_data(td -> socket, STK_SERVER_TIMEOUT)){
      fprintf(stdout,"server_igc_thread(%d): loaded choice with %d cc and %d pc.\n", td -> id, gl -> choice.fleet_commands.size(), gl -> choice.planet_commands.size());
      gl -> loaded = true;
    }else{
      td -> connection_state = STKS_NOTREADY;
    }
  }
}

void STK_IGC_thread::send_choices(){
  int i, n;
  if (!gl -> c_list){
    return;
  }
  if (require_protocol(STKIP_REQUIRE_CHOICES)){
    td -> send(STKIP_CONFIRM);
    n = gl -> c_list -> size();
    td -> send((void*)&n, 4);
    for (i = 0; i < gl -> c_list -> size(); i++){
      if (!(*(gl -> c_list))[i].send_data(td -> socket, STK_SERVER_TIMEOUT)){
	td -> connection_state = STKS_NOTREADY;
	return;
      }
      fprintf(stdout,"server_igc_thread(%d): sent choice %d with %d cc and %d pc to client %d.\n", td -> id, i, (*(gl -> c_list))[i].fleet_commands.size(), (*(gl -> c_list))[i].planet_commands.size(), td -> id);
    }
  }
  gl -> loaded = true;
}
void STK_IGC_thread::load_universe(){
  if (require_protocol(STKIP_VALIDATE_UNIVERSE)){
    td -> send(STKIP_CONFIRM);
    if (gl -> universe.load_data(td -> socket, STK_SERVER_TIMEOUT)){
      gl -> loaded = true;
    }else{
      td -> connection_state = STKS_NOTREADY;
    }
  }
}

void STK_IGC_thread::validate_universe(){
  if (require_protocol(STKIP_REQUIRE_UNIVERSE)){
    td -> send(STKIP_CONFIRM);
    if (gl -> universe_valid){
      td -> send(STKIP_UNIVERSE_VALID);
      gl -> loaded = true;
    }else{
      gl -> loaded = td -> send(STKIP_UNIVERSE_INVALID) && gl -> valid_universe && gl -> valid_universe -> send_data(td -> socket, STK_SERVER_TIMEOUT);
      if (!gl -> loaded){
	td -> connection_state = STKS_NOTREADY;
      }
    }
  }
}

void STK_Spectate_buf_thread::setup(std::vector<STK_Client_thread_data*> *c, boost::mutex *m){
  ctd = c;
  mutex = m;
  tc_in = new int;
  tc_out = new int;
  *tc_in = STKS_RUN;
  *tc_out = STKS_INITIALIZING;
}

void STK_Spectate_buf_thread::terminate(){
  fprintf(stdout, "STK_Spectate_buf_thread::terminate(): waiting for thread to terminate...\n");
  *tc_in = STKS_TERMINATE;

  while (*tc_out == STKS_RUN){
    SDL_Delay(25);
  }

  delete tc_in;
  delete tc_out;
}

void STK_Spectate_buf_thread::operator()(){
  vector<STK_Client_thread_data*>::iterator i;

  *tc_out = STKS_RUN;
  fprintf(stdout, "spectate_buf_thread::(): start\n");
  while (*tc_in == STKS_RUN){
    {
      boost::mutex::scoped_lock l(*mutex);
      fprintf(stdout, "spectate_buf_thread::(): handling %d clients.\n", ctd -> size());
      for (i = ctd -> begin(); i != ctd -> end(); i++){
	if ((*i) -> state == STKS_SPECTATING){
	  (*i) -> get();
	  (*i) -> send(STKIP_STANDBY);
	}
      }
    }
    SDL_Delay(250);
  }
  fprintf(stdout, "spectate_buf_thread::(): end\n");

  *tc_out = STKS_TERMINATE;
}
