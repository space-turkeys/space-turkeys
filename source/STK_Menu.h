#ifndef _STK_MENU
#define _STK_MENU

#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Menu_references.h"

class STK_Menu: public RSK_GUI, public event_listener{
 protected:
  STK_Menu_references *m_ref;
  bool done;

 public:
  STK_Menu(STK_Menu_references *r);
  virtual void evoke();
};
#endif
