#include <ctime>

#include "STK_Definitions.h"
#include "RSK/RskGraphics.h"
#include "STK_Explosion.h"

STK_Randomizer STK_Explosion::randomizer;

STK_Explosion::STK_Explosion(intd p){

  randomizer.seed = time(0);
  randomizer.reset();

  a[0] = (10 * fd::normv(2 * PI * (randomizer.next()/(float)RAND_MAX))).toIntd();
  a[1] = (10 * fd::normv(2 * PI * (randomizer.next()/(float)RAND_MAX))).toIntd();

  c = 0;
  remove_me = false;
  position = p;
}

void STK_Explosion::iterate(){
  remove_me = c++ > 10;
}

void STK_Explosion::paint(SDL_Surface *s, intd p){
  for ( int i = 0; i < 2; i++){
    drawLineSafe(s, p - a[i], p + a[i], 1, fade(0xffaa88, (10 - c) / (float)10));
  }
}

STK_Large_explosion::STK_Large_explosion(intd p, unsigned int color, int n) : STK_Explosion(p){
  int i;
  float a;

  col = color;
  particles.resize(n);
  velocities.resize(n);

  for (i = 0; i < n; i++){
    particles[i] = fd(0,0);
    a = 2 * randomizer.next_float();
    velocities[i] = 10 * fd::normv(2 * PI * a);
    fprintf(stdout, "selected angle: %.3f\n", a);
  }
}

void STK_Large_explosion::iterate(){
  int i;

  for (i = 0; i < particles.size(); i++){
    particles[i] = particles[i] + velocities[i];
    velocities[i] = ((float).9) * velocities[i] + ((float)0.6) * fd::normv(2*PI*randomizer.next_float());
  }

  remove_me = c++ > 40;
}

void STK_Large_explosion::paint(SDL_Surface *s, intd p){
  int i;
  SDL_Rect r;
  int prad = 2;
  int alpha = 100 - 2 * c;
  float line_fade = 2 - c / (float)20;
  int line_alpha = (1700 - c*c) / 10;
  float line_length = 20 - c / 2;

  for (i = 0; i < particles.size(); i++){
    r.x = p.x + particles[i].x - prad;
    r.y = p.y + particles[i].y - prad;
    r.w = 2 * prad+1;
    r.h = 2 * prad+1;
    transparentRect(s, r, alpha, col);
    drawLineSafe(s, fd(p) + particles[i], fd(p) + particles[i] - line_length * velocities[i].normalize(), 1, fade(col & 0xffffff, line_fade)|(line_alpha<<24));
  }
}
