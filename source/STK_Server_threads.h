#ifndef _STK_SERVER_THREADS
#define _STK_SERVER_THREADS

#include <boost/thread/mutex.hpp>

#include "STK_Class_headers.h"
#include "STK_Keys.h"

struct STK_Client_thread{
  STK_Server* serv;
  STK_Client_thread_data* td;
  void operator()();
};

struct STK_IGC_thread{
  STK_Client_thread_data *td;
  STK_Game_loader *gl;
  void operator()();
  void gamestart();
  void load_choices();
  void send_choices();
  void load_universe();
  void validate_universe();
  bool require_protocol(STK_Inet_protocol p);
};

struct STK_Spectate_buf_thread{
  int *tc_in;
  int *tc_out;
  std::vector<STK_Client_thread_data*> *ctd;
  boost::mutex *mutex;

  void operator()();
  void terminate();
  void setup(std::vector<STK_Client_thread_data*> *ctd, boost::mutex *mutex);
};

struct STK_Game_thread{
  STK_Server* serv;
  STK_Game_thread_data *gtd;
  STK_Game_loader *gl;
  bool running;
  boost::mutex *mxp;
  int rand_seed;
  boost::mutex *specbuf_mutex;
  STK_Spectate_buf_thread specbuf_thread;

  STK_Game_thread(STK_Game_thread_data *g);
  ~STK_Game_thread();
  void operator()();
  int flush_clients();
  void setup_command(std::vector<STK_Client_thread_data*> ctd, STK_Inet_protocol p);
  int load_command(std::vector<STK_Client_thread_data*> *ctd, STK_Inet_protocol p, int cmax, int rsize);
  int count_loaded(std::vector<STK_Client_thread_data*> *ctd, int rsize);
  bool dismiss_client(int i);
  bool gamestart();
  bool send_choices();
  bool load_choices();
  bool load_universes();
  void validate_choice(STK_Choice* c, int id);
  bool validate_universes();
  void cleanup();
  int count_running();
  void quit();
};

class STK_Port_listener{
 public:
  STK_SERVER_STATES *tc_in,*tc_out;
  STK_Server *serv;
  static int cid_counter;

  STK_Port_listener();
  void operator()();
  bool accept_client();
};

#endif
