#ifndef _STK_UNIVERSE
#define _STK_UNIVERSE

#include <list>
#include <vector>
#include <SDL/SDL.h>

#include "STK_Class_headers.h"
#include "STK_Universe_settings.h"
#include "STK_Planet_master.h"
#include "STK_Ship_master.h"
#include "STK_Collision_map.h"
#include "STK_Stars.h"
#include "STK_Choice.h"

class STK_Universe{
 private:
  STK_Game *game;
  STK_Universe_settings settings;
  STK_Planet_master planet_master;
  STK_Ship_master ship_master;
  STK_Collision_map collision_map;
  std::vector<int> upgrades;
  int initialized;

  static SDL_Surface *buf_alpha;
  static SDL_Surface *buf_beta;
  static SDL_Surface *buf_gamma;
  static SDL_Surface *buf_delta;

  int get_idx(int id);

 public:
  STK_Stars stars;
  int show_points;
  
  STK_Universe();
  bool initialize(STK_Game* game, STK_Universe_settings s);
  void rebuild_references();
  void set_game_ref(STK_Game *g);
  void iterate();
  void apply_choices(std::vector<STK_Choice> c);
  void paint_grid(SDL_Surface *s);
  void paint(SDL_Surface* s, int pid);
  void paint_stats(SDL_Surface* s, int pid);
  STK_Planet_master *get_planet_master();
  STK_Ship_master *get_ship_master();
  STK_Collision_map *get_collision_map();
  STK_Universe_settings *get_settings();
  std::list<STK_Planet*> get_selected_planets();
  std::list<STK_Fleet*> get_selected_fleets();
  void clear_selection();
  void clear_choice();
  bool has_planets(int id);
  std::vector<int> *get_upgrades();
  void clear_upgrades();
  int get_points(int id);
  void add_points(int id, int points);
  void use_points(int id, int points);
  std::list<STK_Choice> get_residual_choices();
  bool load_ref_data(int sock, int timeout);
  bool load_data(int sock, int timeout);
  bool send_data(int sock, int timeout);
  bool differs(STK_Universe u);
  int calculate_size();
  void cleanup();

  static void load_image_bufs();
  static void free_image_bufs();
};
#endif
