#include <cstdio>
#include <SDL/SDL.h>

#include <boost/random/uniform_int.hpp>
#include <boost/random/mersenne_twister.hpp>

#include "STK_Player_info.h"
#include "RSK/RskGraphics.h"

using namespace std;

/* player_info */
int color_distance(int c1, int c2){
  SDL_Color a = make_sdl_color(c1);
  SDL_Color b = make_sdl_color(c2);
  return sqrt(pow((float)a.r - b.r, 2) + pow((float)a.g - b.g, 2) + pow((float)a.b - b.b, 2));
}

bool player_info::setup_colors(){
  //maximize minimum distance between player colors and black
  int best = 0;
  int i, j, k;
  int max_its = 10000;
  int* buf = new int[num_players];
  int d;

  boost::random::mt19937 rng;
  boost::random::uniform_int_distribution<unsigned int> dist(1, RAND_MAX);

  for (i = 0; i < max_its; i++){
    //generate random distribution
    for (j = 0; j < num_players; j++){
      buf[j] = dist(rng) | 0xff000000;
    }

    //find smallest distance
    int min = sqrt((float)3 * 256 * 256);
    for (j = 0; j < num_players; j++){
      for (k = j + 1; k < num_players; k++){
		d = color_distance(buf[j], buf[k]);
		if (d < min){
		  min = d;
		}
      }

      //check black and grey
      d = color_distance(buf[j], 0);
      if (d < min){
		min = d;
      }
      d = color_distance(buf[j], PLANET_NONE);
      if (d < min){
		min = d;
      }
    }

    //insert if appropriate
    if (min > best){
      best = min;
      for (j = 0; j < num_players; j++){
		player_colors[j] = buf[j];
      }
    }
  }

  delete [] buf;

  return best >= MIN_COLOR_DIST;
}

int player_info::get_color(int id){
  if (id == -1){
    return PLANET_NONE;
  }

  for (int i = 0; i < num_players; i++){
    if (players[i] == id){
      return player_colors[i];
    }
  }
  fprintf(stdout,"player_info::get_color(), %d players: bad id: %d.\n", num_players, id);

  return -1;
}
