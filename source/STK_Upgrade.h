#ifndef _STK_UPGRADE
#define _STK_UPGRADE

struct STK_Upgrade{
  int navigation;
  int warfare;
  int industry;
  int spec;

  static char* get_upgrade_name(int i);

  STK_Upgrade();
  int get_value(int i);
  void add(int i);
  void addSpecial(int i);
  char hasSpecial(int i);
  void clear();
};
#endif
