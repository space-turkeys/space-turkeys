#ifndef _STK_PLANET
#define _STK_PLANET

#include <vector>
#include <list>

#include "STK_Class_headers.h"
#include "STK_Command.h"

class STK_Planet{
 public:

  /* OBSERVE! IF YOU ALTER PLANET FIELDS (after position), YOU MUST ALTER SIZE SPECIFIER IN PLANET::F_SIZE, WHICH IS HARDCODED!!! */

  static float* multiplier;
  static int *def_bonus;
  static std::vector<int> *upgrade_events;
  static float research_complete;
  std::list<STK_Command> command_queue;
  std::list<STK_Command*> command_generation;
  STK_Player *p_owner;
  int is_selected;
  int shutdown;
  int inactive;

  //variables to be sent and loaded
  fd position;
  float radius;
  float productivity;
  float num_turkeys;
  int owner;
  int id;
  float research;
  float resource;

  const static int f_size = 36;

  bool operator == (STK_Planet a);
  void give_command(STK_Command c);
  bool load_data(char *buf, int *c, int max);
  bool send_data(char *buf, int *c, int max);
  float get_production();
  int calculate_size();
  void iterate(STK_Universe *u);
  std::list<STK_Command> retrieve_commands();
  void clear_retrieved_commands();
};
#endif
