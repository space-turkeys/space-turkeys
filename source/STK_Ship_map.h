#ifndef _STK_SHIP_MAP
#define _STK_SHIP_MAP

#include "STK_Class_headers.h"

class STK_Ship_map{
 public:
  STK_Universe_settings *uv_settings;
  STK_Ship** map;

  STK_Ship_map();
  void initialize(STK_Universe_settings* uvs);
  bool differs(STK_Ship_map m);
  void cleanup();
  void clear_map();
};
#endif
