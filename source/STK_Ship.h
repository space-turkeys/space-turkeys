#ifndef _STK_SHIP
#define _STK_SHIP

#include "RSK/RskTypes.h"
#include "STK_Base.h"
#include "STK_Ship_attributes.h"

class STK_Ship{
 private:
  fd position;

 public:
  int associated_id;
  STK_Ship_attributes attributes;
  float angle;
  int owner;
  int hp;
  float required_angle;
  STK_Ship_action action;
  int remove_me;
  int skip_me;

  int explode;

  STK_Fleet *fleet;
  STK_Collision_map *cmap;

  static int id_counter;
  const static int fsize = 9 * 4 + sizeof(STK_Ship_attributes) + sizeof(STK_Ship_action);

  STK_Ship();
  void initialize(STK_Ship_attributes attr, fd p, float a, int pl, STK_Fleet *fleet, STK_Collision_map *cm);
  fd get_position();
  void alter_position(fd d);
  void deal_damage(int war);
  bool operator==(STK_Ship a);
  void print_references();
  void print_techdata();
  bool load_data(char* buf, int* c, int max);
  bool send_data(char* buf, int* c, int max);
};
#endif
