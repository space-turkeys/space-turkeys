#ifndef _STK_SHIP_MASTER
#define _STK_SHIP_MASTER

#include <list>

#include <SDL/SDL.h>

#include "STK_Class_headers.h"
#include "STK_Ship_attributes.h"
#include "STK_Ship_settings.h"
#include "STK_Ship.h"
#include "STK_Fleet.h"
#include "STK_Structures.h"

class STK_Ship_master{
 private:
  STK_Collision_map *cmap;
  STK_Planet_master *planet_master;
  STK_Ship_settings ship_settings;
  std::list<STK_Fleet> fleets;
  std::list<STK_Ship> ships;
  int *rand;
  int c_rand;

 public:
  std::list<STK_Explosion*> explosions;

  STK_Ship_master();
  void initialize(STK_Ship_settings s, STK_Collision_map *cm, STK_Planet_master *pm);
  void iterate(STK_Game *game);
  void select_requests();
  void select_actions();
  void move();
  void evaluate_positions(STK_Game *game);
  STK_Fleet* add_fleet(STK_Ship_attributes a, STK_Command c);
  STK_Fleet* add_fleet(std::list<STK_Ship*> *ships, STK_Command c);
  STK_Fleet* get_fleet(int id);
  bool load_data(char* buf, int* c, int max);
  bool send_data(char* buf, int* c, int max);
  int calculate_size();
  int get_num_ships();
  std::list<STK_Ship> *get_fleet();
  std::list<STK_Fleet> *get_fleets();
  STK_Ship* get_ship(int assoc_id);
  STK_Ship_settings get_settings();
  void rebuild_references(STK_Collision_map *cm, STK_Planet_master *pm);
  void print_references();
  void assign(STK_Ship_master *m);
  void evaluate_fight(STK_Ship* a, STK_Ship* b);
  void cleanup_ships();
  void update_fleets();
  void remove_ship(STK_Ship* id);
  void paint(SDL_Surface *s, int pid);
  void paint_stats(SDL_Surface *s, int pid);
  bool differs(STK_Ship_master *m);
  void cleanup_explosions();
  void cleanup();
  bool hit_planet(STK_Ship *s, STK_Planet *p);
};
#endif
