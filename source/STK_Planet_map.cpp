#include <cstdio>
#include <list>
#include <SDL/SDL.h>
#include "STK_Planet_map.h"
#include "STK_Planet_master.h"
#include "STK_Universe_settings.h"
#include "STK_Planet.h"

using namespace std;

STK_Planet_map::STK_Planet_map(){
  map=0;
}

bool STK_Planet_map::initialize(STK_Universe_settings *uvs, STK_Planet_master* pm){
  /* Initializer for the planet map. Allocates map of size w x h. Observ that these should be the dimensions of the collision map, not of the universe. The map position values are set to -1 where there is no planet, and planet_id where there is a planet. Returns false on failure.*/
  list<STK_Planet*> planets = pm -> get_planets();
  list<STK_Planet*>::iterator i;
  float r;
  int x,y;
  int a;
  intd p;
  intd q;
  int w,h;

  if (!uvs){
    fprintf(stdout, "planet_map::initialize(): can not init w/o uv_settings!\n");
    return false;
  }

  uv_settings = uvs;
  w = uv_settings -> cmap_width();
  h = uv_settings -> cmap_height();
  cleanup();
  if (!(map=new STK_Planet*[w*h])){
    fprintf(stderr,"STK_Planet_map::initialize(): out of memory!\n");
    return false;
  }

  for (a=0; a<w*h; a++){
    map[a]=NULL;
  }

  for (i=planets.begin(); i != planets.end(); i++){
    p = (uv_settings -> ratio * (*i) -> position).toIntd();
    r = uv_settings -> ratio * (*i) -> radius;
    SDL_Rect check = create_sdl_rect(-2 * r, -2 * r, w + 2 * r, h + 2 * r);
    if (rect_contains(&check, p)){
      for (x=-r; x<=r; x++){
	a=sqrt(r*r-x*x);
	for (y=-a; y<=a; y++){
	  q.x=p.x+x;
	  q.y=p.y+y;
	  if (q.x >= 0 && (q.x < w) && q.y >= 0 && (q.y < h)){
	    map[q.y*w+q.x]=*i;
	  }
	}
      }
    }
  }
  return true;
}

bool STK_Planet_map::differs(STK_Planet_map m){
  return false;
}

void STK_Planet_map::cleanup(){
  if (map){
    delete [] map;
  }
  map=0;
}
