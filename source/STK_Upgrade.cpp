#include <cstdio>

#include "STK_Upgrade.h"
#include "STK_Definitions.h"

STK_Upgrade::STK_Upgrade(){
  /* Default constructor for the Upgrade class. */
  clear();
}

int STK_Upgrade::get_value(int i){
  switch(i){
  case PREF_INDUSTRY:
    return industry;
  case PREF_WARFARE:
    return warfare;
  case PREF_NAVIGATION:
    return navigation;
  }
}

void STK_Upgrade::add(int i){
  /* Called when the player has taken an upgrade. The value of the selected preference (defined by i) is increased by 1. */
  if (i==PREF_INDUSTRY){
    industry++;
  }else if (i==PREF_WARFARE){
    warfare++;
  }else if (i==PREF_NAVIGATION){
    navigation++;
  }else{
    fprintf(stdout,"upgrade::add(): attempted to add invalid upgrade!\n");
  }
}
void STK_Upgrade::addSpecial(int i){
  /* The player has recieved a special upgrade, which is entered into the spec variable. */
  spec|=i;
}
char STK_Upgrade::hasSpecial(int i){
  /* Returns whether the player has the special upgrade defined by i. */
  return i&spec;
}
void STK_Upgrade::clear(){
  /* Resets the upgrade object to initial state. */
  navigation=0;
  warfare=0;
  industry=0;
  spec=0;
}

char* STK_Upgrade::get_upgrade_name(int i){
  if (i==PREF_INDUSTRY){
    return (char*)"industry";
  }else if (i==PREF_WARFARE){
    return (char*)"warfare";
  }else if (i==PREF_NAVIGATION){
    return (char*)"navigation";
  }else{
    fprintf(stdout,"upgrade::add(): attempted to add invalid upgrade!\n");
    return (char*)"bad upgrade type";
  }
}
