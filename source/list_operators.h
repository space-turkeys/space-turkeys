#ifndef _LIST_OPERATORS
#define _LIST_OPERATORS

#include <list>
#include <SDL/SDL.h>
bool contains_key(std::list<SDLKey> l, SDLKey x);
std::list<int>::iterator operator +(std::list<int>::iterator j, int k);
#endif
