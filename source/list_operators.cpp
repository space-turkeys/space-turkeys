#include "list_operators.h"

using namespace std;

bool contains_key(list<SDLKey> a, SDLKey x){
  list<SDLKey>::iterator i;
  for (i = a.begin(); i != a.end(); i++){
    if (*i == x){
      return true;
    }
  }
  return false;
}


// template <class T>
// typename list<T>::iterator operator +(typename list<T>::iterator j, int k){
//   int i;
//   for (i = 0; i < k; i++){
//     j++;
//   }

//   return j;
// }

list<int>::iterator operator +(list<int>::iterator j, int k){
  int i;
  for (i = 0; i < k; i++){
    j++;
  }

  return j;
}
