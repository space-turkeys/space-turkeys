#ifndef _STK_EXPLOSION
#define _STK_EXPLOSION

#include <vector>
#include <SDL/SDL.h>

#include "RSK/RskTypes.h"
#include "STK_Randomizer.h"

class STK_Explosion{
 protected:
  intd a[4];
  int c;
  static STK_Randomizer randomizer;

 public:
  bool remove_me;
  intd position;

  STK_Explosion(intd p);
  virtual void iterate();
  virtual void paint(SDL_Surface* s, intd p);
};

class STK_Large_explosion : public STK_Explosion{
 private:
  unsigned int col;
  std::vector<fd> particles;
  std::vector<fd> velocities;

 public:
  STK_Large_explosion(intd p, unsigned int c, int n);
  void iterate();
  void paint(SDL_Surface *s, intd p);
};
#endif
