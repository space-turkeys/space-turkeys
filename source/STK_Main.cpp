#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cstring>

#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_UInterface.h"
#include "STK_Graphics.h"
#include "STK_Compatability.h"
#include "STK_Connection_settings_menu.h"

// void sighandle(int sig){
//   RSG_Component::close_font();
//   exit(0);
// }

int main(int argc, char **argv){

  /* creation and assignment of global variables */

  //random
  srand(time(NULL));

  int width = 0;
  int height = 0;
  int flag = SDL_HWSURFACE | SDL_FULLSCREEN;
  bool lan = false;
  int i;

  for (i = 1; i < argc; i++){
    if (!strcmp(argv[i], "--version")){
      fprintf(stdout, "version: %s\n", VERSION);
      fprintf(stdout, "compat. with server version from: %s\n", COMPAT_CLIENT_SERVER);
      fprintf(stdout, "compat. with client version from: %s\n", COMPAT_CLIENT_CLIENT);
      return 0;
    }else if (!strcmp(argv[i], "-w")){
      width = 800;
      height = 600;
      flag = SDL_HWSURFACE;
    }else if(!strcmp(argv[i], "-local")){
      lan = true;
    }else if(!strcmp(argv[i], "-help")){
      fprintf(stdout, "flags:\n-w: run in windowed mode\n-local: skip connection menu and start directly on local server\n");
      return 0;
    }
  }

  //SDL
  SDL_Surface* screen = initSDL(width, height, flag);
  width = screen -> w;
  height = screen -> h;

// #ifndef __WIN32__
//   //sighandle
//   signal(SIGTERM|SIGINT,sighandle);
// #endif

  //rsg component
  RSG_Component::init_font();

  //control vars
  STK_Control_vars control(screen);
  STK::ns_var=&control;

  // //uinterface
  STK_UInterface iface(width, height);

  // //stk_graphics
  STK_Graphics::init();

  // //universe data
  STK_Universe::load_image_bufs();

  if (lan){
    STK_Connection_settings_menu *cmen = (STK_Connection_settings_menu*)iface.m_ref.connection_settings_menu;
    cmen -> set_address("127.0.0.1");
    cmen -> connect();
  }else{
    iface.evoke();
  }

  STK_Graphics::quit();
  RSG_Component::close_font();
  STK_Universe::free_image_bufs();
  return 0;
}
