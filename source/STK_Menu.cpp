#include <SDL/SDL.h>

#include "STK_Menu.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"

STK_Menu::STK_Menu(STK_Menu_references *r){
  m_ref = r;
  done = false;
}

void STK_Menu::evoke(){
  SDL_Event e;
  done = false;

  while(SDL_PollEvent(&e)){}

  while (!done){
    while (SDL_PollEvent(&e)){
      handle_event(e);
    }
    
    SDL_FillRect(STK::ns_var -> screen, NULL, 0);
    paint(STK::ns_var -> screen);
    SDL_UpdateRect(STK::ns_var -> screen,0,0,0,0);
    SDL_Delay(100);
  }
  while(SDL_PollEvent(&e)){}
}
