#include <cstdlib>

#include "STK_Control_vars.h"
#include "STK_Collision_map.h"

void STK_Control_vars::center_at(fd map_pos){
  int sw = (screen -> w) / (2 * zoom);
  int sh = (screen -> h) / (2 * zoom);
  map_position = (map_pos - fd(sw,sh)).toIntd();
}

void STK_Control_vars::update(SDL_Event e){

  if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_F11){
    SDL_Surface sbuf = *screen;
    if (sbuf.flags & SDL_FULLSCREEN){
      sbuf.flags &= ~SDL_FULLSCREEN;
      fprintf(stdout, "control_vars: switching to windowed mode!\n");
    }else{
      sbuf.flags |= SDL_FULLSCREEN;
      fprintf(stdout, "control_vars: switching to fullscreen mode!\n");
    }
    SDL_Quit();
    if (!(screen = SDL_SetVideoMode(sbuf.w, sbuf.h, 32, sbuf.flags))){
      fprintf(stdout, "control_vars::update(): failed to switch window/fullscreen!\n");
      exit(1);
    }
  }

  if (e.type == SDL_KEYDOWN){
    pressed_keys.press(e.key.keysym.sym);
  }else if (e.type == SDL_KEYUP){
    pressed_keys.release(e.key.keysym.sym);
  }else if(e.type == SDL_MOUSEBUTTONDOWN){
    if (pressed_keys.is_pressed(SDLK_LCTRL)){
      if (e.button.button == 4){
	zoom_by(1.1);
      }else if(e.button.button == 5){
	zoom_by(1/(float)1.1);
      }
    }else{
      minimap_trigger = minimap_active && map != NULL && rect_contains(&minimap_rect, intd(e.button.x, e.button.y));
    }
  }else if(e.type == SDL_MOUSEBUTTONUP && minimap_active){
    minimap_goto(e.button.x, e.button.y);
    minimap_trigger = false;
  }else if (e.type == SDL_MOUSEMOTION && minimap_active){
    minimap_goto(e.motion.x, e.motion.y);
  }

  require_exit |= pressed_keys.is_pressed(SDLK_ESCAPE);
}

void STK_Control_vars::minimap_goto(int x, int y){
  if (minimap_trigger && map != NULL && rect_contains(&minimap_rect, intd(x, y))){
    int minix = x - minimap_rect.x;
    int miniy = y - minimap_rect.y;
    int mapx = (minix * map -> uv_settings -> width) / minimap_rect.w;
    int mapy = (miniy * map -> uv_settings -> height) / minimap_rect.h;
    int sw = (screen -> w) / (2 * zoom);
    int sh = (screen -> h) / (2 * zoom);
    map_position.x = mapx - sw;
    map_position.y = mapy - sh;
    mmove_vel = intd(0,0);
  }
}

bool STK_Control_vars::get_exit(){
  if (require_exit){
    require_exit = false;
    pressed_keys.release(SDLK_ESCAPE);
    return true;
  }

  return false;
}

void STK_Control_vars::iterate(){
  if (!pressed_keys.is_pressed(SDLK_LSHIFT)){
    if (pressed_keys.is_pressed(SDLK_UP)){
      mmove_vel.y -= 10;
    }else if(pressed_keys.is_pressed(SDLK_DOWN)){
      mmove_vel.y += 10;
    }

    if (pressed_keys.is_pressed(SDLK_RIGHT)){
      mmove_vel.x += 10;
    }else if(pressed_keys.is_pressed(SDLK_LEFT)){
      mmove_vel.x -= 10;
    }
  }

  if (pressed_keys.is_pressed(SDLK_PLUS)){
    zoom_by(1.1);
  }

  if (pressed_keys.is_pressed(SDLK_MINUS)){
    zoom_by(1/(float)1.1);
  }

  mmove_vel = (float).8 * mmove_vel;

  map_position = map_position + mmove_vel;
}

void STK_Control_vars::zoom_by(float c){
  fd map_center = (fd)map_position + ((fd)((float).5/zoom * fd(screen -> w, screen -> h)));
  zoom *= c;
  map_position = (map_center - ((fd)((float).5/zoom * fd(screen -> w, screen -> h)))).toIntd();
}

intd STK_Control_vars::map2screen(fd p){
  return (zoom * (p - fd(map_position))).toIntd();
}

intd STK_Control_vars::screen2map(intd p){
  return (((1/(float)zoom) * fd(p)) + fd(map_position)).toIntd();
}

intd STK_Control_vars::map2mini(fd p){
  if (map == NULL){
    return intd(0,0);
  }

  float factor = minimap_rect.w / (float)map -> uv_settings -> width;
  return (factor * p).toIntd();
}

fd STK_Control_vars::mini2map(intd p){
  if (map == NULL){
    return fd(0,0);
  }

  float factor = map -> uv_settings -> width / (float)minimap_rect.w;
  return fd(factor * p);
}

void STK_Control_vars::reset(){
  zoom=1;
  map_position=intd(0,0);
  require_exit=0;
  mmove_vel = intd(0,0);
  map = NULL;
  minimap_trigger = false;
  minimap_active = true;
}

STK_Control_vars::STK_Control_vars(SDL_Surface *s){
  screen = s;
  minimap_rect = create_sdl_rect(10, s -> h - 220, 200, 200);
  minimap_active = true;

  // paint modes
  command_paint_mode = PAINT_MODE_ALL;
  quant_paint_mode = PAINT_MODE_ALL;
  stats_paint_mode = PAINT_MODE_SELECTED;
  position_paint_mode = PAINT_MODE_SELECTED;
  grid_paint_mode = PAINT_MODE_ALL;
  
  reset();
}

void STK_Control_vars::clear(){
  minimap_trigger = false;
  mmove_vel = intd(0,0);
  pressed_keys.clear();
  require_exit = false;
  minimap_active = true;
}
