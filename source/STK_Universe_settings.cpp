#include <cstdio>
#include <cstring>
#include <SDL/SDL.h>

#include "RSK/RskGraphics.h"
#include "STK_Universe_settings.h"

STK_Universe_settings::STK_Universe_settings(){
  
  strcpy(name,"Universe Alpha");

  //map
  width=800;
  height=800;
  ratio=.25;

  //planets
  num_planets=20;
  planet_seperator=30;
  min_radius=10;
  max_radius=40;
  planet_defense=DIRICHLET_BASE / 2;
  planet_productivity=.02;
  planet_base_fleet = 10000;
  productivity_multiplier=5;

  //explosions
  explosion_num_particles=10;

  //stars
  num_stars=0;
  pixels_per_star=600;

  //game
  speed_of_light = 0;
  round_length = 200;
  use_fleets = true;
}

int STK_Universe_settings::calculate_signal_delay(float d){
  return speed_of_light > 0 ? (int)(d/(float)speed_of_light) : 0;
}

int STK_Universe_settings::cmap_width(){
  return ratio * width;
}

int STK_Universe_settings::cmap_height(){
  return ratio * height;
}

