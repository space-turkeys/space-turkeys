#include <cstdio>

#include "STK_Choice.h"
#include "STK_Fleet.h"
#include "STK_Ship_master.h"
#include "STK_Graphics.h"
#include "STK_Basic_communication.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Server_structs.h"

using namespace std;

bool STK_Choice::load_data(int sock, vector<STK_Planet> *planets, STK_Ship_master *sm, int timeout){
  if (!load_data(sock, timeout)){
    return false;
  }
  setup_references(planets, sm);
  return true;
}

void STK_Choice::print_selections(){
  list<STK_Command>::iterator i;

  fprintf(stdout,"choice::print_selections(): planet commands.\n");

  for (i = planet_commands.begin(); i != planet_commands.end(); i++){
    fprintf(stdout,"command from planet %d: %s\n", i -> source.associated_id, i -> is_selected ? "selected" : "deselected");
  }

  for (i = fleet_commands.begin(); i != fleet_commands.end(); i++){
    fprintf(stdout,"command from fleet %d: %s\n", i -> source.associated_id, i -> is_selected ? "selected" : "deselected");
  }
}

void STK_Choice::delete_selected(){
  list<STK_Command>::iterator i;

  for (i = planet_commands.begin(); i != planet_commands.end(); i++){
    if (i -> is_selected){
      i -> source.planet -> command_generation.remove(&*i);
      planet_commands.erase(i--);
    }
  }

  for (i = fleet_commands.begin(); i != fleet_commands.end(); i++){
    if (i -> is_selected){
      i -> source.fleet -> command_generation.remove(&*i);
      fleet_commands.erase(i--);
    }
  }
}

void STK_Choice::add_planet_command(STK_Command c){
  list<STK_Command>::iterator i;

  for (i = planet_commands.begin(); i != planet_commands.end(); i++){
    if (i -> matches(c)){
      fprintf(stdout,"choice::add_planet_command(): already exists!\n");
      return;
    }
  }
  
  planet_commands.push_back(c);
  c.source.planet -> command_generation.push_back(&planet_commands.back());
}

void STK_Choice::add_fleet_command(STK_Command c){
  list<STK_Command>::iterator i;

  for (i = fleet_commands.begin(); i != fleet_commands.end(); i++){
    if (i -> matches(c)){
      fprintf(stdout,"choice::add_fleet_command(): already exists!\n");
      return;
    }
  }
  
  fleet_commands.push_back(c);
  c.source.fleet -> command_generation.push_back(&fleet_commands.back());
}

list<STK_Command*> STK_Choice::get_selected_commands(){
  list<STK_Command*> l;
  list<STK_Command>::iterator i;

  for (i = planet_commands.begin(); i != planet_commands.end(); i++){
    if (i -> is_selected){
      l.push_back(&(*i));
    }
  }

  for (i = fleet_commands.begin(); i != fleet_commands.end(); i++){
    if (i -> is_selected){
      l.push_back(&(*i));
    }
  }

  return l;
}

void STK_Choice::lock_selected(){
  list<STK_Command*> l = get_selected_commands();
  list<STK_Command*>::iterator i;

  for (i = l.begin(); i != l.end(); i++){
    (*i) -> lock_qtty = (*i) -> quantity();
    (*i) -> is_locked = true;
  }

}

void STK_Choice::unlock_selected(){
  list<STK_Command*> l = get_selected_commands();
  list<STK_Command*>::iterator i;

  for (i = l.begin(); i != l.end(); i++){
    (*i) -> is_locked = false;
  }

}

void STK_Choice::increment_selected(int c){
  list<STK_Command*> l = get_selected_commands();
  list<STK_Command*>::iterator i;

  for (i = l.begin(); i != l.end(); i++){
    if (!(*i) -> is_locked){
      (*i) -> priority += c;
      if ((*i) -> priority < 0){
	(*i) -> priority = 0;
      }
      if ((*i) -> priority > 100){
	(*i) -> priority = 100;
      }
    }
  }
}

void STK_Choice::setup_references(vector<STK_Planet> *planets, STK_Ship_master *sm){
  list<STK_Command>::iterator i;

  for (i = planet_commands.begin(); i != planet_commands.end(); i++){
    
    if (i -> order.type == TARGET_PLANET){
      i -> order.planet = &(*planets)[i -> order.associated_id];
    }else if(i -> order.type == TARGET_FLEET){
      i -> order.fleet = sm -> get_fleet(i -> order.associated_id);
    }else if(i -> order.type == TARGET_POSITION){
    }else{
      fprintf(stdout,"choice::setup_references(): bad type.\n");
    }
    i -> source.planet = &(*planets)[i -> source.associated_id];
  }

  for (i = fleet_commands.begin(); i != fleet_commands.end(); i++){
    
    if (i -> order.type == TARGET_PLANET){
      i -> order.planet = &(*planets)[i -> order.associated_id];
    }else if(i -> order.type == TARGET_FLEET){
      i -> order.fleet = sm -> get_fleet(i -> order.associated_id);
    }else if(i -> order.type == TARGET_POSITION){
    }else{
      fprintf(stdout,"choice::setup_references(): bad type.\n");
    }
    i -> source.fleet = sm -> get_fleet(i -> source.associated_id);
  }
}

bool STK_Choice::load_data(int sock, int timeout){
  /* load choice data from socket WITHOUT setting up references. */
  int n_u, n_c, n_p, i;
  int x,y;
  int c;
  int max;
  char* buf;
  STK_Upgrade_choice upc;
  STK_Command com;

  if ((!(buf=load_buffer(sock,&max,BUFKEY_CHOICE, timeout))) || max<1){
    fprintf(stdout,"choice::load_data: failed to load buffer!\n");
    return false;
  }
  c=0;

  upgrades.clear();
  fleet_commands.clear();
  planet_commands.clear();

  //LOAD PLAYER
  if (c+16>max){fprintf(stdout,"choice::load_data: buffer overflow!\n"); delete [] buf; return false;}
  read_from_buf(buf+c,(char*)&player,4);
  c+=4;

  // //Load resign
  // read_from_buf(buf+c,(char*)&resign,4);
  // c+=4;

  //LOAD QUANTITIES
  read_from_buf(buf+c,(char*)&n_u,4);
  c+=4;
  read_from_buf(buf+c,(char*)&n_c,4);
  c+=4;
  read_from_buf(buf+c,(char*)&n_p,4);
  c+=4;

  if (n_u < 0 || n_c < 0 || n_p < 0){fprintf(stdout,"choice::load_data: invalid parameter!\n"); delete [] buf; return false;}
  if (c + n_u*sizeof(STK_Upgrade_choice) + (n_c + n_p)*STK_Command::fsize > max){fprintf(stdout,"choice::load_data: buffer overflow!\n"); delete [] buf; return false;}

  //LOAD UPGRADE DATA
  for (i = 0; i < n_u; i++){
    read_from_buf(buf+c,(char*)&upc, sizeof(STK_Upgrade_choice));
    c += sizeof(STK_Upgrade_choice);
    upgrades.push_back(upc);
    fprintf(stdout, "choice::load_data: upgrade: player %d, c %d\n", upc.player, upc.choice);
  }

  //LOAD FLEET COMMANDS
  for (i = 0; i < n_c; i++){
    // read_from_buf(buf+c,(char*)&com, sizeof(STK_Command));
    // c += sizeof(STK_Command);
    com.load_data(buf, &c, max);
    fleet_commands.push_back(com);
  }

  //LOAD PLANET COMMANDS
  for (i = 0; i < n_p; i++){
    // read_from_buf(buf+c,(char*)&com, sizeof(STK_Command));
    // c += sizeof(STK_Command);
    com.load_data(buf, &c, max);
    planet_commands.push_back(com);
    
  }
	  
  if (c != max){
    fprintf(stdout, "choice::load_data(): buf size does not match! Is %d, should be %d.\n", c, max);
    delete [] buf;
    return false;
    
  }
	  
  fprintf(stdout,"choice::load(): loaded %d fleet commands and %d planet commands.\n", n_c, n_p);

  delete [] buf;
  return true;
}

bool STK_Choice::send_data(int sock, int timeout){
  int n;
  int c;
  int max = calculate_size();
  char* buf;
  list<STK_Upgrade_choice>::iterator i;
  list<STK_Command>::iterator j;

  if (max<1){
    fprintf(stdout,"choice::send_data(): no data!\n");
    return false;
  }

  if (!(buf=new char[max])){
    fprintf(stdout, "choice::send_data(): failed to allocate!\n");
    return false;
  }

  
  
  c=0;

  //SEND PLAYER
  write_to_buf((char*)&player,buf+c,4);
  c+=4;

  //SEND QUANTITIES
  n=upgrades.size();
  write_to_buf((char*)&n,buf+c,4);
  c+=4;
  n=fleet_commands.size();
  write_to_buf((char*)&n,buf+c,4);
  c+=4;
  n=planet_commands.size();
  write_to_buf((char*)&n,buf+c,4);
  c+=4;

  

  //SEND UPGRADE DATA
  for (i = upgrades.begin(); i != upgrades.end(); i++){
    
    write_to_buf((char*)&(*i),buf+c,sizeof(STK_Upgrade_choice));
    c += sizeof(STK_Upgrade_choice);
  }

  //SEND FLEET COMMANDS
  for (j = fleet_commands.begin(); j != fleet_commands.end(); j++){
    // write_to_buf((char*)&(*j),buf+c,sizeof(STK_Command));
    // c += sizeof(STK_Command);
    
    j -> send_data(buf, &c, max);
  }

  

  //SEND PLANET COMMANDS
  for (j = planet_commands.begin(); j != planet_commands.end(); j++){
    // write_to_buf((char*)&(*j),buf+c,sizeof(STK_Command));
    // c += sizeof(STK_Command);
    
    j -> send_data(buf, &c, max);
  }

  fprintf(stdout,"choice: sent %d fleet commands and %d planet commands.\n",fleet_commands.size(), planet_commands.size());

  if (c!=max){
    fprintf(stdout,"choice::send_data: invalid size of buffer!\n");
    delete [] buf;
    return false;
  }
  if (send_buffer(sock,buf,max,BUFKEY_CHOICE,timeout) != STKS_READY){
    fprintf(stdout,"choice::send_data: failed to send buffer!\n");
    delete [] buf;
    return false;
  }
  delete [] buf;
  return true;
}

int STK_Choice::calculate_size(){
  return 16+upgrades.size()*sizeof(STK_Upgrade_choice)+(planet_commands.size()+fleet_commands.size())*STK_Command::fsize;
}

void STK_Choice::paint(SDL_Surface* s){

  list<STK_Command>::iterator i;

  for (i = fleet_commands.begin(); i != fleet_commands.end(); i++){
    switch(STK::ns_var -> command_paint_mode % PAINT_MODE_NUM){
    case PAINT_MODE_ALL:
      STK_Graphics::draw_command(s, *i);
      break;
    case PAINT_MODE_SELECTED:
      if (i -> is_selected || i -> source_selected()){
	STK_Graphics::draw_command(s,*i);
      }
      break;
    case PAINT_MODE_NONE:
      //do nothing
      break;
    }
  }

  for (i = planet_commands.begin(); i != planet_commands.end(); i++){
    switch(STK::ns_var -> command_paint_mode % PAINT_MODE_NUM){
    case PAINT_MODE_ALL:
      STK_Graphics::draw_command(s, *i);
      break;
    case PAINT_MODE_SELECTED:
      if (i -> is_selected || i -> source_selected()){
	STK_Graphics::draw_command(s,*i);
      }
      break;
    case PAINT_MODE_NONE:
      //do nothing
      break;
    }
  }
}

void STK_Choice::clear_selection(){

  list<STK_Command>::iterator i;

  for (i = fleet_commands.begin(); i != fleet_commands.end(); i++){
    i -> is_selected = false;
  }
  for (i = planet_commands.begin(); i != planet_commands.end(); i++){
    i -> is_selected = false;
  }
}

STK_Upgrade STK_Choice::total_upgrade(){
  list<STK_Upgrade_choice>::iterator i;
  STK_Upgrade u;

  for (i = upgrades.begin(); i != upgrades.end(); i++){
    switch(i -> choice){
    case PREF_INDUSTRY:
      u.industry++;
      break;
    case PREF_WARFARE:
      u.warfare++;
      break;
    case PREF_NAVIGATION:
      u.navigation++;
    }
  }

  return u;
}
