#ifndef _STK_CHOICE
#define _STK_CHOICE

#include <list>
#include <vector>

#include "STK_Class_headers.h"
#include "STK_Structures.h"
#include "STK_Planet.h"
#include "STK_Upgrade.h"

struct STK_Choice{
  int player;
  /* int resign; */
  std::list<STK_Upgrade_choice> upgrades;
  /* commands for fleets */
  std::list<STK_Command> fleet_commands;
  /* commands for planets */
  std::list<STK_Command> planet_commands;

  bool load_data(int sock, std::vector<STK_Planet>* p, STK_Ship_master *sm, int timeout);
  bool load_data(int sock, int timeout);
  bool send_data(int sock, int timeout);
  void print_selections();
  void delete_selected();
  void increment_selected(int i);
  void lock_selected();
  void unlock_selected();
  void add_planet_command(STK_Command c);
  void add_fleet_command(STK_Command c);
  std::list<STK_Command*> get_selected_commands();
  void setup_references(std::vector<STK_Planet>* p, STK_Ship_master *sm);
  void paint(SDL_Surface* s);
  void clear_selection();
  int calculate_size();
  STK_Upgrade total_upgrade();
};
#endif
