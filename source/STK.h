#include <vector>
#include <map>
#include <list>
#include <cstring>
//#include <unistd.h>
#include <cstdio>

#ifdef __WIN32__
#include <winsock2.h>
#include <Ws2tcpip.h>
#else
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <arpa/inet.h>
  #include <netdb.h>
  #include <poll.h>
  #include <signal.h>
#endif

#define BOOST_THREAD_USE_LIB

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <RSK/RSK_GUI_V2/RSK_GUI.h>

#ifndef _STK_H
#define _STK_H

/* #if __WORDSIZE == 64 */
/* #include <cstdint> */
/* #define int int32_t */
/* #endif */

#include "list_operators.h"
#include "STK_Definitions.h"
#include "STK_Class_headers.h"
#include "STK_Keys.h"
#include "STK_Basic_communication.h"
#include "STK_Randomizer.h"
#include "STK_Generate_choice_UI.h"
#include "STK_Ship_settings.h"
#include "STK_Player_info.h"
#include "STK_Universe_settings.h"
#include "STK_Game_settings.h"
#include "STK_Structures.h"
#include "STK_Menu_references.h"
#include "STK_Menu.h"
#include "STK_Connection_settings_menu.h"
#include "STK_Input_settings_menu.h"
#include "STK_Select_game_menu.h"
#include "STK_Local_game_menu.h"
#include "STK_Root_menu.h"
#include "STK_Wait_for_game_menu.h"
#include "STK_Upgrade.h"
#include "STK_Choice.h"
#include "STK_Explosion.h"
#include "STK_Planet.h"
#include "STK_Planet_field.h"
#include "STK_Planet_map.h"
#include "STK_Ship_map.h"
#include "STK_Collision_map.h"
#include "STK_Ship.h"
#include "STK_Graphics.h"
#include "STK_Planet_master.h"
#include "STK_Fleet.h"
#include "STK_Ship_master.h"
#include "STK_Stars.h"
#include "STK_Universe.h"
#include "STK_Control_vars.h"
#include "STK_Player.h"
#include "STK_Server_structs.h"
#include "STK_Client.h"
#include "STK_Game.h"
#include "STK_Server.h"
#include "STK_UInterface.h"
#include "STK_Server_threads.h"
#include "STK_Namespace.h"
#include "STK_Statistics.h"

#endif
