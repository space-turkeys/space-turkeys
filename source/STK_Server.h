#ifndef _STK_SERVER
#define _STK_SERVER

#include <list>
#include <vector>
#include <string>

#include "STK_Keys.h"
#include "STK_Networking_base.h"
#include "STK_Server_structs.h"

class STK_Server{
 private:
  STK_SERVER_STATES* tc_in;
  STK_SERVER_STATES* tc_out;
  STK_SERVER_STATES plisten_in, plisten_out;
  sockaddr_in server_info;
  int gid_counter, uid_counter, gtdid_counter;
  int max_games;
  int max_clients;

  std::list<STK_Game_thread_data*> game_data;
  std::list<STK_Client_thread_data*> client_data;
  std::vector<STK_S_Game_info> game_info;

 public:
  int port_number;
  int main_socket;

  STK_Server(STK_SERVER_STATES* i, STK_SERVER_STATES* o);
  void cleanup();
  bool open();
  void operator()();
  void remove_client_from_game(STK_Client_thread_data *c);
  void create_game(STK_Client_thread_data *c);
  void leave_game(STK_Client_thread_data *c);
  void join_game(STK_Client_thread_data *c);
  void spectate_game(STK_Client_thread_data *c);
  void require_games(STK_Client_thread_data *c);
  void require_game_info(STK_Client_thread_data *c);
  void start_game(STK_Client_thread_data *c);
  void require_game_data(STK_Client_thread_data *c);
  void read_request(STK_Inet_protocol p, STK_Client_thread_data *d);
  void check_game_info_states();
  void check_game_threads();
  void check_client_threads();
  void terminate_game_threads();
  void terminate_client_threads();
  void add_client_td(STK_Client_thread_data *d);
  void remove_client_td(int id);
  STK_S_Game_info* get_game_info(int id);
  STK_Client_thread_data* get_client_td(int id);
  STK_Game_thread_data* get_game_td(int id);
  void destroy_client_thread(int id);
  bool clients_full();
  bool games_full();
  bool name_exists(std::string v);
};
#endif
