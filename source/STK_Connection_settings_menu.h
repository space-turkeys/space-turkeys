#ifndef _STK_CONNECTION_SETTINGS_MENU
#define _STK_CONNECTION_SETTINGS_MENU

#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Menu.h"

class STK_Connection_settings_menu : public STK_Menu{
 private:
  RSG_Textfield *input_address, *input_name;
  RSG_Label *label_address, *label_name;
  RSG_Button *button_done;
  RSG_Button *button_back;

 public:
  STK_Connection_settings_menu(STK_Menu_references *r);
  ~STK_Connection_settings_menu();
  void listen(SDL_Event e, RSG_Component *c);
  void paint(SDL_Surface *s);
  void connect();
  void set_address(const char* v);
};
#endif
