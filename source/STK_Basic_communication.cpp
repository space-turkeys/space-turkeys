#include <cstdio>
#ifndef __WIN32__
#include <unistd.h>
#endif
#include <cstring>

#include "STK_Networking_base.h"

using namespace std;

#ifdef __WIN32__
STK_SERVER_STATES lsend(int socket, void* v_data, int size, int timeout){
  int c=0;
  char *data = (char*)v_data;
  STK_SERVER_STATES r;

  setsockopt(socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(int));

  while(size>0){
    c = send(socket, (char*)data, size, 0);
    
    if (c == SOCKET_ERROR){
      fprintf(stdout, "lsend: error: %s\n", WSAGetLastError());
      return STKS_NOTREADY;
    }else if (c<1){
      fprintf(stdout,"lsend: failed to write.\n");
      return STKS_NOTREADY;
    }
    
    size -= c;
    data += c;
  }
  return STKS_READY;
}

STK_SERVER_STATES lget(int socket, void* v_data, int size, int timeout){
  int c = 0;
  char *data = (char*)v_data;
  STK_SERVER_STATES r;

  setsockopt(socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(int));
  
  while (size > 0){
	  c = recv(socket, data, size, 0);
	  if (c == SOCKET_ERROR){
		fprintf(stdout, "lget: error: %s\n", WSAGetLastError());
		return STKS_NOTREADY;
	  }else if (!c){
		fprintf(stdout,"lget: connection closed.\n");
		return STKS_NOTREADY;
	  }
      size -= c;
      data += c;
  }

  return STKS_READY;
}

#else
STK_SERVER_STATES lsend(int socket, void* data, int size, int timeout){
  int c=0;
  STK_SERVER_STATES r;
  while(size>0){
    if ((r=ready_out(socket,timeout))!=STKS_READY){
      fprintf(stdout,"lsend: ready_out returns state %d.\n",(int)r);
      return r;
    }
    c=write(socket,data,size);
    if (c<1){
      fprintf(stdout,"lsend: wrote only 0 bytes (%d required), exiting.\n",size);
      return STKS_NOTREADY;
    }
    size-=c;
    data=(void*)((char*)data+c);
  }
  return STKS_READY;
}

STK_SERVER_STATES lget(int socket, void* data, int size, int timeout){
  int c=0;
  STK_SERVER_STATES r;
  while(size>0){
    if ((r=ready_in(socket,timeout))!=STKS_READY){
      fprintf(stdout,"lget: ready_in returns state %d.\n",(int)r);
      return r;
    }
    c=read(socket,data,size);
    if (c<1){
      fprintf(stdout,"lget: read only 0 bytes (%d required), exiting.\n",size);
      return STKS_NOTREADY;
    }
    size-=c;
    data=(void*)((char*)data+c);
  }
  return STKS_READY;
}


STK_SERVER_STATES ready_in(int socket, int millis){
  pollfd pfd;
  pfd.fd=socket;
  pfd.events=POLLIN;
  int err=poll(&pfd,1,millis);
  if (err<0){
    fprintf(stdout,"ready_in: error %d occured.\n",err);
    return STKS_DEAD;
  }else if (pfd.revents&(POLLERR|POLLHUP|POLLNVAL)){
    fprintf(stdout,"ready_in: following events occured:\n");
    if (pfd.revents&POLLERR){fprintf(stdout,"POLLERR,");}
    if (pfd.revents&POLLHUP){fprintf(stdout,"POLLHUP,");}
    if (pfd.revents&POLLNVAL){fprintf(stdout,"POLLNVAL,");}
    fprintf(stdout,"\n");
    return STKS_DEAD;
  }else if (err==0){
    fprintf(stdout,"ready_in: timed out.\n");
    return STKS_TIMEOUT;
  }else if (pfd.revents&POLLIN){
    return STKS_READY;
  }else{
    fprintf(stdout,"ready_in: not ready.\n");
    return STKS_NOTREADY;
  }
}

STK_SERVER_STATES ready_out(int socket, int millis){
  pollfd pfd;
  pfd.fd=socket;
  pfd.events=POLLOUT;
  int err=poll(&pfd,1,millis);
  if (err<0){
    fprintf(stdout,"ready_out: error %d occured.\n",err);
    return STKS_DEAD;
  }else if (pfd.revents&(POLLERR|POLLHUP|POLLNVAL)){
    fprintf(stdout,"ready_out: following events occured:\n");
    if (pfd.revents&POLLERR){fprintf(stdout,"POLLERR,");}
    if (pfd.revents&POLLHUP){fprintf(stdout,"POLLHUP,");}
    if (pfd.revents&POLLNVAL){fprintf(stdout,"POLLNVAL,");}
    fprintf(stdout,"\n");
    return STKS_DEAD;
  }else if (err==0){
    fprintf(stdout,"ready_out: timed out.\n");
    return STKS_TIMEOUT;
  }else if (pfd.revents&POLLOUT){
    return STKS_READY;
  }else{
    fprintf(stdout,"ready_out: not ready.\n");
    return STKS_NOTREADY;
  }
}
#endif


void write_to_buf(char* source, char* buf, int size){
  //for (int i=0; i<size; i++){buf[i]=source[i];}
  memcpy((void*)buf, (void*)source, size);
}

void read_from_buf(char* buf, char* dest, int size){
  //for (int i=0; i<size; i++){dest[i]=buf[i];}
  memcpy((void*)dest, (void*)buf, size);
}
