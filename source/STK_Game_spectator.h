#ifndef _STK_GAME_SPECTATOR
#define _STK_GAME_SPECTATOR

#include "STK_Game.h"

class STK_Game_spectator : public STK_Game{
 public: 

  void evoke();
  void exit_interface();
  bool wait_for_choices(std::vector<STK_Choice> *l);
  void evaluate_event(SDL_Event e);
};
#endif
