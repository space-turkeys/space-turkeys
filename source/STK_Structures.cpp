#include <list>
#include <vector>
#include <string>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "STK_Structures.h"
#include "list_operators.h"
#include "STK_Keys.h"
#include "STK_Client.h"
#include "STK_Networking_base.h"

using namespace std;

e_pfield::e_pfield(){
  angle = 0;
  length = 0;
  p = NULL;
}

void STK_Pressed_keys::press(SDLKey s){
  if (!contains_key(keys,s)){
    keys.push_back(s);
  }
}

void STK_Pressed_keys::release(SDLKey s){
  keys.remove(s);
}

bool STK_Pressed_keys::is_pressed(SDLKey s){
  return contains_key(keys,s);
}

void STK_Pressed_keys::clear(){
  keys.clear();
}

void STK_Standby_mode::operator()(){
  *state = STKS_RUN;

  while (*command == STKS_RUN){
    client -> confirm_command(STKIP_STANDBY);
    SDL_Delay(500);
  }

  *state = STKS_TERMINATE;
}

void STK_Standby_mode::terminate(){
  *command = STKS_TERMINATE;
  
  while (*state != STKS_TERMINATE){
    SDL_Delay(25);
  }
}

void STK_Standby_mode::start(){
  *command = STKS_RUN;

  boost::thread t(*this);
  
  while (*state != STKS_RUN){
    SDL_Delay(25);
  }
}

bool STK_Ship_order::load_data(char *buf, int *c, int max){
  if (*c + fsize > max){
    return false;
  }
  read_from_buf(buf + *c, (char*)&position, 8);
  *c += 8;
  read_from_buf(buf + *c, (char*)&type, 4);
  *c += 4;
  read_from_buf(buf + *c, (char*)&associated_id, 4);
  *c += 4;

  fprintf(stdout, "ship_order: load: type %d, id %d.\n", type, associated_id);
  return true;
}

bool STK_Ship_order::send_data(char *buf, int *c, int max){
  if (*c + fsize > max){
    return false;
  }
  write_to_buf((char*)&position, buf + *c, 8);
  *c += 8;
  write_to_buf((char*)&type, buf + *c, 4);
  *c += 4;
  write_to_buf((char*)&associated_id, buf + *c, 4);
  *c += 4;

  fprintf(stdout, "ship_order: send: type %d, id %d.\n", type, associated_id);
  return true;
}

void select_game_updater::operator()(){
  vector<STK_Game_info> game_buf;

  if (*tc_in != STKS_RUN){
    fprintf(stdout, "select_game_updater: not starting.\n");
    *tc_out = STKS_TERMINATE;
    return;
  }

  boost::mutex::scoped_lock lock(*mutex);

  lock.unlock();
  *tc_out = STKS_RUN;
  while (*tc_in == STKS_RUN){
    if (client -> get_games_list(&game_buf)){
      vector<vector<string> > data(game_buf.size() + 1);
      char s[128];
    
      data[0].resize(1);
      data[0][0] = "Available games, #players";
      for (int i = 1; i < data.size(); i++){
	data[i].resize(1);
	sprintf(s,"%s (id: %d)",game_buf[i - 1].settings.name,game_buf[i - 1].num_players);
	data[i][0] = s;
      }

      lock.lock();
      table -> update_content(data);
      *games = game_buf;
      lock.unlock();
    }
    SDL_Delay(500);
  }

  lock.lock();
  *tc_out = STKS_TERMINATE;
}

void select_game_updater::terminate(){
  *tc_in = STKS_TERMINATE;
  boost::mutex::scoped_lock lock(*mutex);
  table -> cleanup();
  lock.unlock();

  while(*tc_out != STKS_TERMINATE){
    fprintf(stdout, "waiting for updater thread to terminate...\n");
    SDL_Delay(500);
  }

  lock.lock();
}

void select_game_updater::start(){
  *tc_in = STKS_RUN;
  boost::thread t(*this);
}

STK_Game_info::STK_Game_info(){
  num_players = 0;
  game_state = STKS_READY;
}

void wfg_updater::operator()(){
  if (*tc_in != STKS_RUN){
    fprintf(stdout, "select_game_updater: not starting.\n");
    *tc_out = STKS_TERMINATE;
    return;
  }

  STK_Game_info game_info;

  boost::mutex::scoped_lock lock(*mutex);

  lock.unlock();
  *tc_out = STKS_RUN;
  while (*tc_in == STKS_RUN){
    fprintf(stdout, "wfg_updater: main loop\n");
    lock.lock();
    game_info = client -> get_game_info(game_id);
    *game_state = game_info.game_state;

    switch(game_info.game_state){
    case STKS_PENDING:
      indicator -> set_text("GAME_STATE: PENDING");
      break;
    case STKS_BUILDING:
      indicator -> set_text("GAME_STATE: BUILDING");
      break;
    case STKS_LOADING:
      indicator -> set_text("GAME_STATE: LOADING");
      break;
    case STKS_BUILD_FAILED:
      indicator -> set_text("GAME_STATE: BUILD FAILED!");
      break;
    }
    lock.unlock();
    fprintf(stdout, "wfg_updater: post indicator\n");

    if (game_info.num_players > 0 && game_info.game_state != -1){
      if (game_info.game_state == STKS_LOADING){
	fprintf(stdout, "wfg_updater: set loading!\n");
	*loading = true;
	break;
      }
      vector<vector<string> > s;

      s.resize(game_info.num_players + 1);
      s[0].resize(1);
      s[0][0] = "players";
      fprintf(stdout,"wfg::update_list: %d players.\n",game_info.num_players);
      for (int i = 0; i < game_info.num_players; i++){
	s[i+1].resize(1);
	s[i+1][0] = game_info.names[i];
      }

      lock.lock();
      table -> init(s);
      lock.unlock();
    }else{
      fprintf(stdout, "wfg_updater: ended: %s\n", game_info.game_state == -1 ? "failed to load game info" : "no players in game");
      *end = true;
    }

    fprintf(stdout, "wfg_updater: post update\n");

    SDL_Delay(500);
  }

  fprintf(stdout, "wfg_updater: exit\n");
  *tc_out = STKS_TERMINATE;
  lock.lock();
}


void wfg_updater::terminate(){
  *tc_in = STKS_TERMINATE;

  while(*tc_out != STKS_TERMINATE){
    fprintf(stdout, "waiting for updater thread to terminate...\n");
    SDL_Delay(500);
  }
}


void wfg_updater::start(){
  *tc_in = STKS_RUN;
  *loading = false;
  *end = false;
  boost::thread t(*this);
}
