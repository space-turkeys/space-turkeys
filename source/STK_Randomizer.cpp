#include "STK_Randomizer.h"
#include "STK_Definitions.h"

STK_Randomizer::STK_Randomizer(){
  dist = new boost::random::uniform_int_distribution<unsigned int>(0,RAND_MAX);
}

STK_Randomizer::~STK_Randomizer(){
  delete dist;
}

void STK_Randomizer::reset(){
  rng.seed(seed);
}

unsigned int STK_Randomizer::next(){
  return (*dist)(rng);
}

float STK_Randomizer::next_float(){
  return (*dist)(rng) / (float)RAND_MAX;
}
