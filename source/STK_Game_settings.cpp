#include "STK_Game_settings.h"

STK_Game_settings::STK_Game_settings(){
  fullscreen = true;
  use_smudge = true;
  animate_explosions = true;
  frame_rate = 20;
}
