#include "STK_Ship_attributes.h"
#include "STK_Player.h"
	
STK_Ship_attributes::STK_Ship_attributes(STK_Player *p, STK_Ship_settings s){	
  color = p -> color;
  speed = s.ship_speed + p -> upgrade.navigation/(float)2;
  base_hp = 2 + p -> upgrade.warfare/10;
  warfare = p -> upgrade.warfare;
}

bool STK_Ship_attributes::operator == (STK_Ship_attributes a){
  return speed == a.speed && base_hp == a.base_hp && color == a.color && warfare == a.warfare;
}
