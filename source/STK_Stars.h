#ifndef _STK_STARS
#define _STK_STARS

#include <SDL/SDL.h>
#include "RSK/RskTypes.h"

class STK_Stars{
 private:
  int num;
  intd* stars;

 public:
  STK_Stars();
  void initialize(intd size);
  void paint(SDL_Surface *s);
  void cleanup();
};
#endif
