#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "STK_Server.h"
#include "STK_Namespace.h"
#include "STK_Universe.h"
#include "STK_Server_threads.h"
#include "STK_Compatability.h"

using namespace std;

/* Clients must have ID since game info objects track which clients they have. */
STK_Server::STK_Server(STK_SERVER_STATES* i, STK_SERVER_STATES* o){
  tc_in=i;
  tc_out=o;
  errno=0;
  port_number=0;
  main_socket=0;
  gid_counter=0;
  gtdid_counter=0;
  uid_counter=0;
  max_games = 128;
  max_clients = 1024;
}

void STK_Server::cleanup(){
  int x,c;
  bool done=false;
  list<STK_Client_thread_data*>::iterator i;
  list<STK_Game_thread_data*>::iterator j;

  boost::mutex::scoped_lock lock1(STK::info_lock);
  boost::mutex::scoped_lock lock4(STK::game_info_lock);
  boost::mutex::scoped_lock lock5(STK::universe_info_lock);
  terminate_game_threads();
  terminate_client_threads();
  
  boost::mutex::scoped_lock lock2(STK::game_data_lock);
  boost::mutex::scoped_lock lock3(STK::client_data_lock);
  plisten_in=STKS_TERMINATE;
  c=0;
  while(!done && c++<100 && *tc_in!=STKS_TERMINATE_NOW){
    if (plisten_out==STKS_TERMINATE){done=true;}
    SDL_Delay(100);
  }
  if (!done){
    fprintf(stdout,"server::cleanup(): listener failed to terminate.\n");
  }
  
  for (i = client_data.begin(); i != client_data.end(); i++){
    if (*i){
      delete *i;
    }
  }
  for (j = game_data.begin(); j != game_data.end(); j++){
    if (*j){
      delete *j;
    }
  }
  client_data.clear();
  game_data.clear();
  for (x = 0; x < game_info.size(); x++){
    game_info[x].cleanup();
  }
  game_info.clear();

#ifdef __WIN32__
  WSACleanup();
#endif
      
  port_number=0;
  main_socket=0;
  gid_counter=0;
  uid_counter=0;
}

bool STK_Server::open(){
  /* Begin listening for connections on the selected port. Returns true on success or false on failure. */
  cleanup();
  boost::mutex::scoped_lock lock(STK::info_lock);
  
  
  // Step 0: init winsock if on windows
#ifdef __WIN32__
    WSADATA wsa_data;
    int iResult = WSAStartup(MAKEWORD(2,2), &wsa_data);
    if (iResult != NO_ERROR) {
      printf("WSAStartup failed: %d\n", iResult);
      return 1;
    }
#endif
  
  port_number=STK_MAINPORT;
  main_socket=socket(AF_INET,SOCK_STREAM,0);
  if (main_socket<0){
    fprintf(stdout,"server: failed to create main socket\n");
    return false;
  }
  server_info.sin_family = AF_INET;
  server_info.sin_addr.s_addr = htonl(INADDR_ANY);
  server_info.sin_port = htons(port_number);

  if (::bind(main_socket,(sockaddr*)&server_info,sizeof(server_info))<0){
    fprintf(stdout,"server: failed to bind main socket(port in use)\n");
    return false;
  }
  fprintf(stdout,"socket bound to port %d\n",port_number);
  
  if (listen(main_socket,5)<0){
    fprintf(stdout,"server: failed to listen\n");
    return false;
  }

  fprintf(stdout,"server: listening...\n");
  srand(time(NULL));
  return true;
}

void STK_Server::remove_client_from_game(STK_Client_thread_data *c){
  STK_S_Game_info *g;
  int i;

  boost::mutex::scoped_lock lock(STK::game_info_lock);

  if (c->game_id>-1){
    if (!(g = get_game_info(c->game_id))){
      c -> game_id = -1;
      return;
    }
 
    if (c -> id == g -> root_client()){
      fprintf(stdout, "server: root client leaves game %d, destroying!\n",g -> id);

      while (g -> game_state == STKS_BUILDING){
	SDL_Delay(50);
      }

      for (i = 0; i < g -> num_players; i++){
	g -> players[i] -> game_id = -1;
      }
      g -> cleanup();
    }else{
      g->remove_client(c->id);
      c->game_id=-1;
    }
  }
}

void STK_Server::create_game(STK_Client_thread_data *c){
  STK_S_Game_info h, *g;

  /* --------------------------------- */
  /* ----------CREATE A GAME---------- */
  /* --------------------------------- */
  if(games_full() ){
    fprintf(stdout,"client %s (%d) may not create game, max games reached!\n",c->name,c->id);
    c->send(STKIP_DENY);
  }else{
    remove_client_from_game(c);
  
    if(!c->send(STKIP_CONFIRM)){
      fprintf(stdout,"server: send failed.\n"); return; 
    }
    if (!c->get((void*)&h.settings,sizeof(STK_Universe_settings))){
      fprintf(stdout,"server: client %s required create game, but does not send valid data.\n",c->name);
      return;
    }

    boost::mutex::scoped_lock lock(STK::game_info_lock);
    h.game_state=STKS_PENDING;
    h.num_players=0;
    h.add_client(c);
    h.initialized=1;
    h.id=gid_counter++;
    h.universe = 0;
    c->game_id=h.id;
    c->state=STKS_PENDING;
    game_info.insert(game_info.end(),h);
    if (!c->send((void*)&h.id,4)){
      fprintf(stdout,"server: client %s required create game, but does not recieve all data.\n",c->name);
      return;
    }

    fprintf(stdout,"server: client %s (%d) created game %d with universe '%s' and %d players.\n",c->name,c->id,c->game_id, h.settings.name, h.num_players);
  }
}

void STK_Server::leave_game(STK_Client_thread_data *c){
  STK_S_Game_info *g;
  int i;

  /* --------------------------------- */
  /* ----------LEAVE A GAME---------- */
  /* --------------------------------- */

  remove_client_from_game(c);
  c -> send(STKIP_CONFIRM);
  fprintf(stdout,"server: client %s (%d) leaves game %d\n",c->name,c->id,c->game_id);
}

void STK_Server::join_game(STK_Client_thread_data *c){
  STK_S_Game_info *g;
  STK_Inet_protocol p;
  int gid, j;

  /* --------------------------------- */
  /* ----------JOIN A GAME---------- */
  /* --------------------------------- */
  if (c->game_id>-1){
    fprintf(stdout,"client %s (%d) in game %d required to join a game!\n",c->name,c->id, c->game_id);
    remove_client_from_game(c);
  }

  if(!c->send(STKIP_CONFIRM)){
    fprintf(stdout,"server: send failed.\n"); 
    return; 
  }

  if (!c->get((void*)&gid,4)){
    fprintf(stdout,"server: client %s failed to specify which game to join.\n",c->name);
    return;
  }

  g=get_game_info(gid);

  if (g){
    for (j = 0; j < g -> num_players; j++){
      if (!(compatible_client_client(get_client_td(g -> player_id[j]) -> version, c -> version))){
	fprintf(stdout, "client %s (%d) of version %d required to join game %d, which has client %d of incompatible version %d!\n", c -> name, c -> id, c -> version, g -> id, g -> player_id[j], get_client_td(g -> player_id[j]) -> version);
	c -> send(STKIP_INVALID_VERSION);
	return;
      }
    }
  }
  
  if (g!=NULL && g->game_state==STKS_PENDING){
    if (!(g->add_client(c))){
      fprintf(stdout,"client %s (%d) can not join game %d because it is full!\n",c->name,c->id,gid);
      p=STKIP_DENY;
    }else{
      c->game_id=gid;
      c->state=STKS_PENDING;
      p=STKIP_CONFIRM;
      fprintf(stdout,"client %s (%d) has joined game %d!\n",c->name,c->id,c->game_id);
    }
  }else{
    fprintf(stdout,"client %s (%d) can not join game %d because it is already running.\n",c->name,c->id,gid);
    p=STKIP_DENY;
  }
  if(!c->send(p)){
    fprintf(stdout,"server: send failed.\n");
    return; 
  }
}

void STK_Server::spectate_game(STK_Client_thread_data *c){
  STK_Game_thread_data *g;
  STK_Inet_protocol p;
  int gid, j;

  /* --------------------------------- */
  /* ---------SPECTATE A GAME--------- */
  /* --------------------------------- */
  if (c->game_id>-1){
    fprintf(stdout,"client %s (%d) in game %d required to join a game!\n",c->name,c->id, c->game_id);
    remove_client_from_game(c);
  }

  if(!c->send(STKIP_CONFIRM)){
    fprintf(stdout,"server: send failed.\n"); 
    return; 
  }

  if (!c->get((void*)&gid,4)){
    fprintf(stdout,"server: client %s failed to specify which game to join.\n",c->name);
    return;
  }

  if (!(g = get_game_td(gid))){
    c -> send(STKIP_DENY);
    return;
  }

  if (g -> tc_out != STKS_RUN){
    c -> send(STKIP_DENY);
    return;
  }

  for (j = 0; j < g -> ctd.size(); j++){
    if (!(compatible_client_client(get_client_td(g -> ctd[j] -> id) -> version, c -> version))){
      fprintf(stdout, "client %s (%d) of version %d required to spectate game %d, which has client %d of incompatible version %d!\n", c -> name, c -> id, c -> version, g -> id, g -> ctd[j] -> id, get_client_td(g -> ctd[j] -> id) -> version);
      c -> send(STKIP_INVALID_VERSION);
      return;
    }
  }

  boost::mutex::scoped_lock lock(STK::game_data_lock);
  c -> tc_in = STKS_SPECTATING;
  c -> gl = 0;
  g -> ctd_spectators_buf.push_back(c);

  c -> send(STKIP_CONFIRM);
}

void STK_Server::require_games(STK_Client_thread_data *c){
  STK_Game_info cgame;
  int i,j;

  /* --------------------------------- */
  /* ----------REQUIRE GAMES---------- */
  /* --------------------------------- */
  remove_client_from_game(c);
  if(!c->send(STKIP_CONFIRM)){
    fprintf(stdout,"server: send failed.\n"); return; 
  }
  fprintf(stdout,"server: sending games list to client!\n");
  i=game_info.size();
  if (!c->send((void*)&i,4)){
    fprintf(stdout,"server: send games: client failed to read metainfo.\n");
    return;
  }
    
  for (j = 0; j < i; j++){
    cgame = (STK_Game_info)game_info[j];
    if (!c->send((void*)&cgame,sizeof(STK_Game_info))){
      fprintf(stdout,"server: send games: client failed to read.\n");      
      return;
    }
  }
  fprintf(stdout,"server: successfully sent games list to client %s!\n",c->name);
}

void STK_Server::require_game_info(STK_Client_thread_data *c){
  int i;
  STK_Game_info cgame;
  STK_S_Game_info *g;

  /* --------------------------------- */
  /* ----------REQUIRE A GAME---------- */
  /* --------------------------------- */
  if(!c->send(STKIP_CONFIRM)){
    fprintf(stdout,"server: send failed.\n"); return; 
  }
  if (!c->get((void*)&i,4)){
    fprintf(stdout,"server: client required game info but failed to specify id.\n");
    return;
  }
  if (!c->send((g=get_game_info(i))?STKIP_CONFIRM:STKIP_DENY)){
    fprintf(stdout,"server: client required game info but failed to recieve confirmation.\n");
    return;
  }
  if (g){
    boost::mutex::scoped_lock lock(STK::game_info_lock);
    cgame = (STK_Game_info)*g;
    if (!c -> send((void*)&cgame, sizeof(STK_Game_info))){
      fprintf(stdout,"server: client required game info but failed to recieve data.\n");
      return;
    }
  }
  fprintf(stdout,"server: sent game info (%d) to client %s (%d).\n",i,c->name,c->id);
}

void STK_Server::start_game(STK_Client_thread_data *c){
  STK_S_Game_info *g;
  STK_Inet_protocol p;
  STK_Universe_builder builder;

  /* --------------------------------- */
  /* ----------START A GAME---------- */
  /* --------------------------------- */
  if (c->state!=STKS_PENDING || c->game_id<0 || !(g=get_game_info(c->game_id)) || c -> id != g -> root_client() || (g->game_state!=STKS_PENDING && g -> game_state != STKS_BUILD_FAILED) || g -> num_players < 2){
    fprintf(stdout,"client %s (%d) can not start the game (%d).\n",c->name,c->id,c->game_id);
    if (c->state!=STKS_PENDING){fprintf(stdout,"client state != pending.\n");}
    if (c->game_id<0){fprintf(stdout,"client game id < 0.\n");}
    if (!g){fprintf(stdout,"game not found.\n");}else if (g->game_state!=STKS_PENDING){fprintf(stdout,"game state != pending.\n");}
    c->send(STKIP_DENY); 
    return;
  }
  fprintf(stdout,"server: client %s (%d) requires start of game %d\n",c->name,c->id,c->game_id);

  g -> game_state = STKS_BUILDING;
  builder.g = g;
  boost::thread t(builder);

  if(!c->send(STKIP_CONFIRM)){
    fprintf(stdout,"server: start: send failed.\n");
  }

}

void STK_Server::require_game_data(STK_Client_thread_data *c){
  STK_S_Game_info *g;

  /* --------------------------------- */
  /* ----------LOAD GAME DATA--------- */
  /* --------------------------------- */
  fprintf(stdout, "loading data for client %d in game %d...\n", c -> id, c -> game_id);

  g = c -> game_id >= 0 ? get_game_info(c->game_id) : NULL;

  if (!g){
    c->send(STKIP_DENY); 
    return;
  }
  
  if (g -> game_state != STKS_LOADING){
    fprintf(stdout,"server: client %s (%d) (game_id=%d) can not load game data.\n",c->name,c->id,c->game_id);
    c->send(STKIP_DENY); 
    return;
  }

  fprintf(stdout,"server: client %s (%d) permitted to load data.\n",c->name,c->id);
  if(!c->send(STKIP_CONFIRM)){
    fprintf(stdout,"server: send failed.\n"); return; 
  }
  if (!g -> universe -> send_data(c->socket, STK_SERVER_TIMEOUT)){
    fprintf(stdout,"server: universe failed to send data.\n");
    return;
  }
  c->state=STKS_LOADING;
}

void STK_Server::read_request(STK_Inet_protocol p, STK_Client_thread_data* c){
  /* This routine handles clients who are not playing a game.  */

  fprintf(stdout,"server reading client %s of id %d (game_id=%d) and command is %d.\n",c->name,c->id,c->game_id,(int)p);

  switch (p){
  case STKIP_CREATE_GAME:
    create_game(c);
    break;
  case STKIP_LEAVE_GAME:
    leave_game(c);
    break;
  case STKIP_JOIN_GAME:
    join_game(c);
    break;
  case STKIP_SPECTATE_GAME:
    spectate_game(c);
    break;
  case STKIP_REQUIRE_GAMES:
    require_games(c);
    break;
  case STKIP_REQUIRE_GAME_INFO:
    require_game_info(c);
    break;
  case STKIP_START_GAME:
    start_game(c);
    break;
  case STKIP_REQUIRE_GAME_DATA:
    require_game_data(c);
    break;
  case STKIP_DO_NOTHING:
    /* --------------------------------- */
    /* ----------DO NOTHING--------- */
    /* --------------------------------- */
    break;
  case STKIP_STANDBY:
    c -> send(STKIP_CONFIRM);
    break;
  case STKIP_QUIT:
    c -> tc_in = STKS_TERMINATE;
    break;
  default:
    fprintf(stdout,"server: client %d gave invalid command: %d, returning: %s\n", c->id, (int)p, c -> state == STKS_LOADING || c -> state == STKS_PLAYING ? "STKIP_STANDBY" : "STKIP_INVALID_COMMAND");
    fprintf(stdout, " -> game_id: %d, game_state: %s\n", c -> game_id, get_game_info(c -> game_id) ? (get_game_info(c -> game_id) -> game_state == STKS_LOADING ? "loading" : "non-loading") : "no game");

    c->send(c -> state == STKS_LOADING || c -> state == STKS_PLAYING ? STKIP_STANDBY : STKIP_INVALID_COMMAND);
  }
}

void STK_Server::add_client_td(STK_Client_thread_data *c){
  STK_Client_thread ct;

  while (name_exists(c -> name) && strlen(c -> name) < STK_MAX_NAME){
    strcat(c -> name, "I");
  }

  client_data.push_back(c);
  ct.serv = this;
  ct.td = c;
  ct.td -> tc_in = STKS_RUN;
  ct.td -> tc_out = STKS_INITIALIZING;

  fprintf(stdout,"server::add_client_td: id is %d, result is %d.\n",c -> id, ct.td -> id);

  boost::thread t(ct);
}

void STK_Server::check_game_info_states(){
  int i,j,k;
  int count,countout=100;
  for (i=0; i<game_info.size() && *tc_in!=STKS_TERMINATE_NOW; i++){
    if (game_info[i].game_state==STKS_LOADING && game_info[i].num_players > 1){
      fprintf(stdout,"server: found loading game %d.\n",game_info[i].id);
      k=0;
      for (j=0; j<game_info[i].num_players && !k; j++){
	if (!game_info[i].players[j]){
	  k = 1;
	  game_info[i].game_state = STKS_PENDING;
	}else if (game_info[i].players[j]->state!=STKS_LOADING || game_info[i].players[j]->connection_state != STKS_READY){
	  k=1;
	}
      }
      if (!k){
	fprintf(stdout,"server: moving %d clients to game_data-handle.\n",game_info[i].num_players);
	STK_Game_thread_data *g = new STK_Game_thread_data();
	STK_Game_thread gh(g);
	STK_Client_thread_data *c2;
	boost::mutex::scoped_lock locka(STK::game_info_lock);
	game_info[i].game_state=STKS_PLAYING;
	locka.unlock();
	for (j=0; j<game_info[i].num_players; j++){
	  c2=game_info[i].players[j];
	  c2->state=STKS_PLAYING;
	  c2->tc_in=STKS_GOTO_GAME;
	  g -> ctd.insert(g -> ctd.end(),c2);
	}
	
	/* WAIN UNTIL CLIENT THREADS HAVE ENDED. */
	fprintf(stdout,"server: waiting for client threads to terminate.\n");
	count=0;
	while(!k && count++<countout){
	  k=1;
	  for (j=0; j<game_info[i].num_players; j++){
	    if (game_info[i].players[j]->tc_out!=STKS_PLAYING){
	      fprintf(stdout,"server detected: client %d still running!\n",game_info[i].players[j] -> id);
	      k=0;
	    }
	  }
	  if (!k){
	    SDL_Delay(500);
	  }
	}

	if (!k){
	  fprintf(stdout,"server: client threads refuse to terminate properly, can not start game!\n");
	  delete g;
	}else{
	  g -> tc_out = STKS_INITIALIZING;
	  g -> tc_in = STKS_RUN;
	  g -> id = game_info[i].id;
	  g -> home_planets = game_info[i].home_planets;
	  boost::mutex::scoped_lock lockb(STK::game_data_lock);
	  game_data.push_back(g);
	  gh.gtd=g;
	  gh.serv=this;
	  lockb.unlock();
	  boost::thread a(gh);
	  SDL_Delay(100);
	  if (game_data.back() -> tc_out!=STKS_INITIALIZING){
	    fprintf(stdout,"server: game successfully started.\n");
	  }else{
	    fprintf(stdout,"server: game failed to start.\n");
	  }
	  lockb.lock();
	  fprintf(stdout,"server: eraze game of id %d.\n", game_info[i].id);
	  game_info[i].cleanup();
	  game_info.erase(game_info.begin()+i--);
	}
      }
    }else if (game_info[i].num_players == 0){
      boost::mutex::scoped_lock lockc(STK::game_info_lock);
      fprintf(stdout,"server: eraze game of id %d.\n", game_info[i].id);
      game_info.erase(game_info.begin()+i--);
    }else if (game_info[i].game_state == STKS_LOADING && game_info[i].num_players == 1){
      fprintf(stdout, "server: game %d stopped loading because some player left.\n", i);
      game_info[i].game_state = STKS_PENDING;
    }
  }
}

void STK_Server::check_game_threads(){
  /* Checks the states of the game_thread_data objects, which refer the game handling processes. */
  list<STK_Game_thread_data*>::iterator i;
  
  for (i = game_data.begin(); i != game_data.end() && *tc_in!=STKS_TERMINATE_NOW; i++){
    if (*i){
      if ((*i) -> tc_out == STKS_TERMINATE){
	delete *i;
	game_data.erase(i--);
      }
    }else{
      game_data.erase(i--);
    }
  }
}

void STK_Server::check_client_threads(){
  /* Removes client thread data objects which have terminated. This means clients that have been in a game get removed; consider rewriting with seperate code for clients who have been in a game, so they can get reinserted. */
  int j;
  list<STK_Client_thread_data*>::iterator i;
  for (i = client_data.begin(); i != client_data.end() && *tc_in!=STKS_TERMINATE_NOW; i++){
    if (*i){
      if ((*i) -> tc_out == STKS_TERMINATE){
	if ((*i) -> game_id > -1 && get_game_info((*i) -> game_id)){
	  get_game_info((*i) -> game_id) -> remove_client((*i) -> id);
	}
	delete *i;
	client_data.erase(i--);
      }else if ((*i) -> tc_out == STKS_RETURN_TO_MAINFRAME){
	fprintf(stdout,"server: returning client %d from game.\n", (*i) -> id);
	STK_Client_thread ct;
	(*i) -> tc_in = STKS_RUN;
	(*i) -> tc_out = STKS_INITIALIZING;
	ct.serv = this;
	ct.td = *i;
	boost::thread t(ct);
      }
    }else{
      client_data.erase(i--);
    }
  }
}
void STK_Server::terminate_game_threads(){
  int n,c;
  list<STK_Game_thread_data*>::iterator i;

  boost::mutex::scoped_lock lock(STK::game_data_lock);
  for (i = game_data.begin(); i != game_data.end(); i++){
    (*i) -> tc_in = STKS_TERMINATE;
  }
  n=1;
  c=0;
  while(n && c++<100 && *tc_in!=STKS_TERMINATE_NOW){
    n=0;
    for (i = game_data.begin(); i != game_data.end() && !n; i++){
      if ((*i) -> tc_out != STKS_TERMINATE){n = true;}
    }
    SDL_Delay(100);
  }
  if (n){
    fprintf(stdout,"server: failed to terminate game threads.\n");
  }
}

void STK_Server::terminate_client_threads(){
  int c,n;
  list<STK_Client_thread_data*>::iterator i;

  boost::mutex::scoped_lock lock(STK::client_data_lock);
  for (i = client_data.begin(); i != client_data.end(); i++){
    (*i) -> tc_in=STKS_TERMINATE;
  }
  n=1;
  c=0;
  while(n && *tc_in!=STKS_TERMINATE_NOW){
    n=0;
    for (i = client_data.begin(); i != client_data.end(); i++){
      if ((*i) -> tc_out != STKS_TERMINATE){n=1;}
    }
    if (c++>100){
      fprintf(stdout,"server: terminate clients: failed.\n");
      return;
    }
    SDL_Delay(100);
  }
}

void STK_Server::operator()(){
  int i;
  STK_Port_listener listener;
#ifndef __WIN32__
  signal(SIGPIPE, SIG_IGN);
  //signal(SIGABRT, SIG_IGN);
#endif
  fprintf(stdout,"server() called!\n");
  plisten_out=STKS_TERMINATE;
  if (*tc_in!=STKS_RUN){return;}
  *tc_out=STKS_INITIALIZING;
  if (!(open())){*tc_out=STKS_FAILED; fprintf(stdout,"server: open failed.\n");  return;}
  *tc_out=STKS_RUN;

  listener.tc_in=&plisten_in;
  listener.tc_out=&plisten_out;
  listener.serv=this;
  plisten_in=STKS_RUN;
  boost::thread t(listener);

  while(*tc_in==STKS_RUN){
    check_game_info_states();
    check_game_threads();
    check_client_threads();
    SDL_Delay(100);
  }
  fprintf(stdout,"server: left main loop.\n");
  cleanup();
  *tc_out=STKS_TERMINATE;
}

STK_Game_thread_data* STK_Server::get_game_td(int id){
  list<STK_Game_thread_data*>::iterator i;
  boost::mutex::scoped_lock lock(STK::game_data_lock);
  for (i = game_data.begin(); i != game_data.end(); i++){
    if ((*i) -> id==id){
      return *i;
    }  
  }
  return NULL;
}

STK_S_Game_info *STK_Server::get_game_info(int id){
  int i;
  fprintf(stdout, "get game info(%d): \n", id);
  for (i=0; i<game_info.size(); i++){
    fprintf(stdout, " -> %d\n", game_info[i].id);
    if (game_info[i].id==id){
      return &game_info[i];
    }  
  }

  fprintf(stdout, " -> not found!\n");
  return NULL;
}

STK_Client_thread_data *STK_Server::get_client_td(int id){
  list<STK_Client_thread_data*>::iterator i;

  boost::mutex::scoped_lock lock(STK::client_data_lock);
  for (i = client_data.begin(); i != client_data.end(); i++){
    if ((*i) -> id == id){
      return *i;
    }  
  }
  return NULL;
}

bool STK_Server::clients_full(){
  return client_data.size() >= max_clients;
}

bool STK_Server::games_full(){
  return game_data.size() + game_info.size() > max_games;
}

bool STK_Server::name_exists(string v){
  list<STK_Client_thread_data*>::iterator i;

  for (i = client_data.begin(); i != client_data.end(); i++){
    if (!v.compare((*i) -> name)){
      return true;
    }
  }

  return false;
}

// void STK_Server::remove_client_td(int id){
//   int i;
//   for (i=0; i<client_data.size(); i++){
//     if (client_data[i].id==id){
//       boost::mutex::scoped_lock lock(client_data_lock);
//       client_data.erase(client_data.begin()+i);
//       return;
//     }  
//   }
// }

// int STK_Server::build_universe(STK_Universe_settings g){
//   STK_Universe_info ui;
//   if (!ui.u.initialize(g)){
//     return -1;
//   }
//   ui.id=uid_counter++;
//   boost::mutex::scoped_lock lock(universe_info_lock);
//   universe_info.insert(universe_info.end(),ui);
//   return ui.id;
// }

// STK_Universe* STK_Server::get_universe(int id){
//   int i;
//   if (id<0){return NULL;}
//   for (i=0; i<universe_info.size(); i++){
//     if (universe_info[i].id==id){
//       return &universe_info[i].u;
//     }  
//   }
//   return NULL;
// }
