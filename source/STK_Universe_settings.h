#ifndef _STK_UNIVERSE_SETTINGS
#define _STK_UNIVERSE_SETTINGS

#include "STK_Ship_settings.h"
#include "STK_Player_info.h"

struct STK_Universe_settings{
  char name[STK_MAX_NAME];

  //map
  int width;
  int height;
  float ratio;

  //planets
  int num_planets;
  int planet_seperator;
  int min_radius;
  int max_radius;
  int planet_defense;
  float planet_productivity;
  float planet_base_fleet;
  float productivity_multiplier;

  //explosions
  int explosion_num_particles;

  //ship settings
  STK_Ship_settings ship_settings;

  //stars
  int num_stars;
  int pixels_per_star;

  //game
  int speed_of_light;
  int round_length;
  int use_fleets;

  player_info p_info;

  STK_Universe_settings();
  int calculate_signal_delay(float d);
  int cmap_width();
  int cmap_height();
};
#endif
