#ifndef _STK_STRUCTURES
#define _STK_STRUCTURES

#include <list>
#include <vector>
#include <SDL/SDL.h>
#include <boost/thread/mutex.hpp>

#include "RSK/RSK_GUI_V2/RSK_GUI.h"
#include "STK_Definitions.h"
#include "STK_Class_headers.h"
#include "STK_Universe_settings.h"

struct STK_Standby_mode{
  int *command;
  int *state;
  STK_Client *client;
  
  void operator()();
  void terminate();
  void start();
};

struct STK_Upgrade_event{
  int planet;
  int player;
};

struct STK_Upgrade_choice{
  int choice; //ind, nav, war, tro
  int planet;
  int player;
  int troops;
};

struct STK_Ship_order{
  STK_Fleet *fleet;
  STK_Planet *planet;
  fd position;
  int type;
  int associated_id;
  const static int fsize = 16;

  bool load_data(char* buf, int* c, int max);
  bool send_data(char* buf, int* c, int max);
};

struct STK_Pressed_keys{
  std::list<SDLKey> keys;
  
  void press(SDLKey sym);
  void release(SDLKey sym);
  bool is_pressed(SDLKey sym);
  void clear();
};

class STK_Game_info{
 public:
  STK_Universe_settings settings;
  int game_state;
  int num_players;
  char names[STK_MAX_PPG][STK_MAX_NAME];
  int player_id[STK_MAX_PPG];
  int id;

  STK_Game_info();
};

struct select_game_updater{
  int *tc_in;
  int *tc_out;
  RSG_Table *table;
  STK_Client *client;
  boost::mutex *mutex;
  std::vector<STK_Game_info> *games;
  
  void operator ()();
  void start();
  void terminate();
};

struct wfg_updater{
  int *tc_in;
  int *tc_out;
  RSG_Table *table;
  RSG_Label *indicator;
  STK_Client *client;
  boost::mutex *mutex;
  int game_id;
  bool *loading;
  bool *end;
  int *game_state;
  
  void operator ()();
  void terminate();
  void start();
};

struct e_pfield{
  float angle;
  float length;
  STK_Planet* p;
  
  e_pfield();
};
#endif
