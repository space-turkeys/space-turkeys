#ifndef _STK_BASIC_COMMUNICATION
#define _STK_BASIC_COMMUNICATION

#include "STK_Keys.h"

STK_SERVER_STATES lsend(int socket, void* data, int size, int timeout);
STK_SERVER_STATES lget(int socket, void* data, int size, int timeout);
STK_SERVER_STATES ready_in(int socket, int millis);
STK_SERVER_STATES ready_out(int socket, int millis);
void write_to_buf(char* source, char* buf, int size);
void read_from_buf(char* buf, char* dest, int size);

#endif
