
#ifndef _STK_WAIT_FOR_GAME_MENU
#define _STK_WAIT_FOR_GAME_MENU

#include <boost/thread/mutex.hpp>

#include <RSK/RSK_GUI_V2/RSK_GUI.h>

#include "STK_Structures.h"
#include "STK_Menu.h"

class STK_Wait_for_game_menu : public STK_Menu{
 private:
  RSG_Table *players_list;
  RSG_Button *button_start, *button_back;
  RSG_Label *label_game_state;
  wfg_updater updater;
  int game_state;
  boost::mutex::scoped_lock *lock;

 public:
  STK_Wait_for_game_menu(STK_Menu_references *r);
  ~STK_Wait_for_game_menu();
  void evoke();
  void update_players_list();
  void run();
  void start();
  void listen(SDL_Event e, RSG_Component *c);
  void handle_event(SDL_Event e);
};
#endif
