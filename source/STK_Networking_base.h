
#ifdef __WIN32__
#include <winsock2.h>
#include <Ws2tcpip.h>
#else
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <sys/select.h>
  #include <netinet/in.h>
  #include <arpa/inet.h>
  #include <netdb.h>
  #include <poll.h>
  #include <signal.h>
#endif

#include "STK_Basic_communication.h"
