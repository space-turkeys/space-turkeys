#include "STK_UInterface.h"

#include "STK_Root_menu.h"
#include "STK_Input_settings_menu.h"
#include "STK_Select_game_menu.h"
#include "STK_Local_game_menu.h"
#include "STK_Connection_settings_menu.h"
#include "STK_Wait_for_game_menu.h"

STK_UInterface::STK_UInterface(int w, int h){
  /* create all menus in m_ref */
  if (w > 0 && h > 0){
    m_ref.w = w;
    m_ref.h = h;
    m_ref.root_menu = new STK_Root_menu(&m_ref);
    m_ref.input_settings_menu = new STK_Input_settings_menu(&m_ref);
    m_ref.select_game_menu = new STK_Select_game_menu(&m_ref);
    m_ref.local_game_menu = new STK_Local_game_menu(&m_ref);
    m_ref.connection_settings_menu = new STK_Connection_settings_menu(&m_ref);
    m_ref.wait_for_game_menu = new STK_Wait_for_game_menu(&m_ref);
  }
}

STK_UInterface::~STK_UInterface(){
  /* destroy all menus in m_ref */
  delete m_ref.root_menu;
  delete m_ref.input_settings_menu;
  delete m_ref.select_game_menu;
  delete m_ref.local_game_menu;
  delete m_ref.connection_settings_menu;
  delete m_ref.wait_for_game_menu;

  if (m_ref.client){
    m_ref.client -> stkc_close();
    delete m_ref.client;
  }

  if(m_ref.tc_in && m_ref.tc_out){
    *(m_ref.tc_in) = STKS_TERMINATE;
    fprintf(stdout,"STK_UI: waiting for server to terminate...\n");
    while (*(m_ref.tc_out) != STKS_TERMINATE){
      SDL_Delay(50);
    }
    fprintf(stdout,"STK_UI: server terminated.\n");

    delete m_ref.tc_in;
    delete m_ref.tc_out;
  }
}
void STK_UInterface::evoke(){
  m_ref.connection_settings_menu -> evoke();
}
