#ifndef _STK_GAME_LOADER
#define _STK_GAME_LOADER

#include <boost/thread/mutex.hpp>

#include "STK_Keys.h"
#include "STK_Universe.h"
#include "STK_Choice.h"

struct STK_Game_loader{
  boost::mutex *mutex;
  STK_Inet_protocol command;
  bool loaded;
  bool give_up;
  int universe_valid;
  int regenerate_maps;
  STK_Universe universe;
  STK_Universe *valid_universe;
  STK_Choice choice;
  std::vector<STK_Choice> *c_list;
  int *rand_seed;

  STK_Game_loader();
};
#endif
