#ifndef _STK_UINTERFACE
#define _STK_UINTERFACE

#include "STK_Client.h"
#include "STK_Game.h"
#include "STK_Menu_references.h"

class STK_UInterface{
 private:
  STK_Client client;
  STK_Game game;

 public:
  STK_Menu_references m_ref;

  STK_UInterface(int w, int h);
  ~STK_UInterface();
  void evoke();
};
#endif
