#include <boost/thread/thread.hpp>

#include "STK_Game.h"
#include "STK_Graphics.h"
#include "STK_Client.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Choice_listener.h"
#include "STK_Generate_choice_UI.h"
#include "STK_Statistics.h"

using namespace std;

void STK_Game::initialize(STK_Game_settings gs, STK_Universe_settings us, STK_Client *cl){
  client = cl;
  //universe.initialize(this, us);
  settings = gs;
  gcui = NULL;
}

void STK_Game::cleanup(){
  universe.cleanup();
  if (gcui){
    delete gcui;
  }
  gcui = NULL;
  STK_Graphics::clear_planet_data();
}

void STK_Game::exit_interface(){

  list<string> data;
  int res;
  int sb_com = STKS_RUN, sb_state = STKS_INITIALIZING;
  STK_Standby_mode m;

  m.command = &sb_com;
  m.state = &sb_state;
  m.client = client;
  boost::thread t(m);

  data.push_back("OK");

  switch (client -> pop_leave_condition()){
  case STKS_VICTORY:
    STK_Graphics::make_selection(STK::ns_var -> screen, data, "YOU ARE VICTORIOUS!");
    break;
  case STKS_LOSS:
    data.push_back("Stay and watch");
    res = STK_Graphics::make_selection(STK::ns_var -> screen, data, "YOU HAVE BEEN DEFEATED.");
    require_spectate = res == 1;
    break;
  default:
    STK_Graphics::make_selection(STK::ns_var -> screen, data, "GAME TERMINATED BY SERVER.");
    break;	
  }

  m.terminate();
}

void STK_Game::evoke(){
  bool done = false;
  SDL_Event e;
  vector<STK_Choice> choice_list;
  int c = 0;
  int i;
  STK_Choice *choice = NULL;
  list<STK_Planet*> planets;
  list<STK_Planet*>::iterator j;
  STK_Player *the_player;
  list<string> selection;
  STK_Upgrade no_upgrade;

  bool first_round = true;

  require_spectate = false;
  STK::ns_var -> get_exit();
  STK::ns_var -> reset();
  STK::ns_var -> map = universe.get_collision_map();
  STK_Graphics::set_uvs(universe.get_settings());
  client -> pop_leave_condition();
  STK_Fleet::id_counter = 0;
  STK_Ship::id_counter = 0;
  STK_Planet::research_complete = 8 * universe.get_settings() -> planet_productivity * pow((float)universe.get_settings() -> round_length, 2);

  if (!(the_player = get_player(client -> get_id()))){
    fprintf(stdout, "game::evoke(): player does not exist!\n");
    return;
  }

  STK::ns_var -> center_at(the_player -> home_planet -> position);
  fprintf(stdout, "game::evoke: centered map at planet %d: %d x %d\n", the_player -> home_planet -> id, (int)the_player -> home_planet -> position.x, (int)the_player -> home_planet -> position.y);

  while (!done){
    fprintf(stdout,"game::evoke(): debug step 0.\n");
    universe.get_ship_master() -> print_references();

    fprintf(stdout,"client game round: start.\n");
    while(SDL_PollEvent(&e)){
      if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
	client -> confirm_command(STKIP_LEAVE_GAME);
	cleanup();
	fprintf(stdout,"game::evoke(): ESCAPE pressed.\n");
	return;
      }
    }
    fprintf(stdout,"client: events handled.\n");
    
    if (!client -> r_gamestart()){
      fprintf(stdout, "client: gamestart not confirmed!\n");
      break;
    }

    fprintf(stdout,"client: gamestart confirmed.\n");
    fprintf(stdout, "list of players:\n");
    for (i = 0; i < player.size(); i++){
      fprintf(stdout, " -> %s %d: %s\n", player[i].id == client -> get_id() ? "player" : "opponent", player[i].id, player[i].dead ? "dead" : "living");
    }
    fprintf(stdout, "list of p_info:\n");
    player_info *pinf = &universe.get_settings() -> p_info;
    for (i = 0; i < pinf -> num_players; i++){
      fprintf(stdout, " -> %s %d\n", pinf -> players[i] == client -> get_id() ? "player" : "opponent", pinf -> players[i]);
    }
    fprintf(stdout, "list of upgrade points:\n");
    for (i = 0; i < pinf -> num_players; i++){
      fprintf(stdout, " -> player %d: %d\n", pinf -> players[i], universe.get_points(pinf -> players[i]));
    }

    if (get_player(client -> get_id()) -> dead){
      STK_Graphics::draw_message(STK::ns_var -> screen, "ERROR: NO OWNED PLANETS!");
      SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);
      SDL_Delay(1000);
      cleanup();
      return;
    }

    if (first_round){
      int sb_com = STKS_RUN;
      int sb_state;
      STK_Standby_mode m;
      m.command = &sb_com;
      m.state = &sb_state;
      m.client = client;
      boost::thread t(m);

      first_round = false;

      selection.push_back("Industry");
      selection.push_back("Navigation");
      selection.push_back("Warfare");

      the_player -> preference = STK_Graphics::make_selection(STK::ns_var -> screen, selection, "Select your preference!");

      m.terminate();

      if (client -> peek_leave_condition() != STKS_NONE){
	fprintf(stdout, "client: leave game recieved during pref sel!\n");
	break;
      }
    }
    
    if (!(choice = get_choice())){
      fprintf(stdout, "game::evoke(): failed to create choice!\n");
      cleanup();
      return;
    }

    get_player(client -> get_id()) -> temp_upgrade = no_upgrade;

    if (STK::ns_var -> get_exit()){
      client -> confirm_command(STKIP_LEAVE_GAME);
      cleanup();
      fprintf(stdout,"game::evoke(): post choice exit code.\n");
      return;
    }

    if (client -> peek_leave_condition() != STKS_NONE){
      fprintf(stdout, "client: leave game recieved during get_choice!\n");
      break;
    }

    if (!wait_for_choices(&choice_list, choice)){
      fprintf(stdout,"client: load/send choices NOT confirmed.\n");
      break;
    }
    fprintf(stdout,"client: send/load choice confirmed.\n");
    delete choice;
    
    //game start confirmed and choices successfully exchanged
    universe.apply_choices(choice_list);
    apply_upgrades(choice_list);
    
    universe.clear_choice();
    STK::rand_keeper.reset();
    evaluate_round();

    if (client -> validate_universe(&universe)){
      if (client -> reload_universe(&universe) == STKIP_DENY){
	fprintf(stdout,"game::evoke(): reload universe denied.\n");
	break;
      }else{
	fprintf(stdout,"game::evoke: round completed successfully!\n");
      }
    }else{
      fprintf(stdout,"game::evoke(): validate universe failed.\n");
      break;
    }
  }

  exit_interface();
  cleanup();
  fprintf(stdout,"game::evoke: ended!\n");
}

bool STK_Game::wait_for_choices(vector<STK_Choice> *choice_list, STK_Choice *c){
  STK_Choice_listener listener;
  bool done = false;
  bool success = false;
  SDL_Event event;

  listener.client = client;
  listener.choice_list = choice_list;
  listener.choice = c;
  listener.planet_list = &(universe.get_planet_master() -> planets);
  listener.s_master = universe.get_ship_master();
  listener.done = &done;
  listener.success = &success;
  
  boost::thread t(listener);

  while (!STK::ns_var -> require_exit && !done){
    while(SDL_PollEvent(&event)){
      STK::ns_var -> update(event);
    }

    SDL_FillRect(STK::ns_var -> screen,NULL,0);
    STK::ns_var -> iterate();
    universe.paint(STK::ns_var -> screen, client -> get_id());
    c -> paint(STK::ns_var -> screen);
    if (STK::ns_var -> minimap_active){
      STK_Graphics::draw_minimap(STK::ns_var -> screen, universe.get_collision_map(), &(universe.get_settings() -> p_info));
    }
    drawFrame(STK::ns_var -> screen, create_sdl_rect(0,0,STK::ns_var -> screen -> w, STK::ns_var -> screen -> h), 5, universe.get_settings() -> p_info.get_color(client -> get_id()));
    STK_Graphics::draw_message(STK::ns_var -> screen, "Waiting for other players...");

    SDL_UpdateRect(STK::ns_var -> screen,0,0,0,0);
    SDL_Delay(50);
  }

  listener.terminate();

  return success;
}

void STK_Game::apply_upgrades(vector<STK_Choice> l){
  vector<STK_Choice>::iterator i;
  list<STK_Upgrade_choice>::iterator j;
  STK_Player *player;
  STK_Planet *planet;

  for (i = l.begin(); i != l.end(); i++){
    for (j = i -> upgrades.begin(); j != i -> upgrades.end(); j++){
      player = get_player(j -> player);
      
      if (player){
	fprintf(stdout, "game::apply_upgrades: player %d uses %d points on %d\n", j -> player, j -> choice, player -> upgrade.get_value(j -> choice) + 1);
	player -> upgrade.add(j -> choice);
	universe.use_points(j -> player, player -> upgrade.get_value(j -> choice));
      }else{
	fprintf(stdout, "game::apply_upgrades(): player %d does not exist!\n", j -> player);
      }
    }
  }
}

void STK_Game::evaluate_round(){
  int i=0;
  SDL_Event event;
  double t;
  double f_time = 1/(double)settings.frame_rate;
  int d_time;
  int sb_com = STKS_RUN;
  int sb_state;
  STK_Standby_mode m;
  m.command = &sb_com;
  m.state = &sb_state;
  m.client = client;
  boost::thread thr(m);

  fprintf(stdout,"game::evaluate_round(): start: player upgrades: \n");
  for (i = 0; i < player.size(); i++){
    fprintf(stdout," -> player %d: [%d, %d, %d]\n", player[i].id, player[i].upgrade.industry, player[i].upgrade.navigation, player[i].upgrade.warfare);
  }

  while (i++ < universe.get_settings() -> round_length && !STK::ns_var -> require_exit){
    t = system_seconds();

    while(!STK::ns_var -> require_exit && SDL_PollEvent(&event)){STK::ns_var->update(event);}
    STK::ns_var -> iterate();
    universe.iterate();

    SDL_FillRect(STK::ns_var -> screen,NULL,0);
    universe.paint(STK::ns_var -> screen, client -> get_id());
    universe.paint_stats(STK::ns_var -> screen, client -> get_id());
    if (STK::ns_var -> minimap_active){
      STK_Graphics::draw_minimap(STK::ns_var -> screen, universe.get_collision_map(), &(universe.get_settings() -> p_info));
    }
    SDL_UpdateRect(STK::ns_var -> screen,0,0,0,0);
    
    d_time = 1000 * (f_time - (system_seconds() - t));
    if (d_time > 0){
      SDL_Delay(d_time);
    }
  }

  //check if players still own their home planets
  for (i = 0; i < player.size(); i++){
    if (!player[i].home_planet || player[i].home_planet -> owner != player[i].id){
      list<STK_Planet*> planets;
      list<STK_Planet*>::iterator j;
      player[i].home_planet = NULL;
      planets = universe.get_planet_master() -> get_planets();
      for (j = planets.begin(); j != planets.end(); j++){
	if ((*j) -> owner == player[i].id){
	  player[i].home_planet = *j;
	  break;
	}
      }
      
      if (player[i].home_planet == NULL){
	player[i].dead = true;
      }
    }
  }

  m.terminate();
}

STK_Choice *STK_Game::get_choice(){
  STK_Choice *c = new STK_Choice();
  int i;
  list<STK_Planet*> planets;
  list<STK_Planet*>::iterator j;

  if (!player.size()){
    cout<<"stkgame::get_choice: player doesn't exist.\n";
    exit(1);
  }

  c -> upgrades=select_upgrade();
  universe.clear_choice();
  generate_choice(c);

  return c;
}

list<STK_Upgrade_choice> STK_Game::select_upgrade(){
  int done, done_inner;
  SDL_Event event;
  SDL_Surface *background;
  int alternative;
  int mouse_over=-1;
  STK_Upgrade_choice choice;
  list<STK_Upgrade_choice> r;
  STK_Planet *planet;
  STK_Player *p = get_player(client -> get_id());

  if (p == NULL){
    return r;
  }

  int alt1 = (p -> preference+1)%3;
  int alt2 = (p -> preference+2)%3;
  int pref = p -> preference;
  int inc_pref = 0;
  int inc_alt1 = 0;
  int inc_alt2 = 0;

  //interface
  int w = STK::ns_var -> screen -> w;
  int h = STK::ns_var -> screen -> h;
  int bw = (w-10)/3 - 10;
  RSK_GUI gui;
  SDL_Rect rect;
  char s[128];
  int points = universe.get_points(client -> get_id());
  bool add_choice = false;

  STK_Standby_mode m;
  int sb_com = STKS_RUN;
  int sb_state;

  m.command = &sb_com;
  m.state = &sb_state;
  m.client = client;
  boost::thread t(m);

  if ((background = SDL_CreateRGBSurface(SDL_HWSURFACE, w, h, 32, rmask, gmask, bmask, 0))){
    SDL_BlitSurface(STK::ns_var -> screen, NULL, background, NULL);
  }

  rect = create_sdl_rect(10,10,w-20,80);
  sprintf(s, "SELECT YOUR UPGRADES (%d points)", points);
  RSG_Label *l_title = new RSG_Label(s, rect);
  rect = create_sdl_rect(10,h/2-100,bw, fmin(h - 110, 200));
  sprintf(s, "[P] %s: %d", STK_Upgrade::get_upgrade_name(pref), p -> upgrade.get_value(pref) + 1);
  RSG_Button *b_pref = new RSG_Button(s, rect);
  rect.x += bw + 10;
  sprintf(s, "[A] %s: %d", STK_Upgrade::get_upgrade_name(alt1), p -> upgrade.get_value(alt1) + 1);
  RSG_Button *b_alt1 = new RSG_Button(s, rect);
  sprintf(s, "[A] %s: %d", STK_Upgrade::get_upgrade_name(alt2), p -> upgrade.get_value(alt2) + 1);
  RSG_Button *b_alt2 = new RSG_Button(s, rect);
  rect.x += bw + 10;
  RSG_Button *b_done = new RSG_Button("[RET] DONE", rect);

  gui.add(l_title);
  gui.add(b_pref);
  gui.add(b_alt1);
  gui.add(b_alt2);
  gui.add(b_done);

  done = false;
  done = (points < fmin(p -> upgrade.get_value(pref) + 1, fmin(p -> upgrade.get_value(alt1) + 1, p -> upgrade.get_value(alt2) + 1)));
  fprintf(stdout, "game::select_upgrade: player %d, has %d points (needs %d).\n", client -> get_id(), points, (int)fmin(p -> upgrade.get_value(pref) + 1, fmin(p -> upgrade.get_value(alt1) + 1, p -> upgrade.get_value(alt2) + 1)));
  while (!done){
    sprintf(s, "SELECT YOUR UPGRADES (%d points)", points);
    l_title -> set_text(s);

    /* select available alternative upgrade */
    b_pref -> visible = points > p -> upgrade.get_value(pref) + inc_pref;
    b_alt1 -> visible = points > p -> upgrade.get_value(alt1) + inc_alt1;
    b_alt2 -> visible = points > p -> upgrade.get_value(alt2) + inc_alt2;

    if (b_alt1 -> visible && b_alt2 -> visible){
      alternative=rand()%2;
      b_alt1 -> visible = !(b_alt2 -> visible = alternative);
    }

    choice.player = client -> get_id();
    done_inner = false;
    while(!done_inner && !STK::ns_var -> require_exit){
      /* handle events */
      while(SDL_PollEvent(&event)){
	/* this routine should perhaps use RskGUI? */
	//here, choice.choice and done should be set.
	gui.handle_event(event);
	if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE){
	  done_inner = done = true;
	}
	  
	if (event.type == SDL_KEYDOWN){
	  switch(event.key.keysym.sym){
	  case SDLK_p:
	    b_pref -> set_state(true);
	    break;
	  case SDLK_a:
	    if (alternative){
	      b_alt2 -> set_state(true);
	    }else{
	      b_alt1 -> set_state(true);
	    }
	    break;
	  case SDLK_RETURN:
	    done_inner = done = true;
	    break;
	  }
	}
      }

      if (b_pref -> get_state() && points > p -> upgrade.get_value(pref) + inc_pref){
	b_pref -> reset();
	done_inner = true;
	choice.choice = pref;
	inc_pref++;
	points -= p -> upgrade.get_value(pref) + inc_pref;

	//repaint the button
	sprintf(s, "[P] %s: %d", STK_Upgrade::get_upgrade_name(pref), p -> upgrade.get_value(pref) + inc_pref + 1);
	b_pref -> init(s, NULL);
	add_choice = true;
      }else if (b_alt1 -> get_state() && points > p -> upgrade.get_value(alt1) + inc_alt1){
	b_alt1 -> reset();
	done_inner = true;
	choice.choice = alt1;
	inc_alt1++;
	points -= p -> upgrade.get_value(alt1) + inc_alt1;

	//repaint the button
	sprintf(s, "[A] %s: %d", STK_Upgrade::get_upgrade_name(alt1), p -> upgrade.get_value(alt1) + inc_alt1 + 1);
	b_alt1 -> init(s, NULL);
	add_choice = true;
      }else if (b_alt2 -> get_state() && points > p -> upgrade.get_value(alt2) + inc_alt2){
	b_alt2 -> reset();
	done_inner = true;
	choice.choice = alt2;
	inc_alt2++;
	points -= p -> upgrade.get_value(alt2) + inc_alt2;

	//repaint the button
	sprintf(s, "[A] %s: %d", STK_Upgrade::get_upgrade_name(alt2), p -> upgrade.get_value(alt2) + inc_alt2 + 1);
	b_alt2 -> init(s, NULL);
	add_choice = true;
      }else if (b_done -> get_state()){
	b_done -> reset();
	done_inner = done = true;
      }

      if (background){
	SDL_BlitSurface(background, NULL, STK::ns_var -> screen, NULL);
      }

      gui.paint(STK::ns_var -> screen);
      SDL_UpdateRect(STK::ns_var -> screen,0,0,0,0);
      SDL_Delay(100);
    }

    if (add_choice){
      r.push_back(choice);
      add_choice = false;
    }

    done |= (points < fmin(p -> upgrade.get_value(pref) + inc_pref + 1, fmin(p -> upgrade.get_value(alt1) + inc_alt1 + 1, p -> upgrade.get_value(alt2) + inc_alt2 + 1)));
  }

  universe.show_points = points;

  m.terminate();

  if (background){
    SDL_FreeSurface(background);
  }

  gui.delete_components();
  return r;
}

void STK_Game::generate_choice(STK_Choice* c){
  int done=0;
  SDL_Event event;
  list<STK_Command>::iterator i;
  vector<vector<string> > upgrades;
  STK_Upgrade u = get_player(client -> get_id()) -> upgrade;
  STK_Upgrade u2 = c -> total_upgrade();
  get_player(client -> get_id()) -> temp_upgrade = u2;
  char v[128];
  player_info *p_info = &(universe.get_settings() -> p_info);
  
  int sb_com = STKS_RUN;
  int sb_state;

  STK_Standby_mode m;

  m.command = &sb_com;
  m.state = &sb_state;
  m.client = client;

  boost::thread t(m);

  universe.clear_selection();
  c -> clear_selection();
  STK::ns_var -> clear();
  universe.get_ship_master() -> cleanup_explosions();
  drag.x = -1;
  drag.y = -1;
  drag.w = 0;
  drag.h = 0;

  upgrades.resize(4);

  upgrades[0].resize(1);
  sprintf(v, "Upgrades: %d points", universe.show_points);
  upgrades[0][0] = v;

  upgrades[1].resize(1);
  sprintf(v, "warfare: %d (wins %d%%)", u.warfare + u2.warfare, (int)(100 * (u.warfare + u2.warfare + DIRICHLET_BASE) / (float)(u.warfare + u2.warfare + 2 * DIRICHLET_BASE)));
  upgrades[1][0] = v;

  upgrades[2].resize(1);
  sprintf(v, "industry: %d (+ %d%% prod.)", u.industry + u2.industry, 10 * (u.industry + u2.industry));
  upgrades[2][0] = v;

  upgrades[3].resize(1);
  sprintf(v, "navigation: %d (+ %d%% move speed)", u.navigation + u2.navigation, 50 * (u.navigation + u2.navigation));
  upgrades[3][0] = v;

  gcui = new STK_Generate_choice_UI(upgrades, p_info);

  fprintf(stdout,"client: generate_choice().\n");
  c -> player = client -> get_id();

  while(!STK::ns_var -> require_exit && !gcui -> get_done()){

    if (client -> peek_leave_condition() != STKS_NONE){
      fprintf(stdout, "client: exiting generate choice loop due to leave condition!\n");
      break;
    }

    /* handle availabel events */
    while(!STK::ns_var -> require_exit && SDL_PollEvent(&event)){
      STK::ns_var->update(event);
      evaluate_event(c, event);
      gcui -> handle_event(event);
      if (STK::ns_var -> require_exit && gcui -> get_menu_state()){
	STK::ns_var -> require_exit = false;
	gcui -> set_menu_state(false);
      }
      STK::ns_var -> require_exit |= gcui -> get_leave();
    }

    /* key checks */
    if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT)){
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_UP)){
	c -> increment_selected(+1);
	gcui -> panel_commands -> visible = true;
	gcui -> label_commands_nships -> set_text(((string)"#ships: ").append(int2str(STK::statistics::nships(c -> get_selected_commands()))));
      }else if(STK::ns_var -> pressed_keys.is_pressed(SDLK_DOWN)){
	c -> increment_selected(-1);
	gcui -> panel_commands -> visible = true;
	gcui -> label_commands_nships -> set_text(((string)"#ships: ").append(int2str(STK::statistics::nships(c -> get_selected_commands()))));
      }
    }

    /* draw game graphics */
    SDL_FillRect(STK::ns_var -> screen,NULL,0);
    STK::ns_var -> iterate();
    universe.paint(STK::ns_var -> screen, client -> get_id());
    c -> paint(STK::ns_var -> screen);
    universe.paint_stats(STK::ns_var -> screen, client -> get_id());
    if (drag.w && drag.h){
      transparentRect(STK::ns_var -> screen, drag, 0x80, 0x802060);
      drawFrame(STK::ns_var -> screen, drag, 0x802060);
    }

    gcui -> paint(STK::ns_var -> screen);
    if (STK::ns_var -> minimap_active){
      STK_Graphics::draw_minimap(STK::ns_var -> screen, universe.get_collision_map(), p_info);
    }

    drawFrame(STK::ns_var -> screen, create_sdl_rect(0,0,STK::ns_var -> screen -> w, STK::ns_var -> screen -> h), 5, p_info -> get_color(client -> get_id()));
    SDL_UpdateRect(STK::ns_var -> screen,0,0,0,0);
    SDL_Delay(50);
  }

  fprintf(stdout, "client::game::generate_choice: post loop leave condition: %d\n", (int)(client -> peek_leave_condition() != STKS_NONE));

  fprintf(stdout,"game::generate_choice(): %d planet commands & %d fleet commands.\n", (int)c -> planet_commands.size(), (int)c -> fleet_commands.size());

  for (i = c -> planet_commands.begin(); i != c -> planet_commands.end(); i++){
    fprintf(stdout, " -> source: (t = %d, id = %d), order: (t = %d, id = %d)\n", i -> source.type, i -> source.associated_id, i -> order.type, i -> order.associated_id);
  }

  if (gcui){
    delete gcui;
  }
  gcui = NULL;

  if (STK::ns_var -> require_exit){
    SDL_Rect area = create_sdl_rect(0, 0, STK::ns_var -> screen -> w, STK::ns_var -> screen -> h);
    vector<const char*> re;
    const char* qe = "Really Leave Game?";
    re.resize(2);
    re[0] = "YES";
    re[1] = "NO";

    int result = dialogue(STK::ns_var -> screen, area, qe, re);

    if (result == 1 || result == -1){
      m.terminate();
      generate_choice(c);
    }
  }else if (client -> peek_leave_condition() == STKS_NONE){
    SDL_Rect area = create_sdl_rect(0, 0, STK::ns_var -> screen -> w, STK::ns_var -> screen -> h);
    vector<const char*> re;
    const char* qe = "Really Proceed?";
    re.resize(2);
    re[0] = "YES";
    re[1] = "NO";

    int result = dialogue(STK::ns_var -> screen, area, qe, re);

    if (result == 1 || result == -1){
      m.terminate();
      generate_choice(c);
    }
  }

  m.terminate();
}

void STK_Game::area_select(){
  if (!(drag.w && drag.h)){
    return;
  }

  list<STK_Planet*> planets = universe.get_planet_master() -> get_planets();
  list<STK_Planet*>::iterator i;
  intd ulc(drag.x, drag.y);
  intd brc(drag.x + drag.w, drag.y + drag.h);

  ulc = STK::ns_var -> screen2map(ulc);
  brc = STK::ns_var -> screen2map(brc);
  SDL_Rect r = create_sdl_rect(ulc.x, ulc.y, brc.x - ulc.x, brc.y - ulc.y); 

  universe.clear_selection();

  for (i = planets.begin(); i != planets.end(); i++){
    if (rect_contains(&r, (*i) -> position.toIntd()) && (*i) -> owner == client -> get_id()){
      (*i) -> is_selected = true;
    }
  }
}

// todo: write area_select_fleets

void STK_Game::area_select_fleets(){
  if (!(drag.w && drag.h)){
    return;
  }

  list<STK_Fleet> *fleets = universe.get_ship_master() -> get_fleets();
  list<STK_Fleet>::iterator i;
  intd ulc(drag.x, drag.y);
  intd brc(drag.x + drag.w, drag.y + drag.h);

  ulc = STK::ns_var -> screen2map(ulc);
  brc = STK::ns_var -> screen2map(brc);
  SDL_Rect r = create_sdl_rect(ulc.x, ulc.y, brc.x - ulc.x, brc.y - ulc.y); 

  universe.clear_selection();

  for (i = fleets -> begin(); i != fleets -> end(); i++){
    if (rect_contains(&r, i -> get_position().toIntd()) && i -> owner == client -> get_id()){
      i -> is_selected = true;
    }
  }
}


void STK_Game::run_battle_predictor(intd screen_pos, STK_Choice *c){
  bool invalid = false;
  int na = 0;
  int nb = 0;
  int wfa, wfb;
  STK::statistics::battle_result res;
  char mes[128];
  list<STK_Fleet>::iterator fit;
  fd pos = STK::ns_var -> screen2map(screen_pos);
  STK_Planet *c_planet = NULL;
  STK_Fleet *c_fleet = NULL;
  list<STK_Command>::iterator i;
  list<STK_Fleet> *fleet_ref = universe.get_ship_master() -> get_fleets();

  invalid = false;

  /* first identify a target */
  if (!((c_planet = universe.get_collision_map() -> get_planet(pos)) || (c_fleet = find_fleet(pos)))){
    gcui -> clear_sysmes();
    gcui -> push_sysmes("BATTLE PREDICTOR: no target!");
    return;
  }
  
  /* then find all commands and fleet destinations matching the target */
  na = 0;
  for (i = c -> planet_commands.begin(); i != c -> planet_commands.end(); i++){
    if (i -> order.type == TARGET_FLEET && c_fleet){
      if (i -> order.fleet == c_fleet){
	na += i -> quantity();
      }
    }else if (i -> order.type == TARGET_PLANET && c_planet){
      if (i -> order.planet == c_planet){
	na += i -> quantity();
      }
    }
  }

  for (i = c -> fleet_commands.begin(); i != c -> fleet_commands.end(); i++){
    if (i -> order.type == TARGET_FLEET && c_fleet){
      if (i -> order.fleet == c_fleet){
	na += i -> quantity();
      }
    }else if (i -> order.type == TARGET_PLANET && c_planet){
      if (i -> order.planet == c_planet){
	na += i -> quantity();
      }
    }
  }

  for (fit = fleet_ref -> begin(); fit != fleet_ref -> end(); fit++){
    if (fit -> command_generation.size() == 0){
      if (fit -> get_order().type == TARGET_FLEET && c_fleet){
	if (fit -> get_order().fleet == c_fleet){
	  na += fit -> get_num();
	}
      }else if (fit -> get_order().type == TARGET_PLANET && c_planet){
	if (fit -> get_order().planet == c_planet){
	  na += fit -> get_num();
	}
      }
    }
  }


  wfa = get_player(client -> get_id()) -> upgrade.warfare + get_player(client -> get_id()) -> temp_upgrade.warfare ;
  wfb = 0;
  if ((c_fleet && c_fleet -> owner != -1) || (c_planet && c_planet -> owner != -1)){
    // human player; presume same warfare level
    wfb += wfa;
  }

  nb = c_fleet ? c_fleet -> get_num() : c_planet -> num_turkeys;

  res = STK::statistics::battle_predictor(na, wfa, nb, wfb, c_planet != NULL);

  gcui -> clear_sysmes();

  gcui -> push_sysmes("BATTLE PREDICTION:");
  gcui -> push_sysmes("--------------------");
      
  sprintf(mes, "P(loss < 1/4) = %d%%", res.score[0]);
  gcui -> push_sysmes(mes);

  sprintf(mes, "P(1/4 < loss < 2/4) = %d%%", res.score[1]);
  gcui -> push_sysmes(mes);

  sprintf(mes, "P(2/4 < loss < 3/4) = %d%%", res.score[2]);
  gcui -> push_sysmes(mes);

  sprintf(mes, "P(3/4 < loss) = %d%%", res.score[3]);
  gcui -> push_sysmes(mes);

  sprintf(mes, "P(failure) = %d%%", res.score[4]);
  gcui -> push_sysmes(mes);

}


void STK_Game::evaluate_event(STK_Choice* c, SDL_Event e){
  //manipulates c according to e, adding commands
  fd pos;

  list<STK_Planet*>::iterator pi;
  list<STK_Fleet*>::iterator ci;
  list<STK_Command*>::iterator co;
  list<STK_Planet*> selected_planets;
  list<STK_Planet*> pl;
  list<STK_Fleet*> selected_fleets;
  list<STK_Fleet*> fleet_list;
  list<STK_Command*> selected_commands;
  bool make_command = false;
  STK_Command com;
  intd ulc, brc;

  selected_planets = universe.get_selected_planets();
  selected_fleets = universe.get_selected_fleets();
  selected_commands = c -> get_selected_commands();

  switch(e.type){
  case SDL_MOUSEBUTTONUP:

    if (e.button.button > 3){
      break;
    }

    /* test for area selection */
    if (e.button.button == SDL_BUTTON_LEFT && drag.w && drag.h){
      //the mouse was moved since left click
      area_select();
      drag.x = -1;
      drag.y = -1;
      drag.w = 0;
      drag.h = 0;
      break;
    }

    if (e.button.button == SDL_BUTTON_RIGHT && drag.w && drag.h){
      //the mouse was moved since right click
      area_select_fleets();
      drag.x = -1;
      drag.y = -1;
      drag.w = 0;
      drag.h = 0;
      break;
    }


    drag.x = -1;
    drag.y = -1;
    drag.w = 0;
    drag.h = 0;

    // break if the click was in the mini map
    if (STK::ns_var -> minimap_active && rect_contains(&STK::ns_var -> minimap_rect, intd(e.button.x, e.button.y))){
      break;
    }

    /* something was clicked, better find out what! */
    STK_Planet *c_planet;
    STK_Fleet *c_fleet;
    STK_Command *c_command;

    com.is_selected = true;
    pos = STK::ns_var -> screen2map(intd(e.button.x,e.button.y));

    
    if ((!STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)) && ((c_command = find_command(pos, &(c -> fleet_commands))) || (c_command = find_command(pos, &(c -> planet_commands))))){
      if (!(STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT))){
	universe.clear_selection();
	c -> clear_selection();
      }
      c_command -> is_selected = true;
      c -> print_selections();

      // right clicked planet command: set equipment
      if (c_command -> source.type == TARGET_PLANET && e.button.button == SDL_BUTTON_RIGHT){
	c_command -> equipment = STK_Graphics::make_selection(STK::ns_var -> screen, STKFE_Names, "Select fleet equipment");
      }
    }else if (c_planet = universe.get_collision_map() -> get_planet(pos)){
      fprintf(stdout,"client: planet %d with owner %d detected! (active player is %d).\n",c_planet -> id, c_planet -> owner, client -> get_id());
      if (c_planet -> owner == client -> get_id() && !STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	//clicked friendly planet, add to selection
	if (!(STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT) )){
	  //clear old selections
	  universe.clear_selection();
	  c -> clear_selection();
	}
	c_planet -> is_selected = true;
      }else if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	make_command = true;
	//clicked opponent planet, setup commands for attack!
	
	com.order.type = TARGET_PLANET;
	com.order.associated_id = c_planet -> id;
	com.order.planet = c_planet;
	com.order.fleet = NULL;
      }
    }else if ((STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT)) && !STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL) && (fleet_list = find_owned_fleets(pos)).size()){
      //clicked friendly fleet, add to selection
      for (ci = fleet_list.begin(); ci != fleet_list.end(); ci++){
	(*ci) -> is_selected = true;
      }

    }else if (c_fleet = find_fleet(pos)){
      if (c_fleet -> get_owner() == client -> get_id() && !STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	if (!(STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT) )){
	  //clear old selections
	  universe.clear_selection();
	  c -> clear_selection();
	}
	//clicked friendly fleet, add to selection
	c_fleet -> is_selected = true;

      }else if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	make_command = true;
	//clicked opponent fleet, setup commands for attack!
	
	com.order.type = TARGET_FLEET;
	com.order.associated_id = c_fleet -> id;
	com.order.fleet = c_fleet;
	com.order.planet = NULL;
      }

    }else {

      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	make_command = true;
	//clicked position, setup commands for movement!
	
	com.order.type = TARGET_POSITION;
	com.order.associated_id = -1;
	com.order.fleet = NULL;
	com.order.planet = NULL;
	com.order.position = pos;
      }else{

	//clear old selections
	universe.clear_selection();
	c -> clear_selection();
      }
    }

    //setup a source for the attack
    if (make_command){
      if (!(STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT) || STK::ns_var -> pressed_keys.is_pressed(SDLK_RSHIFT))){
	c -> clear_selection();
      }

      if (selected_planets.size()){
	//there are selected planets
	for (pi = selected_planets.begin(); pi != selected_planets.end(); pi++){
	  
	  com.source.type = TARGET_PLANET;
	  com.source.associated_id = (*pi) -> id;
	  com.source.planet = *pi;
	  com.source.fleet = NULL;
	  com.priority = 100;
	  //no signal delay for planets
	  com.signal_delay = 0;// universe.get_settings() -> calculate_signal_delay(((*pi) -> position - get_player(client -> get_id()) -> home_planet -> position).length());
	  
	  c -> add_planet_command(com);
	}
      }

      if (selected_fleets.size()){
	//there are selected fleets
	for (ci = selected_fleets.begin(); ci != selected_fleets.end(); ci++){
	  if (!(com.order.type == TARGET_FLEET && com.order.fleet == *ci)){
	    com.source.type = TARGET_FLEET;
	    com.source.associated_id = (*ci) -> id;
	    com.source.fleet = *ci;
	    com.source.planet = NULL;
	    com.priority = 100;
	    com.signal_delay = universe.get_settings() -> calculate_signal_delay(((*ci) -> get_position() - universe.get_planet_master() -> closest_owned_planet((*ci) -> get_position(), (*ci) -> owner) -> position).length());
	  
	    c -> add_fleet_command(com);
	  }
	}
      }
    }

    break;
  case SDL_MOUSEBUTTONDOWN:
    if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
      break;
    }

    if (e.button.button == 4){
      c -> increment_selected(1);
    }else if (e.button.button == 5){
      c -> increment_selected(-1);
    }else if (e.button.button == SDL_BUTTON_LEFT || e.button.button == SDL_BUTTON_RIGHT){
      if (STK::ns_var -> minimap_active && rect_contains(&STK::ns_var -> minimap_rect, intd(e.button.x, e.button.y))){
	break;
      }
      if (gcui -> overlaps_menu(e.button.x, e.button.y)){
	break;
      }
      drag.x = e.button.x;
      drag.y = e.button.y;
      drag.w = 0;
      drag.h = 0;
      cpos.x = e.button.x;
      cpos.y = e.button.y;
    }
    break;

  case SDL_MOUSEMOTION:
    if (drag.x > -1 && drag.y > -1){
      ulc.x = fmin(e.motion.x, cpos.x);
      ulc.y = fmin(e.motion.y, cpos.y);
      brc.x = fmax(e.motion.x, cpos.x);
      brc.y = fmax(e.motion.y, cpos.y);
      drag = create_sdl_rect(ulc.x, ulc.y, brc.x - ulc.x, brc.y - ulc.y);
    }
    break;
  case SDL_KEYDOWN:
    switch (e.key.keysym.sym){
    case SDLK_DELETE:
      c -> delete_selected();
      break;
    case SDLK_a:
      // Select all planets
      c -> clear_selection();

      pl = universe.get_planet_master() -> get_planets();
      for (pi = pl.begin(); pi != pl.end(); pi++){
	if ((*pi) -> owner == client -> get_id()){
	  (*pi) -> is_selected = true;
	}
      }
      break;
    case SDLK_s:
      // Select commands of selected planets
      c -> clear_selection();

      for (list<STK_Command>::iterator cit = c -> planet_commands.begin(); cit != c -> planet_commands.end(); cit++){
	cit -> is_selected = cit -> source.planet -> is_selected;
      }

      universe.clear_selection();
      break;
    case SDLK_1:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	for (co = selected_commands.begin(); co != selected_commands.end(); co++){
	  (*co) -> priority = 10;
	}
      }
      break;
    case SDLK_2:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	for (co = selected_commands.begin(); co != selected_commands.end(); co++){
	  (*co) -> priority = 50;
	}
      }
      break;
    case SDLK_3:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	for (co = selected_commands.begin(); co != selected_commands.end(); co++){
	  (*co) -> priority = 80;
	}
      }
      break;
    case SDLK_4:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	for (co = selected_commands.begin(); co != selected_commands.end(); co++){
	  (*co) -> priority = 100;
	}
      }
      break;
    case SDLK_l:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LSHIFT)){
	  c -> unlock_selected();
	}else{
	  c -> lock_selected();
	}
      }
      break;
    case SDLK_u:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	STK::ns_var -> command_paint_mode++;
	
	switch(STK::ns_var -> command_paint_mode % PAINT_MODE_NUM){
	case 0:
	  gcui -> push_sysmes("command display mode: ALL");
	  break;
	case 1:
	  gcui -> push_sysmes("command display mode: SELECTED");
	  break;
	case 2:
	  gcui -> push_sysmes("command display mode: NONE");
	  break;
	}
      }
      break;
    case SDLK_i:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	STK::ns_var -> quant_paint_mode++;
	
	switch(STK::ns_var -> quant_paint_mode % PAINT_MODE_NUM){
	case 0:
	  gcui -> push_sysmes("troop display mode: ALL");
	  break;
	case 1:
	  gcui -> push_sysmes("troop display mode: SELECTED");
	  break;
	case 2:
	  gcui -> push_sysmes("troop display mode: NONE");
	  break;
	}
      }
      break;
    case SDLK_o:
      if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
	STK::ns_var -> stats_paint_mode++;
	
	switch(STK::ns_var -> stats_paint_mode % PAINT_MODE_NUM){
	case 0:
	  gcui -> push_sysmes("stats display mode: ALL");
	  break;
	case 1:
	  gcui -> push_sysmes("stats display mode: SELECTED");
	  break;
	case 2:
	  gcui -> push_sysmes("stats display mode: NONE");
	  break;
	}
      }
      break;
	case SDLK_b:	
		
		if (STK::ns_var -> pressed_keys.is_pressed(SDLK_LCTRL)){
			int mx, my;
			SDL_GetMouseState(&mx, &my);
  		    run_battle_predictor(intd(mx,my), c);
		}
		break;
	}
  }

  //alterations to interface
  if (e.type == SDL_MOUSEBUTTONUP || e.type == SDL_KEYDOWN){
    selected_planets = universe.get_selected_planets();
    selected_fleets = universe.get_selected_fleets();
    selected_commands = c -> get_selected_commands();

    //planets
    if (selected_planets.size() == 1){
      
      gcui -> panel_splanet -> visible = true;
      gcui -> panel_mplanet -> visible = false;

      gcui -> label_splanet_name -> set_text(((string)"planet id: ").append(int2str(selected_planets.back() -> id)));
      gcui -> label_splanet_nships -> set_text(((string)"fleet: ").append(int2str((int)selected_planets.back() -> num_turkeys)));
      gcui -> label_splanet_production -> set_text(((string)"prod.: ").append(int2str(STK::statistics::production(selected_planets.back(), universe.get_settings()))));
      gcui -> label_splanet_leaving -> set_text(((string)"leaving: ").append(int2str(STK::statistics::leaving(selected_planets.back()))));

    }else if (selected_planets.size() > 1){
      
      gcui -> panel_mplanet -> visible = true;
      gcui -> panel_splanet -> visible = false;

      gcui -> label_mplanet_nplanets -> set_text(((string)"#planets: ").append(int2str(selected_planets.size())));
      gcui -> label_mplanet_nships -> set_text(((string)"#ships: ").append(int2str(STK::statistics::nships(selected_planets))));
      gcui -> label_mplanet_production -> set_text(((string)"prod.: ").append(int2str(STK::statistics::production(selected_planets, universe.get_settings()))));
      gcui -> label_mplanet_leaving -> set_text(((string)"leaving: ").append(int2str(STK::statistics::leaving(selected_planets))));

    }else{
      
      gcui -> panel_splanet -> visible = false;
      gcui -> panel_mplanet -> visible = false;
    }

    //fleets
    if (selected_fleets.size() == 1){
      
      gcui -> panel_sfleet -> visible = true;
      gcui -> panel_mfleet -> visible = false;

      gcui -> label_sfleet_nships -> set_text(((string)"fleet id: ").append(int2str((int)selected_fleets.back() -> id)));
      gcui -> label_sfleet_nships -> set_text(((string)"fleet: ").append(int2str((int)selected_fleets.back() -> get_num())));
      gcui -> label_sfleet_leaving -> set_text(((string)"leaving: ").append(int2str(STK::statistics::leaving(selected_fleets.back()))));
      gcui -> label_sfleet_eta -> set_text(((string)"eta: ").append(int2str(STK::statistics::eta(selected_fleets.back(), universe.get_settings() -> ship_settings.ship_speed))));
      gcui -> label_sfleet_dist -> set_text(((string)"dist: ").append(int2str(STK::statistics::dist(selected_fleets.back(), get_player(client -> get_id()) -> home_planet, universe.get_settings()))));

    }else if (selected_fleets.size() > 1){
      gcui -> panel_mfleet -> visible = true;
      gcui -> panel_sfleet -> visible = false;

      gcui -> label_mfleet_nfleets -> set_text(((string)"#fleets: ").append(int2str(selected_fleets.size())));
      gcui -> label_mfleet_nships -> set_text(((string)"#ships: ").append(int2str(STK::statistics::nships(selected_fleets))));
    }else{
      gcui -> panel_sfleet -> visible = false;
      gcui -> panel_mfleet -> visible = false;
    }

    //commands
    if (selected_commands.size()){
      gcui -> panel_commands -> visible = true;
      gcui -> label_commands_nships -> set_text(((string)"#ships: ").append(int2str(STK::statistics::nships(selected_commands))));
    }else{
      gcui -> panel_commands -> visible = false;
    }
  }
}

STK_Player* STK_Game::get_player(int id){
  int i;
  for (i = 0; i < player.size(); i++){
    if (player[i].id == id){
      return &player[i];
    }
  }
  return NULL;
}

bool STK_Game::player_exists(int id){
  int i;
  for (i = 0; i < player.size(); i++){
    if (player[i].id == id){
      return true;
    }
  }
  return false;
}

list<STK_Fleet*> STK_Game::find_owned_fleets(fd p){
  int x, y, a;
  int w;
  int cmap_width = universe.get_settings() -> cmap_width();
  intd q = universe.get_collision_map() -> map2col(p);
  intd q2;
  STK_Ship_map *ship_map = &universe.get_collision_map() -> ship_map;
  STK_Ship *ship;
  list<STK_Fleet*> list;

  w = FLEET_SCAN_RADIUS;

  for (a = 0; a < w * w; a++){
    x = a % w - w / 2;
    y = a / w - w / 2;

    q2 = q + intd(x,y);
    if (universe.get_collision_map() -> validate_position(q2) && (ship = ship_map -> map[cmap_width * q2.y + q2.x]) && ship -> owner == client -> get_id() && !STK::statistics::contains_fleet(list, ship -> fleet)){
      list.push_back(ship -> fleet);
    }
  }

  return list;
}

list<STK_Fleet*> STK_Game::find_all_fleets_lookup(fd p){
  list<STK_Fleet*> flist;
  list<STK_Fleet>::iterator i;
  list<STK_Fleet> *lookup = universe.get_ship_master() -> get_fleets();
  float srad = FLEET_SCAN_RADIUS / universe.get_settings() -> ratio;

  for (i = lookup -> begin(); i != lookup -> end(); i++){
    if ((p - i -> get_position()).length() < srad){
      flist.push_back(&*i);
    }
  }

  return flist;
}

list<STK_Fleet*> STK_Game::find_all_fleets(fd p){
  STK_Collision_map *colmap = universe.get_collision_map();
  if (!colmap -> validate_position(colmap -> map2col(p))){
    fprintf(stdout, "game::find_all_fleets(): calling list lookup.\n");
    return find_all_fleets_lookup(p);
  }

  int x, y, a;
  int w;
  int cmap_width = universe.get_settings() -> cmap_width();
  intd q = colmap -> map2col(p);
  intd q2;
  STK_Ship_map *ship_map = &colmap -> ship_map;
  STK_Ship *ship;
  list<STK_Fleet*> flist;

  w = FLEET_SCAN_RADIUS;

  for (a = 0; a < w * w; a++){
    x = a % w - w / 2;
    y = a / w - w / 2;

    q2 = q + intd(x,y);
    if (colmap -> validate_position(q2) && (ship = ship_map -> map[cmap_width * q2.y + q2.x]) && !STK::statistics::contains_fleet(flist, ship -> fleet)){
      flist.push_back(ship -> fleet);
    }
  }

  return flist;
}

STK_Fleet* STK_Game::find_fleet(fd p){
  float min = 100;
  list<STK_Fleet*>::iterator i;
  list<STK_Fleet*> list = find_all_fleets(p);
  STK_Fleet *c = NULL;
  
  for (i = list.begin(); i != list.end(); i++){
    float r = (p - (*i) -> get_position()).length();
    
    if (r < min){
      c = *i;
      min = r;
      
    }
  }

  return c;
}

STK_Command* STK_Game::find_command(fd p, list<STK_Command> *l){
  float min = 10 / STK::ns_var -> zoom;
  list<STK_Command>::iterator i;
  STK_Command *c = NULL;
  
  for (i = l -> begin(); i != l -> end(); i++){
    if (STK::ns_var -> command_paint_mode % PAINT_MODE_NUM == PAINT_MODE_ALL || (STK::ns_var -> command_paint_mode % PAINT_MODE_NUM == PAINT_MODE_SELECTED && (i -> is_selected || i -> source_selected()))){
      fd s,d;
      i -> get_positions(&s, &d);

      // compensate planet radius
      if (i -> source.type == TARGET_PLANET && (d - s).length() > 0){
	s = s + i -> source.planet -> radius * fd::normv((d-s).getAngle());
      }

      float r = shortestDistance(s,d,p);
      if (r < min){
	c = &(*i);
	min = r;
      }
    }
  }

  return c;
}
