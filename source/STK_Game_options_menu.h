
class STK_Game_options_menu : public STK_Menu{
 public:
  STK_Game_options_menu(STK_Menu_references *r);
  ~STK_Game_options_menu();
  void listen(SDL_Event e, RSG_Component *c);
};
