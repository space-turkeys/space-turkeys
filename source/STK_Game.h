#ifndef _STK_GAME
#define _STK_GAME

#include <vector>
#include <list>

#include "SDL/SDL.h"

#include "STK_Structures.h"
#include "STK_Player.h"
#include "STK_Universe.h"
#include "STK_Game_settings.h"

class STK_Game{
 protected:
  STK_Generate_choice_UI *gcui;
  intd cpos;
  SDL_Rect drag;
 public:
  STK_Client *client;
  STK_Game_settings settings;
  std::vector<STK_Player> player;
  STK_Universe universe;
  bool require_spectate;

  void initialize(STK_Game_settings s, STK_Universe_settings uvs, STK_Client *cl);
  virtual void evoke();
  virtual void exit_interface();
  STK_Choice *get_choice();
  void run_battle_predictor(intd p, STK_Choice *c);
  bool wait_for_choices(std::vector<STK_Choice> *l, STK_Choice *c);
  void evaluate_round();
  void apply_upgrades(std::vector<STK_Choice> l);
  void generate_choice(STK_Choice* c);
  void area_select();
  void area_select_fleets();
  std::list<STK_Upgrade_choice> select_upgrade();
  virtual void evaluate_event(STK_Choice* c, SDL_Event e);
  STK_Player* get_player(int id);
  bool player_exists(int id);
  std::list<STK_Fleet*> find_all_fleets(fd p);
  std::list<STK_Fleet*> find_all_fleets_lookup(fd p);
  std::list<STK_Fleet*> find_owned_fleets(fd p);
  STK_Fleet* find_fleet(fd pos);
  STK_Command* find_command(fd pos, std::list<STK_Command> *c);
  void cleanup();
};
#endif
