#include "STK_Universe.h"
#include "STK_Class_headers.h"
#include "RSK/RskTypes.h"
#include "STK_Structures.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Basic_communication.h"
#include "STK_Server_structs.h"

using namespace std;

SDL_Surface *STK_Universe::buf_alpha = 0;
SDL_Surface *STK_Universe::buf_beta = 0;
SDL_Surface *STK_Universe::buf_gamma = 0;
SDL_Surface *STK_Universe::buf_delta = 0;

STK_Universe::STK_Universe(){
  initialized=false;
}

void STK_Universe::iterate(){
  /* Run an iteration of all game objects in the universe. Graphical objects are not iterated in this routine. */
  ship_master.iterate(game);
  planet_master.iterate();
}

int STK_Universe::get_idx(int id){
  int i, idx = -1;
  fprintf(stdout, "universe::get_idx: %d players: ", settings.p_info.num_players);
  for (i = 0; i < settings.p_info.num_players; i++){
    fprintf(stdout, "%d ", settings.p_info.players[i]);
    if (settings.p_info.players[i] == id){
      idx = i;
      break;
    }
  }

  fprintf(stdout, "\n");

  if (idx == -1){
    fprintf(stdout, "universe::get_idx: invalid id: %d!\n", id);
    exit(-1);
  }

  return idx;
}

int STK_Universe::get_points(int id){
  return upgrades[get_idx(id)];
}

void STK_Universe::use_points(int id, int p){
  int idx = get_idx(id);
  upgrades[idx] -= p;
  if (upgrades[idx] < 0){
    fprintf(stdout, "universe::use_points: invalid point usage!\n");
    exit(-1);
  }
}

void STK_Universe::add_points(int id, int p){
  int idx = get_idx(id);
  upgrades[idx] += p;
}

void STK_Universe::apply_choices(vector<STK_Choice> c){

  list<STK_Command>::iterator i;
  vector<STK_Choice>::iterator j;

  fprintf(stdout,"universe::apply_choices()\n");

  for (j = c.begin(); j != c.end(); j++){
    for (i = j -> fleet_commands.begin(); i != j -> fleet_commands.end(); i ++){
      fprintf(stdout,"universe::apply_choices(): adding fleet command.\n");
      i -> source.fleet -> give_command(*i);
    }
    for (i = j -> planet_commands.begin(); i != j -> planet_commands.end(); i ++){
      fprintf(stdout,"universe::apply_choices(): adding planet.\n");
      i -> source.planet -> give_command(*i);
    }
  }
}

void STK_Universe::paint_grid(SDL_Surface *s){
  fd north(settings.width/2, 0);
  fd south(settings.width/2, settings.height);
  fd east(settings.width, settings.height/2);
  fd west(0, settings.height/2);

  north = STK::ns_var -> map2screen(north);
  south = STK::ns_var -> map2screen(south);
  east = STK::ns_var -> map2screen(east);
  west = STK::ns_var -> map2screen(west);
  SDL_Rect b;
  int spacing = 5;

  //alpha quadrant symbols
  b = create_sdl_rect(north.x - buf_alpha -> w - spacing, north.y, buf_alpha -> w, buf_alpha -> h);
  SDL_BlitSurface(buf_alpha, 0, s, &b);

  b = create_sdl_rect(west.x, west.y - buf_alpha -> h - spacing, buf_alpha -> w, buf_alpha -> h);
  SDL_BlitSurface(buf_alpha, 0, s, &b);

  //beta quadrant symbols
  b = create_sdl_rect(north.x + spacing, north.y, buf_beta -> w, buf_beta -> h);
  SDL_BlitSurface(buf_beta, 0, s, &b);

  b = create_sdl_rect(east.x - buf_beta -> w, east.y - buf_beta -> h - spacing, buf_beta -> w, buf_beta -> h);
  SDL_BlitSurface(buf_beta, 0, s, &b);

  //gamma quadrant symbols
  b = create_sdl_rect(west.x, west.y + spacing, buf_gamma -> w, buf_gamma -> h);
  SDL_BlitSurface(buf_gamma, 0, s, &b);

  b = create_sdl_rect(south.x - buf_gamma -> w - spacing, south.y - buf_gamma -> h, buf_gamma -> w, buf_gamma -> h);
  SDL_BlitSurface(buf_gamma, 0, s, &b);

  //delta quadrant symbols
  b = create_sdl_rect(south.x + spacing, south.y - buf_delta -> h, buf_delta -> w, buf_delta -> h);
  SDL_BlitSurface(buf_delta, 0, s, &b);

  b = create_sdl_rect(east.x - buf_delta -> w, east.y + spacing, buf_delta -> w, buf_delta -> h);
  SDL_BlitSurface(buf_delta, 0, s, &b);

  drawLineSafe(s, north, south, 1, 0xaaffff);
  drawLineSafe(s, east, west, 1, 0xaaffff);
}

void STK_Universe::load_image_bufs(){
  
  buf_alpha = SDL_LoadBMP("data/alpha_test.bmp");
  if (!buf_alpha){
    fprintf(stdout, "universe::load_image_bufs: failed to load data!\n");
    exit(1);
  }

  SDL_SetAlpha(buf_alpha, SDL_SRCALPHA, 0);
  buf_alpha->format->Amask = 0xFF000000;
  buf_alpha->format->Ashift = 24; 
  
  buf_beta = SDL_LoadBMP("data/beta_test.bmp");
  if (!buf_beta){
    fprintf(stdout, "universe::load_image_bufs: failed to load data!\n");
    exit(1);
  }

  SDL_SetAlpha(buf_beta, SDL_SRCALPHA, 0);
  buf_beta->format->Amask = 0xFF000000;
  buf_beta->format->Ashift = 24; 
  
  buf_gamma = SDL_LoadBMP("data/gamma_test.bmp");
  if (!buf_gamma){
    fprintf(stdout, "universe::paint_grid: failed to load data!\n");
    exit(1);
  }

  SDL_SetAlpha(buf_gamma, SDL_SRCALPHA, 0);
  buf_gamma->format->Amask = 0xFF000000;
  buf_gamma->format->Ashift = 24; 
  
  buf_delta = SDL_LoadBMP("data/delta_test.bmp");
  if (!buf_delta){
    fprintf(stdout, "universe::paint_grid: failed to load data!\n");
    exit(1);
  }

  SDL_SetAlpha(buf_delta, SDL_SRCALPHA, 0);
  buf_delta->format->Amask = 0xFF000000;
  buf_delta->format->Ashift = 24; 
}

void STK_Universe::free_image_bufs(){
  if (buf_alpha){
    SDL_FreeSurface(buf_alpha);
  }
  if (buf_beta){
    SDL_FreeSurface(buf_beta);
  }
  if (buf_gamma){
    SDL_FreeSurface(buf_gamma);
  }
  if (buf_delta){
    SDL_FreeSurface(buf_delta);
  }
}

void STK_Universe::paint_stats(SDL_Surface* s, int pid){
  planet_master.paint_stats(s, pid);
  ship_master.paint_stats(s, pid);
}

void STK_Universe::paint(SDL_Surface* s, int pid){
  /* Draw graphics for all game objects + stars to the main screen. */
  stars.paint(s);
  paint_grid(s);
  planet_master.paint(s, pid);
  ship_master.paint(s, pid);
}

int STK_Universe::calculate_size(){
  /* this routine calculates the memory size of the universe object in bytes, including all referenced objects. This size is used to create the data buffer which is sent to the server. */
  return 4 + sizeof(STK_Universe_settings) + upgrades.size() * sizeof(int) + ship_master.calculate_size() + planet_master.calculate_size();
}

void STK_Universe::set_game_ref(STK_Game *g){
  game = g;
}

bool STK_Universe::load_ref_data(int sock, int timeout){
  if (load_data(sock, timeout)){
    rebuild_references();
    return true;
  }
  return false;
}

bool STK_Universe::load_data(int sock, int timeout){
  /* Attempts to load a universe object from the socket sock. If this succedes, the universe data is cleared and replaced by the loaded data and true is returned. Else, false is returned. */ 
  int max;
  int c=0;
  int n;
  int i;
  char* buf=load_buffer(sock,&max,BUFKEY_UNIVERSE, timeout);
  list<STK_Planet*> p;

  if (!buf || max < sizeof(STK_Universe_settings)){
    fprintf(stdout,"universe::load: failed to load buffer.\n");
    return false;
  }

  /* read universe settings */
  read_from_buf(buf,(char*)&settings,sizeof(STK_Universe_settings));
  c = sizeof(STK_Universe_settings);
  /* read planet master */
  if (!planet_master.load_data(buf,&c,max)){fprintf(stdout,"uv::load: planet master failed!\n"); delete [] buf; return false;}
  /* read ship master */
  if (!ship_master.load_data(buf,&c,max)){fprintf(stdout,"uv::load: ship master failed!\n"); delete [] buf; return false;}
  
  /* read num_upgrades and upgrades */
  if (c+4>max){fprintf(stdout,"uv::load: buffer overflow!\n"); delete [] buf; return false;}
  read_from_buf(buf+c,(char*)&n,4);
  if (n<0){
    fprintf(stdout,"uv::load: bad parameters!\n");
    delete [] buf; return false;
  }
  c+=4;

  if (c+n*sizeof(int)>max){fprintf(stdout,"uv::load: buffer overflow!\n"); delete [] buf; return false;}
  upgrades.clear();

  fprintf(stdout,"universe::load_data(): reading %d upgrade events:\n", n);
  upgrades.resize(n);
  read_from_buf(buf+c, (char*)&upgrades[0], n * sizeof(int));
  fprintf(stdout,"uv::load: loaded %d upgrades.\n", upgrades.size());

  initialized=true;
  delete [] buf;
  return true;
}

bool STK_Universe::send_data(int sock, int timeout){
  /* Attempts to send all universe data to the socket sock. Returns true on success and false on fail. */
  int max;
  char *buf;
  int c=0;
  // list<STK_Upgrade_event>::iterator i;
  int n;
  bool success;
  if (!initialized){fprintf(stdout,"uv::send(): uninit!\n"); return false;}

  fprintf(stdout,"universe::send(): generating data...\n");

  /* generate buffer */
  max=calculate_size();
  if (max<5){
    fprintf(stdout,"universe::send(): nothing to do.\n");
    return false;
  }
  if (!(buf=new char[max])){
    fprintf(stdout,"univers::send(): out of memory!\n");
    return false;
  }

  write_to_buf((char*)&settings,buf,sizeof(STK_Universe_settings));
  c = sizeof(STK_Universe_settings);

  n=upgrades.size();
  fprintf(stdout,"universe::send: calcluated max: %d\n",max);
  if (!planet_master.send_data(buf,&c,max)){fprintf(stdout,"uv::send: planet master failed!\n"); delete [] buf; return false;}
  if (!ship_master.send_data(buf,&c,max)){fprintf(stdout,"uv::send: ship master failed!\n"); delete [] buf; return false;}

  if (c+4+n*sizeof(int)>max){fprintf(stdout,"uv::send: buffer overflow! %d>%d\n",c+4+n*sizeof(int),max); delete [] buf; return false;}
  write_to_buf((char*)&n,buf+c,4);
  c+=4;

  fprintf(stdout,"uv::send: sending %d upgrades:\n", upgrades.size());
  write_to_buf((char*)&upgrades[0], buf+c, n * sizeof(int));

  /* send buffer */
  fprintf(stdout,"universe::send(): transmitting data...\n");
  if(send_buffer(sock,buf,max,BUFKEY_UNIVERSE,timeout) != STKS_READY){
    fprintf(stdout,"universe::send(): connection timed out!\n");
    delete [] buf;
    return false;
  }
  fprintf(stdout,"universe::send(): done!\n");
  delete [] buf;
  return true;
}

bool STK_Universe::differs(STK_Universe u){
  vector<int>::iterator i,j;
  int c;
  char *p1 = (char*)(&settings);
  char *p2 = (char*)(u.get_settings());
  bool dif_sm, dif_pm, dif_cm;
  
  /* compare settings */
  for (c = 0; c < sizeof(STK_Universe_settings); c++){
    if (*(p1 + c) != *(p2 + c)){
      fprintf(stdout,"universe::differs(): settings @ %x.\n", c);
      return true;
    }
  }

  /* compare upgrade events */
  i = upgrades.begin(); 
  j = u.get_upgrades() -> begin();
  if (upgrades.size() != u.get_upgrades() -> size()){
    fprintf(stdout,"universe::differs(): upgrades have different size: %d != %d.\n", upgrades.size(), u.get_upgrades() -> size());
    return true;
  }

  fprintf(stdout, "universe::differs(): comparing event lists: \n");
  while (i != upgrades.end()){
    fprintf(stdout, "%d to %d\n", *i, *j);
    i++;
    j++;
  }
  
  i = upgrades.begin(); 
  j = u.get_upgrades() -> begin();
  while (i != upgrades.end()){
    if (*i != *j){
      fprintf(stdout,"universe::differs: %d != %d.\n", *i, *j);
      return true;
    }
    i++;
    j++;
  }

  /* compare planets, ships and maps */
  dif_sm = ship_master.differs(&u.ship_master);
  dif_pm = planet_master.differs(&u.planet_master);
  dif_cm = collision_map.differs(u.collision_map);
  
  if (dif_sm){
    fprintf(stdout,"universe::differs(): ship_master differs!\n");
  }
  if (dif_pm){
    fprintf(stdout,"universe::differs(): planet_master differs!\n");
  }
  if (dif_cm){
    fprintf(stdout,"universe::differs(): collision_map differs!\n");
  }
  
  return dif_sm || dif_pm || dif_cm;
}

void STK_Universe::rebuild_references(){
  vector<int>::iterator i;

  list<STK_Ship> *ships = ship_master.get_fleet();
  list<STK_Ship>::iterator j;


  if (!game){
    fprintf(stdout, "universe::rebuild_references(): no game!\n");
    exit(1);
  }

  STK_Planet::multiplier = &settings.productivity_multiplier;
  STK_Planet::def_bonus = &settings.planet_defense;
  STK_Planet::upgrade_events = &upgrades;

  planet_master.rebuild_references(game, &settings.p_info, &ship_master);
  ship_master.rebuild_references(&collision_map, &planet_master);
  collision_map.settings_ref(&settings);
  collision_map.rebuild_references(&planet_master);

  //redraw ships
  for (j = ships -> begin(); j != ships -> end(); j++){
    collision_map.add_ship(j -> get_position(), &*j);
  } 
}

bool STK_Universe::initialize(STK_Game *game, STK_Universe_settings s){
  fprintf(stdout,"universe::initialize()\n");
  if (s.width<STK_MIN_UNIVERSEDIM || s.height<STK_MIN_UNIVERSEDIM || s.ratio<STK_MIN_UNIVERSERATIO){
    fprintf(stdout,"universe::initialize(): bad parameters!\n");
    return false;
  }

  settings = s;
  //stars should be initialized by client.
  // fprintf(stdout,"universe::initialize(): initializing stars!\n");
  // stars.initialize(intd(s.width,s.height));

  fprintf(stdout,"universe::initialize(): initializing planets!\n");
  if (!planet_master.initialize(game, s, &settings.p_info, &ship_master)){
    return false;
  }

  //collision map is initialized in load_data.  
  // fprintf(stdout,"universe::initialize(): initializing collision map!\n");
  // collision_map.initialize(&settings,&planet_master);

  fprintf(stdout,"universe::initialize(): initializing ship_master!\n");
  ship_master.initialize(s.ship_settings, &collision_map, &planet_master);

  fprintf(stdout, "universe::initialize(): %d upgrades", s.p_info.num_players);
  upgrades.clear();
  upgrades.resize(s.p_info.num_players);
  memset(&upgrades[0], 0, s.p_info.num_players * sizeof(int));

  fprintf(stdout,"universe::initialize(): complete!\n");

  initialized=true;
  return true;
}

STK_Collision_map *STK_Universe::get_collision_map(){
  return &collision_map;
}

STK_Ship_master* STK_Universe::get_ship_master(){
  return &ship_master;
}

STK_Planet_master* STK_Universe::get_planet_master(){
  return &planet_master;
}

list<STK_Planet*> STK_Universe::get_selected_planets(){
  vector<STK_Planet>::iterator i;
  list<STK_Planet*> l;

  for (i = planet_master.planets.begin(); i != planet_master.planets.end(); i++){
    if (i -> is_selected){
      l.push_back(&*i);
    }
  }
  
  return l;
}

list<STK_Fleet*> STK_Universe::get_selected_fleets(){
  list<STK_Fleet>::iterator i;
  list<STK_Fleet*> l;

  for (i = ship_master.get_fleets() -> begin(); i != ship_master.get_fleets() -> end(); i++){
    if (i -> is_selected){
      l.push_back(&*i);
    }
  }
  
  return l;
}

void STK_Universe::clear_choice(){
  vector<STK_Planet>::iterator i;
  list<STK_Fleet>::iterator j;

  for (i = planet_master.planets.begin(); i != planet_master.planets.end(); i++){
    i -> command_generation.clear();
  }

  for (j = ship_master.get_fleets() -> begin(); j != ship_master.get_fleets() -> end(); j++){
    j -> command_generation.clear();
  }  
  
}

void STK_Universe::clear_selection(){
  vector<STK_Planet>::iterator i;
  list<STK_Fleet>::iterator j;

  for (i = planet_master.planets.begin(); i != planet_master.planets.end(); i++){
    i -> is_selected = false;
  }

  for (j = ship_master.get_fleets() -> begin(); j != ship_master.get_fleets() -> end(); j++){
    j -> is_selected = false;
  }  
}

bool STK_Universe::has_planets(int id){
  list<STK_Planet*> planets;
  list<STK_Planet*>::iterator i;
  bool k = false;

  planets = planet_master.get_planets();
  for (i = planets.begin(); i != planets.end() && !k; i++){
    k |= (*i) -> owner == id;
  }
  return k;
}

STK_Universe_settings* STK_Universe::get_settings(){
  return &settings;
}

vector<int> *STK_Universe::get_upgrades(){
  return &upgrades;
}

void STK_Universe::clear_upgrades(){
  upgrades.clear();
}

void STK_Universe::cleanup(){
  planet_master.cleanup();
  ship_master.cleanup();
  collision_map.cleanup();
  upgrades.clear();
  stars.cleanup();
}
