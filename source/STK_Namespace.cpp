#include "STK_Namespace.h"

namespace STK{
  STK_Randomizer rand_keeper;
  STK_Control_vars *ns_var;
  boost::mutex game_data_lock, client_data_lock, game_info_lock, universe_info_lock, info_lock;
};
