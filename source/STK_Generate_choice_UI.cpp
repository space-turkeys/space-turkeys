#include <cstdlib>

#include "STK_Generate_choice_UI.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Graphics.h"

using namespace std;

STK_Generate_choice_UI::STK_Generate_choice_UI(vector<vector<string> > upgrades, player_info *pi){

  color_scheme cs;
  SDL_Rect planet_rect;
  SDL_Rect fleet_rect;
  SDL_Rect command_rect;
  SDL_Rect options_rect;
  SDL_Rect upg_rect;
  int w;
  int h;
  SDL_Rect r; 

  p_info = pi;
  
  w = STK::ns_var -> screen -> w;
  h = STK::ns_var -> screen -> h;
  planet_rect = create_sdl_rect(w - 100, 0, 100, h/3);
  fleet_rect = create_sdl_rect(w - 100, h/3, 100, h/3);
  command_rect = create_sdl_rect(w - 100, 2*h/3, 100, h/3 - 80);
  options_rect = create_sdl_rect(10, 60, 200, 6 * (40 + 5));
  upg_rect = create_sdl_rect(220, 10, 300, 300);

  //options
  r = create_sdl_rect(10, 10, 100, 40);
  button_options = new RSG_Button("Options", r);
  r.w = 180;
  button_options_upgrades = new RSG_Button("Upgrades", r);
  r.y += r.h + 5;
  button_options_players = new RSG_Button("Players", r);
  r.y += r.h + 5;
  button_options_hs_minimap = new RSG_Button("Hide minimap", r);
  r.y += r.h + 5;
  button_options_help = new RSG_Button("HELP", r);
  r.y += r.h + 5;
  button_options_controls = new RSG_Button("Controls", r);
  r.y += r.h + 5;
  button_options_leave = new RSG_Button("Leave game", r);

  panel_options = new RSG_Panel(options_rect);
  panel_options -> add(button_options_upgrades);
  panel_options -> add(button_options_players);
  panel_options -> add(button_options_hs_minimap);
  panel_options -> add(button_options_help);
  panel_options -> add(button_options_controls);
  panel_options -> add(button_options_leave);
  panel_options -> visible = false;

  //upgrades
  table_upgrades = new RSG_Table(upgrades, upg_rect);
  table_upgrades -> visible = false;

  //next turn button
  button_done = new RSG_Button("PROCEED", create_sdl_rect(10 + STK::ns_var -> minimap_rect.w + 20, h - 80, w - 20 - (STK::ns_var -> minimap_rect.w + 20), 60));

  //single planet panel
  panel_splanet = new RSG_Panel(planet_rect);
  r = create_sdl_rect(4,4,planet_rect.w - 8, 20);
  label_splanet_name = new RSG_Label("planet id: xxx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_splanet_nships = new RSG_Label("fleet: xxxx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_splanet_production = new RSG_Label("prod.: xx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_splanet_leaving = new RSG_Label("leaving: xxxx", cs, RSG_Component::default_font, r);

  panel_splanet -> add(label_splanet_name);
  panel_splanet -> add(label_splanet_nships);
  panel_splanet -> add(label_splanet_production);
  panel_splanet -> add(label_splanet_leaving);
  panel_splanet -> visible = false;

  //multiple planets panel
  panel_mplanet = new RSG_Panel(planet_rect);
  r = create_sdl_rect(4,4,planet_rect.w - 8, 20);
  label_mplanet_nplanets = new RSG_Label("#planets: xx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_mplanet_nships = new RSG_Label("#ships: xxxx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_mplanet_production = new RSG_Label("prod.: xxx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_mplanet_leaving = new RSG_Label("leaving: xxxx", cs, RSG_Component::default_font, r);

  panel_mplanet -> add(label_mplanet_nplanets);
  panel_mplanet -> add(label_mplanet_nships);
  panel_mplanet -> add(label_mplanet_production);
  panel_mplanet -> add(label_mplanet_leaving);
  panel_mplanet -> visible = false;

  //single fleet panel
  panel_sfleet = new RSG_Panel(fleet_rect);
  r = create_sdl_rect(4,4,fleet_rect.w - 8, 20);
  label_sfleet_name = new RSG_Label("fleet id: xx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_sfleet_nships = new RSG_Label("fleet: xxx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_sfleet_leaving = new RSG_Label("leaving: xxx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_sfleet_eta = new RSG_Label("eta: xxx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_sfleet_dist = new RSG_Label("dist: xx", cs, RSG_Component::default_font, r);

  panel_sfleet -> add(label_sfleet_name);
  panel_sfleet -> add(label_sfleet_nships);
  panel_sfleet -> add(label_sfleet_leaving);
  panel_sfleet -> add(label_sfleet_eta);
  panel_sfleet -> add(label_sfleet_dist);
  panel_sfleet -> visible = false;

  //multiple fleets panel
  panel_mfleet = new RSG_Panel(fleet_rect);
  r = create_sdl_rect(4,4,fleet_rect.w - 8, 20);
  label_mfleet_nfleets = new RSG_Label("#fleets: xx", cs, RSG_Component::default_font, r);
  r.y += 25;
  label_mfleet_nships = new RSG_Label("#ships: xxxx", cs, RSG_Component::default_font, r);

  panel_mfleet -> add(label_mfleet_nships);
  panel_mfleet -> add(label_mfleet_nships);
  panel_mfleet -> visible = false;

  //commands panel
  panel_commands = new RSG_Panel(command_rect);
  r = create_sdl_rect(4,4,command_rect.w - 8, 20);
  label_commands_nships = new RSG_Label("#ships: xxxx", cs, RSG_Component::default_font, r);
  panel_commands -> add(label_commands_nships);
  panel_commands -> visible = false;

  //add components
  gui.add(panel_splanet);
  gui.add(panel_mplanet);
  gui.add(panel_sfleet);
  gui.add(panel_mfleet);
  gui.add(panel_commands);
  gui.add(panel_options);
  gui.add(button_options);
  gui.add(button_done);
  gui.add(table_upgrades);

  STK::ns_var -> minimap_active = true;
  show_players = false;

}

STK_Generate_choice_UI::~STK_Generate_choice_UI(){
  gui.delete_components();
}

bool STK_Generate_choice_UI::get_done(){
  bool b = button_done -> get_state();
  button_done -> reset();
  return b;
}

bool STK_Generate_choice_UI::get_leave(){
  bool b = button_options_leave -> get_state();
  button_options_leave -> reset();
  return b;
}

void STK_Generate_choice_UI::clear_sysmes(){
  system_message.clear();
}

void STK_Generate_choice_UI::push_sysmes(char *v){
  pair<int, char*> mes;
  if (strlen(v) > 127){
    fprintf(stdout, "gcui::push_sysmes: error: overflow.\n");
    return;
  }

  mes.second = new char[128];

  strcpy((char*)mes.second, v);
  mes.first = 100;

  system_message.push_back(mes);
}

void STK_Generate_choice_UI::paint(SDL_Surface *s){
  int c = 0;
  list<pair<int, char*> >::iterator i;
  gui.paint(s);
  
  for (i = system_message.begin(); i != system_message.end(); i++){
    if (i -> first > 0){
      unsigned char alpha = 500 * (exp(0.1 * i -> first) / (exp(0.1 * i -> first) + 1) - 0.5);
      TTF_Font *f = TTF_OpenFont("fonts/arial.ttf",20);
      SDL_Rect r = testTextSize(i -> second, f);
      r.x = s -> w - r.w - 10;
      r.y = 10 + 20 * c;
      drawText(s, &r, i -> second, 0xffffff | (alpha << 24), f);
      TTF_CloseFont(f);
      i -> first--;
      c++;
    }else{
	  delete [] i -> second;
      system_message.erase(i--);
    }
  }

  if (show_players){
    int w = STK::ns_var -> screen -> w;
    int h = STK::ns_var -> screen -> h;
    SDL_Rect area = create_sdl_rect(w / 4, h / 4, w / 2, h /2);
    SDL_Rect r = area;
    r.h /= p_info -> num_players;

    SDL_FillRect(STK::ns_var -> screen, &area, 0);

    for (int j = 0; j < p_info -> num_players; j++){
      r.y = area.y + j * r.h;
      drawText(STK::ns_var -> screen, &r, p_info -> player_names[j], p_info -> get_color(p_info -> players[j]), .8 * r.h);
    }
  }

}

void STK_Generate_choice_UI::handle_event(SDL_Event e){
  gui.handle_event(e);

  if (e.type == SDL_MOUSEBUTTONDOWN){
    if (panel_options -> visible && (!rect_contains(&panel_options -> bounds, intd(e.button.x, e.button.y))) && !(table_upgrades -> visible && rect_contains(&table_upgrades -> bounds, intd(e.button.x, e.button.y)))){
      panel_options -> visible = false;
      table_upgrades -> visible = false;
      show_players = false;
    }
  }
  
  if (e.type == SDL_KEYDOWN && (e.key.keysym.sym == SDLK_SPACE || e.key.keysym.sym == SDLK_RETURN)){
    button_done -> set_state(true);
  }

  if (button_options -> get_state()){
    button_options -> reset();
    panel_options -> visible = !panel_options -> visible;
    table_upgrades -> visible &= panel_options -> visible;
  }

  if (button_options_upgrades -> get_state()){
    button_options_upgrades -> reset();
    table_upgrades -> visible = !table_upgrades -> visible;
    show_players = false;
  }

  if (button_options_hs_minimap -> get_state()){
    button_options_hs_minimap -> reset();
    if (STK::ns_var -> minimap_active){
      STK::ns_var -> minimap_active = false;
      button_options_hs_minimap -> init("Show minimap", NULL);
    }else{
      STK::ns_var -> minimap_active = true;
      button_options_hs_minimap -> init("Hide minimap", NULL);
    }
  }

  if (button_options_players -> get_state()){
    button_options_players -> reset();
    show_players = !show_players;
    table_upgrades -> visible = false;
  }

  if (button_options_help -> get_state()){
    button_options_help -> reset();
    STK_Graphics::display_from_file("about.txt");
  }

  if (button_options_controls -> get_state()){
    button_options_controls -> reset();
    STK_Graphics::display_from_file("controls.txt");
  }
}

bool STK_Generate_choice_UI::overlaps_menu(int x, int y){
  intd pos(x,y);
  return (panel_options -> visible && rect_contains(&panel_options -> bounds, pos)) || 
    (table_upgrades -> visible && rect_contains(&table_upgrades -> bounds, pos));
}

void STK_Generate_choice_UI::set_menu_state(bool vis){
  panel_options -> visible = vis;
  table_upgrades -> visible = vis;
  show_players = vis;
}

bool STK_Generate_choice_UI::get_menu_state(){
  return panel_options -> visible;
}

void STK_Generate_choice_UI::set_visible(bool b){
  panel_splanet -> visible = b;
  panel_mplanet -> visible = b;
  panel_sfleet -> visible = b;
  panel_mfleet -> visible = b;
  panel_commands -> visible = b;
}
