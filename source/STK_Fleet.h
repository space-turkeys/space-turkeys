#ifndef _STK_FLEET
#define _STK_FLEET

#include <list>

#include "RSK/RskTypes.h"
#include "STK_Class_headers.h"
#include "STK_Structures.h"
#include "STK_Command.h"

class STK_Fleet{
 private:
  std::list<STK_Ship*> fleet;
  std::list<STK_Command> commands;
  STK_Ship_master *ship_master;
  STK_Ship_order order;
  fd position;
  int tactics;

 public:
  std::list<STK_Command*> command_generation;
  static int id_counter;
  int id;
  int is_selected;
  int owner;
  int converge;
  int update_counter;
  int equipment;

  STK_Fleet(STK_Ship_master* sm);
  void assign(std::list<STK_Ship*> *a);
  void assimilate(STK_Fleet *a);
  void cleanup();
  void remove(STK_Ship *s);
  void give_command(STK_Command c);
  void apply_command(STK_Command c);
  void clear_fleet_order(STK_Fleet *f);
  bool send_data(char* buf, int* c, int max);
  void rebuild_references(STK_Ship_master *sm, STK_Planet_master *pm);
  void print_references();
  bool load_data(char* buf, int* c, int max);
  void add_ship(STK_Ship *s);
  int calculate_size();
  int summon_radius();
  int get_num();
  void update();
  int get_owner();
  fd get_position();
  fd get_destination_position();
  fd get_direction_position();
  bool approx_angle(float *a);
  std::list<STK_Ship*> *get_fleet();
  std::list<STK_Command> get_commands();
  STK_Ship_order get_order();
  STK_Ship_order *get_order_p();
  int get_tactics();
  void calculate_position();
  void cleanup_ships();
  bool differs(STK_Fleet c);
};
#endif
