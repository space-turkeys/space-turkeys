#include <cstdio>
#include <SDL/SDL.h>

#include "STK_Collision_map.h"
#include "STK_Universe_settings.h"
#include "STK_Planet_master.h"
#include "STK_Structures.h"
#include "STK_Ship.h"
#include "STK_Planet.h"
#include "STK_Namespace.h"

STK_Collision_map::STK_Collision_map(){
  initialized = false;
  ref_width = -1;
  ref_height = -1;
}

bool STK_Collision_map::initialize(STK_Universe_settings *uvs, STK_Planet_master *p){
  uv_settings = uvs;
  if (!uv_settings || uv_settings -> cmap_width() < 1 || uv_settings -> cmap_height()< 1){
    fprintf(stdout,"STK_Collision_map::initialize(): bad parameters!\n");
    return false;
  }

  ship_map.initialize(uv_settings);
  planet_map.initialize(uv_settings, p);
  planet_field.initialize(uv_settings,p,this);
  initialized=1;
  return true;
}

void STK_Collision_map::rebuild_references(STK_Planet_master *p){
  if (!uv_settings){
    fprintf(stdout,"collision_map::rebuild_references(): no uv_settings!\n");
    return;
  }

  if (initialized && ref_width == uv_settings -> cmap_width() && ref_height == uv_settings -> cmap_height()){
    ship_map.clear_map();
    if (p -> regenerate_maps){
      planet_map.initialize(uv_settings, p);
      planet_field.rebuild_references(p);
    }
  }else{
    ref_width = uv_settings -> cmap_width();
    ref_height = uv_settings -> cmap_height();
    initialize(uv_settings, p);
  }
}

void STK_Collision_map::settings_ref(STK_Universe_settings* uvs){
  uv_settings = uvs;
}

STK_Planet* STK_Collision_map::get_planet(fd pos){
  intd p=map2col(pos);

  if (!validate_position(p)){
    return NULL;
  }

  return planet_map.map[uv_settings -> cmap_width()*p.y+p.x];
}

STK_Ship* STK_Collision_map::get_ship(fd pos){
  intd p=map2col(pos);
  if (!validate_position(p)){
    return NULL;
  }
  return ship_map.map[uv_settings -> cmap_width()*p.y+p.x];
}

STK_Ship* STK_Collision_map::find_opponent_ship(STK_Ship *ship){
  intd p = map2col(ship -> get_position());
  int i;
  int op_search = 9;
  int n = op_search * op_search;
  STK_Ship *s;
  
  for (i = 0; i < n; i++){
    int x = i%op_search - op_search/2;
    int y = i/op_search - op_search/2;
    
    intd q = p + intd(x,y);

    if (validate_position(q)){
      s = ship_map.map[uv_settings -> cmap_width()*q.y+q.x];
      if (s && s -> owner != ship -> owner){
	return s;
      }
    }
  }

  return NULL;
}

void STK_Collision_map::add_ship(fd position, STK_Ship *ship){
  intd p=map2col(position.toIntd());
  if (!initialized || !validate_position(p)){
    return;
  }
  ship_map.map[p.y*uv_settings -> cmap_width()+p.x]=ship;
}

void STK_Collision_map::remove_ship(fd position, STK_Ship *ship){
  intd p=map2col(position.toIntd());
  if (!initialized || !validate_position(p)){
    return;
  }
  if (ship_map.map[p.y*uv_settings -> cmap_width()+p.x]==ship){
    ship_map.map[p.y*uv_settings -> cmap_width()+p.x]=NULL;
  }
}

e_pfield STK_Collision_map::get_field(fd position){
  intd p=map2col(position.toIntd());
  e_pfield r;
  if (!initialized || !validate_position(p)){
    return r;
  }
  return planet_field.map[p.y*uv_settings -> cmap_width()+p.x];
}

fd STK_Collision_map::closest_free_square(fd position, int player){
  int max_search=fmax(uv_settings -> cmap_width(), uv_settings -> cmap_height());
  int r;
  int a;
  intd p=map2col(position.toIntd());
  intd q;
  int offset;
  if (!initialized){
    return fd(0,0);
  }
  
  for (r=1; r<max_search; r++){
    offset=STK::rand_keeper.next()%int(2*PI*r);
    for (a=0; a<2*PI*r; a++){
      q=p+(r*fd::normv((a+offset)/(float)r)).toIntd();
      if (validate_position(q) && !ship_map.map[q.y*uv_settings -> cmap_width()+q.x] && (!planet_map.map[q.y*uv_settings -> cmap_width()+q.x] || planet_map.map[q.y*uv_settings -> cmap_width()+q.x] -> owner == player)){
	return col2map(q);
      }
    }
  }
  
  return fd(0,0);
}

fd STK_Collision_map::neighbour_directions(STK_Ship *ship){
  fd p = ship -> get_position();
  intd x = map2col(p);
  int i, w = SWARM_DIAMETER * SWARM_DIAMETER;
  int r = SWARM_DIAMETER / 2;
  int j,k;
  fd v(0,0);
  intd q;
  STK_Ship* s = NULL;

  if (!validate_position(x)){
    return v;
  }

  for (i = 0; i < w; i++){
    j = i%SWARM_DIAMETER - r;
    k = i/SWARM_DIAMETER - r;
    q = intd(j,k) + x;

    if (validate_position(q)){
      s = ship_map.map[q.y * uv_settings -> cmap_width() + q.x];
      if (s && s -> owner == ship -> owner){
	v = v + fd::normv(s -> angle);
      }
    }
  }

  return v;
}

intd STK_Collision_map::col2map(intd p){
  float f=1/uv_settings -> ratio;
  return intd(f*p.x, f*p.y);
}

intd STK_Collision_map::map2col(fd p){
  return (uv_settings -> ratio*p).toIntd();
}

float STK_Collision_map::map2col(float a){
  return uv_settings -> ratio*a;
}

bool STK_Collision_map::validate_position(intd p){
  return p.x>=0 && p.x<uv_settings -> cmap_width() && p.y>=0 && p.y<uv_settings -> cmap_height();
}

bool STK_Collision_map::is_occupied(fd p, STK_Ship* me, STK_Planet* target){
  STK_Planet *planet;
  STK_Ship *ship;

  planet=get_planet(p);
  ship=get_ship(p);

  return (planet && planet -> owner != me -> owner && planet != target);// || (ship && ship != me && ship -> owner == me -> owner);
}

bool STK_Collision_map::differs(STK_Collision_map m){
  if (initialized!=m.initialized){
    return true;
  }
  return ship_map.differs(m.ship_map) || planet_map.differs(m.planet_map) || planet_field.differs(m.planet_field);
}

void STK_Collision_map::cleanup(){
  initialized=0;
  uv_settings = NULL;
  ship_map.cleanup();
  planet_map.cleanup();
  planet_field.cleanup();
}
