#ifndef _STK_RANDOMIZER
#define _STK_RANDOMIZER

#include <boost/random/uniform_int.hpp>
#include <boost/random/mersenne_twister.hpp>

class STK_Randomizer{
 private:
  boost::random::mt19937 rng;
  boost::random::uniform_int_distribution<unsigned int> *dist;
 public:
  int seed;

  STK_Randomizer();
  ~STK_Randomizer();
  void reset();
  unsigned int next();
  float next_float();
};

#endif
