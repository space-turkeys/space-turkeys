#include <cstdio>
#include <vector>
#include <string>

#include "STK_Select_game_menu.h"
#include "STK_Client.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Graphics.h"

using namespace std;

STK_Select_game_menu::STK_Select_game_menu(STK_Menu_references *r):STK_Menu(r){
  vector<vector<string> > games;
  color_scheme cs;
  SDL_Rect b;
  int bh = (m_ref -> h - 10) / 4;
  int bw = m_ref -> w / 4 - 20;
  int tw = 3 * m_ref -> w / 4 - 20;

  do_create = false;
  do_join = false;

  games.resize(1);
  games[0].resize(1);
  games[0][0] = "Available games, #players";
  selected_game = -1;

  b = create_sdl_rect(bw + 30, 10, tw, m_ref -> h - 20);
  games_table = new RSG_Table(games,b);
  games_table -> listener = this;

  b = create_sdl_rect(10,10,bw,bh - 10);
  button_create = new RSG_Button(this,"Create Game",cs,NULL,b);

  b.y += bh;
  button_back = new RSG_Button(this,"Back",cs,NULL,b);

  add(games_table);
  add(button_create);
  add(button_back);
};

STK_Select_game_menu::~STK_Select_game_menu(){
  delete_components();
}

void STK_Select_game_menu::listen(SDL_Event e, RSG_Component *c){
  if (e.type == SDL_MOUSEBUTTONDOWN){
    if (c == button_back){
      button_back -> reset();
      done = true;
    }else if (c == button_create){
      button_create -> reset();
      do_create = true;
    }
  }
}

void STK_Select_game_menu::handle_event(SDL_Event e){
  if (e.type == SDL_KEYDOWN){
    switch(e.key.keysym.sym){
    case SDLK_c:
      button_create -> reset();
      do_create = true;
      break;
    case SDLK_j:
      do_join = true;
      break;
    case SDLK_ESCAPE:
      button_back -> reset();
      done = true;
      break;
    }
  }
  
  boost::mutex::scoped_lock lock(*updater.mutex);
  RSK_GUI::handle_event(e);
}

void STK_Select_game_menu::join_game(){
  boost::mutex::scoped_lock lock(*updater.mutex);
  
  do_join = false;
  if (selected_game >= 0 && selected_game < games.size()){
    m_ref -> game_id = games[selected_game].id;
  }else if (games.size()){
    m_ref -> game_id = games[0].id;
  }else{
    fprintf(stdout,"select_game: no game index %d to join!\n", selected_game);
    return;
  }
  lock.unlock();

  updater.terminate();
  if (m_ref -> client -> join_game(m_ref -> game_id)){
    fprintf(stdout,"select_game: joining game %d.\n", m_ref -> game_id);
    m_ref -> wait_for_game_menu -> evoke();
  }else{
    fprintf(stdout,"select_game: failed to join game %d.\n", m_ref -> game_id);
    m_ref -> game_id = 0;

    if (m_ref -> client -> server_message == STKIP_INVALID_VERSION){
      STK_Graphics::draw_message(STK::ns_var -> screen, "Can not join game; incompatible version!");
    }else{
      STK_Graphics::draw_message(STK::ns_var -> screen, "Failed to join game!");
    }
    SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);
    SDL_Delay(2000);

  }
  updater.start();
  lock.lock();
}

void STK_Select_game_menu::create_game(){
  do_create = false;
  updater.terminate();
  m_ref -> input_settings_menu -> evoke();
  updater.start();
}

void STK_Select_game_menu::evoke(){
  SDL_Event e;
  int c = 0;
  int tc_in = STKS_RUN;
  int tc_out = STKS_NONE;
  boost::mutex mutex;

  done = false;

  /* assure client is initialized */
  if (m_ref -> client && m_ref -> client -> state == STKS_DEAD){
    m_ref -> client -> stkc_close();
    delete m_ref -> client;
    m_ref -> client = NULL;
  }

  if (!m_ref -> client){
    m_ref -> connection_settings_menu -> evoke();
    if (!m_ref -> client){
      return;
    }
  }

  updater.tc_in = &tc_in;
  updater.tc_out = &tc_out;
  updater.client = m_ref -> client;
  updater.table = games_table;
  updater.mutex = &mutex;
  updater.games = &games;
  updater.start();
  boost::mutex::scoped_lock lock(mutex);
  lock.unlock();

  //wait for updater to start
  while (tc_out != STKS_RUN){
    if (tc_out == STKS_TERMINATE){
      fprintf(stdout, "SGM::evoke: updater didn't start!\n");
      return;
    }
    SDL_Delay(50);
  }

  /* select game loop */
  while(!done){
    while(SDL_PollEvent(&e)){
      handle_event(e);
    }

    if (m_ref -> client == NULL){
      break;
    }

    if (do_create){
      create_game();
    }

    if (do_join || selected_game > -1){
      join_game();
    }

    lock.lock();
    selected_game = games_table -> get_selected_row();

    SDL_FillRect(STK::ns_var -> screen, NULL, 0);
    paint(STK::ns_var -> screen);
    lock.unlock();

    SDL_UpdateRect(STK::ns_var -> screen, 0,0,0,0);
    SDL_Delay(100);
  }

  updater.terminate();
  lock.lock();
  while(SDL_PollEvent(&e)){}
}
