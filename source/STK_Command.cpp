#include "STK_Keys.h"
#include "STK_Command.h"
#include "STK_Planet.h"
#include "STK_Fleet.h"
#include "STK_Basic_communication.h"

using namespace std;

STK_Command::STK_Command(){
  is_selected = false;
  is_locked = false;
  equipment = STKFE_Fight;
}

bool STK_Command::operator == (STK_Command c){
  for (int i=0; i<STK_Command::fsize; i++){
    if (((char*)this)[i]!=((char*)&c)[i]){return false;}
  }
  return true;
}

bool STK_Command::source_selected(){
  return (source.type == TARGET_PLANET && source.planet -> is_selected)
    || (source.type == TARGET_FLEET && source.fleet -> is_selected);
}

bool STK_Command::matches(STK_Command c){
  if (source.type == TARGET_PLANET){
    if (c.source.type == TARGET_PLANET){
      if (source.planet != c.source.planet){
	return false;
      }
    }else{
      return false;
    }
  }else if (source.type == TARGET_FLEET){
    if (c.source.type == TARGET_FLEET){
      if (source.fleet != c.source.fleet){
	return false;
      }
    }else{
      return false;
    }
  }

  if (order.type == TARGET_PLANET){
    if (c.order.type == TARGET_PLANET){
      if (order.planet != c.order.planet){
	return false;
      }
    }else{
      return false;
    }
  }else if (order.type == TARGET_FLEET){
    if (c.order.type == TARGET_FLEET){
      if (order.fleet != c.order.fleet){
	return false;
      }
    }else{
      return false;
    }
  }else if (order.type == TARGET_POSITION){
    if (c.order.type == TARGET_POSITION){
      if (order.position.x != c.order.position.x || order.position.y != c.order.position.y){
	return false;
      }
    }else{
      return false;
    }
  }

  return true;
}

void STK_Command::get_positions(fd *s, fd *d){

  switch(source.type){
  case TARGET_PLANET:
    *s = source.planet -> position;
    break;
  case TARGET_FLEET:
    *s = source.fleet -> get_position();
    break;
  default:
    fprintf(stdout,"draw_command: command has bad source type!\n");
    break;
  }

  switch(order.type){
  case TARGET_PLANET:
    *d = order.planet -> position;
    break;
  case TARGET_FLEET:
    *d = order.fleet -> get_position();
    break;
  case TARGET_POSITION:
    *d = order.position;
    break;
  default:
    fprintf(stdout,"draw_command: command has bad target type!\n");
    break;
  }
}

int STK_Command::max_quantity(){
  if (source.type == TARGET_PLANET){
    return source.planet -> num_turkeys;
  }else if (source.type == TARGET_FLEET){
    return source.fleet -> get_num();
  }
}

int STK_Command::quantity(){
  if (is_locked){
    return lock_qtty;
  }

  if (priority == 0){
    return 0;
  }

  int psum = 0;
  int maxq = 0;
  list<STK_Command*>::iterator i;
  list<STK_Command>::iterator j;

  if (source.type == TARGET_PLANET){
    maxq = source.planet -> num_turkeys;
    for (i = source.planet -> command_generation.begin(); i != source.planet -> command_generation.end(); i++){
      if ((*i) -> is_locked){
	maxq -= (*i) -> quantity();
      }else{
	psum += (*i) -> priority;
      }
    }
    for (j = source.planet -> command_queue.begin(); j != source.planet -> command_queue.end(); j++){
      if (j -> is_locked){
	maxq -= j -> quantity();
      }else{
	psum += j -> priority;
      }
    }
  }else{

    list<STK_Command> fcoms = source.fleet -> get_commands();
    maxq = source.fleet -> get_num();
    for (i = source.fleet -> command_generation.begin(); i != source.fleet -> command_generation.end(); i++){
      if ((*i) -> is_locked){
	maxq -= (*i) -> quantity();
      }else{
	psum += (*i) -> priority;
      }
    }
    for (j = fcoms.begin(); j != fcoms.end(); j++){
      if (j -> is_locked){
	maxq -= (j -> signal_delay <= signal_delay) * j -> quantity();
      }else{
	psum += (j -> signal_delay <= signal_delay) * j -> priority;
      }
    }

    fprintf(stdout, "command fleet %d: maxq = %d, psum = %d, prio = %d.\n", source.fleet -> id, maxq, psum, priority);
  }

  return (priority * maxq) / fmax(psum, 100);
}

bool STK_Command::load_data(char *buf, int *c, int max){
  if (*c + fsize > max){
    return false;
  }
  if (!order.load_data(buf, c, max)){
    return false;
  }
  if (!source.load_data(buf, c, max)){
    return false;
  }
  read_from_buf(buf + *c, (char*)&priority, 4);
  *c += 4;
  read_from_buf(buf + *c, (char*)&signal_delay, 4);
  *c += 4;
  read_from_buf(buf + *c, (char*)&is_locked, 4);
  *c += 4;
  read_from_buf(buf + *c, (char*)&lock_qtty, 4);
  *c += 4;
  read_from_buf(buf + *c, (char*)&equipment, 4);
  *c += 4;
  is_selected = false;
  return true;
}

bool STK_Command::send_data(char *buf, int *c, int max){
  if (*c + fsize > max){
    return false;
  }

  if (!order.send_data(buf, c, max)){
    return false;
  }
  if (!source.send_data(buf, c, max)){
    return false;
  }
  write_to_buf((char*)&priority, buf + *c, 4);
  *c += 4;
  write_to_buf((char*)&signal_delay, buf + *c, 4);
  *c += 4;
  write_to_buf((char*)&is_locked, buf + *c, 4);
  *c += 4;
  write_to_buf((char*)&lock_qtty, buf + *c, 4);
  *c += 4;
  write_to_buf((char*)&equipment, buf + *c, 4);
  *c += 4;
  is_selected = false;
  return true;
}
