#include "STK_Compatability.h"

bool compatible_client_client(int c1, int c2){
  return c1 == 207 && c2 == 207;
}

bool compatible_client_server(int c, int s){
  return c >= 207 && s >= 207;
}
