#ifndef _STK_STATISTICS
#define _STK_STATISTICS

#include <vector>
#include <list>

#include "STK_Class_headers.h"
#include "STK_Universe_settings.h"
#include "STK_Planet.h"

namespace STK{
  namespace statistics{
    struct best_homes_stats{
      float unfairness;
      std::vector<int> homes;
      std::vector<float> scores;
    };

    struct battle_result{
      int score[5];
    };

    int leaving(STK_Planet *p);
    int production(STK_Planet *p, STK_Universe_settings *us);
    int production(std::list<STK_Planet*> p, STK_Universe_settings *us);
    int nships(std::list<STK_Planet*> p);
    int leaving(std::list<STK_Planet*> p);
    int leaving(STK_Fleet *f);
    int eta(STK_Fleet *f, float v);
    int dist(STK_Fleet *f, STK_Planet *hp, STK_Universe_settings *us);
    int nships(std::list<STK_Fleet*> f);
    int nships(std::list<STK_Command*> f);
    bool contains_fleet(std::list<STK_Fleet*> list, STK_Fleet* x);
    bool dirichlet(float a, float b);
    battle_result battle_predictor(int na, int wfa, int nb, int wfb, bool planet);
    int scramble_quantity(int quantity);

    int closest_planet(std::vector<STK_Planet> planets, fd p);
    std::vector<STK_Planet> scramble_planets(STK_Universe_settings s);
    float planet_score(std::vector<STK_Planet> p, int i, STK_Universe_settings s);
    best_homes_stats best_homes(std::vector<STK_Planet> p, int n, STK_Universe_settings s);
  };
};
#endif
