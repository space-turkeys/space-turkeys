#ifndef _STK_ROOT_MENU
#define _STK_ROOT_MENU

#include <RSK/RSK_GUI_V2/RSK_GUI.h>
#include "STK_Menu.h"

class STK_Root_menu : public STK_Menu{
 private:
  RSG_Button *b_online;
  RSG_Button *b_quit;

 public:
  STK_Root_menu(STK_Menu_references *r);
  ~STK_Root_menu();
  void listen(SDL_Event e, RSG_Component *c);
  void handle_event(SDL_Event e);
};
#endif
