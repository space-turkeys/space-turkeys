#ifndef _STK_COMMAND
#define _STK_COMMAND

#include "STK_Structures.h"
#include "RSK/RskTypes.h"

struct STK_Command{
  // game data
  STK_Ship_order source;
  STK_Ship_order order;
  int priority;
  int signal_delay;
  int is_locked;
  int lock_qtty;
  int equipment;

  // interface control variables
  int is_selected;
  const static int fsize = 2 * STK_Ship_order::fsize + 5 * sizeof(int);

  STK_Command();
  bool operator == (STK_Command c);
  bool matches(STK_Command c);
  void get_positions(fd *from, fd *to);
  int quantity();
  int max_quantity();
  bool source_selected();
  bool load_data(char* buf, int* c, int max);
  bool send_data(char* buf, int* c, int max);
};

#endif
