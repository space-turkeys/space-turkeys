#ifndef _STK_NAMESPACE
#define _STK_NAMESPACE

#include <boost/thread/mutex.hpp>

#include "STK_Randomizer.h"
#include "STK_Class_headers.h"

namespace STK{
  extern STK_Randomizer rand_keeper;
  extern STK_Control_vars *ns_var;
  extern boost::mutex game_data_lock, client_data_lock, game_info_lock, universe_info_lock, info_lock;
};
#endif
