#include "STK_Server_structs.h"
#include "STK_Universe.h"

using namespace std;

void STK_Universe_builder::operator () (){
  int i;
  
  g -> settings.p_info.num_players = g -> num_players;
  g -> settings.num_planets = fmax(g -> settings.p_info.num_players, g -> settings.num_planets);
  for (i=0; i<g->num_players; i++){
    g->settings.p_info.players[i]=g->players[i] -> id;
    strcpy(g->settings.p_info.player_names[i],g->players[i] -> name);
    fprintf(stdout,"server: added player %s with id %d.\n", g -> settings.p_info.player_names[i], g -> settings.p_info.players[i]);
  }
    
  if (!g -> settings.p_info.setup_colors()){
    fprintf(stdout,"server(): setup colors failed!\n");
  }

  if (g->build_universe()){
    g -> game_state = STKS_LOADING;
    g -> home_planets.resize(g -> num_players);
    vector<STK_Planet> *pl = &(g -> universe -> get_planet_master() -> planets);
    for (i = 0; i < g -> num_players; i++){
      g -> home_planets[i] = -1;
      for (int j = 0; j < pl -> size(); j++){
	if ((*pl)[j].owner == g -> player_id[i]){
	  g -> home_planets[i] = (*pl)[j].id;
	  fprintf(stdout, "universe_builder::(): homeplanets[%d] = %d\n", g -> player_id[i], (*pl)[j].id);
	}
      }

      if (g -> home_planets[i] == -1){
	fprintf(stdout, "universe_builder::(): warning: player %d has not home planet!\n", g -> player_id[i]);
	g -> game_state = STKS_BUILD_FAILED;
      }
    }
  }else{
    g -> game_state = STKS_BUILD_FAILED;
  }
}

bool STK_Client_thread_data::send(STK_Inet_protocol p){
  return send((void*)&p,4);
}
STK_Inet_protocol STK_Client_thread_data::get(){
  STK_Inet_protocol p;
  if (!get((void*)&p,4)){
    return STKIP_DO_NOTHING;
  }
  return p;
}

bool STK_Client_thread_data::send(void* data, int size){
  bool ret;
  //boost::mutex::scoped_lock lock(STK::client_data_lock);

  fprintf(stdout, "server: send to %d (s=%d, id=%d, p=%d)\n", id, size, qid, *((int*)data));
  return (connection_state = send_buffer_qid(socket, (char*)data, size, BUFKEY_STKIP, qid, STK_SERVER_TIMEOUT)) == STKS_READY;
}

bool STK_Client_thread_data::get(void* data, int size){
  bool ret;

  fprintf(stdout, "server: get for %d (%d)\n", id, size);
  ret = (connection_state = server_load_to_buffer(socket,(char*)data,size,BUFKEY_STKIP, &qid, STK_SERVER_TIMEOUT)) == STKS_READY;

  fprintf(stdout, "server: get for %d: id=%d, p=%d\n", id, qid, *((int*)data));

  return ret;
}

STK_S_Game_info::STK_S_Game_info():STK_Game_info(){
  universe = NULL;
}

void STK_S_Game_info::cleanup(){
  if (universe){
    delete universe;
  }
  initialized=false;
  universe = NULL;
  num_players = 0;
}

bool STK_S_Game_info::add_client(STK_Client_thread_data *c){
  if (num_players>=STK_MAX_PPG){return false;}
  players[num_players] = c;
  strcpy(names[num_players], c -> name);
  player_id[num_players] = c -> id;
  num_players++;
  return true;
}

void STK_S_Game_info::remove_client(int id){
  int i;
  for (i=0; i<num_players; i++){
    if (players[i] -> id==id){
      players[i]=players[--num_players];
      return;
    }
  }
}

bool STK_S_Game_info::build_universe(){
  if (universe){
    delete universe;
  }
  universe = new STK_Universe();
  return universe && universe -> initialize(NULL,settings);
}

int STK_S_Game_info::root_client(){
  return num_players ? players[0] -> id : -1;
}


STK_SERVER_STATES send_buffer(int sock, char* buf, int max, STK_Buffer_key key, int timeout){
  int c;
  STK_SERVER_STATES state;
  int i;
  if (max<0){
    fprintf(stdout,"invalid size!\n");
    return STKS_FAILED;
  }

  if ((state = lsend(sock,(void*)&key,4,timeout))!=STKS_READY){
    fprintf(stdout,"send_buffer: failed to write key!\n");
    return state;
  }

  if ((state = lsend(sock,(void*)&max,4,timeout))!=STKS_READY){
    fprintf(stdout,"send_buffer: failed to write size!\n");
    return state;
  }

  if ((state = lsend(sock,(void*)buf,max,timeout))!=STKS_READY){
    fprintf(stdout,"send_buffer: failed to write data!\n");
  }
  return state;
}

STK_SERVER_STATES send_buffer_qid(int sock, char* buf, int max, STK_Buffer_key key, int qid, int timeout){
  int c;
  STK_SERVER_STATES state;
  int i;

  if (max<0){
    fprintf(stdout,"invalid size!\n");
    return STKS_FAILED;
  }

  if ((state = lsend(sock,(void*)&key,4,timeout))!=STKS_READY){
    fprintf(stdout,"send_buffer: failed to write key!\n");
    return state;
  }

  if ((state = lsend(sock, (void*)&qid, 4, timeout)) != STKS_READY){
    fprintf(stdout, "send_buffer: failed to write qid!\n");
    return state;
  }

  if ((state = lsend(sock,(void*)&max,4,timeout))!=STKS_READY){
    fprintf(stdout,"send_buffer: failed to write size!\n");
    return state;
  }

  if ((state = lsend(sock,(void*)buf,max,timeout))!=STKS_READY){
    fprintf(stdout,"send_buffer: failed to write data!\n");
  }
  return state;
}

STK_SERVER_STATES load_to_buffer(int sock, char* buf, int max, STK_Buffer_key key, int qid, int timeout){
  int c = 0;
  STK_Buffer_key k = BUFKEY_NONE;
  int m;
  int qtest = 0;
  bool test = false;
  STK_SERVER_STATES state;

  while (test == false && c++ < STK_BUFKEY_RETRIES){
    if ((state = lget(sock,(void*)&k,4,timeout))!=STKS_READY){
      fprintf(stdout,"load_to_buffer: failed to read key!\n");
      return state;
    }
    
    if (k == key){
      if ((state = lget(sock, (void*)&qtest, 4, timeout)) != STKS_READY){
	fprintf(stdout, "load_to_buffer: failed to read qid!\n");
	return state;
      }

      test = qtest == qid;
    }
  }

  if (!test){
    fprintf(stdout,"load_to_buffer: invalid key or qid: %d - %d (expected %d - %d)!\n",k, qtest, key, qid);
    return STKS_FAILED;
  }

  if ((state = lget(sock,(void*)&m,4,timeout))!=STKS_READY || m != max){
    fprintf(stdout,"load_buffer: failed to read valid size: %d, should be %d\n", m, max);
    return state;
  }

  if ((state = lget(sock,(void*)buf,max,timeout))!=STKS_READY){
    fprintf(stdout,"load_to_buffer: failed to read!\n");
  }

  return state;
}

STK_SERVER_STATES server_load_to_buffer(int sock, char* buf, int max, STK_Buffer_key key, int *qid, int timeout){
  int c = 0;
  STK_Buffer_key k = BUFKEY_NONE;
  int m;
  int qtest;
  STK_SERVER_STATES state;

  while (k != key && c++ < STK_BUFKEY_RETRIES){
    if ((state = lget(sock,(void*)&k,4,timeout))!= STKS_READY){
      fprintf(stdout,"load_to_buffer: failed to read key!\n");
      return state;
    }
  }

  if (k != key){
    fprintf(stdout,"load_to_buffer: invalid key: %d (expected %d)!\n",k,key);
    return STKS_FAILED;
  }

  if ((state = lget(sock,(void*)&qtest,4,timeout))!= STKS_READY){
    fprintf(stdout,"load_buffer: failed to read qid!\n");
    return state;
  }

  *qid = qtest;

  if ((state = lget(sock,(void*)&m,4,timeout))!= STKS_READY || m != max){
    fprintf(stdout,"load_buffer: failed to read valid size: %d, should be %d\n", m, max);
    return state;
  }

  if ((state = lget(sock,(void*)buf,max,timeout))!= STKS_READY){
    fprintf(stdout,"load_to_buffer: failed to read!\n");
  }

  return state;
}

char* load_buffer(int sock, int* max, STK_Buffer_key key, int timeout){
  //load data of unknown size
  int c = 0;
  bool success;
  char* buf;
  int i;
  STK_Buffer_key k = BUFKEY_NONE;
  bool test = false;

  while (test == false && c++ < STK_BUFKEY_RETRIES){
    if (lget(sock,(void*)&k,4,timeout)!=STKS_READY){
      fprintf(stdout,"load_buffer: failed to read key!\n");
      return NULL;
    }
    test = k == key;
  }

  if (!test){
    fprintf(stdout,"load_buffer: invalid key : %d (expected %d)!\n",k, key);
    return NULL;
  }

  if (lget(sock,(void*)max,4,timeout)!=STKS_READY){
    fprintf(stdout,"load_buffer: failed to read size!\n");
    return NULL;
  }

  fprintf(stdout,"load_buffer(): size=%d\n",*max);
  if (*max<0){
    fprintf(stdout,"invalid size!\n");
    return false;
  }

  if (!(buf=new char[*max])){
    fprintf(stdout,"load buffer: failed to allocate.\n");
    return NULL;
  }

  if (lget(sock,(void*)buf,*max,timeout)!=STKS_READY){
    fprintf(stdout,"load_buffer: failed to read data!\n");
    delete [] buf;
    return NULL;
  }
  return buf;
}
