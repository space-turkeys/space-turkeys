#ifndef _STK_PLAYER_INFO
#define _STK_PLAYER_INFO

#include "STK_Definitions.h"

struct player_info{
  int num_players;
  int players[STK_MAX_PPG];
  char player_names[STK_MAX_PPG][STK_MAX_NAME];
  int initial_home_planets[STK_MAX_PPG];
  int player_colors[STK_MAX_PPG];

  int get_color(int id);
  bool setup_colors();
};

int color_distance(int c1, int c2);
#endif
