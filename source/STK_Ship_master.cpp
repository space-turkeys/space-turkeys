#include "STK_Ship_master.h"
#include "STK_Explosion.h"
#include "STK_Collision_map.h"
#include "STK_Planet.h"
#include "STK_Game.h"
#include "STK_Namespace.h"
#include "STK_Control_vars.h"
#include "STK_Server_structs.h"
#include "STK_Graphics.h"
#include "STK_Statistics.h"

using namespace std;

STK_Ship_master::STK_Ship_master(){
  /* Default constructor for the ship master. Sets all counters to 0. */
  cmap = NULL;
  planet_master = NULL;
}

void STK_Ship_master::initialize(STK_Ship_settings s, STK_Collision_map *cm, STK_Planet_master *pm){
  /* Default constructor for the ship master. Sets all counters to 0. */
  ship_settings = s;
  cmap = cm;
  planet_master = pm;
}

void STK_Ship_master::iterate(STK_Game *game){
  /* This routine calls the ship iteration routines. The steps are performed sectionwise for all ships. */
  select_requests();
  select_actions();
  move();
  evaluate_positions(game);
  cleanup_ships();
  update_fleets();

  for (list<STK_Explosion*>::iterator i = explosions.begin(); i != explosions.end(); i++){
    (*i) -> iterate();
  }
}

void STK_Ship_master::select_requests(){
  /* In this routine, each ship's requested direction of travel is calculated. This is based on the angle to the target and the vicinity of planets (read from the planet field). The requested angles of neighbouring ships are taken into account in the select_actions() routine, wherefore this routine must be run for each ship before any ship can begin the next routine. */

  fd path;
  fd evasion;
  e_pfield p_field;
  fd r;
  float relation;
  float a;
  float pressure;
  fd x, d, summon, neighbours, offset;
  float disp;
  STK_Planet *px;

  for (list<STK_Ship>::iterator i = ships.begin(); i != ships.end(); i++){
    if (! i -> fleet -> update_counter){
      p_field = cmap -> get_field(i -> get_position());
      d = i -> fleet -> get_direction_position();
      x = i -> fleet -> converge ? i -> get_position() : i -> fleet -> get_position();
    
      offset = i -> get_position() - i -> fleet -> get_position();
      disp = offset.length();
      summon = disp > i -> fleet -> summon_radius() ? ((disp - i -> fleet -> summon_radius())/(float)i -> fleet -> summon_radius()) * (-1 * offset) : fd(0,0);

      neighbours = cmap -> neighbour_directions(&*i);
      pressure = neighbours.length();

      if (summon.x == 0 && summon.y == 0){
	if (pressure > FLEET_UNSUMMON_PRESSURE){
	  summon = ((pressure - FLEET_UNSUMMON_PRESSURE)/(float)(FLEET_UNSUMMON_PRESSURE)) * offset.normalize();
	}
      }

      path = d - x;
      if ((summon.x || summon.y) && angleDistance(path.getAngle(), summon.getAngle()) < PI/4){
	path = (d - x).normalize() + (float)SWARM_SUMMON_COMPONENT * summon;
      }else{
	path = (d - x).normalize();
      }

      if (pressure > 0){
	r = (path + (float)(SWARM_SOCIAL_COMPONENT / pressure) * neighbours).normalize();
      }else{
	r = path.normalize();
      }

      //p_field affects ships if the planet is not owned and is not the target
      if (p_field.p && p_field.p -> owner != i -> owner && !(i -> fleet -> get_order().type==TARGET_PLANET && p_field.p == i -> fleet -> get_order().planet)){
	a = normAngle(r.getAngle()-p_field.angle);
	a = a < PI ? a : -2 * PI + a;
	if (fabs(a)<PI/2){
	  if (a>0){
	    evasion=fd::normv(p_field.angle+PI/2);
	  }else{
	    evasion=fd::normv(p_field.angle-PI/2);
	  }
	  r = p_field.length * evasion + (1 - p_field.length) * r;
	}
      }

      //r = vector in requested direction
      i -> required_angle = r.getAngle();

      // fprintf(stdout, "ship_master::select_requests: ship %d of fleet %d: updating req. direction!\n", i -> associated_id, i -> fleet -> id);
      // fprintf(stdout, " -> fleet pos: %.2fx%.2f\n", i -> fleet -> get_position().x, i -> fleet -> get_position().y);
      // if (i -> fleet -> get_order().type == TARGET_PLANET){
      // 	fprintf(stdout, " -> target planet (%d): position: %.2fx%.2f\n", i -> fleet -> get_order().planet -> id, i -> fleet -> get_order().planet -> position.x, i -> fleet -> get_order().planet -> position.y);
      // }
      // fprintf(stdout, " -> d: %.2fx%.2f\n", d.x, d.y);
      // fprintf(stdout, " -> x: %.2fx%.2f\n", x.x, x.y);
      // fprintf(stdout, " -> d - x angle: %f\n", (d - x).getAngle());
      // fprintf(stdout, " -> summon: %d\n", (int)((summon.x || summon.y) && angleDistance(path.getAngle(), summon.getAngle()) < PI/4));
      // fprintf(stdout, " -> pressure: %d\n", (int)(pressure > 0));
      // fprintf(stdout, " -> path angle: %f\n", path.getAngle());
      // fprintf(stdout, " -> r angle: %f\n", r.getAngle());
    }
  }
}

void STK_Ship_master::select_actions(){
  /* This routine is incomplete. It's purpose is to select which direction the ship will rotate, based on required angles of nearby ships. */
  list<STK_Ship>::iterator i;
  float a;
  for (i = ships.begin(); i != ships.end(); i++){
    /*
      Attempt to read requests of adjacent ships heading for same square on colmap?
    */
    a = normAngle(i -> required_angle - i -> angle);
    a = a < PI ? a : -2 * PI + a;
    if (a>0){
      i -> action = STKSA_Rotate_right;
    }else if (a<0){
      i -> action = STKSA_Rotate_left;
    }else{
      i -> action = STKSA_None;
    }
  }
}

void STK_Ship_master::move(){
  /* This routine will move each ship in direction of their current orientation. */
  list<STK_Ship>::iterator i;
  STK_Planet* t;
  fd v;
  for (i = ships.begin(); i != ships.end(); i++){
    if (i -> action == STKSA_Rotate_left){
      i -> angle -= ship_settings.ship_rotate;
    }else if (i -> action == STKSA_Rotate_right){
      i -> angle += ship_settings.ship_rotate;
    }

    v = (1 + ship_settings.equip_factor * (i -> fleet -> equipment == STKFE_Travel)) * i -> attributes.speed * fd::normv(i -> angle);
    t = i -> fleet -> get_order().planet;

    if (i -> fleet -> get_order().type != TARGET_PLANET){
      t = NULL;
    }

    i -> alter_position(i -> get_position() + v); 
  }
}

void STK_Ship_master::evaluate_positions(STK_Game *game){
  /* This routine asseses the consequences of each ship position. If the placing is illegal, the ship is rellocated. If there is an enemy ship there, a fight is evaluated. If the position contains the target planet, the ship lands/fights depending on ownership of the planet.  */
  list<STK_Ship>::iterator i;
  STK_Planet* planet;
  STK_Ship *ship;

  for (i = ships.begin(); i != ships.end(); i++){
    if (planet = cmap -> get_planet(i -> get_position())){
      if (i -> fleet -> get_order().type == TARGET_PLANET && planet == i -> fleet -> get_order().planet){
	if (hit_planet(&(*i), planet)){
	  explosions.push_back(new STK_Large_explosion(planet -> position.toIntd(), planet -> p_owner -> color, 100));
	}
      }else if (planet -> owner != i -> owner){
	i -> alter_position(planet -> position + (planet -> radius + 2) * fd::normv((i -> get_position() - planet -> position).getAngle()));
      }
    }else if ((ship = cmap -> find_opponent_ship(&*i)) && !(ship -> skip_me)){
      evaluate_fight(&(*i),ship);
      ship -> skip_me = true;
    }
  }

  for (i = ships.begin(); i != ships.end(); i++){
    i -> skip_me = false;
  }
}

STK_Fleet* STK_Ship_master::add_fleet(STK_Ship_attributes a, STK_Command c){
  /* This routine creates num new ships owned by player around position pos. A fleet is created, which is linked to the created ships and given the command c. The fleet id of the new fleet is returned. */
  int quantity = c.quantity();

  if (quantity < 1){
    fprintf(stderr,"STK_SM::add_fleet(): attempted to create empty fleet!\n");
    return NULL;
  }

  STK_Fleet clust(this);
  int i;
  STK_Ship s_insert;
  fd p;
  fd pos = c.source.type == TARGET_PLANET ? c.source.planet -> position : c.source.fleet -> get_position();
  int player = c.source.type == TARGET_PLANET ? c.source.planet -> owner : c.source.fleet -> get_owner();
  float angle = 0;
  float rad = sqrt(100 * quantity / PI);


  //fprintf(stdout,"ship_master::add_fleet: fleets.size = %d, order.type = %s.\n", fleets.size(), c.order.type == TARGET_PLANET ? "planet" : c.order.type == TARGET_FLEET ? "fleet" : c.order.type == TARGET_POSITION ? "position" : "invalid");

  //fprintf(stdout,"ship_master::add_fleet: adding %d turkeys!\n", quantity);
  clust.apply_command(c);
  clust.owner = player;
  clust.equipment = c.equipment;
  
  /* initialize fleet */
  fleets.push_back(clust);

  angle = (fleets.back().get_direction_position() - pos).getAngle();

  /* initialize ships and add them to the fleet */
  for (i = 0; i < quantity; i++){
    p = (STK::rand_keeper.next()%1024) * rad / (float)1024 * fd::normv((STK::rand_keeper.next()%100) * 2 * PI / 100);
    //angle = (STK::rand_keeper.next()%1024) * 2 * PI / (float)1024;
    s_insert.initialize(a,pos + p,angle,player,&fleets.back(),cmap);
    ships.push_back(s_insert);
    ships.back().alter_position(s_insert.get_position());
    fprintf(stdout,"ship_master::add_fleet(): added ship %d at %d x %d.\n", s_insert.associated_id, (int)(s_insert.get_position().x), (int)(s_insert.get_position().y));
    fleets.back().add_ship(&ships.back());
  }

  fleets.back().calculate_position();

  return &fleets.back();
}

STK_Fleet* STK_Ship_master::add_fleet(list<STK_Ship*> *s, STK_Command c){
  /* This routine creates a new fleet and assigns the ships pointed by ids in vector s to the new fleet. The fleet is given the command c, and the fleet id is returned. */
  if (s -> size() < 1){
    return NULL;
  }
  
  STK_Fleet a(this);

  a.apply_command(c);
  a.assign(s);
  a.owner = s -> front() -> owner;

  //fprintf(stdout,"ship_master::add_fleet(s -> size() == %d, id = %d)\n", s -> size(), a.id);

  fleets.push_back(a);
  fleets.back().rebuild_references(this, planet_master);
  fleets.back().calculate_position();

  print_references();
  return &fleets.back();
}

void STK_Ship_master::evaluate_fight(STK_Ship *a, STK_Ship *b){
  /* Evaluates a fight between ships of ids id1 and id2. The loosing ship's deal damage routine is called, and will note if the ship is killed. */
  int warfare1;
  int warfare2;
  int percent1;

  if (!(a && b)){
    fprintf(stderr,"STK_SM::evaluate_fight(): bad ship reference.\n");
    return;
  }

  /* increase combat ability if space fighting equipment was loaded on the fleet */
  float efa = (1 + ship_settings.equip_factor * (a -> fleet -> equipment == STKFE_Fight));
  float efb = (1 + ship_settings.equip_factor * (b -> fleet -> equipment == STKFE_Fight));

  if (!STK::statistics::dirichlet(efa * a -> attributes.warfare + DIRICHLET_BASE, efb * b -> attributes.warfare + DIRICHLET_BASE)){
    b -> deal_damage(1);
  }else{
    a -> deal_damage(1);
  }
}


bool STK_Ship_master::load_data(char* buf, int* c, int max){

  /* This routine is intended for server side use, and does not build references. If data has become invalid and needs to be rebuilt, use routine rebuild_data instead. */
  int i,n;
  STK_Ship s;
  STK_Fleet cl(this);
  list<STK_Fleet>::iterator x;

  cleanup();

  if (*c+4>max){
      fprintf(stdout,"ship_master::send: ERROR: buffer overflow!\n");
      return false;
  }

  read_from_buf(buf+*c,(char*)&STK_Fleet::id_counter,4);
  *c+=4;

  read_from_buf(buf+*c,(char*)&STK_Ship::id_counter,4);
  *c+=4;

  fprintf(stdout, "ship_master::load_data: ship::id_counter = %d\n", STK_Ship::id_counter);

  read_from_buf(buf+*c,(char*)&n,4);
  *c+=4;

  fprintf(stdout, "ship_master: load_data: reading %d ships:\n", n);
  ships.clear();
  for (i = 0; i < n; i++){
    s.load_data(buf, c, max);
    s.fleet = NULL;
    s.cmap = NULL;
    s.print_techdata();
    ships.push_back(s);
  }

  read_from_buf(buf+*c,(char*)&n,4);
  *c+=4;
  if (n<0){
    fprintf(stdout,"ship_master::load: ERROR: bad parameters!\n");
    return false;
  }

  /*load fleets*/
  fleets.clear();
  fprintf(stdout,"ship_master::load_data(): loading %d fleets.\n", n);
  for (i=0; i<n; i++){
    if (!(cl.load_data(buf,c,max))){
      return false;
    }
    fleets.push_back(cl);
  }

  fprintf(stdout,"ship_master::load(): %d fleets, %d ships\n",fleets.size(),ships.size());
  return true;
}

bool STK_Ship_master::send_data(char* buf, int *c, int max){
  int n;
  list<STK_Ship>::iterator i;
  list<STK_Fleet>::iterator j;
  fprintf(stdout,"ship_master::send(): %d fleets, %d ships\n",fleets.size(),ships.size());

  if (*c+calculate_size()>max){
      fprintf(stdout,"ship_master::send: ERROR: buffer overflow!\n");
      return false;
  }

  write_to_buf((char*)&STK_Fleet::id_counter, buf+*c, 4);
  *c+=4;

  write_to_buf((char*)&STK_Ship::id_counter, buf+*c, 4);
  *c+=4;

  fprintf(stdout, "ship_master::send_data: ship::id_counter = %d\n", STK_Ship::id_counter);

  n=ships.size();
  write_to_buf((char*)&n, buf+*c, 4);
  *c+=4;

  fprintf(stdout, "ship_master: send_data: sending %d ships:\n", ships.size());
  for (i = ships.begin(); i != ships.end(); i++){
    i -> send_data(buf, c, max);
    i -> print_techdata();
  }

  n=fleets.size();
  write_to_buf((char*)&n, buf+*c, 4);
  *c+=4;

  for (j = fleets.begin(); j != fleets.end(); j++){
    if(!j -> send_data(buf,c,max)){
      fprintf(stdout,"ship_master::send: fleet send failed.\n");
      return false;
    }
  }

  return true;
}

int STK_Ship_master::calculate_size(){
  int s = 4 * sizeof(int) + ships.size() * STK_Ship::fsize;
  list<STK_Fleet>::iterator i;
  for (i = fleets.begin(); i != fleets.end(); i++){
    s += i -> calculate_size();
  }
  return s;
}

void STK_Ship_master::assign(STK_Ship_master *m){
  ship_settings = m -> get_settings();
  fleets = *(m -> get_fleets());
  ships = *(m -> get_fleet());
}

void STK_Ship_master::cleanup_ships(){
  list<STK_Ship>::iterator i;
  list<STK_Fleet>::iterator j;
  list<STK_Explosion*>::iterator k;

  /* remove dead explosions */
  for (k = explosions.begin(); k != explosions.end(); k++){
    if ((*k == NULL) || (*k) -> remove_me){
      if (*k){
	delete *k;
      }
      explosions.erase(k--);
    }
  }

  /* remove dead ships from fleets */
  for (j = fleets.begin(); j != fleets.end(); j++){
    j -> cleanup_ships();
  }
  
  /* remove dead ships from collision map */
  for (i = ships.begin(); i != ships.end(); i++){
    if (i -> remove_me){
      cmap -> remove_ship(i -> get_position(), &(*i));
    }
  }

  /* remove dead ships from database and add explosions */
  for (i = ships.begin(); i != ships.end(); i++){
    if (i -> remove_me){
      if (i -> explode){
	explosions.push_back(new STK_Explosion(i -> get_position().toIntd()));
      }
      ships.erase(i--);
    }
  }
}

void STK_Ship_master::update_fleets(){
  list<STK_Fleet>::iterator i,j;
  STK_Ship_order *p;

  for (i = fleets.begin(); i != fleets.end(); i++){
    i -> update();
    if (i -> get_num() < 1){
      // update any commands refering to this fleet!
      for (j = fleets.begin(); j != fleets.end(); j++){
	j -> clear_fleet_order(&(*i));
      }

      planet_master -> clear_fleet(&(*i));

      //fprintf(stdout,"ship_master::update_fleets(): erased fleet %d.\n", i -> id);
      fleets.erase(i--);
    }
  }
}

STK_Fleet* STK_Ship_master::get_fleet(int id){
  list<STK_Fleet>::iterator i;

  for (i = fleets.begin(); i != fleets.end(); i++){
    if (i -> id == id){
      return &(*i);
    }
  }

  fprintf(stdout,"ship_master::get_fleet(): fleet of id %d not found!\n", id);
  exit(1);
}

list<STK_Fleet> *STK_Ship_master::get_fleets(){
  return &fleets;
}

STK_Ship* STK_Ship_master::get_ship(int assoc_id){
  for (list<STK_Ship>::iterator i = ships.begin(); i != ships.end(); i++){
    if (i -> associated_id == assoc_id){
      return &(*i);
    }
  }

  fprintf(stdout,"ship_master::get_ship(): ship of id %d not found!\n", assoc_id);
  exit(1);
}

STK_Ship_settings STK_Ship_master::get_settings(){
  return ship_settings;
}

void STK_Ship_master::rebuild_references(STK_Collision_map *cm, STK_Planet_master *pm){
  list<STK_Ship>::iterator i;
  list<STK_Fleet>::iterator j;

  cmap = cm;
  planet_master = pm;

  /* set references of contained objects. */
  //ship collision map
  for (i = ships.begin(); i != ships.end(); i++){
    i -> cmap = cmap;
  }

  //ship fleet ref
  //fleet order's planet/fleet ref
  for (j = fleets.begin(); j != fleets.end(); j++){
    j -> rebuild_references(this, planet_master);
  }
}

void STK_Ship_master::print_references(){
  list<STK_Fleet>::iterator i;
  
  fprintf(stdout,"ship_master::print_references:\n");
  fprintf(stdout, " -> has %d fleets.\n", fleets.size());
  for (i = fleets.begin(); i != fleets.end(); i++){
    fprintf(stdout, " -> references for fleet %d (%x): \n", i -> id, (unsigned long int) &*i);
    i -> print_references();
  }
}

void STK_Ship_master::cleanup_explosions(){
  list<STK_Explosion*>::iterator i;

  for (i = explosions.begin(); i != explosions.end(); i++){
    if (*i){
      delete *i;
    }
  }
  explosions.clear();
}

void STK_Ship_master::cleanup(){
  fleets.clear();
  ships.clear();
}

void STK_Ship_master::paint(SDL_Surface* s, int pid){
  list<STK_Ship>::iterator i;
  intd p;
  list<STK_Explosion*>::iterator xi;

  for (i = ships.begin(); i != ships.end(); i++){
    p = STK::ns_var -> map2screen(i -> get_position());
    STK_Graphics::draw_ship(s, p, i -> angle, i -> attributes.color, i -> fleet -> is_selected);
  }

  for (xi = explosions.begin(); xi != explosions.end(); xi++){
    (*xi) -> paint(s, STK::ns_var -> map2screen((*xi) -> position));
  }
}

void STK_Ship_master::paint_stats(SDL_Surface* s, int pid){
  list<STK_Fleet>::iterator j;
  for (j = fleets.begin(); j != fleets.end(); j++){
    STK_Graphics::draw_fleet_data(s, j, j -> get_owner() == pid);
  }
}


list<STK_Ship> *STK_Ship_master::get_fleet(){
  return &ships;
}

bool STK_Ship_master::differs(STK_Ship_master *m){
  list<STK_Ship>::iterator i, j;
  list<STK_Fleet>::iterator k, l;

  if (ships.size() != m -> get_fleet() -> size()){
    fprintf(stdout, "ship_master::differs: num ships!\n");
    return true;
  }

  i = ships.begin();
  j = m -> get_fleet() -> begin();
  while(i != ships.end() && j != m -> get_fleet() -> end()){
    if (!(*i == *j)){
      fprintf(stdout, "ship_master::differs: ship %d - %d\n", i -> associated_id, j -> associated_id);
      return true;
    }
    i++;
    j++;
  }

  if (fleets.size() != m -> get_fleets() -> size()){
    fprintf(stdout, "ship_master::differs: num fleets\n");
    return true;
  }

  k = fleets.begin();
  l = m -> get_fleets() -> begin();
  while (k != fleets.end()){
    if (k -> id != l -> id){
      fprintf(stdout, "ship_master::differs: fleet id %d - %d\n", k -> id, l -> id);
      return true;
    }
    k++;
    l++;
  }

  return false;
}

bool STK_Ship_master::hit_planet(STK_Ship *ship, STK_Planet *planet){
  /* Attempts to perform a hit on this planet. If the planet and ship belong to the same player, the ship is added to the planet fleet. Otherwise, if the planet has a fleet, the fleet is decreased depending on the ship's warfare. Otherwise, the planet is occupied by the ship, leaving it owned by the ship's owner and with a fleet of one ship. This routine does remove the ship from the ship array. */

  STK_Player *ship_owner = planet_master -> game -> get_player(ship -> owner);

  if (planet -> owner == ship -> owner){
    planet -> num_turkeys++;
    ship -> remove_me = true;
    ship -> explode = false;
    fprintf(stdout, "ship_master::hit_planet(id = %d): friendly ship, now has %.2f turkeys.\n", planet -> id, planet -> num_turkeys);
  }else{
    fprintf(stdout, "ship_master::hit_planet(id = %d, fleet: %.2f): enemy ship %d with %d hp (wf %d).\n", planet -> id, planet -> num_turkeys, ship -> associated_id, ship -> hp, ship -> attributes.warfare);
    if (planet -> num_turkeys <= 0){
      planet -> num_turkeys = 1;
      planet -> owner = ship -> owner;
      planet -> p_owner = ship_owner;
      planet -> inactive = PLANET_DEACTIVATION_PHASE;
      ship -> remove_me = true;
      return true;
    }else if (!STK::statistics::dirichlet((1 + ship_settings.equip_factor * (ship -> fleet -> equipment == STKFE_Artillery)) * ship -> attributes.warfare + DIRICHLET_BASE, (planet -> p_owner == NULL ? 0 : planet -> p_owner -> upgrade.warfare) + DIRICHLET_BASE + *STK_Planet::def_bonus)){
      float factor = planet -> p_owner == NULL ? 1 : (1 + planet -> p_owner -> upgrade.industry / (float)10);
      planet -> num_turkeys -= 1/(float)(2 + (int)(planet -> p_owner == NULL ? 1 : planet -> p_owner -> upgrade.warfare) / 10);
      planet -> shutdown = fmax(ship -> attributes.warfare * PLANET_SHUTDOWN_FACTOR, MAX_SHUTDOWN * factor * planet -> productivity * (planet -> owner > -1 ? *STK_Planet::multiplier : 1));
      fprintf(stdout, " -> lose one turkey.\n");
    }else if (--ship -> hp <= 0){
      ship -> remove_me = true;
      ship -> explode = true;
      fprintf(stdout, " -> ship dies\n");
    }else{
      fprintf(stdout, " -> ship takes damage: remains %d hp.\n", ship -> hp);
    }
  }

  return false;
}
