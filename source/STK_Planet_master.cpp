#include <cstdio>
#include "STK_Planet_master.h"
#include "STK_Ship_attributes.h"
#include "STK_Graphics.h"
#include "STK_Statistics.h"
#include "STK_Structures.h"
#include "STK_Ship_master.h"
#include "STK_Fleet.h"
#include "STK_Game.h"
#include "STK_Basic_communication.h"

using namespace std;

STK_Planet_master::STK_Planet_master(){
}

int STK_Planet_master::closest_planet(fd p){
  /* Returns the planet id of the planet located closest to the position 'p' on the map. */
  int i;
  int d;
  int k=0;
  int c;
  if (planets.size()<1){
    return -1;
  }
  d=(p-planets[0].position).length2();
  for (i=1; i<planets.size(); i++){
    if ((c=(p-planets[i].position).length2())<d){
      k=i;
      d=c;
    }
  }
  return k;
}

int STK_Planet_master::get_num_planets(){
  return planets.size();
}

STK_Planet* STK_Planet_master::get_planet(int idx){
  if (idx>=0 && idx<planets.size()){
    return &planets[idx];
  }
  return NULL;
}

STK_Planet* STK_Planet_master::closest_owned_planet(fd pos, int own){
  int i, k;
  float d_min = INFINITY, d;
  STK_Planet *x = NULL;
  
  for (i = 0; i < planets.size(); i++){
    if (planets[i].owner == own){
      d = (planets[i].position - pos).length();
      if (d < d_min){
	d_min = d;
	x = &planets[i];
      }
    }
  }

  if (x){
    return x;
  }else{
    fprintf(stdout, "planet_master::closest_owned_planet(): no owned planet!\n");
    exit(1);
  }
}

list<STK_Planet*> STK_Planet_master::get_planets(){
  list<STK_Planet*> p;

  for (int i = 0; i < planets.size(); i++){
    p.push_back(&planets[i]);
  }
  return p;
}

void STK_Planet_master::iterate(){
  /* Updates all planets, adding new ships to the fleet etc. */
  int i;
  list<STK_Command> cq;
  list<STK_Command>::iterator j;
  int sum;
  float c;

  if (game == NULL){
    fprintf(stdout,"planet_master::iterate: game references is null!\n");
    return;
  }

  for (i = 0; i < planets.size(); i++){
    planets[i].iterate(&game -> universe);
    
    if (game -> player_exists(planets[i].owner)){
      STK_Ship_attributes a(game -> get_player(planets[i].owner), game -> universe.get_settings() -> ship_settings);
      cq = planets[i].retrieve_commands();
      if (cq.size()){fprintf(stdout, "planet_master::iterate(): applying %d commands.\n", cq.size());}
      sum = 0;

      for (j = cq.begin(); j != cq.end(); j++){
	int quant = j -> quantity();
	if (sum + quant > planets[i].num_turkeys){
	  fprintf(stdout, "planet_master::iterate: command turkey sum to large!\n");
	  break;
	}
	sum += quant;
	ship_master -> add_fleet(a, *j);
      }

      planets[i].num_turkeys = fmax(planets[i].num_turkeys - sum, 0);
      planets[i].clear_retrieved_commands();
    }
  }
}

void STK_Planet_master::clear_fleet(STK_Fleet *f){
  int i;
  list<STK_Command>::iterator j;

  fprintf(stdout,"planet_master::clear_fleet(fleet_id = %d)\n", f -> id);

  for (i = 0; i < planets.size(); i++){
    for (j = planets[i].command_queue.begin(); j != planets[i].command_queue.end(); j++){
      if (j -> order.type == TARGET_FLEET && j -> order.fleet == f){
	fprintf(stdout,"planet_master::clear_fleet(): erasing fleet %d from planet %d.\n", f -> id, i);
	planets[i].command_queue.erase(j--);
      }
    }
  }
}

void STK_Planet_master::paint(SDL_Surface *s, int pid){
  /* Draws all planets to the main screen. */
  int i;
  STK_Player *player;
  list<STK_Command>::iterator j;

  player = game -> get_player(pid);

  for (i = 0; i < planets.size(); i++){
    if ((!player -> dead) && player -> home_planet -> id == planets[i].id){
      STK_Graphics::draw_home_planet(s, &planets[i], STK::statistics::leaving(&planets[i]), p_info -> get_color(planets[i].owner));
    }else{
      STK_Graphics::draw_planet(s, &planets[i], STK::statistics::leaving(&planets[i]), p_info -> get_color(planets[i].owner), (!player -> dead) && planets[i].owner == player -> id);
    }
    
    if (planets[i].owner == pid){
      /* draw commands for this planet */

      for (j = planets[i].command_queue.begin(); j != planets[i].command_queue.end(); j++){
	STK_Graphics::draw_command(s, *j);
      }
    }
  }
}


void STK_Planet_master::paint_stats(SDL_Surface *s, int pid){
  /* Draws all planets to the main screen. */
  int i;
  STK_Player *player;
  list<STK_Command>::iterator j;

  player = game -> get_player(pid);

  for (i = 0; i < planets.size(); i++){
    STK_Graphics::draw_planet_data(s, &planets[i], STK::statistics::leaving(&planets[i]), p_info -> get_color(planets[i].owner), (!player -> dead) && planets[i].owner == player -> id);
  }
}


bool STK_Planet_master::initialize(STK_Game *g, STK_Universe_settings s, player_info *pl, STK_Ship_master *sm){
  vector<int> home_planets;
  vector<float> scores;
  float max_score;
  STK::statistics::best_homes_stats test_homes;
  vector<STK_Planet> planet_buf;
  float unfairness = INFINITY, u;
  int i;
  unsigned int initial_clock = clock();

  if (pl == NULL){
    return false;
  }

  game = g;
  p_info = pl;
  ship_master = sm;
  planets.clear();

  for (i = 0; unfairness > 0 && (clock() - initial_clock) / (float)CLOCKS_PER_SEC < STK_BUILD_TIMEOUT; i++){
    planet_buf = STK::statistics::scramble_planets(s);
    if (planet_buf.size()){
      test_homes = STK::statistics::best_homes(planet_buf, p_info -> num_players, s);
      u = test_homes.unfairness;
      if (u < unfairness && test_homes.homes.size()){
	fprintf(stdout, "planet_master::initialize: new best homes, u = %.2f (required: %.2f).\n", u, (float)p_info -> num_players * ALLOWED_HOME_UNFAIRNESS);
	unfairness = u;
	planets = planet_buf;
	home_planets = test_homes.homes;
	scores = test_homes.scores;
      }else{
	fprintf(stdout, "planet_master::initialize: best home failed, u = %.2f (already have %.2f), homes.size = %d\n", u, unfairness, test_homes.homes.size());
      }
    }
  }

  if (unfairness / p_info -> num_players > ALLOWED_HOME_UNFAIRNESS){
    fprintf(stdout, "planet_master: no fair universe configuration found! (best: %.3f, required: %.3f)\n", unfairness, (float)p_info -> num_players * ALLOWED_HOME_UNFAIRNESS);
  }

  max_score = 0;
  for (i = 0; i < p_info -> num_players; i++){
    max_score = fmax(max_score, scores[i]);
  }

  for (i = 0; i < p_info -> num_players; i++){
    p_info -> initial_home_planets[i] = home_planets[i];
    planets[home_planets[i]].owner = p_info -> players[i];
    planets[home_planets[i]].num_turkeys += (max_score - scores[i]) / .8; 
    planets[home_planets[i]].num_turkeys += s.planet_base_fleet * planets[home_planets[i]].productivity;
    fprintf(stdout, "planet_master::initialize: incrementing turkeys for player %d by %.2f\n", i, (max_score - scores[i]) / .8);
  }

  fprintf(stdout, "generated universe with planets at the following positions: \n");
  for (i = 0; i < planets.size(); i++){
    fprintf(stdout, "%.2f x %.2f\n", planets[i].position.x, planets[i].position.y);
  }

  // for (i = 0; i < p_info -> num_players; i++){
  //   planets[home_planets[i]].radius = s.max_radius;
  //   planets[i].productivity = s.planet_productivity;
  //   planets[i].num_turkeys = 1000 * planets[i].productivity;
  // }
  return true;
}

void STK_Planet_master::print_com_gen(){
  int i;
  list<STK_Command*>::iterator j;

  fprintf(stdout, "planet_master::print_com_gen(): \n");

  for (i = 0; i < planets.size(); i++){
    for(j = planets[i].command_generation.begin(); j != planets[i].command_generation.end(); j++){
      fprintf(stdout, "planet %d: com (%x) with prio %d.\n", i, (long long int)&planets[i], (*j) -> priority);
    }
  }
}

void STK_Planet_master::rebuild_references(STK_Game *g, player_info *pi, STK_Ship_master *sm){
  int i;
  list<STK_Command>::iterator j;

  game = g;
  p_info = pi;
  ship_master = sm;
  
  /* Setup the planets' command queues' references to planets and fleets. */
  for (i = 0; i < planets.size(); i++){
    for (j = planets[i].command_queue.begin(); j != planets[i].command_queue.end(); j++){
      j -> source.planet = get_planet(j -> source.associated_id);
      switch(j -> order.type){
      case TARGET_PLANET:
	j -> order.planet = get_planet(j -> order.associated_id);
	break;
      case TARGET_FLEET:
	j -> order.fleet = ship_master -> get_fleet(j -> order.associated_id);
      }
    }

    //player reference
    planets[i].p_owner = g -> get_player(planets[i].owner);
    fprintf(stdout, "pm::rebuild_ref: set p_owner for %d (player %d) to %x.\n", planets[i].id, planets[i].owner, (long long int)planets[i].p_owner);
  }
}

int STK_Planet_master::calculate_size(){
  int s = 8;
  for (int i = 0; i < planets.size(); i++){
    s += planets[i].calculate_size();
  }
  return s;
}

bool STK_Planet_master::send_data(char* buf, int* c, int max){
  int n,i;

  fprintf(stdout,"planet_master::send(): %d planets\n",planets.size());
  n=planets.size();
  if (*c + calculate_size() > max){
    fprintf(stdout,"planet_master::send(): buffer overflow!\n");
    return false;
  }
  write_to_buf((char*)&regenerate_maps,buf+*c,4);
  *c+=4;
  write_to_buf((char*)&n,buf+*c,4);
  *c+=4;
  for (i = 0; i < n; i++){
    if (!planets[i].send_data(buf,c,max)){
      return false;
    }
  }
  
  return true;
}
bool STK_Planet_master::load_data(char* buf, int* c, int max){
  int n;

  if (*c + 8 > max){
    fprintf(stdout,"planet_master::load(): buffer overflow!\n");
    return false;
  }

  read_from_buf(buf+*c,(char*)&regenerate_maps,4);
  *c += 4;

  read_from_buf(buf+*c,(char*)&n,4);
  if (n<0){
    fprintf(stdout,"planet_master::load(): invalid size!\n");
    return false;
  }
  *c+=4;

  if (n != planets.size()){
    planets.resize(n);
    fprintf(stdout,"planet_master::load(): had to resize planet vector!\n");
    regenerate_maps = true;
  }

  for (int i = 0; i < n; i++){
    if (!planets[i].load_data(buf,c,max)){
      return false;
    }
  }
  fprintf(stdout,"planet_master::load(): %d planets\n",planets.size());
  return true;
}

void STK_Planet_master::assign(STK_Planet_master *m){
  planets = m -> planets;
  p_info = m -> p_info;
}

bool STK_Planet_master::differs(STK_Planet_master *m){
  int i;
  list<STK_Command>::iterator j,k;
  regenerate_maps = false;

  if (planets.size()!=m -> planets.size()){
    fprintf(stdout, "planet_master::differs: number of planets is %d, should be %d\n", m -> planets.size(), planets.size());
    regenerate_maps = true;
    return true;
  }

  for (i=0; i<planets.size(); i++){
    if (!(planets[i].position == m -> planets[i].position)){
      regenerate_maps = true;
      fprintf(stdout, "planet_master::differs: planet %d at %.2fx%.2f, should be at %.2f x %.2f.\n", m -> planets[i].position.x, m -> planets[i].position.y, planets[i].position.x, planets[i].position.y);
      return true;
    }
    if (!(planets[i].radius == m -> planets[i].radius)){
      regenerate_maps = true;
      fprintf(stdout, "planet_master::differs: radius of %d\n", i);
      return true;
    }
    if (!(planets[i].productivity == m -> planets[i].productivity)){
      fprintf(stdout, "planet_master::differs: prod of %d\n", i);
      return true;
    }
    if (!(planets[i].num_turkeys == m -> planets[i].num_turkeys)){
      fprintf(stdout, "planet_master::differs: fleet of %d\n", i);
      return true;
    }
    if (!(planets[i].owner == m -> planets[i].owner)){
      regenerate_maps = true;
      fprintf(stdout, "planet_master::differs: owner of %d\n", i);
      return true;
    }
    if (!(planets[i].id == m -> planets[i].id)){
      fprintf(stdout, "planet_master::differs: id of %d\n", i);
      return true;
    }
    if (!(planets[i].command_queue.size() == m -> planets[i].command_queue.size())){
      fprintf(stdout, "planet_master::differs: command queue size of %d\n", i);
      return true;
    }
    
    //is it possible that command order can vary?
    k = m -> planets[i].command_queue.begin();
    for (j = planets[i].command_queue.begin(); j != planets[i].command_queue.end(); j++){
      if (j -> source.associated_id != k -> source.associated_id){
	fprintf(stdout, "planet_master::differs: command source assoc id for planet %d\n", i);
	return true;
      }
      if (j -> order.associated_id != k -> order.associated_id){
	fprintf(stdout, "planet_master::differs: command order assoc id for planet %d\n", i);
	return true;
      }
      if (j -> order.type != k -> order.type){
	fprintf(stdout, "planet_master::differs: command order type for planet %d\n", i);
	return true;
      }
      if (j -> priority != k -> priority){
	fprintf(stdout, "planet_master::differs: command priority for planet %d\n", i);
	return true;
      }
      if (j -> signal_delay != k -> signal_delay){
	fprintf(stdout, "planet_master::differs: command sig del for planet %d\n", i);
	return true;
      }
    }
  }
  return false;
}

void STK_Planet_master::cleanup(){
  planets.clear();
}
