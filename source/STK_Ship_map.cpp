#include <cstdio>
#include "STK_Ship_map.h"
#include "STK_Universe_settings.h"

STK_Ship_map::STK_Ship_map(){
  /* Default constructor for ship map. Map pointer is initialized to 0. */
  map=0;
}

void STK_Ship_map::initialize(STK_Universe_settings *uvs){
  /* Initializer routine for ship map. Allocates map array with dimensions w x h. Initializes each position on the map with value -1 implying no ship. */
  int i;

  if (!uvs){
    fprintf(stdout,"ship_map::initialize(): can not init w/o uv_settings!\n");
    return;
  }

  uv_settings = uvs;
  cleanup();
  int n = uv_settings -> cmap_width() * uv_settings -> cmap_height();
  if (!(map=new STK_Ship*[n])){
    fprintf(stderr,"STK_Ship_map::initialize(): out of memory!\n");
    return;
  }

  for(i=0; i<n ; i++){
    map[i]=NULL;
  }
}

bool STK_Ship_map::differs(STK_Ship_map m){
  return false;
}

void STK_Ship_map::clear_map(){
  int i;
  int m = uv_settings -> cmap_width() * uv_settings -> cmap_height();
  for (i = 0; i < m; i++){
    map[i] = NULL;
  }
}

void STK_Ship_map::cleanup(){
  if (map){
    delete [] map;
  }
  map=0;
}
