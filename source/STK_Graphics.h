#ifndef _STK_GRAPHICS
#define _STK_GRAPHICS

#include <list>
#include <map>
#include <string>

#include <SDL/SDL.h>

#include "RSK/RskTypes.h"
#include "STK_Command.h"

namespace STK_Graphics{
  struct planet_graphics_info{
    int color;
    int radius;
    bool selected;

    planet_graphics_info(int c, int r, bool s);
  };

  bool operator<(planet_graphics_info a, planet_graphics_info b);

  extern std::map<planet_graphics_info,SDL_Surface*> planet_data;
  extern SDL_Surface *ship_glow;
  extern STK_Universe_settings *uvs;

  const char *troop_approx_name(int n);
  void init();
  void set_uvs(STK_Universe_settings *uvs);
  void quit();
  void draw_ship(SDL_Surface* screen, intd pos, float angle, int color, bool sel);
  void draw_planet(SDL_Surface* screen, STK_Planet *p, int n_leaving, int color, bool owned);
  void draw_planet_data(SDL_Surface* screen, STK_Planet *p, int n_leaving, int color, bool owned);
  void draw_home_planet(SDL_Surface* screen, STK_Planet *p, int n_leaving, int color);
  void draw_fleet_data(SDL_Surface *screen, std::list<STK_Fleet>::iterator f, bool owned);
  void troop_indicator(SDL_Surface* screen, const intd pos, const char* s);
  SDL_Surface *generate_planet_image(planet_graphics_info g);
  void clear_planet_data();
  void draw_command(SDL_Surface* screen, STK_Command c);
  void draw_arrow(SDL_Surface* screen, fd from, fd to);
  void draw_message(SDL_Surface* screen, const char* text);
  void display_from_file(const char* v);
  void draw_minimap(SDL_Surface* screen, STK_Collision_map *cm, player_info *pi);
  int make_selection(SDL_Surface* screen, std::list<std::string> selection, std::string title);
  void fancy_spirals(SDL_Surface *screen);
};
#endif
